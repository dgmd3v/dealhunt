# Instalación aplicación DealHunt

# Importante

Este repositorio tiene como función mostrar el código de la aplicación para poder verla en funcionamiento se recomienda ir a [dgmdev.info](dgmdev.info), para poder probarla se tienen los siguientes usuarios:

Nombre de usuario: usuario

Contraseña: usuario

Nombre de usuario: moderador

Contraseña: moderador

Nombre de usuario: administrador

Contraseña: administrador

## Indice

* [1 Introducción](#markdown-header-1-introduccion)
* [2 Requisitos Hardware de la máquina](#markdown-header-2-requisitos-hardware-de-la-maquina)
* [3 Requisitos Software de la máquina](#markdown-header-3-requisitos-software-de-la-maquina)
* [4 Instalación de la aplicación](#markdown-header-4-instalacion-de-la-aplicacion)
    * [4.1 Antes de empezar a usar este manual](#markdown-header-41-antes-de-empezar-a-usar-este-manual)
    * [4.2 Instalando Apache2](#markdown-header-42-instalando-apache2)
    * [4.3 Instalando PHP](#markdown-header-43-instalando-php)
    * [4.4 Instalando MySQL](#markdown-header-44-instalando-mysql)
    * [4.5 Instalando Composer](#markdown-header-45-instalando-composer)
    * [4.6 Instalando Java JDK](#markdown-header-46-instalando-java-jdk)
    * [4.7 Instalando ElasticSearch](#markdown-header-47-instalando-elasticsearch)
    * [4.8 Configurando el sistema](#markdown-header-48-configurando-el-sistema)
    * [4.8.1 Memoria Swap](#markdown-header-481-memoria-swap)
    * [4.8.2 Configuración Apache2](#markdown-header-482-configuracion-apache2)
    * [4.8.3 Configuración zona horaria](#markdown-header-483-configuracion-zona-horaria)
    * [4.8.4 Configuración lenguaje](#markdown-header-484-configuracion-lenguaje)
    * [4.8.5 Creando el grupo uploadsmaster](#markdown-header-485-creando-el-grupo-uploadsmaster)
    * [4.8.6 Configuración zona horaria PHP](#markdown-header-486-configuracion-zona-horaria-php)
    * [4.8.7 Instalando Git](#markdown-header-487-instalando-git)
    * [4.9 Instalación de la aplicación](#markdown-header-49-instalacion-de-la-aplicacion)
    * [4.9.1 Configuración correos electrónicos ](#markdown-header-491-configuracion-correos-electronicos)
    * [4.10 Apéndice](#markdown-header-410-apendice)


## 1 Introducción

El manual de instalación que se detalla en este documento está pensado para el usuario administrador de la maquina pueda probar la aplicación en local. En él se detallan unos requisitos hardware necesarios para poder instalar la aplicación, además se detalla el software necesario para que la aplicación pueda funcionar correctamente, los ficheros de configuración que debemos tocar o como cargar datos de prueba para poder probar la aplicación.

[Volver arriba](#markdown-header-instalacion-aplicacion-dealhunt)

## 2 Requisitos Hardware de la máquina

Debido a la gran cantidad de hardware y combinaciones de este, a continuación se citarán las especificaciones de algunas de las maquinas en las que ha sido probado el proyecto, para poder hacerse una idea del hardware necesario.

Esta aplicación ha sido probada satisfactoriamente en hardware muy diverso, el más bajo es con un portátil con las siguientes especificaciones:
* Procesador: Intel Core 2 Duo T7300
* Memoria RAM: 2 GB DDR2 @ 667 MHz
* Disco duro: Disco mecánico de 128 GB
* Memoria Swap: 4 GB


Es decir, hardware de hace 7-8 años, además ha sido probado en el hosting http://www.digitalocean.com/ en la versión más básica:
* Procesador: 1 núcleo de un procesador Intel Xeon E5-2630L
* Memoria RAM: 512 MB DDR3
* Disco duro: Disco SSD de 20 GB
* Memoria Swap: 4 GB

[Volver arriba](#markdown-header-instalacion-aplicacion-dealhunt)

## 3 Requisitos Software de la máquina

Para que la aplicación web funcione correctamente necesitamos una distribución Linux, para este proyecto se ha usado Ubuntu 14.04 LTS, además debemos tener instalado en nuestra maquina el siguiente software:
* Apache
* PHP
* MySQL
* Composer
* Java JDK
* Git
* ElasticSearch

[Volver arriba](#markdown-header-instalacion-aplicacion-dealhunt)

## 4 Instalación de la aplicación

Esté mismo manual de instalación se puede encontrar en formato HTML en la carpeta que podrá encontrar con el mismo nombre que este fichero. Es recomendable usar la guía HTML por comodidad y porque en formato Word se pueden encontrar problemas con caracteres que cambia o líneas cortadas.

Esta guía está realizada sobre [Ubuntu 14.04.1 LTS (64 bits)](http://www.ubuntu.com/download/desktop/) desde cero y no se ha probado en otras distribuciones/SO. Ubuntu cuenta con un manual por si fuese necesario: [Manual Instalación Ubuntu (inglés)](http://www.ubuntu.com/download/desktop/install-ubuntu-desktop/) . En google se pueden encontrar infinidad de manuales en castellano, por ejemplo: [Manual Instalación Ubuntu (Español)](http://blog.soluciones-libres.com/2014/04/instalar-ubuntu-1404-trusty-thar-como.html).

[Volver arriba](#markdown-header-instalacion-aplicacion-dealhunt)

## 4.1 Antes de empezar a usar este manual

Cuando se instala ubuntu se nos pide un nombre de usuario, ubuntu crea una carpeta personal para nuestros ficheros utilizando dicho nombre, en el caso de este proyecto, al haber escrito dgmdev como nombre de usuario estará ubicada en el directorio /home/dgmdev. Si tú has elegido otro nombre, por ejemplo juan, tu directorio personal será /home/juan, en el manual yo uso mi directorio por lo que tienes que tenerlo en cuenta y ser cuidados@ a la hora de introducir los comandos en consola.

Si no puedes instalar ubuntu de forma normal o no quieres siempre puedes utilizar una máquina virtual bajo virtualbox por ejemplo (no se ha probado con esta guía), aunque el rendimiento debería ser notablemente inferior respecto a una instalación normal.

Si no sabes instalar una máquina virtual en google se pueden encontrar infinidad de guías y video guías, esta te podría servir:

[Cómo virtualizar Ubuntu 14.04 en Windows utilizando VirtualBox](http://blog.uptodown.com/tutorial-virtualizar-ubuntu-14-virtualbox/)

A la hora de copiar y pegar comandos hay que tener cuidado y copiarlos enteros, copiando directamente de Word aquellos comandos que ocupen más de una línea se copiaran mal (en dos líneas en vez de una sola) es conveniente pegarlos primero en un editor de textos como gedit (Ubuntu), además se deben introducir los comandos de uno en uno.

El autor de esta guía no se hace responsable de los posibles problemas derivados del uso de la misma.

[Volver arriba](#markdown-header-instalacion-aplicacion-dealhunt)

## 4.2 Instalando Apache2

Antes de nada ejecutamos el siguiente comando (al introducir sudo se nos pedirá la contraseña de administrador):
```sh
sudo apt-get update
```
Abrimos la terminal, para ello apretamos el botón con el símbolo de Windows de nuestro teclado, se nos abrirá el buscador de ubuntu. Escribimos terminal y nos debería aparecer en la primera fila de resultados, hacemos clic sobre él.

Primero vamos a instalar apache, para lo cual escribimos el siguiente comando en nuestra terminal, nos dirá el espacio que va a ocupar en disco y nos preguntará si queremos continuar, decimos que si (S):
```sh
sudo apt-get install apache2
```
Apache buscará documentos en /var/www, de momento lo vamos a dejar así aunque más adelante lo vamos a cambiar. Si en nuestro navegador abrimos http://127.0.0.1/ deberíamos ver una web en inglés indicando que funciona ("It works!"):

![Alt text](http://i.imgur.com/A6YoWUZ.png)

[Volver arriba](#markdown-header-instalacion-aplicacion-dealhunt)

## 4.3 Instalando PHP

Ahora debemos instalar PHP. Escribimos el siguiente comando, nos dirá el espacio que va a ocupar en disco y nos preguntará si queremos continuar, decimos que si (S):
```sh
sudo apt-get install php5 libapache2-mod-php5 php5-cli php5-mysql php5-curl php5-intl php5-mcrypt php5-json php5-apcu php5-gd
```
En phpmyadmin (se instala más adelante) nos podemos encontrar con el siguiente error que se muestra en un panel de color rojo como (o en español):
```sh
The mcrypt extension is missing. Please check your PHP configuration.
```
Para evitarlo introducimos los siguientes comandos en nuestra consola:
```sh
sudo ln -s /etc/php5/conf.d/mcrypt.ini /etc/php5/mods-available
```
```sh
sudo php5enmod mcrypt
```

Reiniciamos apache escribiendo el comando:
```sh
sudo /etc/init.d/apache2 restart
```
Si nos saliese este error:
```sh
AH00558: apache2: Could not reliably determine the server's fully qualified domain name, 
using 127.0.1.1. Set the 'ServerName' directive globally to suppress this message
```
Debemos editar el archivo servername.conf indicando el nombre del servidor (por defecto debería estar vacío), abrimos el fichero con el editor de texto:
```sh
sudo gedit /etc/apache2/conf-available/servername.conf
```
En el fichero escribimos lo siguiente:
```sh
ServerName localhost
```
Guardamos los cambios realizados en el fichero y cerramos. Para aplicar el cambio escribimos en la terminal estos dos comandos:
```sh
sudo a2enconf servername
```
```sh
service apache2 reload
```
Probamos a reiniciar otra vez apache y ya no nos debería salir el mensaje de error:
```sh
sudo /etc/init.d/apache2 restart
```
Si nos apareciese este mensaje de error:
```sh
apache2: Could not reliably determine the server's fully qualified domain name, using 127.0.1.1 for ServerName... 
waiting apache2: Could not reliably determine the server's fully qualified domain name, using 127.0.1.1 for ServerName
```
Debemos editar el archivo httpd.conf indicando el nombre del servidor (por defecto debería estar vacío), abrimos el fichero con el editor de texto, para ello escribimos en la terminal:
```sh
sudo gedit /etc/apache2/httpd.conf
```
Se nos abrirá el fichero, en el escribimos lo siguiente:
```sh
ServerName localhost
```
Guardamos los cambios realizados en el fichero y cerramos. Probamos a reiniciar otra vez apache y ya no nos debería salir el mensaje de error:
```sh
sudo /etc/init.d/apache2 restart
```
Para probar que funcione vamos a crear un fichero el cual nos debería mostrar la configuración, creamos el fichero test.php escribiendo en la terminal:
```sh
sudo gedit /var/www/html/testphp.php
```
Nota: En otras distribuciones la carpeta puede ser /var/www

Se nos abrirá el gestor de textos y escribimos:
```sh
<?php
    phpinfo();
?>
```
Si ahora abrimos http://127.0.0.1/testphp.php deberíamos ver la configuración.

![alt text](http://i.imgur.com/hO6GsqW.png "Info PHP")

[Volver arriba](#markdown-header-instalacion-aplicacion-dealhunt)

## 4.4 Instalando MySQL

Ahora vamos a instalar MySQL para la base de datos. Se nos pedirá que introduzcamos dos veces una contraseña para el usuario root, en mi caso he usado root, una vez introducida la contraseña le damos a enter. Escribimos lo siguiente en nuestra terminal, nos dirá el espacio que va a ocupar en disco y nos preguntará si queremos continuar, decimos que si (S):
```sh
sudo apt-get install mysql-server mysql-client libmysqlclient-dev
```
Comprobamos que funciona haciendo login (xxxx= contraseña introducida, en mi caso root):
```sh
mysql -uroot -pxxxx
```
Para salir de mysql introducimos exit en nuestra consola.

![alt text](http://i.imgur.com/rq5PSQB.png "MySQL")

Para poder administrar (entre otras posibles tareas) de forma más cómoda nuestras bases de datos vamos a instalar phpMyAdmin.

Se nos preguntará para que servidor web queremos configurar phpMyAdmin, elegimos apache2 aunque ya debería estar seleccionado por defecto, una vez elegido le damos a enter. También se nos pregunta si queremos configurar la base de datos para dbconfig-common, elegimos yes y presionamos enter.

Por último se nos pide la contraseña de MySQL anteriormente introducida y una para phpMyAdmin, en mi caso introduzco dos veces root (mas la confirmación de la contraseña), una vez escrita presiono enter. Escribimos lo siguiente en nuestra terminal, nos dirá que va a ocupar espacio en disco y nos preguntará si queremos continuar, decimos que si (S):
```sh
sudo apt-get install phpmyadmin
```
Para acceder a él visitamos http://127.0.0.1/phpmyadmin

En el caso en el que no podamos acceder a phpMyAdmin (Not Found) debemos realizar un enlace simbólico, introducimos lo siguiente en la terminal:
```sh
sudo ln -s /usr/share/phpmyadmin /var/www/html
```
Ya deberíamos poder ver el panel de login de phpMyAdmin, introducimos el usuario y su contraseña recién creados al instalar MySQL. 

![alt text](http://i.imgur.com/WoKI7n7.png "Login PHPMyAdmin")

[Volver arriba](#markdown-header-instalacion-aplicacion-dealhunt)

## 4.5 Instalando Composer

Para poder gestionar las dependencias en nuestro proyecto necesitamos el gestor Composer. Antes de nada debemos tener instalado curl para poder descargarnos el instalador de la web de composer:
```sh
sudo apt-get install curl
```
Ahora instalamos Composer:
```sh
curl -s https://getcomposer.org/installer | php
```
Movemos el archivo composer.phar a un directorio ejecutable del sistema (el directorio donde está composer debería ser diferente al mío /home/TU_USUARIO/composer.phar):
```sh
sudo mv /home/dgmdev/composer.phar /usr/local/bin/composer
```
Escribimos en nuestra terminal el siguiente comando, si todo ha funcionado deberíamos ver un listado con los comandos de composer:
```sh
composer
```
![alt text](http://i.imgur.com/qAdLJeq.png "Composer")

[Volver arriba](#markdown-header-instalacion-aplicacion-dealhunt)

## 4.6 Instalando Java JDK

En la actualidad el JDK de Java oficial (Oracle) no se ofrece por defecto en Ubuntu por lo que tenemos que añadir los repositorios. Introducimos los siguientes comandos en consola (para el segundo comando tendremos que presionar intro cuando se nos pida):
```sh
sudo apt-get install python-software-properties -y
```
```sh
sudo add-apt-repository ppa:webupd8team/java
```
```sh
sudo apt-get update
```
Ahora instalamos Oracle JDK 8 introduciendo el siguiente comando (se nos pedirá permiso para instalar y se nos preguntara si aceptamos los términos, debemos aceptar o decir si a todo):
```sh
sudo apt-get install oracle-java8-installer
```

[Volver arriba](#markdown-header-instalacion-aplicacion-dealhunt)

## 4.7 Instalando ElasticSearch

Para instalar ElasticSearch lo primero que tenemos que hacer es instalar la clave pública:
```sh
wget -qO - https://packages.elasticsearch.org/GPG-KEY-elasticsearch | sudo apt-key add -
```
Nota: Si fallase usar este:
```sh
wget -qO - https://packages.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
```
Añadimos la definición del repositorio a nuestro /etc/apt/sources.list:
```sh
echo "deb http://packages.elasticsearch.org/elasticsearch/1.5/debian stable main" | sudo tee -a /etc/apt/sources.list
```
Nota: Si fallase usar este:
```sh
echo "deb http://packages.elastic.co/elasticsearch/1.5/debian stable main" | sudo tee -a /etc/apt/sources.list
```
Ejecutamos apt-get update y el repositorio ya estará listo para usar. Lo instalamos con:
```sh
sudo apt-get update && sudo apt-get install elasticsearch
```
Para garantizar que el servicio de ElasticSearch se inicia cada vez que arrancamos el ordenador introducimos los siguientes comandos:
```sh
sudo update-rc.d elasticsearch defaults 95 10
```
```sh
sudo /etc/init.d/elasticsearch start
```

Para comprobar que el servicio está funcionando podemos introducir el siguiente comando:
```sh
sudo service elasticsearch status
```
Nos debería aparecer el mensaje:
```sh
* elasticsearch is running
```

[Volver arriba](#markdown-header-instalacion-aplicacion-dealhunt)

## 4.8 Configurando el sistema

En este apartado se hará una configuración del sistema previa a la instalación del proyecto.

[Volver arriba](#markdown-header-instalacion-aplicacion-dealhunt)

## 4.8.1 Memoria Swap

Si el ordenador en el cual se va a instalar el proyecto tiene poca memoria es necesario tener una memoria Swap debido a que algunas partes del proyecto pueden consumir más memoria que la que tenemos, sobre todo si no tenemos mucha (si ya hubieses creado una partición Swap con anterioridad no hace falta que lo vuelvas a hacer).

Vamos a crear un fichero Swap de 4 GB, escribiremos el siguiente comando en nuestra consola:
```sh
sudo fallocate -l 4G /swapfile
```
Podemos comprobar que se ha reservado la cantidad de espacio correcta con el siguiente comando:
```sh
ls -lh /swapfile
```
Nos debería aparecer algo así:
```sh
sudo chmod 600 /swapfile
```
Verificamos que el fichero tiene los permisos correctos escribiendo el siguiente comando en consola:
```sh
ls -lh /swapfile
```
Debería devolvernos algo como este mensaje que nos indica que el usuario root tiene permisos de escritura y lectura:
```sh
-rw------- 1 root root 4,0G may  4 11:21 /swapfile
```
Ahora podemos decirle al sistema que habilite este espacio swap escribiendo en consola:
```sh
sudo mkswap /swapfile
```
Resultado:
```sh
Configurando la versión swapspace 1, tamaño = 4194300 KiB
sin etiqueta, UUID=45d769d9-d41e-4458-ada1-a1157b730d06
```
Para acabar de habilitar este espacio escribiremos en consola:
```sh
sudo swapon /swapfile
```
Podemos comprobar que el proceso se ha realizado correctamente escribiendo en consola el siguiente comando para saber si el sistema ya reconoce el espacio swap:
```sh
sudo swapon -s

Resultado:

Filename				Type		Size	Used	Priority
/dev/sda6               		partition	2094076	260	-1
/swapfile               		file		4194300	0	-2
```

Podemos usar la utilidad free para comprobar definitivamente que todo está en orden:
```sh
free -m
```
Resultado:
```sh
                   total       usado       libre     compart.    búffers     almac.
Mem:          	    2001       1882        119       55          26          516
-/+ buffers/cache:  1338        662
Intercambio:       6140          0         6140
```
Ahora mismo tenemos nuestro espacio swap habilitado pero al reiniciar el sistema no lo habilitará automáticamente, por ello necesitamos modificar el fichero fstab. Abrimos el fichero:
```sh
sudo gedit /etc/fstab
```
Al final del fichero necesitamos añadir una línea que le diga al SO que use automáticamente el espacio swap creado (cuidado al copiar, debe ser una línea):
```sh
/swapfile   none    swap    sw    0   0
```
Guardamos y cerramos el fichero cuando acabemos.

[Volver arriba](#markdown-header-instalacion-aplicacion-dealhunt)

## 4.8.2 Configuración Apache2

Antes de instalar la aplicación web vamos a configurar un poco el entorno de configuración y vamos a decirle a apache donde debe buscar la aplicación web.

En primer lugar debemos editar el archivo /etc/hosts
```sh
sudo gedit /etc/hosts
```
En la primera línea escribimos:
```sh
127.0.0.1   proyecto.local
```
Debería quedar algo así
```sh
127.0.0.1   proyecto.local
127.0.1.1   dgmdev-F3Sa

# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
```
Guardamos y cerramos. Ahora configuramos nuestro servidor web en un host virtual asociado al dominio ficticio proyecto.local. Abrimos el fichero:
```sh
sudo gedit /etc/apache2/sites-available/proyecto.local.conf
```
Introducimos el siguiente texto (cuidado al copiar, debe quedar como se muestra):
```sh
<VirtualHost *:80>
    DocumentRoot    "/home/dgmdev/proyecto/web"
    DirectoryIndex  app.php
    ServerName  proyecto.local

    <Directory "/home/dgmdev/proyecto/web">
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
```
Nota: Dependiendo e tu nombre de usuario el directorio puede cambiar, ¡recuérdalo!

Guardamos y cerramos el editor de texto. Ahora habilitamos el nuevo virtual host y reiniciamos apache:
```sh
sudo a2ensite proyecto.local.conf
```
```sh
sudo /etc/init.d/apache2 restart
```

[Volver arriba](#markdown-header-instalacion-aplicacion-dealhunt)

## 4.8.3 Configuración zona horaria

Se podría dar el caso en el que la zona horaria del sistema no estuviese bien configurada y por lo tanto tuviésemos problemas a la hora de probar la aplicación, por ello comprobamos si la zona horaria es correcta con el siguiente comando:
```sh
more /etc/timezone
```
Nos debería aparecer en consola:
```sh
Europe/Madrid
```
En caso de que la zona horaria no estuviese bien configurada debemos reconfigurarla con el siguiente comando el cual nos mostrará un menú para cambiarla:
```sh
sudo dpkg-reconfigure tzdata
```
Y por último introducir estos dos comandos:
```sh
sudo /etc/init.d/cron stop
```
```sh
sudo /etc/init.d/cron start
```

[Volver arriba](#markdown-header-instalacion-aplicacion-dealhunt)

## 4.8.4 Configuración lenguaje

Otro posible problema que podría surgir es que no tuviésemos correctamente configurado el idioma lo cual podría dar problemas a la hora de trabajar con etiquetas en la aplicación web. Para comprobar que tenemos correctamente configurado el idioma en el sistema introducimos el siguiente comando:
```sh
locale
```
Nos debería aparecer algo como esto:
```sh
LANG=en_US.UTF-8
LANGUAGE=
LC_CTYPE="es_ES.UTF-8"
LC_NUMERIC="es_ES.UTF-8"
LC_TIME="es_ES.UTF-8"
LC_COLLATE="es_ES.UTF-8"
LC_MONETARY="es_ES.UTF-8"
LC_MESSAGES="es_ES.UTF-8"
LC_PAPER="es_ES.UTF-8"
LC_NAME="es_ES.UTF-8"
LC_ADDRESS="es_ES.UTF-8"
LC_TELEPHONE="es_ES.UTF-8"
LC_MEASUREMENT="es_ES.UTF-8"
LC_IDENTIFICATION="es_ES.UTF-8"
LC_ALL=es_ES.UTF-8
```
Si en vez de es_ES.UTF-8 nos apareciese por ejemplo en_US.UTF-8 debemos cambiarlo. Para ello lo primero que hacemos es introducir estos dos comandos:
```sh
sudo locale-gen es_ES.UTF-8
```
```sh
sudo dpkg-reconfigure locales
```
Ahora debemos editar el fichero /etc/environment, lo abrimos:
```sh
sudo gedit /etc/ environment
```
Añadimos al final del fichero la siguiente línea:
```sh
LC_ALL="es_ES.UTF-8"
```
Guardamos el fichero, cerramos y reiniciamos para que se apliquen los cambios.

[Volver arriba](#markdown-header-instalacion-aplicacion-dealhunt)

## 4.8.5 Creando el grupo uploadsmaster

Creamos un nuevo grupo donde estaremos nosotros (dgmdev o tu usuario si es diferente) y www-data para que se puedan subir imágenes y acceder a ellas. Para ello debemos introducir los siguientes comandos:
```sh
sudo addgroup uploadsmaster
```
```sh
sudo usermod -a -G uploadsmaster root
```
```sh
sudo usermod -a -G uploadsmaster www-data
```

[Volver arriba](#markdown-header-instalacion-aplicacion-dealhunt)

## 4.8.6 Configuración zona horaria PHP

Para instalar correctamente nuestra aplicación es necesario configurar la zona horaria. Para ello debemos editar el archivo php.ini, introducimos lo siguiente para abrirlo con el editor de textos:
```sh
sudo gedit /etc/php5/cli/php.ini
```
Debemos buscar el texto date.timezone (ctrl+f para abrir la búsqueda). Y nos deberíamos encontrar algo así:
```sh
[Date]
; Defines the default timezone used by the date functions
; http://php.net/date.timezone
;date.timezone =
```
Debemos des comentar (quitar el punto y coma) la última línea date.timezone y poner como zona Europe/Madrid, debería quedar así:
```sh
[Date]
; Defines the default timezone used by the date functions
; http://php.net/date.timezone
date.timezone = Europe/Madrid
```
Guardamos y cerramos el editor de texto. Ahora reiniciamos apache:
```sh
sudo /etc/init.d/apache2 restart
```

[Volver arriba](#markdown-header-instalacion-aplicacion-dealhunt)

## 4.8.7 Instalando Git

Para poder descargar el proyecto es necesario la instalación del software de control de versiones Git, introducimos lo siguiente en nuestra consola, nos dirá que va a ocupar espacio en disco y nos preguntará si queremos continuar, decimos que si (S):
```sh
sudo apt-get install git
```
Configuramos git con nuestro nombre y apellidos y nuestro e-mail:
```sh
git config --global user.name TU_NOMBRE TUS_APELLIDOS
```
```sh
git config --global user.email TU_EMAIL
```

[Volver arriba](#markdown-header-instalacion-aplicacion-dealhunt)

## 4.9 Instalación de la aplicación

En primer lugar vamos a /home/dgmdev (donde vamos a instalar el proyecto), en tu caso /home/TU_USUARIO:
```sh
cd /home/dgmdev
```
Y ahora vamos a descargar la aplicación web del repositorio:
```sh
git clone git@bitbucket.org:dgmd3v/dealhunt.git

NOTA IMPORTANTE: Una vez clonado deberiamos tener una carpeta con el nombre proyecto, si no tuviese ese nombre debemos ponerselo.

```
Nos posicionamos en el directorio del proyecto:
```sh
cd proyecto
```
Comprobamos que la aplicación cumple los requisitos, para ello en nuestra consola vamos al directorio donde acabamos de hacer la instalación y ejecutamos el script check.php:
```sh
php app/check.php
```

![alt text](http://i.imgur.com/ZjwRNxf.png "Errores requisitos")

El comando nos muestra los requisitos opcionales y los obligatorios, en nuestro caso tenemos varios requisitos marcados como warning/error, por lo que ahora los vamos a arreglar.

Debido a que no se suben los vendors al repositorio nos debería salir un error, aparte de que algunos ficheros estarán desactualizados y por lo tanto tendremos un warning:
```sh
ERROR    Vendor libraries must be installed
          Vendor libraries are missing. Install composer following instructions from http://getcomposer.org/. Then run "php composer.phar install" to install them.

WARNING  Requirements file should be up-to-date
          Your requirements file is outdated. Run composer install and re-check your configuration.
```
Ambos se solucionan escribiendo en consola, al final se nos pedirá unos parámetros de configuración de momento los dejamos por defecto presionando enter en todos ellos. Debido a un límite de descargas sin autenticar desde github es posible que se nos pida un usuario y una contraseña para continuar (se debe tener una cuenta en GitHub):
```sh
composer install
```
Comprobamos que ha funcionado, ahora debería poner OK y no ERROR/WARNING:
```sh
php app/check.php
```

![alt text](http://i.imgur.com/r8vnQrj.png "Requisitos")

Es posible que nos salga un error como app/cache/ directory must be writable y app/logs/ directory must be writable. Aunque no nos salga y para evitar errores debemos introducir estos comandos en consola:
```sh
sudo rm -rf app/cache/*
```
```sh
sudo rm -rf app/logs/*
```
```sh
HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
```
```sh
sudo setfacl -Rn -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX app/cache app/logs
```
```sh
sudo setfacl -dRn -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX app/cache app/logs
```
Comprobamos que ha funcionado, ahora debería poner OK y no ERROR:
```sh
php app/check.php
```
Una vez solucionados los problemas comprobamos que todo es correcto, el resultado de ejecutar el siguiente comando en nuestra consola debería ser la versión de symfony instalada y los comandos disponibles en la consola:
```sh
php app/console
```

![alt text](http://i.imgur.com/aoMpqCb.png "Consola Symfony")

Para probar la aplicación debemos cargar datos de prueba para lo cual necesitamos crear una nueva base de datos, para ello debemos configurar usuario, contraseña y host en app/config/parameters.yml. También vamos a dejar configurada la cuenta de correo que vamos a utilizar para enviar correos. Abrimos el fichero con el siguiente comando:
```sh
sudo gedit app/config/parameters.yml
```
Lo sustituimos por el siguiente texto (mucho cuidado al ser un fichero .yml debemos dejar cuatro espacios antes de cada fila después de parameters, tampoco uses el tabulador):
```sh
# This file is auto-generated during the composer install
parameters:
    database_driver: pdo_mysql
    database_host: 127.0.0.1
    database_port: null
    database_name: deal
    database_user: root
    database_password: root
    mailer_transport: gmail
    mailer_host: smtp.gmail.com
    mailer_user: xxxx@gmail.com (Rellenar con tu correo)
    mailer_password: xxx (Rellenar con la contraseña de tu correo)
    locale: es
    secret: xxxxx ([Crea un secreto](http://nux.net/secret) )
```
Nota: database_user y database_password pueden variar si a la hora de instalar MySQL pusiste una contraseña que no fuese root. database_name debe ser deal.

Por último vamos a cargar los datos de prueba para lo cual vamos a introducir diversos comandos. Para agilizar la tarea se ha creado un script aunque a continuación se detallarán los comandos que usa el script. Para ejecutar el script primero tenemos que hacerlo ejecutable, para ello ejecutamos el siguiente comando:
```sh
chmod +x DBBatch.sh
```
Ahora ejecutamos el script, se nos harán varias preguntas a todas debemos contestar s (si) o y (yes) dependiendo de lo que se nos pida (puede llevar un rato):
```sh
./DBBatch.sh
```
Nota: La primera vez se mostrara un mensaje de error ya que se intenta borrar una base de datos que todavía no hemos creado, el error se da al introducir el comando php app/console doctrine:database:drop --no-debug --force.

Borrado de los directorios de cacheado de imágenes de liipimaginebundle:
```sh
sudo rm -rf web/media/cache/*
```
Borrado de las caches de desarrollo:
```sh
php app/console cache:clear --env=dev
```
Borrado de las caches de producción:
```sh
php app/console cache:clear --env=prod
```
Borrado de cache de metadata:
```sh
php app/console doctrine:cache:clear-metadata
```
Borrado de cache de consultas:
```sh
php app/console doctrine:cache:clear-query
```
Borrado de cache de resultados de consultas:
```sh
php app/console doctrine:cache:clear-result
```
Instalación de assets:
```sh
php app/console assets:install
```
Borramos si existiese la base de datos:
```sh
php app/console doctrine:database:drop --no-debug --force
```
Creamos la base de datos:
```sh
php app/console doctrine:database:create --no-debug
```
Creamos el esquema de la base de datos:
```sh
php app/console doctrine:schema:create --no-debug
```
Cargamos los fixtures o datos de prueba:
```sh
sudo php app/console doctrine:fixtures:load --no-debug
```
Rellenamos ElasticSearch con los datos de la base de datos:
```sh
php app/console fos:elastica:populate
```
Generamos los assets (css, js, etc):
```sh
php app/console assetic:dump --no-debug
```
Para que el sistema de etiquetas funcione correctamente debemos cambiar el cotejamiento, para ello entramos a la consola de MySQL (se nos pedirá contraseña, en mi caso root):
```sh
mysql -uroot -p
```
Escogemos la base de datos deal, introducimos el siguiente comando en la consola:
```sh
use deal
```
Por ultimo introducimos el siguiente comando en nuestra consola para cambiar el cotejamiento:
```sh
ALTER TABLE `Tag` CHANGE `name` `name` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL ;
```
Para salir escribimos en la consola:
```sh
exit
```

![alt text](http://i.imgur.com/ZA9gFVX.png "MySQL")

Si ahora vamos a http://proyecto.local/app.php/ deberíamos poder ver la aplicación web.

Es importante recordar que si se vuelven a cargar los datos es aconsejable borrar la cache de datos para evitar posibles incongruencias. 

[Volver arriba](#markdown-header-instalacion-aplicacion-dealhunt)


## 4.9.1 Configuración correos electrónicos

Es posible que el correo electrónico para el correcto funcionamiento de la aplicación no esté configurado, para configurarlo correctamente tendremos que revisar dos ficheros de configuración. El primero de ellos es parameters.yml, editamos el fichero:
```sh
sudo nano app/config/parameters.yml
```
En el deberíamos ver lo siguiente:
```sh
# This file is auto-generated during the composer install
parameters:
    database_driver: pdo_mysql
    database_host: 127.0.0.1
    database_port: null
    database_name: deal
    database_user: root
    database_password: root
    mailer_transport: gmail
    mailer_host: smtp.gmail.com
    mailer_user: xxxx (Rellenar con tu correo)
    mailer_password: xxx (Rellenar con la contraseña de tu correo)
    locale: es
    secret: xxxxx (Rellenar con secreto)
```
Debemos rellenar los datos con nuestra cuenta de Gmail (es posible que tengamos que configurar nuestra para que acepte ser maneada por aplicaciones), además debemos rellenar con el secreto, el cual podemos generar en [http://nux.net/secret](http://nux.net/secret)

El otro fichero que debemos comprobar es config.yml, la sección de Swiftmailer, abrimos el fichero config.yml y buscamos “Swiftmailer Configuration”:
```sh
sudo nano app/config/config.yml
```
Una vez realizada la búsqueda veremos algo como lo siguiente, donde tendremos que rellenar con los datos necesarios (entre paréntesis cuales)
```sh
# Swiftmailer Configuration
swiftmailer:
# Gmail
    transport: gmail
    host:      smtp.gmail.com
    username:  XXXXXX (introduce tu cuenta de Gmail)
    password:  XXXXXX (introduce la contraseña de tu cuenta de Gmail)
    spool:     { type: memory }

# Outlook (por si no funcionase Gmail)
#    host: smtp-mail.outlook.com
#    auth_mode: login
#    port: 25
#    encryption: tls
#    username: XXXXXX (introduce tu cuenta de Outlook)
#    password: XXXXXX (introduce la contraseña de tu cuenta de Outlook)
#    spool:     { type: memory }
```
Nota: es posible que tengamos que borrar la cache de desarrollo y producción

[Volver arriba](#markdown-header-instalacion-aplicacion-dealhunt)

## 4.10 Apéndice

A continuación se incluyen los enlaces de manuales extra que se incluyen en este manual y que es posible que no puedan ser accesibles desde este documento.

* Ubuntu 14.04:
    * http://www.ubuntu.com/download/desktop/
* Manual de instalación de Ubuntu en inglés:
    * http://www.ubuntu.com/download/desktop/install-ubuntu-desktop/
* Manual de instalación de Ubuntu en español:
    * http://blog.soluciones-libres.com/2014/04/instalar-ubuntu-1404-trusty-thar-como.html
* Cómo virtualizar Ubuntu 14.04 en Windows utilizando VirtualBox:
    * http://blog.uptodown.com/tutorial-virtualizar-ubuntu-14-virtualbox/

Si necesitases algún dato como los datos de las cuentas de correo utilizadas para probar por ejemplo el formulario de contacto, o si tuvieses cualquier duda contacta conmigo a través de la siguiente cuenta o la suministrada en el CV.

* dealhunter.proyecto@gmail.com

[Volver arriba](#markdown-header-instalacion-aplicacion-dealhunt)


