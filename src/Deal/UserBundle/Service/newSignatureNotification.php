<?php

namespace Deal\UserBundle\Service;

use Doctrine\ORM\EntityManager;
use Deal\UserBundle\Entity\SignatureNotification as SignatureNotification;

/*
 *
 * Clase encargada de crear las notificaciones por firmas
 **/
class newSignatureNotification {

    // Inyecto el EntityManager para poder hacer operaciones en la base de datos
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /*
     * Función encargada de crear una notificación de nueva firma
     *
     * @param Signature $signature firma por la cual se esta creando esta notificación
     * */
    public function newSignatureNotification($signature){

        // Creo la notificación y la seteo
        $signatureNotification = new SignatureNotification();

        $signatureNotification->setSignature($signature);
        $signatureNotification->setUser($signature->getSignedUser());

        // Solo hago persist, el flush lo hago en el controlador
        $this->em->persist($signatureNotification);

    }
} 