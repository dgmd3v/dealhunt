<?php

namespace Deal\UserBundle\Service;

use Doctrine\ORM\EntityManager;

/*
 * Clase encargada gestionar el borrado de las firmas
 **/
class deleteSignature {

    // Inyecto el EntityManager para poder hacer operaciones en la base de datos
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /*
     * Función encargada de borrar una firma
     *
     * @param integer $signature id de la firma a borrar
     * */
    public function delete($signature){

        // Busco la notificación que se había generado al firmar
        $signatureNotificationId = $this->em->getRepository('UserBundle:SignatureNotification')
                                                                          ->findNotificationIdBySignatureId($signature);

        // Borro la notificación
        $this->em->getRepository('UserBundle:BaseNotification')->deleteNotificationById($signatureNotificationId);

        // Borro la firma
        $this->em->remove($signature);
    }
} 