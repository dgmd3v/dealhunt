<?php

namespace Deal\UserBundle\ModerationTools;

use Doctrine\ORM\EntityManager;
use Deal\UserBundle\Entity\User as User;
use Deal\UserBundle\Entity\Ban as Ban;

/*
 *
 * Clase encargada gestionar los baneos de usuarios
 **/
class BanUser {

    // Inyecto el EntityManager para poder hacer operaciones en la base de datos
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function ban(User $user, \DateTime $finishingDate, $reason){

        // Marco al usuario como locked
        $user->setLocked(true);

        // Creo un baneo con la fecha hasta la cual estará baneado el usuario
        $userBan = new Ban();

        // Fecha de fin del baneo (1 de enero de 2015)
        $userBan->setFinishingDate($finishingDate);

        // Razón por la que se le ha baneado
        $userBan->setReason($reason);

        // Incluyo en el ban el usuario baneado
        $userBan->setUser($user);

        // Hago la entidad persistente
        $this->em->persist($userBan);
        $this->em->persist($user);

        // Guardo los datos marcados como persistentes
        $this->em->flush();
    }

    public function unBan(Ban $userBan){

        // Usuario que tiene el ban
        $user = $userBan->getUser();

        // Marco al usuario como no locked
        $user->setLocked(false);

        // Elimino el ban
        $this->em->remove($userBan);

        // Hago la entidad persistente;
        $this->em->persist($user);

        // Guardo los datos marcados como persistentes
        $this->em->flush();
    }
} 