<?php

namespace Deal\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class UserBundle extends Bundle
{
    // Heredo de FOSUserBundle para poder hacer modificaciones sobre vistas, etc
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
