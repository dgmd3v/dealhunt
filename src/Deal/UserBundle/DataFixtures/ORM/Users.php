<?php

namespace Deal\UsersBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

/*
 * Fixtures/Datos de prueba para la entidad user
 * En este fixture creo los users/usuarios indicando su nombre, e-mail, fecha de registro, etc, etc
**/
class Users extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    // Orden en el que se carga este fixture
    public function getOrder()
    {
        return 20;
    }

    private $container;

    // Inyecto el contenedor de servicios para poder codificar las contraseñas
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        // Directorio raíz del proyecto
        $root = $this->container->get('kernel')->getRootDir();

        // Variable para trabajar con el sistema de ficheros
        $filesystem = new Filesystem();

        // Creo la carpeta donde se guardaran las subidas, borrando antes la carpeta si hubiese sido creada
        // con anterioridad
        $uploadsFolder = $root . '/../web/uploads/';
        if($filesystem->exists($uploadsFolder)){
            $filesystem->remove($uploadsFolder);
        }

        $filesystem>mkdir($uploadsFolder, 0775);
        $filesystem->chown($uploadsFolder, 'www-data', true);
        $filesystem->chgrp($uploadsFolder, 'uploadsmaster', true);


        // Creo la carpeta donde se guardaran las imagenes de liipimaginebundle
        $liipImagineBundleMediaFolder = $root . '/../web/media/';
        if($filesystem->exists($liipImagineBundleMediaFolder)){
            $filesystem->remove($liipImagineBundleMediaFolder);
        }

        $filesystem>mkdir($liipImagineBundleMediaFolder, 0775);
        $filesystem->chown($liipImagineBundleMediaFolder, 'www-data', true);
        $filesystem->chgrp($liipImagineBundleMediaFolder, 'uploadsmaster', true);

        $liipImagineBundleCacheFolder = $root . '/../web/media/cache/';
        $filesystem>mkdir($liipImagineBundleCacheFolder, 0775);
        $filesystem->chown($liipImagineBundleCacheFolder, 'www-data', true);
        $filesystem->chgrp($liipImagineBundleCacheFolder, 'uploadsmaster', true);

        // Creo la carpeta donde se guardaran los avatares
        $uploadedImagesFolder = $root . '/../web/uploads/images/';
        if($filesystem->exists($uploadedImagesFolder)){
            $filesystem->remove($uploadedImagesFolder);
        }

        $filesystem>mkdir($uploadedImagesFolder, 0775);
        $filesystem->chown($uploadedImagesFolder, 'www-data', true);
        $filesystem->chgrp($uploadedImagesFolder, 'uploadsmaster', true);

        $avatarsFolder = $root . '/../web/uploads/images/avatar/';
        $filesystem>mkdir($avatarsFolder, 0775);
        $filesystem->chown($avatarsFolder, 'www-data', true);
        $filesystem->chgrp($avatarsFolder, 'uploadsmaster', true);

        // Voy a crear 20 Users, cada uno tendrá 20 posts solo el primero habrá
        // creado temas (esto se hace en el fixture de Posts), todos van a pertenecer al grupo usuarios
        for($i=0 ; $i < 20; $i++)
        {
            $userManager = $this->container->get('fos_user.user_manager');

            $user = $userManager->createUser();
            $user->setUsername('Usuario-'.($i+1));
            $user->setEmail('dealhunter.proyecto+usuario'.($i+1).'@gmail.com');
            $user->setPlainPassword('usuario-'.($i+1));
            $user->setEnabled(true);
            if($i == 0){
                $user->addRole("ROLE_ADMIN");
            }

            else{
                $user->addRole("ROLE_USER");
            }

            // Copio los avatares de los assets a la carpeta de subida de avatares
            $fromHere = $root .'/../web/bundles/user/images/avatar/' . 'avatar_'.($i+1).'.jpg';
            $toHere =  $root . '/../web/uploads/images/avatar/avatar_'.($i+1).'.jpg';
            $filesystem->copy($fromHere, $toHere);

            $user->setAvatarName('avatar_'.($i+1).'.jpg');
            $user->getUserProfile()->setRegDate(new \DateTime('now - '.rand(1, 30).' days'));
            $user->getUserProfile()->setPosts(840);
            $user->getUserProfile()->setLocation('Mi Ciudad');
            $user->getUserProfile()->setBirthday(new \DateTime('now - '.rand(5475, 36500).' days'));
            $user->getUserProfile()->setUserCustomInfo('Información personalizable por el usuario: ' . 'Usuario-'.($i+1));

            // Para los datos de prueba voy a suponer que solo el usuario con id=1 es el único que crea temas
            if($i==0){
                $user->getUserProfile()->setDealsPositive(rand(1, 30));
                $user->getUserProfile()->setDealsNegative(rand(1, 30));
                $user->getUserProfile()->setTopicsStarted(840);
            }

            else {
                $user->getUserProfile()->setDealsPositive(0);
                $user->getUserProfile()->setDealsNegative(0);
                $user->getUserProfile()->setTopicsStarted(0);
            }

            // Hago la entidad persistente
            $manager->persist($user);

        }

        // Usuarios extra, para pruebas Administrador, Moderador, Usuario
        for($i=0 ; $i < 3; $i++)
        {
            switch($i){
                case 0:
                    $username = 'administrador';
                    $mail = 'dealhunter.proyecto+administrador@gmail.com';
                    $role = "ROLE_ADMIN";
                    break;
                case 1:
                    $username = 'moderador';
                    $mail = 'dealhunter.proyecto+moderador@gmail.com';
                    $role = "ROLE_MODERATOR";
                    break;

                case 2:
                    $username = 'usuario';
                    $mail = 'dealhunter.proyecto+usuario@gmail.com';
                    $role = "ROLE_USER";
                    break;
            }

            $user = $userManager->createUser();
            $user->addRole($role);
            $user->setUsername($username);
            $user->setEmail($mail);
            $user->setPlainPassword($username);
            $user->setEnabled(true);
            $user->getUserProfile()->setRegDate(new \DateTime('now '));
            $user->getUserProfile()->setPosts(840);
            $user->getUserProfile()->setLocation('Mi Ciudad');
            $user->getUserProfile()->setBirthday(new \DateTime('now - '.rand(5475, 36500).' days'));
            $user->getUserProfile()->setUserCustomInfo('Información personalizable por el usuario: ' . $username);
            $user->getUserProfile()->setDealsPositive(0);
            $user->getUserProfile()->setDealsNegative(0);
            $user->getUserProfile()->setTopicsStarted(0);
            $user->getUserProfile()->setDealsPositive(0);
            $user->getUserProfile()->setDealsNegative(0);
            $user->getUserProfile()->setTopicsStarted(0);

            // Hago la entidad persistente
            $manager->persist($user);
        }

        // Guardo los datos marcados como persistentes
        $manager->flush();

    }
}

