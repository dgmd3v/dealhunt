<?php

namespace Deal\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Deal\UserBundle\Entity\Signature;

/*
 * Fixtures/Datos de prueba para la entidad Signature
 * En este fixture creo las firmas que se hacen entre los usuarios (Signatures) indicando el el autor de la firma,
 * el usuario al que va dirigida, fecha, ip y texto de la firma
 **/
class Signatures extends AbstractFixture implements OrderedFixtureInterface
{
    // Orden en el que se carga este fixture
    public function getOrder()
    {
        return 340;
    }

    public function load(ObjectManager $manager)
    {

        $users = $manager->getRepository('UserBundle:User')->findAll();

        // Voy a crear 1 firma de los 19 usuarios al usuario 1
        for($i = 1; $i < count($users); $i++){
            $signature = new Signature();

            // Usuario al que se le está firmando
            $signature->setSignedUser($users[0]);

            // Usuario que realiza la firma
            $signature->setSignatureUser($users[$i]);

            // Mensaje de la firma
            $signature->setSignature("Soy el Usuario-" . ($i+1) . " firmando al usuario 1");

            // Fecha de la firma
            $signature->setSignatureDate(new \DateTime('now - ' . $i .' hour' ));

            // Ip de la firma
            $signature->setSignatureIp('127.0.0.0');

            // Hago la entidad persistente
            $manager->persist($signature);
        }

        // Guardo los datos marcados como persistentes
        $manager->flush();

    }
}
