<?php

namespace Deal\PostBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Deal\UserBundle\Entity\Visited;

/*
 * Fixtures/Datos de prueba para la entidad visit
 * En este fixture creo los visits (temas que el usuario ha visitado), diciendo que usuario ha visitado,
 * el último post del tema que ha visitado, etc
 **/
class Visits extends AbstractFixture implements OrderedFixtureInterface
{
    // Orden en el que se carga este fixture
    public function getOrder()
    {
        return 300;
    }

    public function load(ObjectManager $manager)
    {
        // Obtengo todos los usuarios
        $users = $manager->getRepository('UserBundle:User')->findAll();

        // Obtengo los topics
        $topics = $manager->getRepository('TopicBundle:Topic')->findAll();

        // Cada usuario va a visitar el post con su mismo id,
        // es decir el usuario 1 va a visitar el tema 1 y así el resto
        for($i=0; $i < count($users); $i++)
        {
            // Creo una nueva entidad visited
            $visit = new Visited();

            // Incluyo en la visita el usuario que ha visitado el hilo
            $visit->setUser($users[$i]);

            // Número del último post (mensaje) la última vez que el usuario visitó el tema (topic)
            $visit->setLastVisitedPost($topics[$i]->getLastPost());

            // Hora de la última visita realizada al tema
            $visit->setLastVisit(new \DateTime('now'));

            // Hago la entidad persistente
            $manager->persist($visit);
        }

        // Guardo los datos marcados como persistentes
        $manager->flush();

    }// Fin load
}