<?php

namespace Deal\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Deal\UserBundle\Entity\QuoteNotification;

/*
 * Fixtures/Datos de prueba para la entidad QuoteNotification
 * En este fixture creo las citas (QuoteNotification) indicando el mensaje (post) desde el que se cita y
 * el usuario citado
 **/
class QuoteNotifications extends AbstractFixture implements OrderedFixtureInterface
{
    // Orden en el que se carga este fixture
    public function getOrder()
    {
        return 380;
    }

    public function load(ObjectManager $manager)
    {
        // Busco los mensajes (posts) del tema (topic) con id 1 y todos los usuarios
        $posts = $manager->getRepository('PostBundle:Post')->findTopicPostsWithResults(1);
        $users = $manager->getRepository('UserBundle:User')->findAll();

        // Voy a crear 19 Citas (Quotes), todas citaran al usuario 1 desde el tema 1
        for($i=0 ; $i < count($posts); $i++)
        {
            $quote = new QuoteNotification();

            // Mensaje (post) en el cual se cita el mensaje (post) de otro usuario (notifiedUser)
            $quote->setPost($posts[$i]);

            // Usuario al cual se está citando en el Mensaje(post)
            $quote->setUser($users[0]);

            // Fecha
            $quote->setDate(new \DateTime('now'));

            // Hago la entidad persistente
            $manager->persist($quote);
        }

        // Guardo los datos marcados como persistentes
        $manager->flush();
    }
}
