<?php

namespace Deal\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Deal\UserBundle\Entity\ModerationNotification as ModerationNotification;

/*
 * Fixtures/Datos de prueba para la entidad ModerationNotification
 * En este fixture creo las notificaciones indicando que un mensaje del usuario ha sido moderado
 **/
class ModerationNotifications extends AbstractFixture implements OrderedFixtureInterface
{
    // Orden en el que se carga este fixture
    public function getOrder()
    {
        return 400;
    }

    public function load(ObjectManager $manager)
    {
        // Busco los mensajes (posts) del tema (topic) con id 1 y todos los usuarios
        $posts = $manager->getRepository('PostBundle:Post')->findTopicPostsWithResults(1);

        // Voy a crear 1 Notificación de moderación (Notifications) para usuario 2 desde el tema 1
        $moderation = new ModerationNotification();

        // Seteo el mensaje (post) que se ha moderado
        $moderation->setPost($posts[0]);
        $posts[0]->setIsDeleted(true);

        // Fecha
        $moderation->setDate(new \DateTime('now'));

        // Usuario moderado
        $moderation->setUser($posts[0]->getPoster());

        // Hago la entidad persistente
        $manager->persist($moderation);

        // Guardo los datos marcados como persistentes
        $manager->flush();
    }
}
