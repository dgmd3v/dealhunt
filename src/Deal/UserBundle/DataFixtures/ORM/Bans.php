<?php

namespace Deal\PostBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Deal\UserBundle\Entity\Ban;

/*
 * Fixtures/Datos de prueba para la entidad ban
 * En este fixture creo los bans, asignando el usuario al cual está dirigido, su ip, etc
**/
class Bans extends AbstractFixture implements OrderedFixtureInterface
{
    // Orden en el que se carga este fixture
    public function getOrder()
    {
        return 320;
    }

    public function load(ObjectManager $manager)
    {
        // Obtengo el usuario con id 14, el único con un ban
        $user = $manager->getRepository('UserBundle:User')->find(14);

        $user->setLocked(true);

        // Creo una nueva entidad ban
        $ban = new Ban();

        // Fecha de fin del baneo (1 de enero de 2015)
        $ban->setFinishingDate(new \DateTime('2020-01-01'));

        // Razón por la que se le ha baneado
        $ban->setReason("Mal comportamiento");

        // Incluyo en el ban el usuario baneado
        $ban->setUser($user);

        // Hago la entidad persistente
        $manager->persist($ban);

        // Guardo los datos marcados como persistentes
        $manager->flush();

    }// Fin load
}