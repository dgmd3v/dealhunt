<?php

namespace Deal\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Deal\UserBundle\Entity\SignatureNotification;

/*
 * Fixtures/Datos de prueba para la entidad SignatureNotification
 * En este fixture creo las notificaciones de las firmas (SignatureNotification) indicando la firma en el aviso, de
 * donde extraigo el usuario que firma y el firmado
 **/
class SignatureNotifications extends AbstractFixture implements OrderedFixtureInterface
{
    // Orden en el que se carga este fixture
    public function getOrder()
    {
        return 360;
    }

    public function load(ObjectManager $manager)
    {
        $signatures = $manager->getRepository('UserBundle:Signature')->findAll();

        // Voy a crear una notificación por cada firma realizada
        for($i = 0; $i < count($signatures); $i++){
            $signatureNotification = new SignatureNotification();

            // Seteo la firma (donde tengo el usuario firmado y el que firma)
            $signatureNotification->setSignature($signatures[$i]);

            // Usuario firmado
            $signatureNotification->setUser($signatures[$i]->getSignedUser());

            // Hago la entidad persistente
            $manager->persist($signatureNotification);
        }

        // Guardo los datos marcados como persistentes
        $manager->flush();

    }
}
