<?php

namespace Deal\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * QuoteNotification
 *
 * @ORM\Entity(repositoryClass="Deal\UserBundle\Entity\QuoteNotificationRepository")
 */
class QuoteNotification extends BaseNotification
{
    // *************** COLUMNAS DE LA TABLA ***************

    /**
     * @ORM\ManyToOne(targetEntity="Deal\PostBundle\Entity\Post")
     * @ORM\JoinColumn(name="post_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    // Tenemos una relación N-1 entre la cita (quote) y el mensaje (Post) al cual se refiere la cita (quote).
    // Se pueden hacer N citas sobre un único mensaje.
    private $post;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime $date
     */
    // Fecha en la que se creó la notificación
    protected $date;


    // *************** SETTERS Y GETTERS ***************

    /**
     * Set post
     *
     * @param Deal\PostBundle\Entity\Post $post
     * @return QuoteNotification
     */
    public function setPost(\Deal\PostBundle\Entity\Post $post)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return integer
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set date
     *
     * @param DateTime $date
     * @return QuoteNotification
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
}