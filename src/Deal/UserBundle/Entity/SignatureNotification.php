<?php

namespace Deal\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SignatureNotification
 *
 * @ORM\Entity(repositoryClass="Deal\UserBundle\Entity\SignatureNotificationRepository")
 */
class SignatureNotification extends BaseNotification
{
    // *************** COLUMNAS DE LA TABLA ***************

    /**
     * @ORM\OneToOne(targetEntity="Deal\UserBundle\Entity\Signature")
     * @ORM\JoinColumn(name="signature_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    // Tenemos una relación 1-1 entre la notificación de una firma (SignatureNotification) y la firma.
    // Una notificación de una firma solo puede pertenecer a una firma y una firma solo puede pertenecer
    // a una notificación.
    private $signature;

    // *************** SETTERS Y GETTERS ***************

    /**
     * Set signature
     *
     * @param Deal\UserBundle\Entity\Signature $signature
     * @return SignatureNotification
     */
    public function setSignature(\Deal\UserBundle\Entity\Signature $signature)
    {
        $this->signature = $signature;

        return $this;
    }

    /**
     * Get signature
     *
     * @return Deal\UserBundle\Entity\Signature $signature
     */
    public function getSignature()
    {
        return $this->signature;
    }
}