<?php

namespace Deal\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;

/*
 * Clase que sirve como repositorio para las consultas relacionadas con las firmas
 * */
class SignatureRepository extends EntityRepository
{

    /*
     * Devuelve las 10 últimas firmas que ha recibido el usuario
     *
     * @param string $userId Id del usuario del cual se quiere obtener sus firmas
     * @param integer $numberOfSignatures Id número de firmas a buscar
     **/
    public function findLastNUserSignatures($userId, $numberOfSignatures)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT UserSig, signatureU
                      FROM UserBundle:Signature UserSig
                      JOIN UserSig.signedUser signedU
                      JOIN UserSig.signatureUser signatureU
                     WHERE signedU.id = :userId
                     ORDER BY UserSig.signatureDate DESC');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId' => $userId
        ));

        // Limito los resultados
        $query->setMaxResults($numberOfSignatures);

        // Retorno el resultado de la consulta
        return $query->getResult();
    }

    /*
     * Devuelve la consulta encargada de buscar las firmas
     *
     * @param string $userId Id del usuario del cual se quiere obtener sus firmas
     **/
    public function findUserSignatures($userId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT s, sedu, su
                      FROM UserBundle:Signature s
                      JOIN s.signedUser sedu
                      JOIN s.signatureUser su
                     WHERE sedu.id = :userId
                  ORDER BY s.signatureDate DESC');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId' => $userId
        ));

        // Retorno la consulta
        return $query;
    }

    /*
     * Devuelve una firma dada su Id
     *
     * @param string $signatureId Id de la firma que se quiere encontrar
     **/
    public function findSignatureById($signatureId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT s
                      FROM UserBundle:Signature s
                     WHERE s.id = :signatureId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'signatureId' => $signatureId,
        ));

        // Retorno la consulta
        return $query->getOneOrNullResult();
    }

    /*
     * Devuelve el número de firmas que ha recibido el usuario
     *
     * @param string $userId id del usuario del cual se quiere obtener el número de firmas
     **/
    public function findNumberOfUserSignatures($userId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT COUNT(s)
                      FROM UserBundle:Signature s
                     WHERE s.signedUser = :userId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId' => $userId
        ));

        // Limito los resultados
        $query->setMaxResults(1);

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }
}