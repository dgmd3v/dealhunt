<?php

namespace Deal\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;

/*
 * Clase que sirve como repositorio para las consultas relacionadas con las notificaciones de firmas
 * */
class SignatureNotificationRepository extends EntityRepository
{

    /*
     * Devuelve el número de notificaciones por citas que tiene el usuario
     *
     * @param string $userId nombre del usuario del cual se quiere obtener el número notificaciones
     **/
    public function findNumberOfSignatureNotifications($userId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT COUNT(sn)
                      FROM UserBundle:SignatureNotification sn
                     WHERE sn.user = :userId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId' => $userId
        ));

        // Limito los resultados
        $query->setMaxResults(1);

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }

    /*
     * Devuelve el número de notificaciones por citas que tiene el usuario sin leer
     *
     * @param string $userId nombre del usuario del cual se quiere obtener el número notificaciones sin leer
     **/
    public function findNumberOfUnNotifiedSignatures($userId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT COUNT(sn)
                      FROM UserBundle:SignatureNotification sn
                     WHERE sn.user = :userId
                       AND sn.isNotified = 0');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId' => $userId
        ));

        // Limito los resultados
        $query->setMaxResults(1);

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }

    /*
     * Devuelve el id de la notificación de una firma dada la id de la firma
     *
     * @param integer $signatureId Id de la firma de la cual queremos encontrar su notificación
     **/
    public function findNotificationIdBySignatureId($signatureId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT sn.id
                      FROM UserBundle:SignatureNotification sn
                      JOIN sn.signature sig
                     WHERE sig.id = :signatureId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'signatureId' => $signatureId,
        ));

        // Limito los resultados
        $query->setMaxResults(1);

        // Retorno la consulta
        return $query->getSingleScalarResult();
    }

    /*
     * Devuelve la consulta que selecciona las notificaciones por firmas que tiene un usuario
     *
     * @param integer $userId Id del usuario del cual queremos buscar las notificaciones
     **/
    public function findUserSignatureNotifications($userId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT sn, sig, su
                      FROM UserBundle:SignatureNotification sn
                      JOIN sn.signature sig
                      JOIN sig.signatureUser su
                     WHERE sn.user = :userId
                     ORDER BY sig.signatureDate DESC');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId' => $userId,
        ));

        // Retorno la consulta
        return $query;
    }

    /*
     * Actualiza las notificaciones de un usuario marcándolas como notificadas
     *
     * @param integer $userId Id del usuario al cual le estamos actualizando el estado de las notificaciones
     **/
    public function updateNotifiedStatus($userId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        $dql = ('   UPDATE UserBundle:SignatureNotification sn
                       SET sn.isNotified = 1
                     WHERE sn.user = :userId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId' => $userId,
        ));

        // Retorno la consulta
        return $query->getResult();
    }

}