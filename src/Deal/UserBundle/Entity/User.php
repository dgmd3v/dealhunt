<?php

namespace Deal\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * User
 *
 * @ORM\Table(name="MyUser")
 * @ORM\Entity(repositoryClass="Deal\UserBundle\Entity\UserRepository")
 * @UniqueEntity(fields="emailCanonical", message="Este correo ya está en uso")
 * @UniqueEntity(fields="usernameCanonical", message="Este nombre ya está en uso")
 * @Vich\Uploadable
 */
class User extends BaseUser // Heredo de la clase de FOSUserBundle BaseUser
{
    // *************** COLUMNAS DE LA TABLA ***************

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    // ID del usuario
    protected $id;

    /**
     * @var File
     *
     * @Vich\UploadableField(mapping="avatar", fileNameProperty="avatarName")
     * @Assert\Image(maxSize = "3M",
     *               mimeTypes = {"image/jpeg", "image/png", "image/gif"},
     *               mimeTypesMessage= "El archivo no es una imagen válida. Solo puedes subir imágenes jpeg, png y gif")
     */
    // En este campo guardo el objeto UploadedFile después de haber enviado el formulario
    private $avatarFile;

    /**
     * @var string
     *
     * @ORM\Column(name="avatar_name", type="string", length=255, nullable=false)
     * @Assert\Length(max = 255, maxMessage = "El número máximo de caracteres que se pueden escribir para el nombre del avatar son {{ limit }}")
     */
    // Nombre del avatar (imagen del usuario) del usuario
    private $avatarName;

    /**
     * @ORM\Column(name="last_activity", type="datetime", nullable=true)
     *
     * @var \DateTime $lastActivity
     */
    // Fecha de la última actividad del usuario
    // También para saber la hora a la que se actualizo el avatar del usuario, necesario para subir correctamente la
    // imagen
    protected $lastActivity;

    /**
     * @ORM\OneToOne(targetEntity="Deal\UserBundle\Entity\UserProfile", inversedBy="user",
     *               cascade= {"persist", "merge", "remove"})
     * @ORM\JoinColumn(name="user_with_profile", referencedColumnName="id", nullable=false)
     **/
    // Perfil del usuario
    protected $userProfile;

    /**
     * @ORM\ManyToMany(targetEntity="Deal\TopicBundle\Entity\Topic")
     * @ORM\JoinTable(  name="Vote",
     *                  joinColumns         ={@ORM\JoinColumn(name="user_id",   referencedColumnName="id", onDelete="cascade")},
     *                  inverseJoinColumns  ={@ORM\JoinColumn(name="topic_id",  referencedColumnName="id", onDelete="cascade")}
     * )
     */
    // Temas que ha votado este usuario
    private $votes;

    /**
     * @ORM\OneToMany(targetEntity="Deal\UserBundle\Entity\BaseNotification", mappedBy="user")
     */
    // Notificaciones pertenecientes al usuario
    private $notifications;

    public function __construct() {
        parent::__construct();
        $this->votes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->notifications = new \Doctrine\Common\Collections\ArrayCollection();

        // Aprovecho y cuando creo el usuario ya creo su perfil y le doy un avatar por defecto
        $this->userProfile = new UserProfile();
        $this->avatarName = 'noavatar';
    }

    // *************** SETTERS Y GETTERS ***************

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    // Función que devuelve el primer role del usuario (el de mayor rango)
    public function getHighestRole()
    {
        $roles = $this->getRoles();
        $highestRole = 'ROLE_USER';
        foreach($roles as $role){
            if($role == 'ROLE_ADMIN'){
                $highestRole = $role;
                break;
            }

            elseif($highestRole != 'ROLE_ADMIN' && $role == 'ROLE_MODERATOR'){
                $highestRole = $role;
            }
        }
        return $highestRole;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setAvatarFile(File $image)
    {
        $this->avatarFile = $image;

        if ($image) {
            // Es necesario que al menos un campo cambie si se usa doctrine,
            // en otro caso el listener no será llamado y se perderá el fichero
            $this->lastActivity = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    // Devuelvo el objeto UploadedFile
    public function getAvatarFile()
    {
        return $this->avatarFile;
    }

    /**
     * @param string $avatarName
     */
    // Función para "setear" el nombre de la imagen
    public function setAvatarName($avatarName)
    {
        $this->avatarName = $avatarName;
    }

    /**
     * @return string
     */
    // Función que devuelve el nombre de la imagen
    public function getAvatarName()
    {
        return $this->avatarName;
    }

    /**
     * @param DateTime $lastActivity
     */
    public function setLastActivity($lastActivity)
    {
        $this->lastActivity = $lastActivity;
    }

    /**
     * @return DateTime
     */
    public function getLastActivity()
    {
        return $this->lastActivity;
    }


    /**
     * Set userProfile
     *
     * @param \Deal\UserBundle\Entity\UserProfile $userProfile
     */
    public function setUserProfile(\Deal\UserBundle\Entity\UserProfile $userProfile)
    {
        $this->userProfile = $userProfile;

        return $this;
    }

    /**
     * Get userProfile
     *
     * @return \Deal\UserBundle\Entity\UserProfile $userProfile
     */
    public function getUserProfile()
    {
        return $this->userProfile;
    }

    /**
     * Add vote
     *
     * @param Deal\TopicBundle\Entity\Topic $vote
     */
    // Esta función es la encargada de añadir más votos
    public function addTopicVote(\Deal\TopicBundle\Entity\Topic $vote)
    {
        $this->votes[] = $vote;

        return $this;
    }

    /**
     * Get votes
     *
     * @return Deal\TopicBundle\Entity\Topic $votes
     */
    public function getTopicVotes()
    {
        return $this->votes;
    }

    /**
     * Add Notification
     *
     * @param Deal\UserBundle\Entity\BaseNotification $notification
     */
    // Esta función es la encargada de añadir más notificaciones
    public function addNotification(\Deal\UserBundle\Entity\BaseNotification $notification)
    {
        $this->notifications[] = $notification;

        return $this;
    }

    /**
     * Get Notification
     *
     * @return Deal\UserBundle\Entity\BaseNotification $notifications
     */
    public function getNotifications()
    {
        return $this->notifications;
    }
}
