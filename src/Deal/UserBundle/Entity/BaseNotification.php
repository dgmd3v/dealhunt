<?php

namespace Deal\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BaseNotification
 *
 * @ORM\Entity(repositoryClass="Deal\UserBundle\Entity\BaseNotificationRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"Moderation" = "ModerationNotification", "Quote" = "QuoteNotification", "Signature" = "SignatureNotification"})
 */
class BaseNotification
{
    // *************** COLUMNAS DE LA TABLA ***************

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    // ID de la notificación
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_notified", type="boolean")
     */
    // Campo que indica si el usuario ha sido notificado. Por defecto a falso
    private $isNotified;

    /**
     * @ORM\ManyToOne(targetEntity="Deal\UserBundle\Entity\User", inversedBy="notifications")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     **/
    private $user;

    public function __construct() {
        $this->isNotified = false;
    }

    // *************** SETTERS Y GETTERS ***************

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isNotified
     *
     * @param boolean $isNotified
     * @return BaseNotification
     */
    public function setIsNotified($isNotified)
    {
        $this->isNotified = $isNotified;

        return $this;
    }

    /**
     * Get isNotified
     *
     * @return boolean
     */
    public function getIsNotified()
    {
        return $this->isNotified;
    }

    /**
     * @param Deal\UserBundle\Entity\User $user
     */
    public function setUser(\Deal\UserBundle\Entity\User $user)
    {
        $this->user = $user;
    }

    /**
     * @return Deal\UserBundle\Entity\User $user
     */
    public function getUser()
    {
        return $this->user;
    }

}