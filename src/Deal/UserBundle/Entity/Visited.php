<?php

namespace Deal\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Visited
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Deal\UserBundle\Entity\VisitedRepository")
 */
class Visited
{
    // *************** COLUMNAS DE LA TABLA ***************

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_visit", type="datetime")
     */
    // Campo que indica a que hora fue la última visita al tema
    private $lastVisit;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Deal\PostBundle\Entity\Post")
     * @ORM\JoinColumn(name="last_visited_post_id", referencedColumnName="id", onDelete="CASCADE")
     */
    // Campo en el que se indica el último post (mensaje) la última vez que el usuario visitó el tema (topic)
    private $lastVisitedPost;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Deal\UserBundle\Entity\User")
     */
    // Tenemos una relación N-N entre el usuario (user) y el tema (topic).
    // Esta es la parte N-1 (ManyToOne) desde el usuario (user) y de esta parte 
    // obtenemos la primera clave de la clave compuesta
    private $user;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_favorite", type="boolean")
     */
    // Campo que indica si el tema visitado ha sido marcado como favorito por el usuario
    private $isFavorite;

    public function __construct() {
        $this->isFavorite = false;
    }

    // *************** SETTERS Y GETTERS ***************
    
    /**
     * Set lastVisit
     *
     * @param \DateTime $lastVisit
     * @return Visited 
     */
    public function setLastVisit($lastVisit)
    {
       $this->lastVisit = $lastVisit;
       
       return $this;
    }
    
    /**
     * Get lastVisit
     *
     * @return \DateTime 
     */
    public function getLastVisit()
    {
        return $this->lastVisit;
    }

    /**
     * Set setLastVisitedPost
     *
     * @param Deal\PostBundle\Entity\Post $lastVisitedPost
     * @return Visited
     */
    public function setLastVisitedPost(\Deal\PostBundle\Entity\Post $lastVisitedPost)
    {
        $this->lastVisitedPost = $lastVisitedPost;

        return $this;
    }

    /**
     * Get getLastVisitedPost
     *
     * @return Deal\PostBundle\Entity\Post $lastVisitedPost
     */
    public function getLastVisitedPost()
    {
        return $this->lastVisitedPost;
    }

    /**
     * Set user
     *
     * @param Deal\UserBundle\Entity\User $user
     * @return Visited
     */
    public function setUser(\Deal\UserBundle\Entity\User $user)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return integer 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set isFavorite
     *
     * @param boolean $isFavorite
     * @return Visited
     */
    public function setIsFavorite($isFavorite)
    {
        $this->isFavorite = $isFavorite;

        return $this;
    }

    /**
     * Get isFavorite
     *
     * @return boolean
     */
    public function getIsFavorite()
    {
        return $this->isFavorite;
    }
}
