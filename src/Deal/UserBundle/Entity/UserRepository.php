<?php

namespace Deal\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;

/*
 * Clase que sirve como repositorio para las consultas relacionadas con los usuarios
 * */
class UserRepository extends EntityRepository
{
    /*
     * Devuelve información del usuario solicitado
     *
     * @param string $userName nombre del usuario del cual se quiere obtener información
    **/
    public function findUserInfo($userName)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT u, profile
                      FROM UserBundle:User u
                      JOIN u.userProfile profile
                     WHERE u.username = :username');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'username' => $userName
        ));

        // Limito los resultados
        $query->setMaxResults(1);

        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }

    /*
     * Devuelve los últimos X posts (públicos, ROLE_USER) del usuario, además devuelve información del tema
     * y el foro en el que está siendo X el número de posts solicitados
     *
     * @param integer $userId id del usuario del cual queremos obtener sus últimos posts
     * @param integer $numberOfPosts número de post que queremos obtener del usuario
    **/
    public function findUserLastPosts($userId, $numberOfPosts)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT p, t, f
                      FROM PostBundle:Post p
                      JOIN p.topic t
                      JOIN t.forum f
                      JOIN f.accessRole ar
                     WHERE p.poster = :userId
                       AND ar.name =:ROLE_USER
                  ORDER BY p.postingDate DESC');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId'    => $userId,
            'ROLE_USER' => 'ROLE_USER',
        ));

        // Limito los resultados
        $query->setMaxResults($numberOfPosts);

        // Retorno el resultado de la consulta
        return $query->getResult();
    }

    /*
     * Devuelve el usuario dado su Id
     *
     * @param integer $userId id del usuario del cual queremos obtener sus datos
    **/
    public function findOnlyUserById($userId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT u
                      FROM UserBundle:User u
                     WHERE u.id = :userId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId' => $userId
        ));

        // Limito los resultados
        $query->setMaxResults(1);

        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }

    /*
     * Devuelve el usuario con el username dado
     *
     * @param string $userName nombre del usuario que buscamos
    **/
    public function findUserByUserName($userName)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT u
                      FROM UserBundle:User u
                     WHERE u.username = :userName');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userName' => $userName
        ));

        // Limito los resultados
        $query->setMaxResults(1);

        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }

    /*
     * Devuelve 0 si el usuario no ha votado el tema y 1 en caso contrario
     *
     * @param integer $userId id del usuario del cual se quiere obtener el número notificaciones sin leer
     * @param integer $topicId id del que queremos comprobar si ha sido votado
     **/
    public function findAlreadyVotedTopic($userId, $topicId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT COUNT(vt)
                      FROM UserBundle:User u
                      JOIN u.votes vt
                     WHERE u.id = :userId
                       AND vt.id = :topicId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId'    => $userId,
            'topicId'   => $topicId,
        ));

        // Limito los resultados
        $query->setMaxResults(1);

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }

    /*
     * Devuelve los temas votados por el usuario
     *
     * @param string $userId Id del usuario que vota el tema/topic
     * @param array $paginatedTopicsId array de Ids de los temas paginados
     *
     **/
    public function findVotedTopicsByUser($userId, $paginatedTopicsId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT partial u.{id}, partial tv.{id}
                      FROM UserBundle:User u
                      JOIN u.votes tv
                     WHERE u.id = :userId
                       AND tv.id IN (:paginatedTopicsId)
                       ');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId'            => $userId,
            'paginatedTopicsId' => $paginatedTopicsId,

        ));

        // Retorno el resultado de la consulta
        return $query->getArrayResult();
    }
}