<?php

namespace Deal\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;

/*
 * Clase que sirve como repositorio para las consultas relacionadas con las notificaciones base
 * */
class BaseNotificationRepository extends EntityRepository
{
    /*
     * Borra una notificación dada su Id
     *
     * @param integer $notificationId Id de la notificación a borrar
     **/
    public function deleteNotificationById($notificationId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        $dql = ('   DELETE UserBundle:BaseNotification bn
                     WHERE bn.id = :notificationId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'notificationId' => $notificationId,
        ));

        // Retorno la consulta
        return $query->getResult();
    }

    /*
     * Actualiza las notificaciones de un usuario marcándolas como notificadas
     *
     * @param integer $userId Id del usuario al cual le estamos actualizando el estado de las notificaciones
     **/
    public function updateNotifiedStatus($userId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        $dql = ('   UPDATE UserBundle:BaseNotification bn
                       SET bn.isNotified = 1
                     WHERE bn.user = :userId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId' => $userId,
        ));

        // Retorno la consulta
        return $query->getResult();
    }

}