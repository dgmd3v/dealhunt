<?php

namespace Deal\UserBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;

/**
 * UserProfile
 *
 * @ORM\Table(name="MyUserProfile")
 * @ORM\Entity(repositoryClass="Deal\UserBundle\Entity\UserRepository")
 */
class UserProfile
{
    // *************** COLUMNAS DE LA TABLA ***************

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="reg_date", type="datetime")
     */
    // Fecha de registro del usuario
    private $regDate;

    /**
     * @var integer
     * @ORM\Column(name="posts", type="integer")
     */
    // Número de posts del usuario, por defecto 0
    private $posts;

    /**
     * @var string
     * @ORM\Column(name="location", type="string", length=100, nullable=true)
     * @Assert\Length(max = 100, maxMessage = "El número máximo de caracteres que se pueden escribir para tu localización es de {{ limit }}")
     */
    // Ciudad o población del usuario
    // Reglas de validación:    Tiene un máximo de caracteres.
    private $location;

    /**
     * @var \DateTime
     * @ORM\Column(name="birthday", type="datetime", nullable=true)
     */
    // Fecha de nacimiento del usuario
    private $birthday;

    /**
     * @var integer
     * @Assert\GreaterThanOrEqual(value = 0)
     * @ORM\Column(name="deals_positive", type="integer")
     */
    // Número de votos positivos recibidos por las ofertas, etc. 0 por defecto
    private $dealsPositive;

    /**
     * @var integer
     * @Assert\GreaterThanOrEqual(value = 0)
     * @ORM\Column(name="deals_negative", type="integer")
     */
    // Número de votos negativos recibidos por las ofertas, etc. 0 por defecto
    private $dealsNegative;

    /**
     * @var integer
     * @ORM\Column(name="deals_shared", type="integer")
     * @Assert\GreaterThanOrEqual(value = 0)
     */
    // Número de ofertas,etc compartidos por el usuario. 0 por defecto
    private $topicsStarted;

    /**
     * @var string
     *
     * @ORM\Column(name="user_custom_info", type="text", nullable=true)
     * @Assert\Length(max = 10000, maxMessage = "El número máximo de caracteres que se pueden escribir en tu información de usuario es de {{ limit }}")
     */
    // Texto de personalizable por el usuario que se muestra en su perfil
    // Reglas de validación:    Tiene un máximo de caracteres.
    private $userCustomInfo;

    /**
     * @ORM\OneToOne(targetEntity="Deal\UserBundle\Entity\User", mappedBy="userProfile")
     */
    protected $user;

    public function __construct() {
        // Aprovecho y cuando creo el usuario ya introduzco la fecha de registro
        $this->regDate = new \DateTime();
        $this->posts = 0;
        $this->dealsPositive = 0;
        $this->dealsNegative = 0;
        $this->topicsStarted = 0;
    }

    // *************** SETTERS Y GETTERS ***************

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set regDate
     *
     * @param \DateTime $regDate
     * @return User
     */
    public function setRegDate($regDate)
    {
        $this->regDate = $regDate;

        return $this;
    }

    /**
     * Get regDate
     *
     * @return \DateTime
     */
    public function getRegDate()
    {
        return $this->regDate;
    }

    /**
     * Set posts
     *
     * @param integer $posts
     * @return User
     */
    public function setPosts($posts)
    {
        $this->posts = $posts;

        return $this;
    }

    /**
     * Get posts
     *
     * @return integer
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return User
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return User
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set dealsPositive
     *
     * @param integer $dealsPositive
     * @return User
     */
    public function setDealsPositive($dealsPositive)
    {
        $this->dealsPositive = $dealsPositive;

        return $this;
    }

    /**
     * Get dealsPositive
     *
     * @return integer
     */
    public function getDealsPositive()
    {
        return $this->dealsPositive;
    }

    /**
     * Set dealsNegative
     *
     * @param integer $dealsNegative
     * @return User
     */
    public function setDealsNegative($dealsNegative)
    {
        $this->dealsNegative = $dealsNegative;

        return $this;
    }

    /**
     * Get dealsNegative
     *
     * @return integer
     */
    public function getDealsNegative()
    {
        return $this->dealsNegative;
    }

    /**
     * Set topicsStarted
     *
     * @param integer $dealsShared
     * @return User
     */
    public function setTopicsStarted($topicsStarted)
    {
        $this->topicsStarted = $topicsStarted;

        return $this;
    }

    /**
     * Get topicsStarted
     *
     * @return integer
     */
    public function getTopicsStarted()
    {
        return $this->topicsStarted;
    }

    /**
     * Set userCustomInfo
     *
     * @param integer $usercustominfo
     * @return text
     */
    public function setUserCustomInfo($userCustomInfo)
    {
        $this->userCustomInfo = $userCustomInfo;

        return $this;
    }

    /**
     * Get userCustomInfo
     *
     * @return text
     */
    public function getUserCustomInfo()
    {
        return $this->userCustomInfo;
    }

    // Método mágico para que PHP sepa como convertir la entidad en una cadena de texto
    public function __toString()
    {
        return (string) $this->getId();
    }
}
