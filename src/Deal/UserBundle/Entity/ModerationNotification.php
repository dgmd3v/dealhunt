<?php

namespace Deal\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ModerationNotification
 *
 * @ORM\Entity(repositoryClass="Deal\UserBundle\Entity\ModerationNotificationRepository")
 */
class ModerationNotification extends BaseNotification
{
    // *************** COLUMNAS DE LA TABLA ***************

    /**
     * @ORM\ManyToOne(targetEntity="Deal\PostBundle\Entity\Post")
     * @ORM\JoinColumn(name="post_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    // Tenemos una relación N-1 entre el aviso de moderación (ModerationNotification) y el comentario (Post)
    // al cual se refiere la notificación. Podemos tener N notificaciones de moderación sobre un post.
    private $post;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime $date
     */
    // Fecha en la que se creo la notificación
    protected $date;

    // *************** SETTERS Y GETTERS ***************

    /**
     * Set post
     *
     * @param Deal\PostBundle\Entity\Post $post
     * @return ModerationNotification
     */
    public function setPost(\Deal\PostBundle\Entity\Post $post)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return Deal\PostBundle\Entity\Post $post
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * @param DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

}