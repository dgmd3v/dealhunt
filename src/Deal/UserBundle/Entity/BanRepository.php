<?php

namespace Deal\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;

/*
 * Clase que sirve como repositorio para las consultas relacionadas con los bans
 * */
class BanRepository extends EntityRepository
{
    /*
     * Devuelve información de un ban dado un id de usuario (incluyendo al usuario baneado)
     *
     * @param integer $userId id del usuario
    **/
    public function findBanByUserId($userId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT b, u
                      FROM UserBundle:Ban b
                      JOIN b.user u
                     WHERE u.id = :userId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId' => $userId
        ));

        // Limito los resultados
        $query->setMaxResults(1);

        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }
}