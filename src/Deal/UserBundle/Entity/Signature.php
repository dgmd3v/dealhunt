<?php

namespace Deal\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Signature
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Deal\UserBundle\Entity\SignatureRepository")
 */
class Signature
{
    // *************** COLUMNAS DE LA TABLA ***************      

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    // ID de la firma
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="signature", type="text")
     * @Assert\NotBlank()
     * @Assert\Length(max = 400, maxMessage = "El número máximo de caracteres que se pueden escribir en un mensaje son {{ limit }}")
     */
    // Texto de la firma
    // Reglas de validación:    No puede estar vacío. Y tiene un máximo de caracteres.
    private $signature;

    /**
     * @var integer
     *
     * @ORM\Column(name="signature_ip", type="string", length=46)
     * @Assert\Length(max = 46, maxMessage = "El número máximo de caracteres que se pueden escribir para la ip son {{ limit }}")
     */
    // Ip con la que se ha publicado la firma
    private $signatureIp;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="signature_date", type="datetime")
     */
    // Fecha en la que se hizo el post
    private $signatureDate;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Deal\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="signed_user_id", referencedColumnName="id", nullable=false)
     */
    // Usuario que recibe la firma. Relación User-Signature (1-N),
    // una firma solo va dirigida hacia un único User, mientras que un user puede recibir N firmas
    private $signedUser;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Deal\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="signature_user_id", referencedColumnName="id", nullable=false)
     */
    // Usuario que realiza la firma. Relación User-Signature (1-N),
    // una firma solo puede ser realizada por un solo user, mientras que un user puede tener realizar N firmas
    private $signatureUser;

    // *************** SETTERS Y GETTERS ***************  
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set signature
     *
     * @param string $signature
     * @return Signature
     */
    public function setSignature($signature)
    {
        $this->signature = $signature;
    
        return $this;
    }

    /**
     * Get signature
     *
     * @return string 
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * Set signatureIp
     *
     * @param string $signatureIp
     * @return Signature
     */
    public function setSignatureIp($signatureIp)
    {
        $this->signatureIp = $signatureIp;

        return $this;
    }

    /**
     * Get signatureIp
     *
     * @return string
     */
    public function getSignatureIp()
    {
        return $this->signatureIp;
    }

    /**
     * Set signatureDate
     *
     * @param \DateTime $signatureDate
     * @return Signature
     */
    public function setSignatureDate($signatureDate)
    {
        $this->signatureDate = $signatureDate;
    
        return $this;
    }

    /**
     * Get signatureDate
     *
     * @return \DateTime 
     */
    public function getSignatureDate()
    {
        return $this->signatureDate;
    }

    /**
     * Set signedUser
     *
     * @param Deal\UserBundle\Entity\User $signedUser
     * @return Signature
     */
    // Esta función espera que se le pase un objeto de tipo User
    public function setSignedUser(\Deal\UserBundle\Entity\User $signedUser)
    {
        $this->signedUser = $signedUser;

        return $this;
    }

    /**
     * Get signedUser
     *
     * @return Deal\UserBundle\Entity\User $signedUser
     */
    public function getSignedUser()
    {
        return $this->signedUser;
    }

    /**
     * Set signatureUser
     *
     * @param Deal\UserBundle\Entity\User $signatureUser
     * @return Signature
     */
    // Esta función espera que se le pase un objeto de tipo User 
    public function setSignatureUser(\Deal\UserBundle\Entity\User $signatureUser)
    {
        $this->signatureUser = $signatureUser;
    
        return $this;
    }

    /**
     * Get signatureUser
     *
     * @return Deal\UserBundle\Entity\User $signatureUser
     */
    public function getSignatureUser()
    {
        return $this->signatureUser;
    }

}
