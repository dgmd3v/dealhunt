<?php

namespace Deal\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Ban
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Deal\UserBundle\Entity\BanRepository")
 * @UniqueEntity(fields="user", message="Este usuario ya está baneado.")
 */
class Ban
{    
    // *************** COLUMNAS DE LA TABLA ***************

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Deal\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    // Relación 1-1 (OneToOne) entre las entidades User y Ban, en este caso
    // realizada de forma unidireccional. Un usuario solo puede tener un ban y un ban es solo de un usuario.
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finishing_date", type="datetime")
     */
    // Fecha de fin del ban
    private $finishingDate;

    /**
     * @var string
     *
     * @ORM\Column(name="reason", type="text")
     * @Assert\Length(max = 250, maxMessage = "El número máximo de caracteres que se pueden escribir para la razón del ban es de {{ limit }} caracteres")
     */
    // Razón, motivo por el que se le ha baneado
    private $reason;

    // *************** SETTERS Y GETTERS ***************

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param Deal\UserBundle\Entity\User
     */
    // Esta función espera que se le pase un objeto
    public function setUser(\Deal\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return Deal\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set finishingDate
     *
     * @param \DateTime $finishingDate
     * @return Ban
     */
    public function setFinishingDate($finishingDate)
    {
        $this->finishingDate = $finishingDate;
    
        return $this;
    }

    /**
     * Get finishingDate
     *
     * @return \DateTime 
     */
    public function getFinishingDate()
    {
        return $this->finishingDate;
    }

    /**
     * Set reason
     *
     * @param string $reason
     * @return Ban
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
    
        return $this;
    }

    /**
     * Get reason
     *
     * @return string 
     */
    public function getReason()
    {
        return $this->reason;
    }
}
