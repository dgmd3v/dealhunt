<?php

namespace Deal\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;

/*
 * Clase que sirve como repositorio para las consultas relacionadas con las notificaciones de citas
 * */
class QuoteNotificationRepository extends EntityRepository
{

    /*
     * Devuelve el número de notificaciones por citas que tiene el usuario
     *
     * @param string $userId nombre del usuario del cual se quiere obtener el número notificaciones
     **/
    public function findNumberOfQuoteNotifications($userId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT COUNT(qn)
                      FROM UserBundle:QuoteNotification qn
                     WHERE qn.user = :userId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId' => $userId
        ));

        // Limito los resultados
        $query->setMaxResults(1);

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }

    /*
     * Devuelve el número de notificaciones por citas que tiene el usuario sin leer
     *
     * @param string $userId nombre del usuario del cual se quiere obtener el número notificaciones sin leer
     **/
    public function findNumberOfUnNotifiedQuotes($userId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT COUNT(qn)
                      FROM UserBundle:QuoteNotification qn
                     WHERE qn.user = :userId
                       AND qn.isNotified = 0');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId' => $userId
        ));

        // Limito los resultados
        $query->setMaxResults(1);

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }

    /*
     * Devuelve la consulta que selecciona las notificaciones por citas que tiene un usuario
     *
     * @param integer $userId Id del usuario del cual queremos buscar las notificaciones
     **/
    public function findUserQuoteNotifications($userId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT qn, p, t, pstr
                      FROM UserBundle:QuoteNotification qn
                      JOIN qn.post p
                      JOIN p.topic t
                      JOIN p.poster pstr
                     WHERE qn.user = :userId
                     ORDER BY qn.date DESC');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId' => $userId,
        ));

        // Retorno la consulta
        return $query;
    }

    /*
     * Actualiza las notificaciones de un usuario marcándolas como notificadas
     *
     * @param integer $userId Id del usuario al cual le estamos actualizando el estado de las notificaciones
     **/
    public function updateNotifiedStatus($userId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        $dql = ('   UPDATE UserBundle:QuoteNotification qn
                       SET qn.isNotified = 1
                     WHERE qn.user = :userId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId' => $userId,
        ));

        // Retorno la consulta
        return $query->getResult();
    }

}