<?php

namespace Deal\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;

/*
 * Clase que sirve como repositorio para las consultas relacionadas con los temas visitados por el usuario
 * */
class VisitedRepository extends EntityRepository
{

    /*
     * Devuelve una o ninguna visitas realizadas a un tema/topic por un usuario
     *
     * @param string $topicId Id del tema visitado
     * @param string $userId Id del usuario que visita el tema/topic
     **/
    public function findOneTopicVisitedByUser($topicId, $userId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT v, lvp, t, f
                      FROM UserBundle:Visited v
                      JOIN v.lastVisitedPost lvp
                      JOIN lvp.topic t
                      JOIN t.forum f
                     WHERE v.user = :userId
                       AND t.id = :topicId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId' => $userId,
            'topicId'=> $topicId,
        ));

        // Limito los resultados
        $query->setMaxResults(1);

        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }

    /*
     * Devuelve los temas visitados por el usuario en los ultimos 30 días
     *
     * @param integer $userId Id del usuario que visita el tema/topic
     * @param integer $forumId Id del foro que está visitando el usuario
     * @param array $paginatedTopicsId array de Ids de los temas paginados
     *
     **/
    public function findVisitedTopicsLastMonthByUser($userId, $forumId, $paginatedTopicsId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT v, partial lvp.{id, number}, partial t.{id}
                      FROM UserBundle:Visited v
                      JOIN v.lastVisitedPost lvp
                      JOIN lvp.topic t
                     WHERE v.user = :userId
                       AND t.forum = :forumId
                       AND t.id IN (:paginatedTopicsId)
                       AND v.lastVisit > :lastMonth
                       ');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId' => $userId,
            'lastMonth' => new \DateTime('now - 31 days'),
            'forumId' => $forumId,
            'paginatedTopicsId' =>$paginatedTopicsId,

        ));

        // Retorno el resultado de la consulta
        return $query->getArrayResult();
    }

    /*
     * Devuelve los temas/topics marcados como favoritos por el usuario
     *
     * @param integer $userId Id del usuario
     *
     **/
    public function findUserFavoriteTopics($userId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT fav, lvp, t, ltp, f
                      FROM UserBundle:Visited fav
                      JOIN fav.lastVisitedPost lvp
                      JOIN lvp.topic t
                      JOIN t.lastPost ltp
                      JOIN t.forum f
                     WHERE fav.user = :userId
                       AND fav.isFavorite = 1
                  ORDER BY ltp.postingDate DESC');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId' => $userId,


        ));

        // Retorno la consulta
        return $query;
    }

    /*
     * Devuelve el número de temas favoritos que tiene el usuario
     *
     * @param string $userId nombre del usuario del cual se quiere obtener el número temas/topics favoritos
     **/
    public function findNumberOfFavoriteTopics($userId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT COUNT(vf)
                      FROM UserBundle:Visited vf
                     WHERE vf.user = :userId
                       AND vf.isFavorite = 1');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId' => $userId
        ));

        // Limito los resultados
        $query->setMaxResults(1);

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }

    /*
     * Devuelve un escalar que indicara si ha encontrado el tema marcado como favorito o no
     *
     * @param string $topicId Id del tema visitado
     * @param string $userId Id del usuario que visita el tema/topic
     **/
    public function findIfTopicIsFavorite($topicId, $userId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT COUNT(vf)
                      FROM UserBundle:Visited vf
                      JOIN vf.lastVisitedPost lvp
                      JOIN lvp.topic t
                     WHERE vf.user = :userId
                       AND t.id = :topicId
                       AND vf.isFavorite = 1');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId' => $userId,
            'topicId'=> $topicId,
        ));

        // Limito los resultados
        $query->setMaxResults(1);

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }

    /*
     * Devuelve el número de temas favoritos que tiene el usuario con mensajes sin leer
     *
     * @param string $userId nombre del usuario del cual se quiere obtener el número temas/topics favoritos sin leer
     **/
    public function findNumberOfUnreadFavorites($userId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT COUNT(vf)
                      FROM UserBundle:Visited vf
                      JOIN vf.lastVisitedPost lvp
                      JOIN lvp.topic t
                      JOIN t.lastPost lp
                     WHERE vf.user = :userId
                       AND vf.isFavorite = 1
                       AND lp.number > lvp.number');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId' => $userId
        ));

        // Limito los resultados
        $query->setMaxResults(1);

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }
}