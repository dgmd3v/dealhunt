<?php

namespace Deal\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;

/*
 * Clase que sirve como repositorio para las consultas relacionadas con las notificaciones de moderación
 * */
class ModerationNotificationRepository extends EntityRepository
{
    /*
     * Devuelve el número de notificaciones por posts/mensajes moderados que tiene el usuario
     *
     * @param string $userId nombre del usuario del cual se quiere obtener el número notificaciones
     **/
    public function findNumberOfModerationNotifications($userId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT COUNT(mn)
                      FROM UserBundle:ModerationNotification mn
                     WHERE mn.user = :userId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId' => $userId
        ));

        // Limito los resultados
        $query->setMaxResults(1);

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }

    /*
     * Devuelve el número de notificaciones por posts/mensajes moderados que tiene el usuario sin leer
     *
     * @param string $userId nombre del usuario del cual se quiere obtener el número notificaciones sin leer
     **/
    public function findNumberOfUnNotifiedModeration($userId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT COUNT(mn)
                      FROM UserBundle:ModerationNotification mn
                     WHERE mn.user = :userId
                       AND mn.isNotified = 0');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId' => $userId
        ));

        // Limito los resultados
        $query->setMaxResults(1);

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }

    /*
     * Devuelve la consulta que selecciona las notificaciones por moderación que tiene un usuario
     *
     * @param integer $userId Id del usuario del cual queremos buscar las notificaciones
     **/
    public function findUserModerationNotifications($userId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT mn, p, t
                      FROM UserBundle:ModerationNotification mn
                      JOIN mn.post p
                      JOIN p.topic t
                     WHERE mn.user = :userId
                     ORDER BY mn.date DESC');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId' => $userId,
        ));

        // Retorno la consulta
        return $query;
    }

    /*
     * Actualiza las notificaciones de un usuario marcándolas como notificadas
     *
     * @param integer $userId Id del usuario al cual le estamos actualizando el estado de las notificaciones
     **/
    public function updateNotifiedStatus($userId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        $dql = ('   UPDATE UserBundle:ModerationNotification mn
                       SET mn.isNotified = 1
                     WHERE mn.user = :userId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId' => $userId,
        ));

        // Retorno el resultado de la consulta
        return $query->getResult();
    }

}