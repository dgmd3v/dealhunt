<?php

namespace Deal\UserBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;

/*
 * Clase de configuración para la administración de las firmas en el panel de administración
 * */
class SignatureAdmin extends Admin {

    // Campos que se van a mostrar en los formularios crear/editar entidades
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('signedUser', null, array( 'label' => 'Usuario firmado'))
            ->add('signatureUser', null, array( 'label' => 'Usuario firmante'))
            ->add('signatureIp', 'text', array('label' => 'Ip'))
            ->add('signatureDate', 'datetime', array('label' => 'Fecha'))
            ->add('signature', 'textarea', array('label' => 'Firma'))
        ;
    }

    // Campos a mostrar en los formularios para filtrar entidades
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, array('label' => 'Id'))
            ->add('signedUser', null, array('label' => 'Usuario firmado'))
            ->add('signatureUser', null, array('label' => 'Usuario firmante'))
            ->add('signatureIp', null, array('label' => 'Ip'))
        ;
    }

    // Campos para ser mostrados en los listados de entidades
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, array('label' => 'Id'))
            ->addIdentifier('signedUser', null, array(  'sortable'=>true,
                                        'sort_field_mapping'=> array('fieldName'=>'id'),
                                        'sort_parent_association_mappings' => array(array('fieldName'=>'signedUser')),
                                        'label' => 'Usuario firmado'))
            ->add('signatureUser', null, array('sortable'=>true,
                                    'sort_field_mapping'=> array('fieldName'=>'id'),
                                    'sort_parent_association_mappings' => array(array('fieldName'=>'signatureUser')),
                                    'label' => 'Usuario firmante'))
            ->add('signatureIp', 'text', array('label' => 'Ip'))
            ->add('signatureDate', 'datetime', array('label' => 'Fecha'))
            ->add('signature', 'text', array('label' => 'Firma'))
        ;
    }

    // Si creo una firma lo debo notificar
    public function postPersist($object) {
        $container = $this->getConfigurationPool()->getContainer();

        $container->get('deal.userbundle.service.signaturenotification')->newSignatureNotification($object);
        $container->get('doctrine.orm.entity_manager')->flush();
    }
} 