<?php

namespace Deal\UserBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;

/*
 * Clase de configuración para la administración de los perfiles de los usuarios en el panel de administración
 * */
class UserProfileAdmin extends Admin {

    // Campos que se van a mostrar en los formularios crear/editar entidades
    protected function configureFormFields(FormMapper $formMapper)
    {
        $actualDate = new \DateTime('now - 1 year');
        $maxYear = $actualDate->format("Y");

        $formMapper
            ->with('Datos Obligatorios')
            ->add('regDate', 'datetime', array('label' => 'Fecha de registro'))

            ->add('posts', 'number', array(
                'label' => 'Mensajes',
                'read_only' => true,
                'disabled'  => true,
            ))
            ->add('dealsPositive', 'number', array('label' => 'Positivos recibidos'))
            ->add('dealsNegative', 'number', array('label' => 'Negativos recibidos'))
            ->add('topicsStarted', 'number', array(
                    'label' => 'Temas creados',
                    'read_only' => true,
                    'disabled'  => true,
            ))
            ->end()
            ->with('Datos No Obligatorios')
            ->add('location', 'text', array('required'=> false, 'label' => 'Población'))
            ->add('userCustomInfo', 'textarea', array('required'=> false, 'label' => 'Información del usuario'))
            ->add('birthday', 'datetime', array('required'=> false,
                'years' => range(1900, $maxYear),
                'label' => 'Fecha de nacimiento'))
            ->end()
        ;
    }

    // Campos a mostrar en los formularios para filtrar entidades
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, array('label' => 'Id'))
        ;
    }

    // Campos para ser mostrados en los listados de entidades
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, array('label' => 'Id'))
            ->add('location', 'text', array('label' => 'Población'))
            ->add('userCustomInfo', 'textarea', array('label' => 'Información del usuario'))
            ->add('regDate', 'datetime', array('label' => 'Fecha de registro'))
            ->add('birthday', 'datetime', array('label' => 'Fecha de nacimiento'))
            ->add('posts', 'number', array('label' => 'Mensajes'))
            ->add('dealsPositive', 'number', array('label' => 'Positivos recibidos'))
            ->add('dealsNegative', 'number', array('label' => 'Negativos recibidos'))
            ->add('topicsStarted', 'number', array('label' => 'Temas creados'))
        ;
    }

    // Evito que se pueda borrar y crear perfiles de usuario
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('create');
    }
} 