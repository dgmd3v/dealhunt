<?php

namespace Deal\UserBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;

/*
 * Clase de configuración para la administración de los usuarios en el panel de administración
 * */
class UserAdmin extends Admin {

    protected $formOptions = array(
        'validation_groups' => array('MyProfile', 'Default')
    );

    // Campos que se van a mostrar en los formularios crear/editar entidades
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Datos Obligatorios')
            ->add('username', null, array(
                'label' => 'Nombre',
                'read_only' => true,
                'disabled'  => true,
            ))
            ->add('email', 'email', array('label' => 'E-mail'))
            ->add('roles', 'choice',
                array('choices'=> array('ROLE_ADMIN' => 'ROLE_ADMIN',
                                        'ROLE_MODERATOR' => 'ROLE_MODERATOR',
                                        'ROLE_USER' => 'ROLE_USER'),
                    'expanded'=> true,
                    'multiple'=> true))
            ->end()
            ->with('Datos No Obligatorios')
            ->add('enabled', 'checkbox', array('required'=> false, 'label' => '¿Activado?'))
            ->add('last_login', 'datetime', array('required'=> false, 'label' => 'Último login'))
            ->add('avatarFile', 'file', array('required'=> false, 'label' => 'Avatar'))
            ->end()
        ;

        $subject = $this->getSubject();

        // Si estoy creando el usuario muestro el campo de contraseña
        if(!$subject->getId() > 0) {
            $this->formOptions = array('validation_groups' => array('MyRegistration', 'Default'));

            $formMapper
                ->with('Datos Obligatorios')
                ->add('username', null, array(
                    'label' => 'Nombre',
                ))
                ->add('plainPassword', 'repeated', array(
                    'type' => 'password',
                    'options' => array('translation_domain' => 'FOSUserBundle'),
                    'first_options' => array('label' => 'form.password'),
                    'second_options' => array('label' => 'form.password_confirmation'),
                    'invalid_message' => 'fos_user.password.mismatch',
                ))
                ->end()
            ;
        }
    }

    // Campos a mostrar en los formularios para filtrar entidades
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, array('label' => 'Id'))
            ->add('username', null, array('label' => 'Nombre'))
            ->add('email', null, array('label' => 'E-mail'))
            ->add('enabled', null, array('label' => '¿Activado?'))
            ->add('locked', null, array('label' => '¿Baneado?'))
            ->add('roles', null, array('label' => 'Roles'))
        ;
    }

    // Campos para ser mostrados en los listados de entidades
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, array('label' => 'Id'))
            ->addIdentifier('username', null, array('label' => 'Nombre'))
            ->add('email', 'text', array('label' => 'E-mail'))
            ->add('enabled', 'boolean', array('label' => '¿Activado?'))
            ->add('lastLogin', 'datetime', array('label' => 'Último login'))
            ->add('lastActivity', 'datetime', array('label' => 'Última actividad'))
            ->add('locked', 'boolean', array('label' => '¿Baneado'))
            ->add('roles', 'array', array('label' => 'Roles'))
            ->add('avatarName', 'text', array('label' => 'Nombre avatar'))
            ->add('userProfile', null, array(  'sortable'=>true,
                'sort_field_mapping'=> array('fieldName'=>'id'),
                'sort_parent_association_mappings' => array(array('fieldName'=>'userProfile')),
                'class' => 'Deal\UserBundle\Entity\UserProfile',
                'label' => 'Perfil'))
        ;
    }

    // Evito que se puedan borrar los usuarios
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
    }

} 