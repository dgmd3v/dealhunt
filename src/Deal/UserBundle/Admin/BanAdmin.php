<?php

namespace Deal\UserBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;

/*
 * Clase de configuración para la administración de los bans en el panel de administración
 * */
class BanAdmin extends Admin {

    // Campos que se van a mostrar en los formularios crear/editar entidades
    protected function configureFormFields(FormMapper $formMapper)
    {
        $actualDate = new \DateTime('now + 100 year');
        $maxYear = $actualDate->format("Y");

        $formMapper
            ->add('user', null, array('class' => 'Deal\UserBundle\Entity\User','label' => 'Usuario a banear'))
            ->add('finishingDate', 'datetime', array('years' => range(2014, $maxYear), 'label' => 'Finaliza'))
            ->add('reason', 'text', array('label' => 'Motivo'))
        ;
    }

    // Campos a mostrar en los formularios para filtrar entidades
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, array( 'label' => 'Id'))
            ->add('user', null, array('label' => 'Usuario baneado'))
        ;
    }

    // Campos para ser mostrados en los listados de entidades
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, array( 'label' => 'Id'))
            ->add('user', null, array(  'sortable'=>true,
                                            'sort_field_mapping'=> array('fieldName'=>'id'),
                                            'sort_parent_association_mappings' => array(array('fieldName'=>'user')),
                                            'class' => 'Deal\UserBundle\Entity\User',
                                            'label' => 'Usuario baneado'))
            ->add('finishingDate', 'datetime', array('label' => 'Finaliza'))
            ->add('reason', 'text', array('label' => 'Motivo'))
        ;
    }

    // Después del persist marco al usuario como locked/bloqueado
    public function prePersist($object) {
        $userBan = $this->getSubject()->getUser();
        $userBan->setLocked(true);
    }

    // Después de borrar el ban desmarco al usuario como locked/bloqueado
    public function preRemove($object) {
        $userBan = $object->getUser();
        $userBan->setLocked(false);
    }

} 