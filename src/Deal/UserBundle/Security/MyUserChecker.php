<?php

namespace Deal\UserBundle\Security;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserChecker;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;

/*
 * Sobrescribo la clase UserChecker (y el servicio security.user_checker) de symfony para quitar un ban a un usuario
 * si ya ha pasado el tiempo de baneo establecido.
 **/
class MyUserChecker extends UserChecker {

    private $entityManager;
    private $session;

    /**
     * Constructor que recoge los argumentos pasado por Symfony2 al crear el servicio
     *
     * @param EntityManager $entityManager
     * @param Session $session
     */
    public function __construct(EntityManager $entityManager,  Session $session)
    {
        $this->entityManager = $entityManager;
        $this->session = $session;
    }

    public function checkPreAuth(UserInterface $user)
    {
        // Si está baneado compruebo que la fecha actual no sea superior a la fecha del fin del baneo, en caso contrario
        // le quito el baneo
        if(!$user->isAccountNonLocked()){

            $userBan = $this->entityManager->getRepository('UserBundle:Ban')->findBanByUserId($user->getId());

            $userBanFinishingDate = $userBan->getFinishingDate();

            $actualDateTime = new \DateTime('now');

            if($userBanFinishingDate < $actualDateTime ){
                $user->setLocked(false);

                // Persisto el usuario y borro el ban
                $this->entityManager->persist($user);
                $this->entityManager->remove($userBan);

                // Guardo los cambios en la base de datos
                $this->entityManager->flush();
            }

            else{
                // Mensaje flashbag informando de que el registro se ha realizado
                $this->session->getFlashBag()->add('error',
                    'Has sido baneado hasta el ' . $userBanFinishingDate->format('d-m-Y H:i:s') . '. Motivo: '
                    . $userBan->getReason()
                );
            }
        }
        parent::checkPreAuth($user);
    }

    public function checkPostAuth(UserInterface $user)
    {
        parent::checkPostAuth($user);
    }
}

