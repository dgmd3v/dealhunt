<?php

namespace Deal\UserBundle\Security;

use Deal\ForumBundle\Security\AbstractVoter\AbstractVoter;
use Symfony\Component\Security\Core\User\UserInterface;

/*
 * Clase encargada del BanUserVoter que se ocupa dar o denegar el acceso al recurso (usuarios, en este se usará para
 * banear a otros usuarios)
 * */
class BanUserVoter extends AbstractVoter {

    /**
     * Return an array of supported classes. This will be called by supportsClass
     *
     * @return array an array of supported classes, i.e. array('Acme\DemoBundle\Model\Product')
     */
    protected function getSupportedClasses()
    {
        // Para este voter la clase soportada sera el foro/forum
        return array('Deal\UserBundle\Entity\User');
    }

    /**
     * Return an array of supported attributes. This will be called by supportsAttribute
     *
     * @return array an array of supported attributes, i.e. array('CREATE', 'READ')
     */
    protected function getSupportedAttributes()
    {
        // Se llamara a este voter cuando se usen los siguiente atributos
        return array('ROLE_CAN_BAN_USER');
    }

    /**
     * Perform a single access check operation on a given attribute, object and (optionally) user
     * It is safe to assume that $attribute and $object's class pass supportsAttribute/supportsClass
     * $user can be one of the following:
     *   a UserInterface object (fully authenticated user)
     *   a string               (anonymously authenticated user)
     *
     * @param string $attribute
     * @param object $object
     * @param UserInterface|string $user
     *
     * @return bool
     */
    protected function isGranted($attribute, $object, $user = null)
    {
        // Si no está logueado no le doy permiso
        if(!is_object($user) ){
            return false;
        }

        // Roles del usuario que solicita permiso y del usuario sobre el que se va a tomar la decisión
        $userRole = $user->getHighestRole();
        $objectRole = $object->getHighestRole();

        // Un admin podra banear a moderadores y usuarios y un moderador a usuarios
        switch($userRole){
            case 'ROLE_MODERATOR':
                if($objectRole == 'ROLE_USER'){
                    return true;
                }

                else{
                    return false;
                }
                break;

            case 'ROLE_ADMIN':
                if($objectRole == 'ROLE_USER' || $objectRole == 'ROLE_MODERATOR'){
                    return true;
                }

                else{
                    return false;
                }
                break;

            default:
                return false;
                break;
        }

        return false;
    }
}