<?php

namespace Deal\UserBundle\Security;

use Deal\ForumBundle\Security\AbstractVoter\AbstractVoter;
use Symfony\Component\Security\Core\User\UserInterface;

/*
 * Clase encargada del DeleteSignatureVoter que se ocupa dar o denegar el acceso al recurso (borrar firmas)
 * */
class DeleteSignatureVoter extends AbstractVoter {

    /**
     * Return an array of supported classes. This will be called by supportsClass
     *
     * @return array an array of supported classes, i.e. array('Acme\DemoBundle\Model\Product')
     */
    protected function getSupportedClasses()
    {
        // Para este voter la clase soportada sera el foro/forum
        return array('Deal\UserBundle\Entity\Signature');
    }

    /**
     * Return an array of supported attributes. This will be called by supportsAttribute
     *
     * @return array an array of supported attributes, i.e. array('CREATE', 'READ')
     */
    protected function getSupportedAttributes()
    {
        // Se llamara a este voter cuando se usen los siguiente atributos
        return array('ROLE_DELETE_SIGNATURE');
    }

    /**
     * Perform a single access check operation on a given attribute, object and (optionally) user
     * It is safe to assume that $attribute and $object's class pass supportsAttribute/supportsClass
     * $user can be one of the following:
     *   a UserInterface object (fully authenticated user)
     *   a string               (anonymously authenticated user)
     *
     * @param string $attribute
     * @param object $object
     * @param UserInterface|string $user
     *
     * @return bool
     */
    protected function isGranted($attribute, $object, $user = null)
    {
        // Si no está logueado no le doy permiso
        if(!is_object($user) ){
            return false;
        }

        $userRole = $user->getHighestRole();

        // Si compruebo para los usuarios logueados si es el destinatario de la firma o si es un moderador/administrador
        if( ($object->getSignedUser()->getId()==$user->getId())||
            ($userRole=='ROLE_MODERATOR')||
            ($userRole=='ROLE_ADMIN')){
            return true;
        }

        return false;
    }
}