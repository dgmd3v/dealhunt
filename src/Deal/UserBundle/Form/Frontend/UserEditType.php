<?php

namespace Deal\UserBundle\Form\Frontend;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Deal\UserBundle\Form\Frontend\UserRegisterType;

/*
 * Formulario para editar el perfil del usuario y hacer cambios en su información.
 **/
class UserEditType extends UserRegisterType
{
    /**
     * Esta función elimina del formulario de edición el campo de nombre dado que no queremos que se pueda cambiar
    **/
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->remove('username',   'text')
            ->remove('captcha', 'genemu_recaptcha')
            ->add('avatarFile', 'file', array(
                'required' => false,
                'label'=>'Avatar',
                'render_optional_text' => false
            ))
            ->add('userProfile', new UserProfileType());
    }

    /*
     * Esta función indica que grupo de validación debe usar este formulario, en este caso el default
     * (cascade_validation para que valide las sub-entidades, en este caso UserProfile)
    **/
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(   'data_class'        => 'Deal\UserBundle\Entity\User',
                                        'validation_groups' => array('Default', 'MyProfile'),
                                        'cascade_validation' => true
            )
        );
    }

    public function getName()
    {
        return 'deal_userbundle_useredittype';
    }
}