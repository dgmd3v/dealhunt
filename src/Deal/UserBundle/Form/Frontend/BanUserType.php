<?php

namespace Deal\UserBundle\Form\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/*
 * Formulario para banear a un usuario
 **/
class BanUserType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('finishingDate', 'date', array(
                'label' => 'Finalización del ban',
                'empty_value' => array('year' => 'Año', 'month' => 'Mes', 'day' => 'Día'),
                'years' => range(date('Y'), date('Y') + 120),
            ))
            ->add('reason', 'text', array(
                'label' => 'Motivo del Ban',
                'help_block' => 'El motivo del ban no puede exceder los 250 caracteres ',
            ))
        ;
    }

    /*
     * Esta función indica que grupo de validación debe usar este formulario, en este caso el default
     **/
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class'        => 'Deal\UserBundle\Entity\Ban',
                'validation_groups' => array('Default')
            )
        );
    }

    public function getName()
    {
        return 'deal_userbundle_banusertype';
    }

} 