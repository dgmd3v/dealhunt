<?php

namespace Deal\UserBundle\Form\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/*
 * Formulario de registro de Users/Usuarios
 **/
class UserRegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(  'username','text', array(
                    'label' => 'Nombre',
                    'attr'  => array( 'placeholder' => "Escribe tu nombre de usuario (solo letras, números y -_ )"),

            ))
            ->add(  'email', 'repeated', array(
                    'type'              => 'email',
                    'invalid_message'   => 'Los dos e-mail deben coincidir',
                    'first_options'     => array(   'label' => 'E-mail',
                                                    'attr' =>
                                                        array(
                                                           'placeholder' => "Escribe tu dirección de correo electrónico"
                                                )),
                    'second_options'    => array('label' => 'Confirma E-mail', 'attr' => array('placeholder' => "Vuelve a escribir tu dirección de correo electrónico"))
            ))
            ->add(  'plainPassword', 'repeated', array(
                    'type'=>'password',
                    'invalid_message'   => 'Las dos contraseñas deben coincidir',
                    'first_options'     => array(   'label' => 'Contraseña',
                                                    'attr' => array('placeholder' => "Escribe tu contraseña")),
                    'second_options'    => array(  'label' => 'Confirma Contraseña',
                                                   'attr' => array('placeholder' => "Vuelve a escribir tu contraseña")),
            ))
            ->add('captcha', 'genemu_recaptcha', array(
                'mapped' => false,
                'configs' => array(
                    'theme' => 'white',
                ),
                'invalid_message'   => 'Captcha erróneo',
            ));
    }

    /*
     * Esta función indica el namespace de la entidad que vamos a modificar con este formulario. Además indica
     * que grupo de validación debe usar este formulario, en este caso el default y registration
     **/
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(   'data_class'        => 'Deal\UserBundle\Entity\User',
                                        'validation_groups' => array('Default', 'MyRegistration')
            )
        );
    }

    public function getName()
    {
        return 'deal_userbundle_userregistertype';
    }
} 