<?php

namespace Deal\UserBundle\Form\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/*
 * Formulario para firmar a otro usuario
 **/
class SignatureType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('signature', 'textarea', array(
                'label' => false,
                'horizontal_input_wrapper_class' => 'col-sm-12',
                'attr'  => array( 'placeholder' => "Escribe aquí tu firma (no puede exceder los 400 caracteres)"),
            ))
        ;
    }

    /*
     * Esta función indica que grupo de validación debe usar este formulario, en este caso el default
     **/
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class'        => 'Deal\UserBundle\Entity\Signature',
                'validation_groups' => array('Default')
            )
        );
    }

    public function getName()
    {
        return 'deal_userbundle_signaturetype';
    }

} 