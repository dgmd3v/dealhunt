<?php

namespace Deal\UserBundle\Form\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/*
 * Formulario para editar algunos de los campos extras añadidos al usuario de FOSUserBundle
 **/
class UserProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('location', 'text', array(
                'label' => 'Población',
                'attr'  => array('placeholder' => 'Escribe la población donde vives')
            ))
            ->add(  'birthday', 'birthday', array(
                'widget'        => 'choice',
                'label'         =>'Fecha de nacimiento',
                'empty_value'   => array('year' => 'Año', 'month' => 'Mes', 'day' => 'Día'),
                'required'      => false,
                'render_optional_text' => false,
            ))
            ->add(  'userCustomInfo', 'textarea', array(
                'label' => 'Información del usuario',
                'attr'  => array( 'placeholder' => "Escribe aquí información sobre ti, tus gustos, el tiempo en tu ciudad, lo que quieras!!!", 'rows' => 7),
                'required'      => false,
                'render_optional_text' => false
            ));
    }

    /*
     * Esta función indica que grupo de validación debe usar este formulario, en este caso el default
    **/
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(   'data_class'        => 'Deal\UserBundle\Entity\UserProfile',
                                        'validation_groups' => array('Default')
            )
        );
    }

    public function getName()
    {
        return 'deal_userbundle_userprofiletype';
    }
}