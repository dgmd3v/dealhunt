/*
 * Función destinada a ejecutar la acción del controlador encargada de des-banear un usuario y mostrar un mensaje
 * de información
 * */
$(document).on('click', '.ajaxUnBanUser',
    function() {
        var dialogModalAcceptButton = $("#dialogModalAcceptButton");
        var path = dialogModalAcceptButton.attr("href");
        var postNumber = dialogModalAcceptButton.attr("data-postnumber");
        var userId= dialogModalAcceptButton.attr("data-userid");
        var userName= dialogModalAcceptButton.attr("data-username");
        var env = dialogModalAcceptButton.attr("data-environment");

        var resultModal = $('#resultModal');

        $.ajax({
            type: "POST",
            url: path,
            data: {  userId: userId, userName: userName, postNumber: postNumber },
            dataType: "json",
            success: function(response) {
                if (response.responseCode == 200) {

                    resultModal.find('.modal-body').text(response.successMsg);
                    resultModal.find('.modal-title').text('Felicidades!!!');
                    resultModal.modal();

                    if(response.postNumber == '1'){
                        var postDiv = $('.first-post');
                    }

                    else{
                        var postDiv = $('#post-' + response.postNumber);
                    }

                    postDiv.find(".poster a").removeClass('banned');
                    postDiv.find(".poster img").removeClass('hidden');

                    postDiv.find(".date-avatar-mini a").removeClass('banned');
                    postDiv.find(".date-avatar-mini img").removeClass('hidden');

                    var unBanButton = postDiv.find('.ban-unban-button');
                    unBanButton.attr({
                        "data-title": "Banear Usario",
                        "data-path": path.replace('unBanUser', 'banUser'),
                        "data-target": "#dialogBanModal",
                        "data-type": ""
                    });

                    unBanButton.find('span').text(' Banear');
                    unBanButton.find('span').attr("title", "Banear Usario");
                    unBanButton.find('span').attr("class", "glyphicon glyphicon-ban-circle")
                }

                else if (response.responseCode == 400) {
                    resultModal.find('.modal-title').text("Error");
                    resultModal.find('.modal-body').text("Error: " + response.errorMsg);
                    resultModal.modal();
                }
            }
        });
    }
);