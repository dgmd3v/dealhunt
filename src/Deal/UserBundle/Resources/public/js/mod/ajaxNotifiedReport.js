/*
* Función destinada a ejecutar la acción del controlador encargada marcar como leído/no leído un reporte
* de información
* */
$(document).on('click', '.notified_unnotified_button',
    function() {
                var notButton = $(this);
                var userId = notButton.attr("data-userid");
                var postId = notButton.attr("data-postid");
                var path = notButton.attr("data-path");
                var markAs = notButton.attr("data-markas");

                $.ajax({
                    type: "POST",
                    url: path,
                    data: {userId: userId, postId: postId, markAs: markAs },
                    dataType: "json",
                    success: function(response) {
                        if (response.responseCode == 200) {

                            var notButtonSpan = notButton.find('span');

                            if(markAs == 'notified'){
                                notButton.attr("title", "Desmarcar como leido este reporte");
                                notButtonSpan.attr("class", "glyphicon glyphicon-ok");
                                notButton.attr({
                                    "data-markas": "unnotified"
                                });
                            }

                            else if(markAs == 'unnotified'){
                                notButton.attr("title", "Marcar como leido este reporte");
                                notButtonSpan.attr("class", "glyphicon glyphicon-remove");
                                notButton.attr({
                                    "data-markas": "notified"
                                });
                            }
                        }

                        else if (response.responseCode == 400) {
                            var resultModal = $('#resultModal');
                            resultModal.find('.modal-title').text("Error");
                            resultModal.find('.modal-body').text("Error: " + response.errorMsg);
                            resultModal.modal();
                        }
                    }
                });
            }
    );