/*
 * Función destinada a ejecutar la acción del controlador encargada de banear un usuario y mostrar un mensaje
 * de información
 * */
$("#dialogBanModal").on('submit',
    function(event) {

        var form = $("#dialogBanModal form");
        var path = form.attr("action");
        var dialogBanModal = $("#dialogBanModal");
        var userId = dialogBanModal.attr("data-userid");
        var userName = dialogBanModal.attr("data-username");
        var postNumber = dialogBanModal.attr("data-postnumber");
        var env = dialogBanModal.attr("data-environment");

        var resultModal = $('#resultModal');

        $.ajax({
            type: "POST",
            url: path,
            data: {  form: form.serialize(), userId: userId, postNumber: postNumber },
            dataType: "json",
            success: function(response) {
                if (response.responseCode == 200) {

                    dialogBanModal.modal('hide');

                    resultModal.find('.modal-body').text(response.successMsg);
                    resultModal.find('.modal-title').text('Felicidades!!!');
                    resultModal.modal();

                    if(response.postNumber == '1'){
                        var postDiv = $('.first-post');
                    }

                    else{
                        var postDiv = $('#post-' + response.postNumber);
                    }

                    postDiv.find(".poster a").addClass('banned');
                    postDiv.find(".poster img").addClass('hidden');

                    postDiv.find(".date-avatar-mini a").addClass('banned');
                    postDiv.find(".date-avatar-mini img").addClass('hidden');

                    var unBanButton = postDiv.find('.ban-unban-button');
                    unBanButton.attr({
                        "data-title": "Des-Banear Usario",
                        "data-path": path.replace('banUser', 'unBanUser'),
                        "data-target": "#dialogModal",
                        "data-type": "ajaxUnBanUser",
                        "data-message":"¿Está segur@ de que desea desbanear al usuario " + userName + "?"
                    });

                    unBanButton.find('span').text(' Des-Banear');
                    unBanButton.find('span').attr("title", "Des-Banear Usuario");
                    unBanButton.find('span').attr("class", "glyphicon glyphicon-ok-circle")
                }

                else if (response.responseCode == 400) {
                    dialogBanModal.modal('hide');

                    resultModal.find('.modal-title').text("Error");
                    resultModal.find('.modal-body').text("Error: " + response.errorMsg);
                    resultModal.modal();
                }
            }
        });

        event.preventDefault();
    }
);