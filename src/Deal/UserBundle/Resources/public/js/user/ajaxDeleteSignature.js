/*
 * Función destinada a ejecutar la acción del controlador encargada de borrar una firma
 * */
$(document).on('click', '.ajaxDeleteSignature',
    function() {
        var dialogModalAcceptButton = $("#dialogModalAcceptButton");
        var path = dialogModalAcceptButton.attr("href");
        var signatureId = dialogModalAcceptButton.attr("data-signatureid");

        var resultModal = $('#resultModal');

        $.ajax({
            type: "POST",
            url: path,
            data: {signatureId: signatureId },
            dataType: "json",
            success: function(response) {
                if (response.responseCode == 200) {

                    resultModal.find('.modal-body').text(response.successMsg);
                    resultModal.find('.modal-title').text('Felicidades!!!');
                    resultModal.modal();

                    var signatureDiv = $('#signature-' + signatureId);
                    signatureDiv.remove();
                }

                else if (response.responseCode == 400) {
                    resultModal.find('.modal-title').text("Error");
                    resultModal.find('.modal-body').text("Error: " + response.errorMsg);
                    resultModal.modal();
                    $('#dialogModal').removeClass('ajaxDeleteSignature');
                }
            }
        });
    }
);