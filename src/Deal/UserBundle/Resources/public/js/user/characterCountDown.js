/*
 * Función/plugin destinada contar los caracteres restantes que se pueden introducir en un textarea
 * */
(function( $ ){
    $.fn.characterCountDown = function(options) {

        // Seteo las opciones
        var settings = $.extend({
            maxChar: 400
        }, options );


        $(this).keyup(function(){
            var charLeft = settings.maxChar - $(this).val().length;

            if(charLeft < 0){
                $("#char_left").html('<span class="text-danger">Te estás pasando ' + (charLeft*-1) + ' caracteres!!!</span>' );
            }

            else{
                $("#char_left").html(charLeft);
            }
        });
    };
})( jQuery );