/*
 * Función destinada a avisar con un signo de exclamación por si el usuario tuviese notificaciones pendientes
 * */
$(document).ready(function () {
    if ( $( ".new-notifications" ).length ) {
        $( "#bars" ).empty().append('<span class="new-notifications-collapsed glyphicon glyphicon-exclamation-sign"><span>');
    }
});