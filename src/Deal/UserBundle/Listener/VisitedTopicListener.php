<?php

namespace Deal\UserBundle\Listener;


use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\DependencyInjection\Container;
use Deal\UserBundle\Entity\Visited;
use Deal\UserBundle\Entity\User;
use Deal\TopicBundle\Entity\Topic;

/**
 * Clase encargada de guardar las páginas visitadas por el usuario
 */
class VisitedTopicListener
{
    private $container;
    private $context;
    private $routesToCheck;


    /**
     * Constructor que recoge los argumentos pasado por Symfony2 al crear el servicio
     *
     * @param EntityManager $entityManager
     * @param SecurityContext $context
     */
    public function __construct(Container $container,  SecurityContext $context)
    {
        $this->container = $container;
        $this->context = $context;

        // Rutas a chequear
        $this->routesToCheck = array(   'topic_show_topic',
            '_topic_show_topic',
            'topic_show_topic_page',
            '_topic_show_topic_page'
        );
    }

    /**
     * En esta función actualizo o creo una visita a una página
     *
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        // Ignoro las SUB_REQUESTS
        if ($event->getRequestType() !== HttpKernel::MASTER_REQUEST) {
            return;
        }

        $user = $this->context->getToken()->getUser();

        // Compruebo que $user pertenece a la clase User
        if( ($user instanceof User) )
        {
            $request = $event->getRequest();

            // Extraigo de la petición la ruta que se está intentando visitar
            $visitedRoute  = $request->attributes->get('_route');

            // Si está dentro de las rutas pertenecientes a la visita de temas/topics la guardo o la actualizo si
            // hiciese falta
            if(in_array($visitedRoute, $this->routesToCheck)){
                // Extraigo los parámetros de la ruta
                $routeParams = $request->attributes->get('_route_params');
                $topicId = $routeParams['topicId'];

                // EntityManager
                $entityManager = $this->container->get('doctrine.orm.entity_manager');

                // Busco si el usuario ha visitado el tema
                $topicExists = $entityManager->getRepository('TopicBundle:Topic')->findOnlyTopicById($topicId);

                // Compruebo primero que el tema existe
                if($topicExists != null){
                    $topicPageVisited = 1;

                    if(array_key_exists('pageNumber', $routeParams)){
                        $topicPageVisited = $routeParams['pageNumber'];
                    }

                    // Busco si el usuario ha visitado el tema
                    $visitedTopic = $entityManager->getRepository('UserBundle:Visited')
                                                                           ->findOneTopicVisitedByUser($topicId, $user);

                    // Si ya ha visitado el tema en alguna ocasión
                    if($visitedTopic){

                        // Tema ya visitado
                        $topic = $visitedTopic->getLastVisitedPost()->getTopic();

                        $lastViewedPostNumber = $visitedTopic->getLastVisitedPost()->getNumber();
                        $lastTopicPostNumber = $topic->getLastPost()->getNumber();

                        $globalVariables = $entityManager->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
                        $maxPostsPerTopicPage = $globalVariables->getMaxPostsPerTopicPage();

                        // Si el número del último post visto por el usuario es menor que el número del último post del
                        // tema es posible que tenga que actualizar los datos
                        if($lastViewedPostNumber < $lastTopicPostNumber){

                            $topicPosts = $topic->getLastPost()->getNumber();
                            $topicPages = ceil($topicPosts/$maxPostsPerTopicPage);

                            // Si la página que está visitando el usuario es igual a las páginas del tema debo
                            // actualizar
                            if($topicPageVisited == $topicPages){
                                $visitedTopic->setLastVisitedPost($topic->getLastPost());
                                $visitedTopic->setLastVisit(new \DateTime('now'));

                                $entityManager->persist($visitedTopic);
                                $entityManager->flush();
                            }

                            // Si la página que está visitando el usuario es menor que las páginas totales del topic
                            // debo comprobar si tengo que actualizar la visita
                            elseif($topicPageVisited < $topicPages){
                                // Si el número del último post de la página visitada ($pageLastPostNumber) es mayor que
                                // el actual de post guardado en la visita, debo actualizar
                                $pageLastPostNumber = ($topicPageVisited*$maxPostsPerTopicPage)+1;

                                if($pageLastPostNumber > $lastViewedPostNumber){

                                    $lastViewedPost = $entityManager->getRepository('PostBundle:Post')
                                                                    ->findPostByNumber($topicId, $pageLastPostNumber);

                                    $visitedTopic->setLastVisitedPost($lastViewedPost);
                                    $visitedTopic->setLastVisit(new \DateTime('now'));

                                    $entityManager->persist($visitedTopic);
                                    $entityManager->flush();
                                }
                            }
                        }
                    }

                    // Si no ha sido visitado nunca por el usuario creo una nueva visita
                    if(!$visitedTopic){

                        $globalVariables = $entityManager->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
                        $maxPostsPerTopicPage = $globalVariables->getMaxPostsPerTopicPage();

                        $topic = $entityManager->getRepository('TopicBundle:Topic')->findOnlyTopicAndLastPostById($topicId);

                        $topicPosts = $topic->getLastPost()->getNumber();
                        $topicPages = ceil($topicPosts/$maxPostsPerTopicPage);

                        $visited = new Visited();
                        $visited->setUser($user);
                        $visited->setLastVisit(new \DateTime('now'));

                        // Si estoy visitando la última página del hilo guardo como último post visitado el último post
                        // del hilo
                        if($topicPageVisited == $topicPages){
                            $visited->setLastVisitedPost($topic->getLastPost());
                            $entityManager->persist($visited);
                            $entityManager->flush();
                        }

                        // Si no estoy visitando la última página debo encontrar el último post de la página que estoy
                        // visitando para poder guardarlo
                        elseif ($topicPageVisited < $topicPages){

                            $pageLastPostNumber = ($topicPageVisited*$maxPostsPerTopicPage)+1;
                            $lastViewedPost = $entityManager->getRepository('PostBundle:Post')
                                                        ->findOnlyPostAndTopicByNumber($topicId, $pageLastPostNumber);

                            $visited->setLastVisitedPost($lastViewedPost);
                            $entityManager->persist($visited);
                            $entityManager->flush();
                        }


                    }
                }
            }
        }
    }
} 