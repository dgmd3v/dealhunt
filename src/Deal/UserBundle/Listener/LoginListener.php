<?php

namespace Deal\UserBundle\Listener;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\Router;

/*
 * Clase encargada de ejecutar tareas después del evento de login
 **/
class LoginListener
{
    private $session;

    /*
     * Constructor que recoge los argumentos pasado por Symfony2 al crear el servicio
     *
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    /*
     * En esta función genero un mensaje para informar de que se ha logueado de forma correcta
     *
     * @param InteractiveLoginEvent $event objeto con información sobre el evento
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        // Mensaje flashbag informando de que el login se ha realizado
        $this->session->getFlashBag()->add('success',
            'Login realizado correctamente'
        );
    }
}