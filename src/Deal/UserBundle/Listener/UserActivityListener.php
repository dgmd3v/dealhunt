<?php

namespace Deal\UserBundle\Listener;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Doctrine\ORM\EntityManager;
use Deal\UserBundle\Entity\User;

/*
 * Clase encargada de actualizar la fecha de última actividad del usuario
 **/
class UserActivityListener
{
    private $entityManager;
    private $context;

    /**
     * Constructor que recoge los argumentos pasado por Symfony2 al crear el servicio
     *
     * @param EntityManager $entityManager
     * @param SecurityContext $context
     */
    public function __construct(EntityManager $entityManager,  SecurityContext $context)
    {
        $this->entityManager = $entityManager;
        $this->context = $context;
    }

    /**
     * En esta función actualizo la fecha de la última actividad del usuario
     *
     * @param FilterControllerEvent $event
     */
    public function onCoreController(FilterControllerEvent $event)
    {
        // Ignoro las SUB_REQUESTS
        if ($event->getRequestType() !== HttpKernel::MASTER_REQUEST) {
            return;
        }

        $user = $this->context->getToken()->getUser();

        // Compruebo que $user pertenece a la clase User para acceder a $lastActivity
        if( ($user instanceof User) )
        {
            $currentTimeMinusThree = new \DateTime('now - 3 minutes');

            if(is_null($user->getLastActivity())){
                $user->setLastActivity(new \DateTime('now'));
                $this->entityManager->flush($user);
            }

            elseif(($user->getLastActivity() < $currentTimeMinusThree)){
                $user->setLastActivity(new \DateTime('now'));
                $this->entityManager->flush($user);
            }

        }

    }
} 