<?php

namespace Deal\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/*
 * Clase usada para Actions actions AJAX de UserBundle
 * */
class AjaxController extends Controller
{
    /*
     * Borra una firma dada su Id
     *
     * @param integer $signatureId Id de la firma a borrar
     **/
    public function ajaxDeleteSignatureAction()
    {
        $request = $this->getRequest();

        // Si se trata de una petición AJAX serializo los resultados de la consulta a la base de datos
        // y devuelvo la respuesta
        if ($request->isXmlHttpRequest()) {
            // Obtengo el objeto del entity manager para realizar consultas
            $em = $this->getDoctrine()->getManager();

            // Solicito los datos pasados a través del script jquery, en este caso el id del post/mensaje a borrar
            $signatureId = $request->request->get('signatureId');

            // Busco la firma a borrar
            $signature = $em->getRepository('UserBundle:Signature')->findSignatureById($signatureId);

            // Llamo al serializador para poder utilizarlo
            $serializer = $this->container->get('jms_serializer');

            // Si no se ha encontrado la firma debo mostrar un mensaje de error
            if (!$signature) {
                $responseArray = array("responseCode" => 400, "errorMsg" => "No se ha encontrado la firma solicitada");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            if(!$this->get('security.context')->isGranted('ROLE_DELETE_SIGNATURE', $signature)){
                $responseArray = array("responseCode" => 400, "errorMsg" => "Debes estar logueado y que las firmas "
                    . "vayan dirigidas hacia ti para poder borrarlas (o ser moderador)");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Si encuentro la firma la borro y seriealizo los datos de vuelta
            $this->container->get('deal.userbundle.service.deletesignature')->delete($signature);
            $em->flush();

            $responseArray = array("responseCode" => 200, "successMsg" => "Se ha borrado correctamente la firma");
            $jsonContent = $serializer->serialize($responseArray, 'json');

            return new Response($jsonContent);
        }

        // En caso de no ser una petición AJAX retorno un mensaje de error
        return new Response('Esto no es ajax!', 400);
    }
}