<?php

namespace Deal\UserBundle\Controller;

use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\HttpFoundation\Request;

/*
 * Clase usada para sobreescribir Actions del controlador SecurityController de FOSUserBundle
 * */
class SecurityController extends BaseController
{
    /*
     * Función que antes de ejecutar la action de login comprueba si el usuario ya está logueado,
     * en ese caso se le redirige a su perfil y se le avisa
     * */
    public function loginAction(Request $request)
    {
        $response = parent::loginAction($request);

        // Si se está intentando loguearse estando logueado lo redirijo a su perfil y muestro un mensaje de info
        if($this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')){
            $this->get('session')->getFlashBag()->add('info',
                'Ya estás logueado!!!'
            );

            return $this->redirect($this->generateUrl('fos_user_profile_show'));
        }

        return $response;
    }
}
