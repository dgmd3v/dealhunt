<?php

namespace Deal\UserBundle\Controller;

use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Symfony\Component\HttpFoundation\Request;

/*
 * Clase usada para sobreescribir Actions del controlador RegisterController de FOSUserBundle
 * */
class RegistrationController extends BaseController
{
    /*
     * Función que antes de ejecutar la action de registro comprueba si el usuario ya está logueado,
     * en ese caso se le redirige a su perfil y se le avisa
     * */
    public function registerAction(Request $request)
    {
        $response = parent::registerAction($request);

        // Si se está intentando registrando estando logueado lo redirijo a su perfil y muestro un mensaje de info
        if($this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')){
            $this->get('session')->getFlashBag()->add('info',
                'Ya estás registrado!!!'
            );

            return $this->redirect($this->generateUrl('fos_user_profile_show'));
        }

        return $response;
    }
}