<?php

namespace Deal\UserBundle\Controller;

use FOS\UserBundle\Controller\ProfileController as BaseController;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Deal\UserBundle\Entity\Signature as Signature;
use Deal\UserBundle\Form\Frontend\SignatureType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/*
 * Clase usada para sobrescribir Actions del controlador ProfileController de FOSUserBundle
 * */
class ProfileController extends BaseController
{
    /*
     * Sobrescribo el controlador para mostrar el usuario para añadirle más funcionalidades
     */
    public function showAction()
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('Este usuario no tiene acceso a esta sección.');
        }

        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->container->get('doctrine.orm.entity_manager');

        // Busco los mensajes del usuario
        $globalVariables = $em->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
        $numberOfUserLastPosts = $globalVariables->getNumberOfUserLastPosts();

        $userPosts = $em->getRepository('UserBundle:User')->findUserLastPosts($user->getId(), $numberOfUserLastPosts);

        // Busco las firmas del usuario
        $globalVariables = $em->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
        $maxProfileSignature = $globalVariables->getMaxProfileSignatures();

        $userSignatures = $em->getRepository('UserBundle:Signature')->findLastNUserSignatures($user->getId(), $maxProfileSignature);

        return $this->render('FOSUserBundle:Profile:show.html.twig', array( 'user' => $user,
                                                            'userPosts'  => $userPosts,
                                                            'userSignatures' => $userSignatures,
                                                            'numberOfUserLastPosts'=> $numberOfUserLastPosts));
    }

    /*
     * Muestra en una plantilla el perfil del usuario mostrando información del mismo
     *
     * @param string $userName usuario del cual queremos consultar el perfil
     **/
    public function profileAction($userName)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->container->get('doctrine.orm.entity_manager');

        // Busco los datos del usuario para mostrar en su perfil
        $user = $em->getRepository('UserBundle:User')->findUserInfo($userName);

        // Si por algún motivo no encuentro al usuario muestro un mensaje de error
        if(!$user){
            throw $this->createNotFoundException(   'No se ha encontrado '
                . 'información del '
                . 'usuario solicitado');
        }

        // Hago estas consultas solo si el usuario no está baneado
        if(!$user->isLocked()){
            // Busco los posts del usuario
            $globalVariables = $em->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
            $numberOfUserLastPosts = $globalVariables->getNumberOfUserLastPosts();

            $userPosts = $em->getRepository('UserBundle:User')->findUserLastPosts($user->getId(), $numberOfUserLastPosts);

            // Busco las firmas del usuario
            $maxProfileSignature = $globalVariables->getMaxProfileSignatures();
            $userSignatures = $em->getRepository('UserBundle:Signature')->findLastNUserSignatures($user->getId(), $maxProfileSignature);

            return $this->render('FOSUserBundle:Profile:show.html.twig', array( 'user' => $user,
                                                                'userPosts'  => $userPosts,
                                                                'userSignatures' => $userSignatures,
                                                                'numberOfUserLastPosts'=> $numberOfUserLastPosts));
        }

        return $this->render('FOSUserBundle:Profile:show.html.twig', array('user' => $user));
    }

    /*
     * Muestra en una plantilla las firmas recibidas por el usuario
     *
     * @param string $userName usuario del cual queremos consultar las firmas
     * @param integer $pageNumber página de las firmas que vamos a mostrar, por defecto 1
     **/
    public function showUserSignaturesAction($userName, $pageNumber = 1)
    {
        // Compruebo que el número de página sea mayor que 0
        if($pageNumber < 1){
            return new RedirectResponse(
                $this->generateUrl( 'user_signatures_page',
                    array ( 'userName' => $userName,
                            'pageNumber'=> 1))
            );
        }

        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->container->get('doctrine.orm.entity_manager');

        // Busco los datos del usuario para mostrar en su perfil
        $user = $em->getRepository('UserBundle:User')->findUserByUserName($userName);

        // Si por algún motivo no encuentro al usuario muestro un mensaje de error
        if(!$user){
            throw $this->createNotFoundException(   'No se ha encontrado '
                . 'información del '
                . 'usuario solicitado');
        }

        // No permito ver las firmas si el usuario está baneado
        if(!$user->isAccountNonLocked()){
            return $this->render('@User/Profile/show_signatures.html.twig', array('user' => $user, 'userIsNotBanned' => false));
        }

        // Busco el número de firmas que tiene el usuario
        $signatureCount = $em->getRepository('UserBundle:Signature')->findNumberOfUserSignatures($user);

         // Genero la consulta que voy a utilizar para consultar las firmas recibidas por el usuario
        $signatures = $em->getRepository('UserBundle:Signature')->findUserSignatures($user);

        // Pagino los resultados con la ruta personalizada
        $globalVariables = $em->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
        $maxSignaturesPerPage = $globalVariables->getMaxSignaturesPerPage();

        $maxResultsPages = ceil($signatureCount/$maxSignaturesPerPage);
        if( $pageNumber > $maxResultsPages){

            if($maxResultsPages ==  0){
                // Formulario vacío para nuevas firmas
                $form = $this->createForm(new SignatureType());

                return $this->render('@User/Profile/show_signatures.html.twig', array('results' => false,
                                                                            'user' => $user,
                                                                            'signatureForm' => $form->createView(),
                                                                            'userIsNotBanned' => true));
            }

            $pageNumber = $maxResultsPages;

            return new RedirectResponse(
                $this->generateUrl('user_signatures_page', array (
                    'userName'      => $userName,
                    'pageNumber'    => $pageNumber,
                ))
            );
        }

        $signatures->setHint('knp_paginator.count', $signatureCount);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate( $signatures,
                                            $this->get('request')->query->get('page', $pageNumber),
                                            $maxSignaturesPerPage,
                                            array('distinct' => false));

        // Ruta y plantilla de paginación personalizadas
        $pagination->setUsedRoute('user_signatures_page');

        // Formulario vacío para nuevas firmas
        $form = $this->createForm(new SignatureType());

        return $this->render('@User/Profile/show_signatures.html.twig', array('pagination'        => $pagination,
                                                                    'results'           => true,
                                                                    'user'              => $user,
                                                                    'lastPageNumber'    => $maxResultsPages,
                                                                    'signatureForm'     => $form->createView(),
                                                                    'userIsNotBanned'   => true));

    }

    /*
     * Crea una firma o muestra el formulario para crearla
     *
     * @param integer signedUserId id del usuario al que se le va a firmar
     **/
    public function newSignatureAction($signedUserId)
    {
        // Obtengo el usuario logueado a través del token creado por el componente de seguridad
        $signatureUser = $this->get('security.context')->getToken()->getUser();

        // Si se está intentando crear una firma sin ser usuario muestro un mensaje informando del error y
        // redirijo al login
        if($signatureUser == null || !$this->get('security.context')->isGranted('ROLE_USER')){
            $this->get('session')->getFlashBag()->add('info',
                'Para poder firmar debes haberte registrado y estar logueado'
            );

            $request = $this->container->get('request');
            $session = $request->getSession();
            $session->set('requestedPage', $request->getPathInfo());

            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        // No dejo que el usuario se auto-firme
        if($signatureUser->getId() == $signedUserId){
            $this->get('session')->getFlashBag()->add('error',
                'No puedes firmarte a ti mismo!!!'
            );

            return $this->redirect($this->generateUrl(  'user_signatures',
                                                        array('userName' => $signatureUser->getUserName()))
                                                      );
        }

        $request = $this->getRequest();

        $signature = new Signature();

        $form = $this->createForm(new SignatureType(), $signature);

        // Relaciono el formulario con los datos introducidos por el usuario en el mismo
        $form->handleRequest($request);

        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getDoctrine()->getManager();

        $signedUser = $em->getRepository('UserBundle:User')->findOnlyUserById($signedUserId);

        // Si por algún motivo no encuentro al usuario muestro un mensaje de error
        if(!$signedUser){
            throw $this->createNotFoundException(   'No se ha encontrado '
                . 'información del '
                . 'usuario solicitado');
        }

        // No permito firmas si el usuario está baneado
        if(!$signedUser->isAccountNonLocked()){
            Throw new AccessDeniedHttpException('No puedes firmar a un usuario baneado.');
        }

        // Compruebo que los datos enviados son correctos
        if($form->isValid()){

            // "Seteo" la firma, el usuario al que va dirigida, etc, etc
            $signature->setSignatureUser($signatureUser);
            $signature->setSignatureDate(new \DateTime('now'));
            $signature->setSignedUser($signedUser);
            $signatureIp = $this->container->get('request')->getClientIp();
            $signature->setSignatureIp($signatureIp);

            // Guardo los datos en la base de datos
            $em->persist($signature);

           // Creo la notificación de nueva firma
           $this->container->get('deal.userbundle.service.signaturenotification')->newSignatureNotification($signature);

            $em->flush();

            // Mensaje flashbag informando de que se ha creado la firma correctamente
            $this->get('session')->getFlashBag()->add('success',
                'Has firmado correctamente'
            );

            return $this->redirect  ($this->generateUrl(  'user_signatures',
                                        array(  'userName'     => $signedUser->getUserName(),
                                        ))
                                    );
        }

        // En caso de no introducir los datos de forma valida devuelvo al usuario al formulario para crear una nueva
        // firma
        return $this->render(   '@User/Default/newSignature.html.twig',
            array(  'signatureForm' => $form->createView(),
                    'user'          => $signedUser,
            )
        );
    }

    /*
     * Muestra en una plantilla las firmas recibidas por el usuario
     *
     * @param integer $pageNumber página de las firmas que vamos a mostrar, por defecto 1
     **/
    public function showFavoriteTopicsAction($pageNumber = 1)
    {
        // Compruebo que el número de página sea mayor que 0
        if($pageNumber < 1){
            return new RedirectResponse(
                $this->generateUrl( 'user_favorites_page',
                    array ( 'pageNumber'=> 1))
            );
        }

        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->container->get('doctrine.orm.entity_manager');

        // Obtengo el usuario logueado a través del token creado por el componente de seguridad
        $user = $this->get('security.context')->getToken()->getUser();

        // Si se está intentando ver sus favoritos sin ser usuario muestro un mensaje informando del error y
        // redirijo al login
        if($user == null || !$this->get('security.context')->isGranted('ROLE_USER')){
            $this->get('session')->getFlashBag()->add('info',
                'Para poder ver tus temas favoritos debes estar registrado y logueado'
            );

            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        // Busco número de temas favoritos del usuario
        $favoritesCount = $em->getRepository('UserBundle:Visited')->findNumberOfFavoriteTopics($user);

        // Genero la consulta que voy a utilizar para consultar los temas favoritos del usuario
        $favorites = $em->getRepository('UserBundle:Visited')->findUserFavoriteTopics($user);

        // Pagino los resultados con la ruta personalizada
        $globalVariables = $em->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
        $maxFavoritesPerPage = $globalVariables->getMaxFavoritesPerPage();

        $maxResultsPages = ceil($favoritesCount/$maxFavoritesPerPage);

        if( $pageNumber > $maxResultsPages){

            if($maxResultsPages ==  0){
                return $this->render('@User/Profile/show_favorites.html.twig', array('results' => false, 'user' => $user));
            }

            if($pageNumber == 0) $pageNumber = 1;

            else{
                $pageNumber = $maxResultsPages;
            }

            return new RedirectResponse(
                $this->generateUrl('user_favorites_page', array (
                    'pageNumber'    => $pageNumber,
                ))
            );
        }

        $favorites->setHint('knp_paginator.count', $favoritesCount);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate( $favorites,
                                            $this->get('request')->query->get('page', $pageNumber),
                                            $maxFavoritesPerPage,
                                            array('distinct' => false));

        // Ruta y plantilla de paginación personalizadas
        $pagination->setUsedRoute('user_favorites_page');

        return $this->render('@User/Profile/show_favorites.html.twig', array( 'pagination'        => $pagination,
                                                                    'results'           => true,
                                                                    'lastPageNumber'    => $maxResultsPages,
                                                                    'user'              => $user));
    }

    /*
     * Muestra en una plantilla las notificaciones del usuario
     *
     * @param integer $pageNumber página de las notificaciones que vamos a mostrar, por defecto 1
     **/
    public function showNotificationsAction($notificationType = 'citas', $pageNumber = 1)
    {
        // Compruebo que el número de página sea mayor que 0
        if($pageNumber < 1){
            return new RedirectResponse(
                $this->generateUrl( 'user_notifications_page',
                    array ( 'notificationType' => $notificationType,
                            'pageNumber'=> 1))
            );
        }

        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->container->get('doctrine.orm.entity_manager');

        // Obtengo el usuario logueado a través del token creado por el componente de seguridad
        $user = $this->get('security.context')->getToken()->getUser();

        // Si se está intentando ver sus notificaciones sin ser usuario muestro un mensaje informando del error y
        // redirijo al login
        if($user == null || !$this->get('security.context')->isGranted('ROLE_USER')){
            $this->get('session')->getFlashBag()->add('info',
                'Para poder ver tus notificaciones debes estar registrado y logueado'
            );

            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        switch($notificationType){
            case 'citas':
                // Busco las notificaciones del usuario y las cuento
                $notificationsCount = $em->getRepository('UserBundle:QuoteNotification')->findNumberOfQuoteNotifications($user);

                // Genero la consulta que voy a utilizar para consultar las notificaciones del usuario
                $notifications = $em->getRepository('UserBundle:QuoteNotification')->findUserQuoteNotifications($user);
                break;

            case 'firmas':
                // Busco las notificaciones del usuario y las cuento
                $notificationsCount = $em->getRepository('UserBundle:SignatureNotification')
                                                                            ->findNumberOfSignatureNotifications($user);

                // Genero la consulta que voy a utilizar para consultar las notificaciones del usuario
                $notifications = $em->getRepository('UserBundle:SignatureNotification')->findUserSignatureNotifications($user);
                break;

            case 'moderacion':
                // Busco las notificaciones del usuario y las cuento
                $notificationsCount = $em->getRepository('UserBundle:ModerationNotification')
                                                                        ->findNumberOfModerationNotifications($user);

                // Genero la consulta que voy a utilizar para consultar las notificaciones del usuario
                $notifications = $em->getRepository('UserBundle:ModerationNotification')->findUserModerationNotifications($user);
                break;
        }

        // Pagino los resultados con la ruta personalizada
        $globalVariables = $em->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
        $maxNotificationsPerPage = $globalVariables->getNotificationsPerPage();

        $maxResultsPages = ceil($notificationsCount/$maxNotificationsPerPage);

        if( $pageNumber > $maxResultsPages){

            if($maxResultsPages ==  0){
                return $this->render('@User/Profile/show_notifications.html.twig', array( 'results'          => false,
                                                                                'user'             => $user,
                                                                                'notificationType' => $notificationType,
                                                                                ));
            }

            $pageNumber = $maxResultsPages;

            return new RedirectResponse(
                $this->generateUrl('user_notifications_page', array (
                    'notificationType'  => $notificationType,
                    'pageNumber'        => $pageNumber,
                ))
            );
        }

        $notifications->setHint('knp_paginator.count', $notificationsCount);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate( $notifications,
                                            $this->get('request')->query->get('page', $pageNumber),
                                            $maxNotificationsPerPage,
                                            array('distinct' => false));

        // Ruta y plantilla de paginación personalizadas
        $pagination->setUsedRoute('user_notifications_page');

        foreach($pagination as $notification ){
            // Si alguna notificación estaba sin notificar las marco como notificadas
            if(!$notification->getIsNotified()){
                // Marco las notificaciones como notificadas/leídas
                switch($notificationType){
                    case 'citas':
                        $em->getRepository('UserBundle:QuoteNotification')->updateNotifiedStatus($user);
                        break;

                    case 'firmas':
                        $em->getRepository('UserBundle:SignatureNotification')->updateNotifiedStatus($user);
                        break;

                    case 'moderacion':
                        $em->getRepository('UserBundle:ModerationNotification')->updateNotifiedStatus($user);
                        break;
                }
                break;
            }
        }

        return $this->render('@User/Profile/show_notifications.html.twig', array(
                                                                    'pagination'        => $pagination,
                                                                    'results'           => true,
                                                                    'user'              => $user,
                                                                    'lastPageNumber'    => $maxResultsPages,
                                                                    'notificationType'  => $notificationType));
    }


    /*
     * Muestra en una plantilla el panel de control del usuario, con sus notificaciones, enlace a su perfil, etc
     *
     **/
    public function showUserControlPanelAction()
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->container->get('doctrine.orm.entity_manager');

        // Obtengo el usuario logueado a través del token creado por el componente de seguridad
        $user = $this->get('security.context')->getToken()->getUser();

        // Si se está intentando ver sus panel de control sin ser usuario muestro un mensaje informando del error y
        // redirijo al login
        if($user == null || !$this->get('security.context')->isGranted('ROLE_USER')){
            $this->get('session')->getFlashBag()->add('info',
                'Debes estar logueado para acceder a tu panel de control'
            );

            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        // Busco si el usuario tiene temas favoritos con mensajes sin leer
        $favoritesUnread = $em->getRepository('UserBundle:Visited')->findNumberOfUnreadFavorites($user);

        // Busco si el usuario tiene notificaciones sin leer, citas, firmas, moderación ...
        $quotesUnNotified = $em->getRepository('UserBundle:QuoteNotification')->findNumberOfUnNotifiedQuotes($user);
        $signaturesUnNotified = $em->getRepository('UserBundle:SignatureNotification')->findNumberOfUnNotifiedSignatures($user);
        $moderationUnNotified = $em->getRepository('UserBundle:ModerationNotification')->findNumberOfUnNotifiedModeration($user);

        if($this->get('security.context')->isGranted('ROLE_MODERATOR')){

            $reportsUnNotified = $em->getRepository('PostBundle:Report')->findNumberOfUnNotifiedReports();

            return $this->render(   '@User/Profile/controlPanel.html.twig',
                array(
                    'user'                  => $user,
                    'favoritesUnread'       => $favoritesUnread,
                    'quotesUnNotified'      => $quotesUnNotified,
                    'signaturesUnNotified'  => $signaturesUnNotified,
                    'moderationUnNotified'  => $moderationUnNotified,
                    'reportsUnNotified'     => $reportsUnNotified
                )
            );
        }


        return $this->render(   '@User/Profile/controlPanel.html.twig',
                                array(
                                    'user'                  => $user,
                                    'favoritesUnread'       => $favoritesUnread,
                                    'quotesUnNotified'      => $quotesUnNotified,
                                    'signaturesUnNotified'  => $signaturesUnNotified,
                                    'moderationUnNotified'  => $moderationUnNotified,
                                )
                            );

    }

}