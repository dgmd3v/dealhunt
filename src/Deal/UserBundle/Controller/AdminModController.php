<?php

namespace Deal\UserBundle\Controller;

use Deal\UserBundle\Form\Frontend\BanUserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;


/*
 * Clase encargada de las acciones de administración relacionadas con los usuarios
 * */
class AdminModController extends Controller {

    /*
     * Acción encargada de banear un usuario
     *
     * @param integer $postId Id del post que pertenece al usuario a banear
     **/
    public function ajaxBanUserAction()
    {
        $request = $this->getRequest();

        // Si se trata de una petición AJAX serializo los resultados de la consulta a la base de datos
        // y devuelvo la respuesta
        if ($request->isXmlHttpRequest()) {

            // Llamo al serializador para poder utilizarlo
            $serializer = $this->container->get('jms_serializer');

            // Obtengo el objeto del entity manager para realizar consultas
            $em = $this->getDoctrine()->getManager();

            // Solicito los datos pasados a través del script jquery, en este caso el número del post del usuario
            // a banear, el usuario a banear, la fecha en la que acaba el baneado y la razón del baneo
            $userId = $request->request->get('userId');
            $postNumber = $request->request->get('postNumber');
            $serializedForm = $request->request->get('form');

            // Paso los datos serializados a un array y los utilizo
            parse_str($serializedForm, $form);

            $form = reset($form);

            // Si no se introduce una fecha completa ...
            if (empty($form['finishingDate']['year']) ||
                empty($form['finishingDate']['month']) ||
                empty($form['finishingDate']['day'])) {
                $responseArray = array( "responseCode" => 400,
                    "errorMsg" =>   "Debes introducir una fecha completa."
                );
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            $finishingDate = new \DateTime();
            $finishingDate->setDate($form['finishingDate']['year'],
                                    $form['finishingDate']['month'],
                                    $form['finishingDate']['day']);


            $reason = $form['reason'];

            // Busco usuario a banear a partir de su nombre de usuario
            $user = $em->getRepository('UserBundle:User')->findOnlyUserById($userId);

            // Si no se ha encontrado al usuario debo mostrar un mensaje de error
            if (!$user) {
                $responseArray = array( "responseCode" => 400,
                    "errorMsg" =>   "No se ha encontrado el usuario solicitado, " .
                        "el usuario no ha sido baneado.");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Compruebo que pueda banear al usuario
            if(!$this->get('security.context')->isGranted('ROLE_CAN_BAN_USER', $user)){
                $responseArray = array( "responseCode" => 400,
                    "errorMsg" => "No tienes permisos suficientes para banear a este usuario.");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Si ya estuviese baneado lo aviso
            if ($user->isLocked()==1) {
                $responseArray = array("responseCode" => 400, "errorMsg" => "El usuario ya estaba baneado");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            $currentDate = new \DateTime('now');

            // Si la fecha es anterior a la fecha actual muestro un mensaje de error
            if ($finishingDate < $currentDate) {
                $responseArray = array( "responseCode" => 400,
                                        "errorMsg" =>   "La fecha de finalización no puede ser anterior" .
                                                        " a la fecha actual, el usuario no ha sido baneado.");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Si no se ha introducido un motivo muestro un mensaje de error
            if (empty($reason)) {
                $responseArray = array( "responseCode" => 400,
                                        "errorMsg" =>   "Debes introducir un motivo de baneo, " .
                                                        "el usuario no ha sido baneado.");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Si la razón del baneo es muy larga
            $reasonMaxLength = 250;
            if (strlen($reason) > $reasonMaxLength) {
                $responseArray = array( "responseCode" => 400,
                                        "errorMsg" =>   "El motivo del baneo no puede exceder los " . $reasonMaxLength .
                                                        " caracteres, el usuario no ha sido baneado.");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Si encuentro al usuario lo baneado y seriealizo los datos de vuelta
            $this->get('deal.userbundle.moderationtools.banuser')->ban($user, $finishingDate, $reason);

            $responseArray = array( "responseCode" => 200,
                                    "postNumber" => $postNumber,
                                    "successMsg" => "El usuario ". $user->getUsername() . " ha sido baneado con éxito");
            $jsonContent = $serializer->serialize($responseArray, 'json');

            return new Response($jsonContent);
        }

        // En caso de no ser una petición AJAX retorno un mensaje de error
        return new Response('Esto no es ajax!', 400);
    }

    /*
     * Acción encargada de banear un usuario
     *
     * @param integer $postId Id del post que pertenece al usuario a banear
     **/
    public function ajaxUnBanUserAction()
    {
        $request = $this->getRequest();

        // Si se trata de una petición AJAX serializo los resultados de la consulta a la base de datos
        // y devuelvo la respuesta
        if ($request->isXmlHttpRequest()) {

            // Llamo al serializador para poder utilizarlo
            $serializer = $this->container->get('jms_serializer');

            // Obtengo el objeto del entity manager para realizar consultas
            $em = $this->getDoctrine()->getManager();

            // Solicito los datos pasados a través del script jquery, en este caso el número del post del usuario
            // a des-banear, el usuario a des-banear
            $userId = $request->request->get('userId');
            $userName = $request->request->get('userName');
            $postNumber = $request->request->get('postNumber');

            // Busco el ban del usuario
            $userBan = $em->getRepository('UserBundle:Ban')->findBanByUserId($userId);

            // Si no existiese ningún baneo para el usuario
            if (!$userBan) {
                $responseArray = array( "responseCode" => 400,
                                        "errorMsg" => "El usuario " . $userName . " no estaba baneado.");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Compruebo que pueda des-banear al usuario
            if(!$this->get('security.context')->isGranted('ROLE_CAN_BAN_USER', $userBan->getUser())){
                $responseArray = array( "responseCode" => 400,
                    "errorMsg" => "No tienes permisos suficientes para des-banear a este usuario.");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Si encuentro al usuario lo des-baneado y seriealizo los datos de vuelta
            $this->get('deal.userbundle.moderationtools.banuser')->unBan($userBan);

            $responseArray = array( "responseCode" => 200,
                                    "postNumber" => $postNumber,
                                    "successMsg" => "El usuario " . $userName . " ha sido des-baneado con éxito");
            $jsonContent = $serializer->serialize($responseArray, 'json');

            return new Response($jsonContent);
        }

        // En caso de no ser una petición AJAX retorno un mensaje de error
        return new Response('Esto no es ajax!', 400);
    }

    /*
     * Acción encargada de mostrar el formulario de baneo
     *
     **/
    public function banFormModalAction(){
        $form = $this->createForm(new BanUserType());

        // Devuelvo el formulario vacío a la plantilla
        return $this->render(   '@User/Default/includes/banModal.html.twig',
            array(  'banForm'      => $form->createView(),
            )
        );
    }

    /*
     * Muestra en una plantilla los mensajes de un tema/topic el cual pertenece a un foro y tiene una id concreta.
     *
     * @param string $show slug con los reportes que se van a mostrar, todos, leídos, no leídos
     * @param integer $pageNumber página de los reportes que vamos a mostrar, por defecto 1
     **/
    public function showReportsAction($show='todos', $pageNumber = 1)
    {
        // Compruebo que el número de página sea mayor que 0
        if($pageNumber < 1){
            return new RedirectResponse(
                $this->generateUrl( 'user_moderation_show_reports_page',
                    array ( 'show' => $show,
                            'pageNumber'=> 1))
            );
        }

        // Obtengo el usuario logueado a través del token creado por el componente de seguridad
        $user = $this->get('security.context')->getToken()->getUser();

        // Si no se encuentra al usuario, no es un usuario registrado y no es moderador
        if($user == null){
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        if(!$this->get('security.context')->isGranted('ROLE_MODERATOR')){
            $this->get('session')->getFlashBag()->add('info',
                'Para poder ver los reportes debes estar registrado, logueado y ser moderador'
            );

            return new RedirectResponse($this->generateUrl( 'fos_user_profile_show'));
        }

        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getDoctrine()->getManager();

        // Cuento los reports y genero la consulta para buscarlos
        switch($show){
            case 'todos':
                $reportCount = $em->getRepository('PostBundle:Report')->findNumberOfReports();
                $reports = $em->getRepository('PostBundle:Report')->findAllReports();
                break;
            case 'no-leidos':
                $reportCount = $em->getRepository('PostBundle:Report')->findNumberOfUnNotifiedReports();
                $reports = $em->getRepository('PostBundle:Report')->findUnNotifiedReports();
                break;
            case 'leidos':
                $reportCount = $em->getRepository('PostBundle:Report')->findNumberOfNotifiedReports();
                $reports = $em->getRepository('PostBundle:Report')->findNotifiedReports();
                break;
            default:
                throw $this->createNotFoundException(   'No se han encontrado '
                    . 'reportes de tipo ' . $show);
                break;
        }

        // Pagino los resultados con la ruta personalizada
        $globalVariables = $em->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
        $maxReportsPerPage = $globalVariables->getReportsPerPage();

        // Si se solicita una página de reports sin resultados, devuelvo la última página existente.
        $maxResultsPages = ceil(($reportCount)/$maxReportsPerPage);

        if( $pageNumber > $maxResultsPages){

            if($maxResultsPages ==  0){
                return $this->render('@User/Profile/show_reports.html.twig', array( 'results' => false,
                                                                                    'show' => $show));
            }

            $pageNumber = $maxResultsPages;

            return new RedirectResponse(
                $this->generateUrl('user_moderation_show_reports_page', array (
                    'show'          => $show,
                    'pageNumber'    => $pageNumber,
                ))
            );
        }

        $reports->setHint('knp_paginator.count', $reportCount);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate( $reports,
                                            $this->get('request')->query->get('page', $pageNumber),
                                            $maxReportsPerPage,
                                            array('distinct' => false));

        // Ruta y plantilla de paginación personalizadas
        $pagination->setUsedRoute('user_moderation_show_reports_page');

        if($show == 'no-leidos') $show = 'no leidos';

        // Retorno una plantilla generada con twig a la que le paso los mensajes
        // buscados, el tema de los mensajes, el foro al que pertenece y la categoría al que pertenece el tema
        return $this->render(   '@User/Profile/show_reports.html.twig',
            array(
                'pagination'    => $pagination,
                'show'          => $show,
                'results'       => true
            )
        );

    }

    /*
     * Acción encargada de marcar/desmarcar un reporte como notificado/leído
     *
     * @param integer $postId Id del post que pertenece al usuario a banear
     **/
    public function ajaxReportNotifiedStatusAction()
    {
        $request = $this->getRequest();

        // Si se trata de una petición AJAX serializo los resultados de la consulta a la base de datos
        // y devuelvo la respuesta
        if ($request->isXmlHttpRequest()) {
            // Obtengo el usuario logueado a través del token creado por el componente de seguridad
            $user = $this->get('security.context')->getToken()->getUser();

            // Llamo al serializador para poder utilizarlo
            $serializer = $this->container->get('jms_serializer');

            // Si no se encuentra al usuario, no es un usuario registrado y no es moderador
            if($user == null || !$this->get('security.context')->isGranted('ROLE_MODERATOR')){
                $responseArray = array( "responseCode" => 400,
                                        "errorMsg" =>   "Para poder marcar/desmarcar como leídos reportes" .
                                                        " debes estar registrado, logueado y ser moderador");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Obtengo el objeto del entity manager para realizar consultas
            $em = $this->getDoctrine()->getManager();

            // Solicito los datos pasados a través del script jquery, en este caso el número del post del usuario
            // a des-banear, el usuario a des-banear
            $userId = $request->request->get('userId');
            $postId = $request->request->get('postId');
            $markAs = $request->request->get('markAs');

            // Busco el reporte
            $report = $em->getRepository('PostBundle:Report')->findReportById($userId, $postId);

            // Si no existiese ningún baneo para el usuario
            if (!$report) {
                $responseArray = array("responseCode" => 400, "errorMsg" => "El reporte no existe.");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Si ya estuviese marcado como notificado/leído ...
            elseif (  ($report->getIsNotified()==true) && ($markAs == 'notified')) {
                $responseArray = array( "responseCode" => 400,
                                        "errorMsg" => "El reporte ya estaba marcado como leído.");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Si ya estuviese marcado como NO notificado/leído ...
            elseif (  ($report->getIsNotified()==false) && ($markAs == 'unnotified')) {
                $responseArray = array( "responseCode" => 400,
                                        "errorMsg" => "El reporte ya estaba marcado como NO leído.");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            switch($markAs){
                case 'notified':
                    $report->setIsNotified(true);
                    $em->persist($report);
                    $em->flush($report);
                    break;

                case 'unnotified':
                    $report->setIsNotified(false);
                    $em->persist($report);
                    $em->flush($report);
                    break;

                default:
                    $responseArray = array( "responseCode" => 400,
                                            "errorMsg" => "No se puede marcar el reporte como" . $markAs);
                    $jsonContent = $serializer->serialize($responseArray, 'json');

                    return new Response($jsonContent);
                    break;

            }

            $responseArray = array("responseCode" => 200);
            $jsonContent = $serializer->serialize($responseArray, 'json');

            return new Response($jsonContent);
        }

        // En caso de no ser una petición AJAX retorno un mensaje de error
        return new Response('Esto no es ajax!', 400);
    }

} 