<?php

namespace Deal\PostBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ReportReasonRepository extends EntityRepository
{

    /*
     * Devuelve la razón del reporte dado un Id
     *
     * @param int $reportReasonId id de la razón del reporte
     **/
    public function findReportReasonById($reportReasonId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT rr
                      FROM PostBundle:ReportReason rr
                     WHERE rr.id = :reportReasonId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'reportReasonId'    => $reportReasonId,
        ));

        // Uso la cache para guardar este resultado (durante 604800s = 1 semana )
        $query->useResultCache(true, 604800);

        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }

    /*
     * Devuelve una razón de reporte dado su nombre
     *
     * @param string $reportReasonName nombre de la razón de reporte a buscar
     **/
    public function findReportReasonByName($reportReasonName)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT rr
                      FROM PostBundle:ReportReason rr
                     WHERE rr.name = :reportReasonName');

        $query = $em->createQuery($dql);

        // Limito los resultados
        $query->setMaxResults(1);

        // Parámetros de la consulta
        $query->setParameters(array(
            'reportReasonName' => $reportReasonName
        ));

        // Uso la cache para guardar este resultado (durante 604800s = 1 semana )
        $query->useResultCache(true, 604800);

        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }

    /*
     * Devuelve todas las razones de reporte, y cacheo el resultado
     *
     * @param string $reportReasonName nombre de la razón de reporte a buscar
     **/
    public function findAll()
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT rr
                      FROM PostBundle:ReportReason rr');

        $query = $em->createQuery($dql);

        // Uso la cache para guardar este resultado (durante 604800s = 1 semana )
        $query->useResultCache(true, 604800);

        // Retorno el resultado de la consulta
        return $query->getResult();
    }

}
