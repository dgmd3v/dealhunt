<?php

namespace Deal\PostBundle\Entity;

use Doctrine\ORM\EntityRepository;

class PostRepository extends EntityRepository
{
    /*
     * Devuelve los número posts/mensajes de un topic/tema solicitado
     *
     * @param int $topicID id del topic/tema del cual se quieren obtener el número de posts/mensajes
     **/
    public function findNumberOfPosts($topicId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        $dql = (  ' SELECT COUNT(p)
                      FROM PostBundle:Post p
                     WHERE p.topic = :topicId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'topicId' => $topicId
        ));

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }


    /*
     * Devuelve la consulta para obtener posts/mensajes de un topic/tema solicitado (excepto el primero)
     *
     * @param int $topicID id del topic/tema del cual se quieren obtener los posts/mensajes (excepto el primero)
     **/
    public function findTopicPosts($topicId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();     
        
        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT p, u
                      FROM PostBundle:Post p
                      JOIN p.poster u
                     WHERE p.topic = :topicId
                       AND p.number != 1
                  ORDER BY p.postingDate ASC');

        $query = $em->createQuery($dql);
        
        // Parámetros de la consulta
        $query->setParameters(array(
                'topicId' => $topicId
        ));
        
        // Retorno la consulta
        return $query;
    }

    /*
     * Devuelve los posts/mensajes de un topic/tema solicitado (excepto el primero)
     *
     * @param int $topicID id del topic/tema del cual se quieren obtener los posts/mensajes (excepto el primero)
     **/
    public function findTopicPostsWithResults($topicId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT p, u
                      FROM PostBundle:Post p
                      JOIN p.poster u
                     WHERE p.topic = :topicId
                       AND p.number != 1
                  ORDER BY p.postingDate ASC');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'topicId' => $topicId
        ));

        // Retorno el resultado de la consulta
        return $query->getResult();
    }

    /*
     * Devuelve los posts/mensajes de un topic/tema solicitado
     *
     * @param int $topicID id del topic/tema del cual se quieren obtener los posts/mensajes
     **/
    public function findAllTopicPosts($topicId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT p, u
                      FROM PostBundle:Post p
                      JOIN p.poster u
                     WHERE p.topic = :topicId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'topicId' => $topicId
        ));

        // Retorno el resultado de la consulta
        return $query->getResult();
    }

    /*
     * Devuelve el mensaje con el número solicitado en un tema
     *
     * @param int $topicId id del topic/tema del cual se quieren obtener el post/mensaje
     * @param int $postNumber número del post del cual se quieren obtener información
     **/
    public function findPostByNumber($topicId, $postNumber)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT p, u, t, f
                      FROM PostBundle:Post p
                      JOIN p.poster u
                      JOIN p.topic t
                      JOIN t.forum f
                     WHERE t.id = :topicId
                       AND p.number = :postNumber');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'topicId'       => $topicId,
            'postNumber'    => $postNumber
        ));

        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }

    /*
     * Devuelve el mensaje con el id solicitado, junto con su usuario y el tema al que pertenece
     *
     * @param int $postId id post/mensaje
     **/
    public function findPostById($postId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT p, u, t
                      FROM PostBundle:Post p
                      JOIN p.poster u
                      JOIN p.topic t
                     WHERE p.id = :postId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'postId'       => $postId,
        ));

        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }

    /*
     * Devuelve el mensaje con el id solicitado
     *
     * @param int $postId id post/mensaje
     **/
    public function findOnlyPostById($postId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT p
                      FROM PostBundle:Post p
                     WHERE p.id = :postId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'postId'       => $postId,
        ));

        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }

    /*
     * Devuelve el mensaje/post con el número solicitado en un tema y el tema al que pertenece
     *
     * @param int $topicId id del topic/tema del cual se quieren obtener el post/mensaje
     * @param int $postNumber número del post del cual se quieren obtener información
     **/
    public function findOnlyPostAndTopicByNumber($topicId, $postNumber)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT p, t
                      FROM PostBundle:Post p
                      JOIN p.topic t
                     WHERE t.id = :topicId
                       AND p.number = :postNumber');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'topicId'       => $topicId,
            'postNumber'    => $postNumber
        ));

        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }

    /**
     * Devuelve el mensaje solicitado por su id,  además del topic/tema su foro y el rol de acceso de este
     *
     * @param int $postId id post/mensaje
     *
     **/
    public function findPostTopicForumAndAccessById($postId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT p, t, f, ar
                     FROM PostBundle:Post p
                      JOIN p.topic t
                      JOIN t.forum f
                      JOIN f.accessRole ar
                     WHERE p.id = :postId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'postId' => $postId
        ));

        // Limito los resultados
        $query->setMaxResults(1);

        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }

}
