<?php

namespace Deal\PostBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ReportRepository extends EntityRepository
{

    /*
     * Devuelve un escalar con el número de reportes existentes de un usuario X a un post Y concretos
     *
     * @param int $reportedPostId id del post reportado (también puede ser el objeto post)
     * @param int $userId id del usuario que reporta (también puede ser el objeto usuario)
     **/
    public function findAlreadyReportedByUser($reportedPostId, $userId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT COUNT(r)
                      FROM PostBundle:Report r
                     WHERE r.post = :reportedPostId
                       AND r.user = :userId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'reportedPostId'    => $reportedPostId,
            'userId'            => $userId
        ));

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }

    /*
     * Devuelve un escalar con el número de reportes creados por el usuario en el día actual
     *
     * @param int $userId id del usuario que reporta (también puede ser el objeto usuario)
     **/
    public function findTodaysReportsByUser($userId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT COUNT(r)
                      FROM PostBundle:Report r
                     WHERE r.user = :userId
                       AND r.date >= :todaysDate');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId'            => $userId,
            'todaysDate'        => new \DateTime('today'),
        ));

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }

    /*
     * Devuelve un escalar con el número de reportes existentes
     **/
    public function findNumberOfReports()
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT COUNT(r)
                      FROM PostBundle:Report r');

        $query = $em->createQuery($dql);

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }

    /*
     * Devuelve un escalar con el número de reportes sin notificar
     **/
    public function findNumberOfUnNotifiedReports()
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT COUNT(r)
                      FROM PostBundle:Report r
                     WHERE r.isNotified = 0');

        $query = $em->createQuery($dql);

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }

    /*
     * Devuelve un escalar con el número de reportes notificados
     **/
    public function findNumberOfNotifiedReports()
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT COUNT(r)
                      FROM PostBundle:Report r
                     WHERE r.isNotified = 1');

        $query = $em->createQuery($dql);

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }

    /*
     * Devuelve todos los reportes
     **/
    public function findAllReports()
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT r, rr, p, ur, t, f
                      FROM PostBundle:Report r
                      JOIN r.reportReason rr
                      JOIN r.post p
                      JOIN p.topic t
                      JOIN t.forum f
                      JOIN r.user ur
                      ORDER BY r.date DESC');

        $query = $em->createQuery($dql);

        // Retorno la consulta
        return $query;
    }

    /*
     * Devuelve los reportes no notificados/leídos
     **/
    public function findUnNotifiedReports()
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT r, rr, p, ur, t, f
                      FROM PostBundle:Report r
                      JOIN r.reportReason rr
                      JOIN r.post p
                      JOIN p.topic t
                      JOIN t.forum f
                      JOIN r.user ur
                     WHERE r.isNotified = 0
                      ORDER BY r.date DESC');

        $query = $em->createQuery($dql);

        // Retorno la consulta
        return $query;
    }

    /*
     * Devuelve los reportes notificados/leídos
     **/
    public function findNotifiedReports()
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT r, rr, p, ur, t, f
                      FROM PostBundle:Report r
                      JOIN r.reportReason rr
                      JOIN r.post p
                      JOIN p.topic t
                      JOIN t.forum f
                      JOIN r.user ur
                     WHERE r.isNotified = 1
                      ORDER BY r.date DESC');

        $query = $em->createQuery($dql);

        // Retorno la consulta
        return $query;
    }

    /*
     * Devuelve el reporte con la clave compuesta userId/postId
     *
     * @param int $userId id del usuario al que le pertenece el reporte
     * @param int $postId id del post reportado
     **/
    public function findReportById($userId, $postId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT r, p, ur
                      FROM PostBundle:Report r
                      JOIN r.post p
                      JOIN r.user ur
                     WHERE ur.id = :userId
                      AND  p.id = :postId');

        $query = $em->createQuery($dql);

        // Limito los resultados
        $query->setMaxResults(1);

        // Parámetros de la consulta
        $query->setParameters(array(
            'userId'  => $userId,
            'postId'  => $postId
        ));

        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }

    /*
     * Devuelve los reportes que tengan en común la razón de reporte
     *
     * @param id $reportReason id de la razón de reporte de la cual queremos buscar los reportes
     **/
    public function findAllReportsByReason($reportReason)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT r
                      FROM PostBundle:Report r
                      JOIN r.reportReason rr
                     WHERE rr.id = :reportReason');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'reportReason' => $reportReason
        ));

        // Retorno el resultado de la consulta
        return $query->getResult();
    }

    /*
     * Devuelve los reportes que tengan en común que su razón de reporte es NULL
     **/
    public function findAllReportsWithNullReason()
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT r
                      FROM PostBundle:Report r
                     WHERE r.reportReason IS NULL');

        $query = $em->createQuery($dql);

        // Retorno el resultado de la consulta
        return $query->getResult();
    }

}
