<?php

namespace Deal\PostBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Report
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Deal\PostBundle\Entity\ReportRepository")
 */
class Report
{
    // *************** COLUMNAS DE LA TABLA ***************

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Deal\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    // Tenemos una relación N-N entre el usuario (user) y el post.
    // Esta es la parte N-1 (ManyToOne) desde el usuario (user) y de esta parte
    // obtenemos la primera parte de la clave compuesta
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Deal\PostBundle\Entity\Post")
     * @ORM\JoinColumn(name="post_id", referencedColumnName="id", onDelete="cascade", nullable=false)
     */
    // Tenemos una relación N-N entre el usuario (user) y el post.
    // Esta es la parte N-1 (ManyToOne) desde el post y de esta parte obtenemos la segunda parte de la clave compuesta
    private $post;

    /**
     * @var integer
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Deal\PostBundle\Entity\ReportReason")
     * @ORM\JoinColumn(name="reason_id", referencedColumnName="id", onDelete="SET NULL")
     */
    // Motivo del report. Relación ReportReason-Report (1-N),
    // un report solo puede pertenecer a un motivo de reporte,
    // mientras que un motivo de reporte, puede tener N reportes
    // Reglas de validación: No puede estar vacío.
    private $reportReason;

    /**
     * @var boolean
     *
     * @ORM\Column(name="notified", type="boolean")
     */
    // Booleano que indica si ha sido notificado/leído o no el reporte,
    // por defecto a falso, no ha sido visto por un moderador/administrador
    private $isNotified;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    // Fecha del report
    private $date;

    public function __construct() {
        $this->isNotified = false;
    }
    
    // *************** SETTERS Y GETTERS ***************

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param Deal\UserBundle\Entity\User $user
     * @return Report
     */
    // Esta función espera que se le pase un objeto de tipo User
    public function setUser(\Deal\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return integer
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set post
     *
     * @param \Deal\PostBundle\Entity\Post $post
     * @return Report
     */
    // Esta función espera que se le pase un objeto de tipo post
    public function setPost(\Deal\PostBundle\Entity\Post $post)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return integer
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set reportReason
     *
     * @param integer Deal\PostBundle\Entity\ReportReason $reportReason
     * @return Report
     */
    public function setReportReason(\Deal\PostBundle\Entity\ReportReason $reportReason)
    {
        $this->reportReason = $reportReason;

        return $this;
    }

    /**
     * Get reportReason
     *
     * @return integer
     */
    public function getReportReason()
    {
        return $this->reportReason;
    }

    /**
     * Set isNotified
     *
     * @param boolean $isNotified
     * @return Report
     */
    public function setIsNotified($isNotified)
    {
        $this->isNotified = $isNotified;
    
        return $this;
    }

    /**
     * Get isNotified
     *
     * @return boolean 
     */
    public function getIsNotified()
    {
        return $this->isNotified;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Report
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

}
