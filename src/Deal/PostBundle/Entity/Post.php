<?php

namespace Deal\PostBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Deal\PostBundle\Validator\Constraints as MyAssert;

/**
 * Post
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Deal\PostBundle\Entity\PostRepository")
 * @MyAssert\isAlreadyHiddenOrDeleted
 */
class Post
{
    // *************** COLUMNAS DE LA TABLA ***************      

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    // ID del post
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="number", type="integer")
     */
    // Número del post dentro del tema
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text")
     * @Assert\NotBlank()
     * @Assert\Length(max = 20000, maxMessage = "El número máximo de caracteres que se pueden escribir en un mensaje son {{ limit }}")
     */
    // Texto del mensaje
    // Reglas de validación:    No puede estar vacío. Y tiene un máximo de caracteres.
    private $message;

    /**
     * @var integer
     *
     * @ORM\Column(name="message_ip", type="string", length=46)
     * @Assert\Length(max = 46, maxMessage = "El número máximo de caracteres que se pueden escribir para la ip son {{ limit }}")
     */
    // Ip con la que se ha publicado el mensaje
    private $messageIp;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="posting_date", type="datetime")
     */
    // Fecha en la que se hizo el post
    private $postingDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="edit_date", type="datetime", nullable=true)
     */
    // Fecha en la que fue editado el post, puede ser null si no se ha editado
    private $editDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_hidden", type="boolean")
     */
    // Campo en el que se indica si el mensaje ha sido ocultado por un moderador/administrador,
    // por defecto está a false
    private $isHidden;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_deleted", type="boolean")
     */
    // Campo en el que se indica si el mensaje ha sido borrado por un moderador/administrador,
    // por defecto está a false
    private $isDeleted;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Deal\TopicBundle\Entity\Topic")
     * @ORM\JoinColumn(name="topic_id", referencedColumnName="id", onDelete="CASCADE")
     */
    // Tema (topic) al que pertenece el post. Relación Topic-Post (1-N), un post
    // solo puede pertenecer a un topic, mientras que un topic puede tener N posts
    private $topic;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Deal\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="poster_id", referencedColumnName="id", nullable=false)
     */
    // Usuario (poster) al que pertenece el post. Relación User-Post (1-N), 
    // un post solo puede pertenecer a un solo user, mientras que un user puede tener N posts
    private $poster;

    public function __construct() {
        $this->isHidden = false;
        $this->isDeleted = false;
    }

    // *************** SETTERS Y GETTERS ***************  
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param integer $number
     * @return Post
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Post
     */
    public function setMessage($message)
    {
        $this->message = $message;
    
        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set messageIp
     *
     * @param string $messageIp
     * @return Post
     */
    public function setMessageIp($messageIp)
    {
        $this->messageIp = $messageIp;

        return $this;
    }

    /**
     * Get messageIp
     *
     * @return string
     */
    public function getMessageIp()
    {
        return $this->messageIp;
    }

    /**
     * Set postingDate
     *
     * @param \DateTime $postingDate
     * @return Post
     */
    public function setPostingDate($postingDate)
    {
        $this->postingDate = $postingDate;
    
        return $this;
    }

    /**
     * Get postingDate
     *
     * @return \DateTime 
     */
    public function getPostingDate()
    {
        return $this->postingDate;
    }

    /**
     * Set editDate
     *
     * @param \DateTime $editDate
     * @return Post
     */
    public function setEditDate($editDate)
    {
        $this->editDate = $editDate;
    
        return $this;
    }

    /**
     * Get editDate
     *
     * @return \DateTime 
     */
    public function getEditDate()
    {
        return $this->editDate;
    }

    /**
     * Set isHidden
     *
     * @param boolean $isHidden
     * @return Post
     */
    public function setIsHidden($isHidden)
    {
        $this->isHidden = $isHidden;

        return $this;
    }

    /**
     * Get isHidden
     *
     * @return boolean
     */
    public function getIsHidden()
    {
        return $this->isHidden;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * @return Post
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set topic
     *
     * @param Deal\TopicBundle\Entity\Topic $topic
     * @return Post
     */
    // Esta función espera que se le pase un objeto de tipo Topic
    public function setTopic(\Deal\TopicBundle\Entity\Topic $topic)
    {
        $this->topic = $topic;

        return $this;
    }

    /**
     * Get topic
     *
     * @return integer
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * Set poster
     *
     * @param Deal\UserBundle\Entity\User $poster
     * @return Post
     */
    // Esta función espera que se le pase un objeto de tipo User 
    public function setPoster(\Deal\UserBundle\Entity\User $poster)
    {
        $this->poster = $poster;
    
        return $this;
    }

    /**
     * Get poster
     *
     * @return integer 
     */
    public function getPoster()
    {
        return $this->poster;
    }

    // Método mágico para que PHP sepa como convertir la entidad en una cadena de texto
    public function __toString()
    {
        return (string) $this->getId();
    }
}
