<?php

namespace Deal\PostBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;

/*
 * Clase de configuración para la administración de los reportes en el panel de administración
 * */
class ReportAdmin extends Admin {

    // Campos que se van a mostrar en los formularios crear/editar entidades
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Datos Obligatorios')
            ->add('user', null, array(
                'class' => 'Deal\UserBundle\Entity\User',
                'label' => 'Usuario que reporta',
                'read_only' => true,
                'disabled'  => true,
            ))
            ->add('post.id', null, array(
                'label' => 'Mensaje reportado (id)',
                'read_only' => true,
                'disabled'  => true,
            ))
            ->add('reportReason', null, array(
                'class' => 'Deal\PostBundle\Entity\ReportReason',
                'label' => 'Motivo',
                'read_only' => true,
                'disabled'  => true,
            ))
            ->add('date', 'datetime', array(
                'label' => 'Fecha de reporte',
                'read_only' => true,
                'disabled'  => true,
            ))
            ->end()
            ->with('Datos No Obligatorios')
            ->add('isNotified', 'checkbox', array('required'=> false, 'label' => '¿Leído?'))
            ->end()
        ;
    }

    // Campos a mostrar en los formularios para filtrar entidades
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, array('label' => 'Id'))
            ->add('user', null, array('class' => 'Deal\UserBundle\Entity\User', 'label' => 'Usuario que reporta'))
            ->add('post.id', null, array('class' => 'Deal\PostBundle\Entity\Post', 'label' => 'Mensaje reportado (id)'))
            ->add('isNotified', null, array('label' => '¿Leído?'))
        ;
    }

    // Campos para ser mostrados en los listados de entidades
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, array('label'=> 'Id'))
            ->add('user', null, array(  'sortable'=>true,
                                        'sort_field_mapping'=> array('fieldName'=>'username'),
                                        'sort_parent_association_mappings' => array(array('fieldName'=>'user')),
                                        'class' => 'Deal\UserBundle\Entity\User',
                                        'label' => 'Usuario que reporta'))
            ->add('post', null, array(  'sortable'=>true, 'sort_field_mapping'=> array('fieldName'=>'id'),
                                        'sort_parent_association_mappings' => array(array('fieldName'=>'post')),
                                        'class' => 'Deal\PostBundle\Entity\Post',
                                        'label' => 'Mensaje reportado (id)'))
            ->add('reportReason', null, array(  'sortable'=>true,
                                                'sort_field_mapping'=> array('fieldName'=>'name'),
                                                'sort_parent_association_mappings' => array(
                                                                                    array('fieldName'=>'reportReason')),
                                                'class' => 'Deal\PostBundle\Entity\ReportReason',
                                                'label' => 'Motivo'))
            ->add('date', 'datetime', array('label' => 'Fecha de reporte'))
            ->add('isNotified', 'boolean', array('label' => '¿Leído?'))
        ;
    }

    // Evito que se puedan crear los reportes
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
    }
} 