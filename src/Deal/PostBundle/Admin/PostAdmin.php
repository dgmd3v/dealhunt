<?php

namespace Deal\PostBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;

/*
 * Clase de configuración para la administración de los mensajes en el panel de administración
 * */
class PostAdmin extends Admin {

    // Campos que se van a mostrar en los formularios crear/editar entidades
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Datos Obligatorios')
            ->add('message', 'textarea', array('label' => 'Mensaje'))
            ->add('messageIp', 'text', array(   'label' => 'Ip del Mensaje',
                                                'read_only' => true,
                                                'disabled'  => true,
            ))
            ->add('postingDate', 'datetime', array( 'label' => 'Fecha de creación',
                                                    'read_only' => true,
                                                    'disabled'  => true,
            ))
            ->add('topic', null, array( 'class' => 'Deal\TopicBundle\Entity\Topic',
                                            'label' => 'Tema',
                                            'read_only' => true,
                                            'disabled'  => true,
            ))
            ->add('poster', null, array(    'class' => 'Deal\UserBundle\Entity\User',
                                                'label' => 'Usuario',
                                                'read_only' => true,
                                                'disabled'  => true,
            ))
        ->end()
         ->with('Datos No Obligatorios')
            ->add('editDate', 'datetime', array('required'=> false,'label' => 'Fecha de edición'))
            ->end()
        ;

        $subject = $this->getSubject();

        // Muestro este campo solo si el mensaje no es el primero del tema
        if ($subject->getNumber() > 1) {
            $formMapper
                ->with('Datos No Obligatorios')
                ->add('isHidden', 'checkbox', array('required'=> false, 'label' => '¿Oculto?'))
                ->add('isDeleted', 'checkbox', array('required'=> false, 'label' => '¿Borrado?'))
                ->end()
            ;
        }
    }

    // Campos a mostrar en los formularios para filtrar entidades
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, array('label' => 'Id'))
            ->add('number', null, array('label' => 'Número de Mensaje'))
            ->add('messageIp', null, array('label' => 'Ip del Mensaje'))
            ->add('postingDate', null, array('label' => 'Fecha de creación'))
            ->add('isHidden', null, array('label' => '¿Oculto?'))
            ->add('isDeleted', null, array('label' => '¿Borrado?'))
            ->add('topic.id', null, array('class' => 'Deal\TopicBundle\Entity\Topic', 'label' => 'Id del Tema'))
            ->add('poster', null, array('class' => 'Deal\UserBundle\Entity\User', 'label' => 'Usuario'))
        ;
    }

    // Campos para ser mostrados en los listados de entidades
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, array('label' => 'Id'))
            ->addIdentifier('number', null, array('label' => 'Número de mensaje'))
            ->add('message', 'text', array('label' => 'Mensaje'))
            ->add('messageIp', 'text', array('label' => 'Ip del Mensaje'))
            ->add('postingDate', 'datetime', array('label' => 'Fecha de creación'))
            ->add('editDate', 'datetime', array('label' => 'Fecha de edición'))
            ->add('isHidden', 'boolean', array('label' => '¿Oculto?'))
            ->add('isDeleted', 'boolean', array('label' => '¿Borrado?'))
            ->add('topic', null, array(     'sortable' => true,
                                            'sort_field_mapping' => array('fieldName'=>'title'),
                                            'sort_parent_association_mappings' => array(array('fieldName'=>'topic')),
                                            'class' => 'Deal\TopicBundle\Entity\Topic',
                                            'label' => 'Tema'))
            ->add('poster', null, array(    'sortable' => true,
                                            'sort_field_mapping' => array('fieldName'=>'username'),
                                            'sort_parent_association_mappings' => array(array('fieldName'=>'poster')),
                                            'class' => 'Deal\UserBundle\Entity\User',
                                            'label' => 'Usuario'))
        ;
    }

    // Evito que se puedan borrar/crear los mensajes
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('create');
    }

    // Si oculto o borro un mensaje debo crear un mensaje notificándolo
    public function preUpdate($object) {
        $container = $this->getConfigurationPool()->getContainer();
        $originalEntity = $container->get('doctrine.orm.entity_manager')->getUnitOfWork()->getOriginalEntityData($object);

        $oldHiddenStatus = $originalEntity["isHidden"];
        $oldDeletedStatus = $originalEntity["isDeleted"];

        $newHiddenStatus = $object->getIsHidden();
        $newDeletedStatus = $object->getIsDeleted();

        if($newHiddenStatus != $oldHiddenStatus && $oldHiddenStatus==0)
        {
            $container->get('deal.postbundle.moderationtools.posthide')->moderationNotification($object);
        }

        if($newDeletedStatus != $oldDeletedStatus && $oldDeletedStatus==0)
        {
                $container->get('deal.postbundle.moderationtools.postdelete')->moderationNotification($object);
        }
    }

}