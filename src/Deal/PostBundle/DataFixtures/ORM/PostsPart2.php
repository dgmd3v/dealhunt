<?php

namespace Deal\PostBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/*
 * Fixtures/Datos de prueba para la entidad post (segunda parte).
 * En este fixture asigno los posts ya creados a los temas a los que pertenecen
 **/
class PostsPart2 extends AbstractFixture implements OrderedFixtureInterface
{
    // Orden en el que se carga este fixture
    public function getOrder()
    {
        return 240;
    }

    public function load(ObjectManager $manager)
    {
        // Obtengo los temas ya creados
        $topics = $manager->getRepository('TopicBundle:Topic')->findAll();

        // Obtengo los usuarios ya creados
        $users = $manager->getRepository('UserBundle:User')->findAll();
        $countUsers = count($users);

        // Obtengo los mensajes ya creados
        $posts = $manager->getRepository('PostBundle:Post')->findAll();

        // Edito los mensajes indicando el tema al que pertenecen
        for($i=0; $i < count($topics); $i++)
        {
            for($j=0; $j < $countUsers; $j++)
            {
                // Tema en el que se encuentra el post
                $posts[($i*$countUsers)+$j]->setTopic($topics[$i]);

                // Hago la entidad persistente
                $manager->persist($posts[($i*$countUsers)+$j]);
            }
        }

        // Guardo los datos marcados como persistentes
        $manager->flush();

    }// Fin load
}