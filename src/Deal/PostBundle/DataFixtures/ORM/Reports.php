<?php

namespace Deal\PostBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Deal\PostBundle\Entity\Report;

/*
 * Fixtures/Datos de prueba para la entidad reporte.
 * En este fixture para cada post/mensaje reportado creo dicho reporte y le asigno datos como quien reporta, motivo, etc
 **/
class Reports extends AbstractFixture implements OrderedFixtureInterface
{
    // Orden en el que se carga este fixture
    public function getOrder()
    {
        return 260;
    }

    public function load(ObjectManager $manager)
    {
        // Obtengo el usuario con id 1 que será el que reporte todos los mensajes
        $user = $manager->getRepository('UserBundle:User')->find(1);

        // Obtengo los mensajes que han sido reportados
        $posts = $manager->getRepository('PostBundle:Post')->findAll();

        // Obtengo los motivos de reporte existentes
        $reportReasons = $manager->getRepository('PostBundle:ReportReason')->findAll();

        // Creo un reporte por cada mensaje reportado, con el usuario que reporta
        // el mensaje reportado, el motivo y pongo el estado como no notificado/leído
        // Reporto solo los 20 primeros mensajes
        for($i=0; $i < 20; $i++)
        {
            // A los mensajes con número par los voy a reportar
            if( $posts[$i]->getNumber() % 2 == 0){
                // Creo una nueva entidad reporte
                $report = new Report();

                // Incluyo en el reporte el usuario que ha reportado
                $report->setUser($user);

                // Incluyo en el reporte el post reportado
                $report->setPost($posts[$i]);

                // El motivo por el que se ha reportado, asignado de forma aleatoria
                $report->setReportReason($reportReasons[array_rand($reportReasons, 1)]);

                // Pongo el reporte como no notificado/leído (a falso) dado que ningún  administrador lo ha visto todavía
                $report->setIsNotified(false);

                // Fecha del reporte
                $report->setDate(new \DateTime('now - ' . $i . ' hours'));

                // Hago la entidad persistente
                $manager->persist($report);
            }
        }

        // Guardo los datos marcados como persistentes
        $manager->flush();

    }// Fin load
}