<?php

namespace Deal\PostBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Deal\PostBundle\Entity\Post;

// Debido a la gran cantidad de posts necesito quitar el limite máximo de memoria a usar
ini_set('memory_limit', '-1');

/*
 * Fixtures/Datos de prueba para la entidad post.
 * En este fixture creo posts los relleno con texto de prueba, se les asigna el usuario que lo ha creado, etc, etc
 **/
class Posts extends AbstractFixture implements OrderedFixtureInterface
{
    // Orden en el que se carga este fixture
    public function getOrder()
    {
        return 100;
    }

    public function load(ObjectManager $manager)
    {
        // Obtengo los usuarios ya creados
        $users = $manager->getRepository('UserBundle:User')->findAll();


        // Voy a crear 200 temas por cada foro, y cada usuario va a crear un mensaje
        $forums = $manager->getRepository('ForumBundle:Forum')->findAllForums();

        foreach($forums as $forum){
            for($i=0; $i < 200; $i++)
            {
                $quote = '';
                if($i==0) $quote = '#1 ';

                $days = rand(0, 31);
                for($j=0; $j < count($users); $j++)
                {
                    // Creo una nueva entidad post
                    $post = new Post();

                    // Seteo el número del post dentro del topic
                    $post->setNumber($j+1);

                    // Creo el mensaje del post y lo guardo en la entidad creada
                    $testMessage =  $this->randomMessage($forum);

                    $post->setMessage($quote . $testMessage);

                    // Todos los temas serán creados de hoy a 600 horas hacia atrás
                    $post->setPostingDate(new \DateTime('now - '. $days . ' days + ' .($j). ' minutes -200 minutes'));

                    // A los mensajes con id par los voy a marcar como editados
                    if( ($j+1)%2 == 0){
                        $post->setEditDate(new \DateTime('now - '. $days . ' days + ' .($j+10). ' minutes - 200 minutes'));
                    }

                    // Ip del mensaje
                    $post->setMessageIp('127.0.0.0');

                    // Usuario que ha creado el post
                    $post->setPoster($users[$j]);

                    // Hago la entidad persistente
                    $manager->persist($post);
                }

            }
        }

        // Guardo los datos marcados como persistentes
        $manager->flush();

    }// Fin load

    /*
     * Genero un mensaje aleatorio para el mensaje/post
     *
     * @param Forum $forum foro en el que va a estar el mensaje
     *
     **/
    private function randomMessage($forum)
    {

        $commontDealFreeVoucher = array(
            'La tienda tiene muy buena fama',
            'La tienda tiene muy mala fama',
            'Calidad de la buena',
            'Yo tengo al menos dos y está muy bien',
            'Mi reacción al leer el titulo y luego entrar al tema …

                            [img]http://i.imgur.com/4g9IYwi.gif[/img]',
            'Se agradece

                              [spoiler][img]http://i.imgur.com/bQDYeA1.gif[/img][/spoiler]',
            'Vaya spam más descarado',
            'Vaya oferta, party time:

                            [video]https://youtu.be/o4qF5A8CSLA[/video]',
            'Ni un céntimo me dejo yo en eso',
            '+1',
            '-1',
            ':laugh:',
            'Alguien lo tiene para saber que tal? ',
            'Alguna opinión del producto?',
            'A mi me da error al darle a comprar',
            'Ha caducado ya la oferta?',
            'Yo lo he comprado ya hace tiempo y me ha salido bastante mal',
            'Totalmente recomendable',
            'Perfecto para un regalo que tengo que hacer!!!',
            'Vaya cosas sin sentido que hay que leer por aquí',
            'No me funciona el enlace seguro que es el maldito Internet Explorer',
            'Cuantos días tarda en llegar esto?',
            'Hay que pagar aduanas?',
            'A ver si leemos que la gente ha preguntado varias veces la misma cosa',
            '[img]http://i.imgur.com/XDJR4AM.gif[/img]',
            'Creo que he llegado tarde',
            'Me lo pillo',
            'Que cosa mas horrorosa',
            'Esto es un timo lo he pedido y no me ha llegado',
            'Creo que he visto alguno mejor, pero no esta mal',
            'Baneadlo de una vez!!!',
            'Vaya troll',
            'Que bien me lo paso en este foro',
            'Si',
            'No',
            'No me entero de nada',
            'Vaya películas se monta la gente jajaja',
            'Reportado',
            'Pero que me estás container',
            'Las críticas no serán agradables, pero son necesarias',
            'Espera que tomo nota en mi maquina de escribir invisible',
        );


        $dealsArray = array(
            'Se puede pagar a contra reembolso?',
            'Buena oferta, un mini punto para ti',
            'Menudo timo de oferta',
            'Yo me he comprado cinco, gracias!!!',
            'Este producto sale bastante mal, yo no me lo compraría',
            'Yo he votado negativo, no me parece muy buena oferta :crazy:',
            'No compro ahí ni loco :devil:',
            'Gracias por compartir la oferta!!! :kissy:',
            'Para compartir esta oferta mejor no hubieses compartido nada!!',
            'Has buscado primero en google shopping? está mas barato en otras tiendas',
            'Ofertaza!!! me compro un camión entero',
            'A gastar se ha dicho!!!

                                [img]http://i.imgur.com/xiOO1EQ.gif[/img]',
            '[img]http://i.imgur.com/wqRCpYI.gif[/img]',
            'Yo no compraría en esa tienda

                                [img]http://i.imgur.com/g6pNPYN.gif[/img]',
            'Buena oferta:

                                P.D.:

                                [spoiler]No [/spoiler]',


            'Esta oferta ya la han puesto como mil veces',
            'La próxima vez busca aquí:

                            [url=www.google.es/shopping]Google Shopping[/url]',

            'Alguien tiene un vale descuento para esto?',
            'No me lo compro ni con tu dinero',
            'Muy caro',
            'Esta tirado',
            'En ebay esta más barato seguro'
        );
        $dealsArray = array_merge($dealsArray, $commontDealFreeVoucher);


        $vouchersArray= array(
            'Se puede pagar a contra reembolso?',
            'Hasta con el descuento me parece caro',
            'Yo me he comprado cinco con el cupón, gracias!!!',
            'Este producto sale bastante mal, yo no me lo compraría',
            'Yo he votado negativo, no me funciona :crazy:',
            'No compro ahí ni loco :devil:',
            'Gracias por compartir el cupón!!! :kissy:',
            'Para compartir este cupón mejor no hubieses compartido nada!!',
            'Has buscado primero en google shopping? está mas barato en otras tiendas sin el descuento',
            'Ofertaza!!! me compro un camión entero',
            'A gastar se ha dicho!!!

                                [img]http://i.imgur.com/xiOO1EQ.gif[/img]',
            '[img]http://i.imgur.com/wqRCpYI.gif[/img]',
            'Yo no compraría en esa tienda

                                [img]http://i.imgur.com/g6pNPYN.gif[/img]',
            'Buen cupón:

                                P.D.:

                                [spoiler]No [/spoiler]',

            'La próxima vez busca aquí:

                            [url=www.google.es/shopping]Google Shopping[/url]',
            'No me lo compro ni con tu dinero',
            'Ha caducado ya el cupón?',
            'Se puede pagar a contra reembolso?',
            'Muy caro',
            'Esta tirado',
            'En ebay esta más barato seguro',
            'Hay que gastar un mínimo para usar el cupón?',
            'Donde se mete el cupón? ',
            'Son acumulables los cupones?',
            'Hay que imprimir el vale?',
            'Vaya descuento mas raquítico',
            'Se puede usar en tiendas físicas?'
        );
        $vouchersArray = array_merge($vouchersArray, $commontDealFreeVoucher);


        $freebiesArray = array('Gratis!!!!',
            'Para compartir esta cosa mejor no hubieses compartido nada!!',
            'Este producto sale bastante mal',
            'Yo he votado negativo, no me funciona :crazy:',
            'Gracias por compartir!!! :kissy:',
            'Its Free!!',
            'Menos da una piedra',
            'Ni me molesto',
            'Pa la buchaca',
            'Ni con un palo',
            '[img]http://i.imgur.com/gnK4hZO.gif[/img]',
            '[img]http://i.imgur.com/aEjkeOM.gif[/img]',
            '[img]http://i.imgur.com/A05Xy4y.gif[/img]',
            '[img]http://i.imgur.com/nnkdM6K.gif[/img]',
            '[img]http://i.imgur.com/hC8VL9D.gif[/img]',
            'No me lo compro ni con tu dinero',
            'Cuanto tiempo tarda en llegar?',
            'Por algo es gratis',
            'Nadie regala duros a cuatro pesetas',
            'Gratis total?',
            'Me pillo 100 y monto tienda en ebay',
            'FREE FREE FREE',
            'GRATIS!!!!!!!!!!!',
            'Seguro que es gratis?',
            'Siendo gratis no puedo perder nada',
            'Esto tiene truco

             [img]http://i.imgur.com/g6pNPYN.gif[/img]');
        $freebiesArray = array_merge($freebiesArray, $commontDealFreeVoucher);


        $commontMiscFeedModAdmin = array(
            'A ver si un admin se pasa por aquí ',
            'Baneadlo de una vez!!!',
            'Vaya troll',
            '+1',
            '-1',
            ':laugh:',
            'Cierto toda la razón',
            'No sabría que decirte ',
            'Menudo hilo',
            'Que bien me lo paso en este foro',
            'Si',
            'No',
            'No me entero de nada',
            'Vaya películas se monta la gente jajaja',
            'Reportado',
            '[img]http://i.imgur.com/rlKPy7D.gif[/img]',
            '[img]http://i.imgur.com/A05Xy4y.gif[/img]',
            '[img]http://i.imgur.com/bQDYeA1.gif[/img]',
            'Pero que me estás container',
            'Pero,¿que puedo hacer?, soy solo un hombre',
            'Ya estaba así cuando llegue',
            '¡Lisa!, ¡en esta casa respetamos las leyes de la termodinámica!',
            'Niños... os esforzasteis ¡y fracasasteis miserablemente! La moraleja es: NUNCA OS ESFORCEIS...',
            'Consiste en lanzar aros',
            'Momento que soy lento...',
            'Un fanático es alguien que no puede cambiar de opinión y no quiere cambiar de tema',
            'El éxito es aprender a ir de fracaso en fracaso sin desesperarse',
            'Una buena conversación debe agotar el tema, no a los interlocutores',
            'Soy optimista. No parece muy útil ser otra cosa.',
            'Las críticas no serán agradables, pero son necesarias',
            'Espera que tomo nota en mi maquina de escribir invisible',
            'Que cansino'
        );


        $miscArray = $commontMiscFeedModAdmin;


        $feedbackArray = array(
            'Pues a mi me va bien',
            'Me pasa lo mismo',
            'Otro por aquí con el mismo problema',
            'Seguro que es por usar el internet explorer, pásate a firefox!!!!',
            'Nadie es perfecto',
            'Has borrado la cache?',
            'Has apagado y encendido otra vez el router',
            'Has apagado y encendido otra vez el ordenador',
            'Eso es un virus, seguro!!!'
        );
        $feedbackArray = array_merge($feedbackArray, $commontMiscFeedModAdmin);


        $modArray = array(
            'Ban ya!!!',
            'Yo no le pondría un ban ',
            'Es un troll pero es soportable',
            'Yo le haría moderador');
        $modArray = array_merge($modArray, $commontMiscFeedModAdmin);


        $adminArray = array(
            'Deberíamos borrar la cache',
            'Ese error es de la cache seguro',
            'Igual es error de algún bundle'
        );
        $adminArray = array_merge($adminArray, $commontMiscFeedModAdmin);

        switch($forum->getForumTopicsType()){
            case 'TopicDeal':
                $random = array_rand($dealsArray, 1);
                return $dealsArray[$random];
                break;

            case 'TopicVoucher':
                $random = array_rand($vouchersArray, 1);
                return $vouchersArray[$random];
                break;

            case 'TopicFree':
                $random = array_rand($freebiesArray, 1);
                return $freebiesArray[$random];
                break;
            default:
                switch($forum->getSlug()){
                    case 'miscelanea':
                        $random = array_rand($miscArray, 1);
                        return $miscArray[$random];
                        break;

                    case 'feedback':
                        $random = array_rand($feedbackArray, 1);
                        return $feedbackArray[$random];
                        break;

                    case 'moderacion':
                        $random = array_rand($modArray, 1);
                        return $modArray[$random];
                        break;

                    case 'administracion':
                        $random = array_rand($adminArray, 1);
                        return $adminArray[$random];
                        break;

                    default:
                        $random = array_rand($commontMiscFeedModAdmin, 1);
                        return $commontMiscFeedModAdmin[$random];
                }
        }
    }
}