<?php

namespace Deal\PostBundle\DataFixtures\ORM;

use Deal\PostBundle\Entity\ReportReason;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/*
 * Fixtures/Datos de prueba para la entidad report.
 * En este fixture para cada post/mensaje reportado creo dicho reporte y le asigno datos como quien reporta, motivo, etc
 **/
class ReportReasons extends AbstractFixture implements OrderedFixtureInterface
{
    // Orden en el que se carga este fixture
    public function getOrder()
    {
        return 255;
    }

    public function load(ObjectManager $manager)
    {
        // Array con los nombres de los motivos
        $reasons = array     (  'Spam',
                                'Insultos',
                                'Contenido Inapropiado',
                                'Otro',
        );

        // Creo nuevas entidades de tipo ReportReason y relleno con su nombre
        foreach ($reasons as $reason){
            $entity = new ReportReason();

            $entity->setName($reason);

            // Hago la entidad persistente
            $manager->persist($entity);

        }// Fin foreach

        // Guardo los datos marcados como persistentes
        $manager->flush();
    }
}