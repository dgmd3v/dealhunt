<?php

namespace Deal\PostBundle\ModerationTools;

use Doctrine\ORM\EntityManager;
use Deal\PostBundle\Entity\Post as Post;
use Deal\UserBundle\Entity\ModerationNotification;

/*
 * Clase encargada de marcar como oculto o no mensajes/posts
 **/
class PostHide {

    // Inyecto el EntityManager para poder hacer operaciones en la base de datos
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /*
     * Función encargada ocultar mensajes
     *
     * @param Post $post mensaje a ocultar
     * */
    public function hide(Post $post){

        // Marco el tema como ocultado:
        $post->setIsHidden(true);

        // Creo la notificación por la ocultación del mensaje-post
        $this->moderationNotification($post);

        // Persisto el post
        $this->em->persist($post);

        // Guardo los cambios en la base de datos
        $this->em->flush();
    }

    /*
     * Función encargada des-ocultar mensajes
     *
     * @param Post $post mensaje a des-ocultar
     * */
    public function unHide(Post $post){

        // Marco el tema como NO ocultado:
        $post->setIsHidden(false);

        // Persisto el post
        $this->em->persist($post);

        // Guardo los cambios en la base de datos
        $this->em->flush();
    }

    /*
     * Función encargada de crear y persistir la notificación por moderación
     *
     * @param Post $post mensaje por el cual se crea la notificación
     * */
    public function moderationNotification(Post $post){
        // Creo una notificación para avisar al usuario que su mensaje ha sido ocultado
        $moderationNotification = new ModerationNotification();

        $moderationNotification->setPost($post);

        $moderationNotification->setDate(new \DateTime('now'));
        $moderationNotification->setUser($post->getPoster());

        // Persisto la notificación
        $this->em->persist($moderationNotification);
    }
} 