<?php

namespace Deal\PostBundle\ModerationTools;

use Doctrine\ORM\EntityManager;
use Deal\PostBundle\Entity\ReportReason as ReportReason;

/*
 * Clase encargada de gestionar/preparar el borrado de una razón de reporte
 **/
class ReportReasonRemover {

    // Inyecto el EntityManager para poder hacer operaciones en la base de datos
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /*
     * Función que cambia la razón de reporte dada en los reportes por la razón de reporte Otro
     *
     * @param ReportReason $reportReason razón del reporte
     * */
    public function changeReportNotNullReasonToOther (ReportReason $reportReason){
        // Busco los reportes con la razón de reporte dada
        $reports = $this->em->getRepository('PostBundle:Report')->findAllReportsByReason($reportReason);

        if($reports != null){
            $this->changeReportsReasonToOther($reports);
        }
    }

    /*
     * Función que cambia la razón de reporte por la razón de reporte Otro cuando es null
     * */
    public function changeReportNullReasonToOthers (){
        // Busco los reportes con la razón de reporte dada
        $reports = $this->em->getRepository('PostBundle:Report')->findAllReportsWithNullReason();

        if($reports != null){
            $this->changeReportsReasonToOther($reports);
        }
    }

    /*
     * Función que cambia la razón de reporte por la razón de reporte Otro en un array de reportes, si no existe Otro
     * lo crea
     *
     * @param array $reports array de reportes donde tengo que cambiar la razón del reporte
     * */
    protected function changeReportsReasonToOther($reports){
        // Busco la razón de reporte Otro
        $otherReason = $this->em->getRepository('PostBundle:ReportReason')->findReportReasonByName('Otro');

        // Si no la encuentro la creo
        if($otherReason == null){
            $newReportReason = new ReportReason();
            $newReportReason->setName('Otro');

            $this->em->persist($newReportReason);

            // Guardo los cambios en la base de datos
            $this->em->flush();

            $otherReason = $newReportReason;
        }

        // Cambio la razón de reporte de los reportes
        foreach ($reports as $report) {
            $report->setReportReason($otherReason);
            $this->em->persist($report);
        }

        // Guardo los cambios en la base de datos
        $this->em->flush();
    }


} 