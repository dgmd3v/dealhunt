<?php

namespace Deal\PostBundle\ModerationTools;

use Doctrine\ORM\EntityManager;
use Deal\PostBundle\Entity\Report as Report;

/*
 * Clase encargada gestionar los reportes de los posts
 **/
class ReportPost {

    // Inyecto el EntityManager para poder hacer operaciones en la base de datos
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /*
     * Función encargada reportar mensajes
     *
     * @param Post $reportedPost mensaje a reportar
     * @param User $user usuario que reporta
     * @param ReportReason $reportReason razón del reporte
     * */
    public function reportPost($reportedPost, $user, $reportReason){

        // Creo un nuevo reporte
        $report = new Report();

        // Seteo el reporte con el post reportado, el usuario que lo está reportando, la razón del reporte y la fecha
        // del reporte
        $report->setPost($reportedPost);
        $report->setUser($user);
        $report->setReportReason($reportReason);
        $report->setDate(new \DateTime('now'));

        // Hago la entidad persistente
        $this->em->persist($report);

        // Guardo los datos marcados como persistentes
        $this->em->flush();
    }

} 