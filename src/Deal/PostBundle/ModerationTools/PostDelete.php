<?php

namespace Deal\PostBundle\ModerationTools;

use Doctrine\ORM\EntityManager;
use Deal\PostBundle\Entity\Post as Post;
use Deal\UserBundle\Entity\ModerationNotification;

/*
 * Clase encargada de marcar como borrado o no mensajes/posts
 **/
class PostDelete {

    // Inyecto el EntityManager para poder hacer operaciones en la base de datos
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /*
     * Función encargada del borrado de mensajes
     *
     * @param Post $post mensaje a borrar
     * */
    public function delete(Post $post){

        // Marco el tema como borrado
        $post->setIsDeleted(true);

        // Creo la notificación por el borrado del mensaje-post
        $this->moderationNotification($post);

        // Persisto el post
        $this->em->persist($post);

        // Guardo los cambios en la base de datos
        $this->em->flush();
    }

    /*
     * Función encargada del des-borrado del mensajes
     *
     * @param Post $post mensaje a borrar
     * */
    public function unDelete (Post $post){

        // Marco el tema como NO ocultado:
        $post->setIsDeleted(false);

        // Persisto el post
        $this->em->persist($post);

        // Guardo los cambios en la base de datos
        $this->em->flush();
    }

    /*
     * Función encargada de crear y persistir la notificación por moderación
     *
     * @param Post $post mensaje por el cual se crea la notificación
     * */
    public function moderationNotification(Post $post){
        // Creo una notificación para avisar al usuario que su mensaje ha sido eliminado
        $moderationNotification = new ModerationNotification();

        $moderationNotification->setPost($post);

        $moderationNotification->setDate(new \DateTime('now'));
        $moderationNotification->setUser($post->getPoster());

        // Persisto la notificación
        $this->em->persist($moderationNotification);
    }
} 