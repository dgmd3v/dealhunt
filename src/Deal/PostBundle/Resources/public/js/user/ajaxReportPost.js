/*
 * Función destinada a ejecutar la acción del controlador encargada reportar un mensaje/post y mostrar un mensaje
 * de información
 * */
$("#dialogReportModal").on('submit',
    function(event) {

        var form = $("#dialogReportModal form");
        var path = form.attr("action");
        var dialogReportModal = $("#dialogReportModal");
        var postNumber = dialogReportModal.attr("data-postnumber");
        var postId = dialogReportModal.attr("data-postid");
        var resultModal = $('#resultModal');

        $.ajax({
            type: "POST",
            url: path,
            data: {  form: form.serialize(), postId: postId, postNumber: postNumber },
            dataType: "json",
            success: function(response) {
                if (response.responseCode == 200) {

                    dialogReportModal.modal('hide');

                    resultModal.find('.modal-body').text(response.successMsg);
                    resultModal.find('.modal-title').text('Felicidades!!!');
                    resultModal.modal();
                }

                else if (response.responseCode == 400) {
                    dialogReportModal.modal('hide');

                    resultModal.find('.modal-title').text("Error");
                    resultModal.find('.modal-body').text("Error: " + response.errorMsg);
                    resultModal.modal();
                }
            }
        });

        event.preventDefault();
    }
);