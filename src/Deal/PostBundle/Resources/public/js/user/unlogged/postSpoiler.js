/*
* Función destinada a mostrar/ocultar un spoiler
* **/
$(document).on('click', '.spoiler',
    function (e) {
        e.preventDefault();
        var spoilerLink = $(this); // Spoiler que lanzó el este js
        spoilerLink.next().toggleClass('hidden-content');
        }
);