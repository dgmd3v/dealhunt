/*
 * Función destinada a mostrar/ocultar un mensaje
 * **/
$(document).on('click', '.hidden-post-notice a',
    function(e){
        e.preventDefault();
        var postId = '#post-' + $(this).attr('id');

        $(postId).animate({
            height: [ "toggle", "swing" ],
            opacity: "toggle"
        }, 700, "linear");
    }
);
