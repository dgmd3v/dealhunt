/*
* Función destinada a resaltar el mensaje solicitado en la URL
* **/
$(document).ready(function()
{
    var postNumber = window.location.hash.replace('#', '');

    if(postNumber && postNumber != 'comentarios') {
        var nextPostNumber = parseInt(postNumber) + 1;

        if($( '#post-' + nextPostNumber ).length){
            $('#post-' + postNumber).addClass('requested-post');
        }

        else{
            $('#post-' + postNumber).addClass('requested-post-is-latest');
        }

    }
});