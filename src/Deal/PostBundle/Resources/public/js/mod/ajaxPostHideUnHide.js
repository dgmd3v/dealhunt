/*
* Función destinada a ejecutar la acción del controlador encargada de ocultar/des-ocultar un post/mensaje y mostrar un
* mensaje de información
* */
$(document).on('click', '.ajaxPostHideUnHide',
    function() {
                var dialogModalAcceptButton = $("#dialogModalAcceptButton");
                var path = dialogModalAcceptButton.attr("href");
                var postId = dialogModalAcceptButton.attr("data-postid");
                var action = dialogModalAcceptButton.attr("data-action");

                var resultModal = $('#resultModal');

                $.ajax({
                    type: "POST",
                    url: path,
                    data: {postId: postId, action: action },
                    dataType: "json",
                    success: function(response) {
                        if (response.responseCode == 200) {

                            resultModal.find('.modal-body').text(response.successMsg);
                            resultModal.find('.modal-title').text('Felicidades!!!');
                            resultModal.modal();

                            var postDiv = $('#post-' + response.postNumber);
                            var hideUnHideButton = postDiv.find('.hide-unhide-button');
                            var hideUnHideButtonSpan = hideUnHideButton.find('span');

                            if(response.action == "ajax-post-hide"){
                                $('#dialogModal').removeClass('ajax-post-hide');
                                dialogModalAcceptButton.removeClass("ajax-post-hide");

                                var hiddenPostDiv =
                                    '<div id="hidden-post-' + response.postNumber +
                                        '" class="hidden-post-notice">' +
                                        '<a title="Click para mostrar mensaje oculto"'+
                                        ' id="' + response.postNumber + '" href="#' + response.postNumber + '"> Mostrar mensaje #' +
                                        response.postNumber + ' ocultado por incumplir las normas</a></div>';
                                $( hiddenPostDiv ).insertBefore( postDiv );

                                postDiv.addClass('hidden-content');

                                var postNumberAnchor = postDiv.find('#' + response.postNumber);
                                postNumberAnchor.attr('id', 'hidden-anchor-'+ response.postNumber);

                                postDiv.removeClass('show_post');

                                hideUnHideButton.attr({
                                    "data-title": "Des-Ocultar Mensaje",
                                    "data-message": "¿Está segur@ de que desea des-ocultar el mensaje #" + response.postNumber + "?",
                                    "data-action": "unhide"
                                });

                                hideUnHideButtonSpan.text(' Des-Ocultar');
                                hideUnHideButtonSpan.attr("title", "Des-Ocultar mensaje");
                                hideUnHideButtonSpan.attr("class", "glyphicon glyphicon-eye-open");
                            }

                            else if(response.action == "ajax-post-unhide"){
                                $('#dialogModal').removeClass('ajax-post-unhide');
                                dialogModalAcceptButton.removeClass("ajax-post-unhide");
                                $('#hidden-post-' + response.postNumber).remove();

                                postDiv.removeClass('hidden-content');

                                var postNumberAnchor = postDiv.find('#hidden-anchor-' + response.postNumber);

                                postNumberAnchor.attr('id', response.postNumber);

                                hideUnHideButton.attr({
                                    "data-title": "Ocultar Mensaje",
                                    "data-message": "¿Está segur@ de que desea ocultar el mensaje #" + response.postNumber + "?",
                                    "data-action": "hide"
                                });

                                hideUnHideButtonSpan.text(' Ocultar');
                                hideUnHideButtonSpan.attr("title", "Ocultar mensaje");
                                hideUnHideButtonSpan.attr("class", "glyphicon glyphicon-eye-close");
                            }


                        }

                        else if (response.responseCode == 400) {
                            resultModal.find('.modal-title').text("Error");
                            resultModal.find('.modal-body').text("Error: " + response.errorMsg);
                            resultModal.modal();
                        }
                    }
                });
            }
    );