/*
 * Función destinada a ejecutar la acción del controlador encargada de des-borrar/borrar un post/mensaje y mostrar un mensaje
 * de información
 * */
$(document).on('click', '.ajaxPostDeleteUndelete',
    function() {
        var dialogModalAcceptButton = $("#dialogModalAcceptButton");
        var path = dialogModalAcceptButton.attr("href");
        var postId = dialogModalAcceptButton.attr("data-postid");
        var action = dialogModalAcceptButton.attr("data-action");

        var resultModal = $('#resultModal');

        $.ajax({
            type: "POST",
            url: path,
            data: {postId: postId, action: action },
            dataType: "json",
            success: function(response) {
                if (response.responseCode == 200) {

                    resultModal.find('.modal-body').text(response.successMsg);
                    resultModal.find('.modal-title').text('Felicidades!!!');
                    resultModal.modal();

                   var postDiv = $('#post-' + response.postNumber);
                   var deleteUnDeleteButton = postDiv.find('.delete_undelete_button');
                   var deleteUnDeleteButtonSpan = deleteUnDeleteButton.find('span');

                    if(response.action == "ajax-post-delete"){
                        $('#dialogModal').removeClass('ajax-post-delete');
                        dialogModalAcceptButton.removeClass("ajax-post-delete");

                        var deletedPostDiv =
                            '<div id="deleted-post-' + response.postNumber +
                                '" class="deleted-post-notice">' +
                                'BORRADO mensaje ' +
                                '<a id="' + response.postNumber + '" title="Mensaje borrado" '+
                                '" href="#' + response.postNumber + '">#' + response.postNumber +
                                '</a> por incumplir las normas</div>';
                        $( deletedPostDiv ).insertBefore( '#post-' + response.postNumber );

                        $('#post-' + response.postNumber).addClass('deleted-post');

                        postDiv.find('.post-number a').removeAttr('id');

                        deleteUnDeleteButton.attr({
                            "data-title": "Des-Borrar Mensaje",
                            "data-message": "¿Está segur@ de que desea des-borrar el mensaje #" + response.postNumber + "?",
                            "data-action": "undelete"
                        });

                        deleteUnDeleteButtonSpan.text(' Des-Borrar');
                        deleteUnDeleteButtonSpan.attr("title", "Des-Borrar mensaje");
                        deleteUnDeleteButtonSpan.attr("class", "glyphicon glyphicon-ok");
                    }

                    else if(response.action == "ajax-post-undelete"){
                        $('#dialogModal').removeClass('ajax-post-undelete');
                        dialogModalAcceptButton.removeClass("ajax-post-undelete");
                        $('#deleted-post-' + response.postNumber).remove();

                        postDiv.removeClass('deleted-post');
                        postDiv.find('.post-number a').attr('id', response.postNumber);
;
                        deleteUnDeleteButton.attr({
                            "data-title": "Borrar Mensaje",
                            "data-message": "¿Está segur@ de que desea borrar el mensaje #" + response.postNumber + "?",
                            "data-action": "delete"
                        });

                        deleteUnDeleteButtonSpan.text(' Borrar');
                        deleteUnDeleteButtonSpan.attr("title", "Borrar mensaje");
                        deleteUnDeleteButtonSpan.attr("class", "glyphicon glyphicon-remove");
                    }
                }

                else if (response.responseCode == 400) {
                    resultModal.find('.modal-title').text("Error");
                    resultModal.find('.modal-body').text("Error: " + response.errorMsg);
                    resultModal.modal();
                }
            }
        });
    }
);