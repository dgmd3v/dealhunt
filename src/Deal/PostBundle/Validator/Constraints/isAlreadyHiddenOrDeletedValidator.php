<?php

namespace Deal\PostBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/*
 * Clase validator que comprueba si se ha marcado un mensaje/post como borrado y como oculto a la vez
 */
class isAlreadyHiddenOrDeletedValidator extends ConstraintValidator
{
    public function validate($entity, Constraint $constraint)
    {
        $isHidden = $entity->getIsHidden();
        $isDeleted = $entity->getIsDeleted();

        // Comprobamos si se han marcado como oculto y como borrado
        if($isHidden == 1 && $isDeleted == 1){
            $this->context->addViolationAt('isHidden',
                $constraint->errorMessage
            );
        }
    }
}
