<?php

namespace Deal\PostBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class isAlreadyHiddenOrDeleted extends Constraint
{
    public $errorMessage = 'Estas intentendo marcar como borrado y oculto un mensaje a la vez, no puedes.';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    public function validatedBy()
    {
        return 'isalreadyhiddenordeleted_validator';
    }
}
