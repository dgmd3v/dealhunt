<?php

namespace Deal\PostBundle\Service;

use Doctrine\ORM\EntityManager;
use Deal\PostBundle\Entity\Post as Post;
use Deal\UserBundle\Entity\QuoteNotification as QuoteNotification;

/*
 * Clase encargada gestionar los quotes/citas
 **/
class PostQuote {

    // Inyecto el EntityManager para poder hacer operaciones en la base de datos
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /*
     * Se encarga de generar citas en mensajes nuevos
     **/
    public function newPostQuotes(Post $newPost){

        // Compruebo si existen citas en el mensaje quito los repetidos
        $quotes = $this->searchQuotes($newPost->getMessage());

        $quotes = array_unique($quotes);

        // Si hay citas en el mensaje las creo
        if(!is_null($quotes)){

            $lastPostNumber = $newPost->getTopic()->getLastPost()->getNumber();
            $quotingUser = $newPost->getPoster();

            foreach($quotes as $quotedPostNumber){

                // No creo notificaciones para mensajes no existentes
                if($quotedPostNumber <= $lastPostNumber){
                    $quotedPost = $this->em->getRepository('PostBundle:Post')->findPostByNumber($newPost->getTopic(), $quotedPostNumber);

                    // No creo notificaciones para las "auto-citas"
                    if($quotedPost->getPoster() != $quotingUser){
                        $quoteNotification = new QuoteNotification();
                        $quoteNotification->setUser($quotedPost->getPoster());
                        $quoteNotification->setPost($newPost);
                        $quoteNotification->setDate(new \DateTime('now'));
                        $quoteNotification->setUser($quotedPost->getPoster());

                        // Hago la entidad persistente
                        $this->em->persist($quoteNotification);
                    }
                }
            }
        }
    }

    /*
     * Se encarga de generar citas de mensajes que están siendo editados
     **/
    public function editPostQuotes(Post $editedPost, $oldPostMessage){

        // Compruebo si existen citas nuevas en el mensaje editado
        $editedPostQuotes = $this->searchQuotes($editedPost->getMessage());
        $oldPostQuotes = $this->searchQuotes($oldPostMessage);

        $newQuotes = $this->searchNewQuotes($editedPostQuotes, $oldPostQuotes);

        $newQuotes = array_unique($newQuotes);

        // Si hay citas nuevas en el mensaje editado creo las notificaciones
        if(!is_null($newQuotes)){

            $lastPostNumber = $editedPost->getTopic()->getLastPost()->getNumber();
            $quotingUser = $editedPost->getPoster();

            foreach($newQuotes as $quotedPostNumber){

                // No creo notificaciones para mensajes no existentes
                if($quotedPostNumber <= $lastPostNumber){

                    $quotedPost = $this->em->getRepository('PostBundle:Post')
                                                        ->findPostByNumber($editedPost->getTopic(), $quotedPostNumber);

                    // No creo notificaciones para las "auto-citas"
                    if($quotedPost->getPoster() != $quotingUser){
                        $quoteNotification = new QuoteNotification();
                        $quoteNotification->setUser($quotedPost->getPoster());
                        $quoteNotification->setPost($editedPost);
                        $quoteNotification->setDate(new \DateTime('now'));
                        $quoteNotification->setUser($quotedPost->getPoster());

                        // Hago la entidad persistente
                        $this->em->persist($quoteNotification);
                    }
                }
            }
            // Guardo los datos marcados como persistentes
            $this->em->flush();
        }
    }

    /*
     * Función que busca citas en mensajes
     **/
    public function searchQuotes ($postMessage){

        preg_match_all('/\B#(\d+)(?!\B)/', $postMessage, $quotes);

        return $quotes[1];
    }

    /*
     * Función que busca citas nuevas en mensajes que están siendo editados
     * */
    public function searchNewQuotes($editedPostQuotes, $oldPostQuotes){

        $result = array();

        foreach ($editedPostQuotes as $editedPostQuote) {

            $key = array_search($editedPostQuote, $oldPostQuotes);

            if($key === false) $result[] = $editedPostQuote;

            else{
                unset($oldPostQuotes[$key]);
            }
        }
        return $result;
    }
} 