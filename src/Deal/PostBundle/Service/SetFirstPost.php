<?php

namespace Deal\PostBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Deal\TopicBundle\Entity\Topic;
use Deal\PostBundle\Entity\Post;

/*
 * Clase encargada setear el primer mensaje del tema y actualizar datos de usuario
 **/
class SetFirstPost {

    // Inyecto el contenedor de servicios
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /*
     * Función que rellena el primer mensaje del tema y lo setea en el tema
     **/
    public function set(Topic $topic, Post $post){

        // Obtengo el usuario logueado a través del token creado por el componente de seguridad
        $user = $this->container->get('security.context')->getToken()->getUser();

        $post->setNumber(1);
        $post->setPostingDate(new \DateTime('now'));
        $post->setTopic($topic);
        $messageIp = $this->container->get('request')->getClientIp();
        $post->setMessageIp($messageIp);
        $post->setPoster($user);

        // Actualizo datos del usuario
        $userProfile = $user->getUserProfile();

        $userProfile->setPosts($user->getUserProfile()->getPosts()+1);
        $userProfile->setTopicsStarted($user->getUserProfile()->getTopicsStarted()+1);

        // "Seteo" la Id del primer y último mensaje del tema
        $topic->setfirstPost($post);
        $topic->setLastPost($post);

        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->persist($userProfile);
        $em->persist($topic);
    }
} 