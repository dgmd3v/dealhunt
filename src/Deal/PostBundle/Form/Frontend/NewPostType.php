<?php

namespace Deal\PostBundle\Form\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/*
 * Formulario para la creación de posts/mensajes
 **/
class NewPostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('message', 'textarea', array(
            'attr'  => array( 'placeholder' => "Escribe aquí tu mensaje", 'rows' => '7'),
            'horizontal_input_wrapper_class' => 'col-xs-12',
            'label'=> false,
        ));
    }

    /*
     * Esta función indica el namespace de la entidad que vamos a modificar con este formulario.
    **/
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array('data_class'=> 'Deal\PostBundle\Entity\Post'));
    }

    public function getName()
    {
        return 'deal_postbundle_newposttype';
    }
} 