<?php

namespace Deal\PostBundle\Form\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/*
 * Formulario para reportar un post/mensaje
 **/
class ReportPostType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('reportReason', 'entity', array(
                'label' => 'Motivo',
                'class' => 'PostBundle:ReportReason',
                'property'  => 'name',
            ))
        ;
    }

    /*
     * Esta función indica el namespace de la entidad que vamos a modificar con este formulario.
     **/
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class'        => 'Deal\PostBundle\Entity\Report',
                'validation_groups' => array('Default')
            )
        );
    }

    public function getName()
    {
        return 'deal_postbundle_reportposttype';
    }

} 