<?php

namespace Deal\PostBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Deal\PostBundle\Entity\Post;
use Deal\PostBundle\Form\Frontend\NewPostType;
use Symfony\Component\Security\Core\SecurityContext;
use Deal\PostBundle\Form\Frontend\ReportPostType;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/*
 * Clase encargada de las acciones “habituales” relacionadas con los mensajes
 * */
class DefaultController extends Controller
{
    /*
     * Función encargada de crear nuevos mensajes
     *
     * @param integer $topicId id del tema donde se va a crear el mensaje
     **/
    public function newPostAction($topicId)
    {
        // Obtengo el usuario logueado a través del token creado por el componente de seguridad
        $user = $this->get('security.context')->getToken()->getUser();

        // Si se está intentando crear un post/mensaje sin ser usuario muestro un mensaje informando del error y
        // redirijo al login
        if($user == null || !$this->get('security.context')->isGranted('ROLE_USER')){
            $this->get('session')->getFlashBag()->add('info',
                'Para publicar un mensaje debes haberte registrado y estar logueado'
            );

            $request = $this->container->get('request');
            $session = $request->getSession();
            $session->set('requestedPage', $request->getPathInfo());

            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getDoctrine()->getManager();

        // Busco el tema donde se va a publicar el nuevo mensaje
        $topic = $em->getRepository('TopicBundle:Topic')->findTopicById($topicId);

        if(!$topic){
            $this->get('session')->getFlashBag()->add('error',
                'El tema en el que quieres publicar un mensaje no existe.'
            );

            return $this->redirect($this->generateUrl('forum_front_page'));
        }

        if(!$this->get('security.context')->isGranted('ROLE_ACCESS_FORUM', $topic->getForum())){
            Throw new AccessDeniedHttpException('No tienes permisos suficientes para acceder a esta zona.');
        }

        // Solo puede postear en un tema cerrado un moderador
        if(($topic->getIsClosed()==1) && !$this->get('security.context')->isGranted('ROLE_MODERATOR')){
            Throw new AccessDeniedHttpException('No tienes permisos suficientes para escribir en un tema cerrado.');
        }

        $request = $this->getRequest();

        $post = new Post();

        $form = $this->createForm(new NewPostType(), $post);

        // Relaciono el formulario con los datos introducidos por el usuario en el mismo
        $form->handleRequest($request);

        // Compruebo que los datos enviados son correctos
        if($form->isValid()){

            // "Seteo" el mensaje, el usuario que lo publica, etc, etc
            $post->setPostingDate(new \DateTime('now'));
            $post->setTopic($topic);
            $post->setPoster($user);
            $messageIp = $this->container->get('request')->getClientIp();
            $post->setMessageIp($messageIp);
            $postNumber = $topic->getLastPost()->getNumber()+1;
            $post->setNumber($postNumber);

            $topic->setLastPost($post);

            $user->getUserProfile()->setPosts($user->getUserProfile()->getPosts()+1);

            // Guardo los datos en la base de datos
            $em->persist($topic);
            $em->persist($user);
            $em->persist($post);

            // Creo las notificaciones por las citas del mensaje si las hubiese
            $this->container->get('deal.postbundle.service.postquote')->newPostQuotes($post);

            $em->flush();

            // Mensaje flashbag informando de que se ha creado el mensaje/posts correctamente
            $this->get('session')->getFlashBag()->add('success',
                'Has posteado correctamente'
            );

            // Obtengo el objeto del entity manager para realizar consultas
            $globalVariables = $em->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
            $maxPostsPerTopicPage = $globalVariables->getMaxPostsPerTopicPage();

            $pageNumber = $this->container->get('deal.forumbundle.twig.extension.dealextension')->postPageNumber(
                $postNumber,
                $maxPostsPerTopicPage
            );

            return $this->redirect($this->generateUrl(  'topic_show_topic_page',
                    array(  'forumSlug'     => $topic->getForum()->getSlug(),
                            'topicSlug'     => $topic->getSlug(),
                            'topicId'       => $topicId,
                            'pageNumber'    => $pageNumber
                        )) . '#' . $postNumber);
        }

        // En caso de no introducir los datos de forma valida devuelvo al usuario al formulario de registro
        return $this->render(   'PostBundle:Default:replyForm.html.twig',
            array(  'form'          => $form->createView(),
                    'topic'         => $topic,
                    'forumSlug'     => $topic->getForum()->getSlug(),
                    'category'      => $topic->getCategory(),
                    'criterionSlug' => 'mas-votados'
                 )
        );
    }

    /*
     * Función encargada de editar mensajes
     *
     * @param integer $topicId id del tema donde se encuentra el mensaje a editar
     * @param integer $postId id del mensaje a editar
     **/
    public function editPostAction($topicId, $postId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getDoctrine()->getManager();

        // Busco el post que se va a editar
        $post = $em->getRepository('PostBundle:Post')->findPostById($postId);

        if(!$post){
            $this->get('session')->getFlashBag()->add('error',
                'El tema en el que quieres editar un mensaje no existe.'
            );

            return $this->redirect($this->generateUrl('forum_front_page'));
        }

        if(!$this->get('security.context')->isGranted('ROLE_ACCESS_FORUM', $post->getTopic()->getForum())){
            Throw new AccessDeniedHttpException('No tienes permisos suficientes para acceder a esta zona.');
        }

        // Antiguo mensaje del post
        $oldPostMessage = $post->getMessage();

        // Tema donde se está editando el mensaje
        $topic = $post->getTopic();

        // Foro donde se está editando el mensaje
        $forum = $topic->getForum();

        // (o el usuario que intenta editar no es el propietario del post) muestro un mensaje informando del error y
        // redirijo al login
        if(!$this->get('security.context')->isGranted('ROLE_EDIT_POST', $post)){
            $this->get('session')->getFlashBag()->add('info',
                'Para editar un mensaje debes haberte registrado, estar logueado,'
                . ' que el mensaje no este oculto/borrado ni en un tema cerrado y sea tuyo'
            );

            $globalVariables = $em->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
            $maxPostsPerTopicPage = $globalVariables->getMaxPostsPerTopicPage();

            $pageNumber = $this->container->get('deal.forumbundle.twig.extension.dealextension')->postPageNumber(
                $post->getNumber(),
                $maxPostsPerTopicPage
            );

            return $this->redirect($this->generateUrl(  'topic_show_topic_page',
                    array(  'forumSlug'     => $post->getTopic()->getForum()->getSlug(),
                        'topicSlug'     => $post->getTopic()->getSlug(),
                        'topicId'       => $topicId,
                        'pageNumber'    => $pageNumber
                    )));
        }

        $form = $this->createForm(new NewPostType(), $post);

        // Relaciono el formulario con los datos introducidos por el usuario en el mismo
        $request = $this->getRequest();
        $form->handleRequest($request);

        $globalVariables = $em->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
        $maxPostsPerTopicPage = $globalVariables->getMaxPostsPerTopicPage();

        $pageNumber = $this->container->get('deal.forumbundle.twig.extension.dealextension')->postPageNumber(
            $post->getNumber(),
            $maxPostsPerTopicPage
        );

        // Compruebo que los datos enviados son correctos
        if($form->isValid()){
            // "Seteo" la hora de edición del mensaje/post
            $post->setEditDate(new \DateTime('now'));

            // Guardo los datos en la base de datos
            $em->persist($post);

            // Creo las notificaciones por las citas del mensaje si las hubiese
            $this->container->get('deal.postbundle.service.postquote')->editPostQuotes($post, $oldPostMessage);

            $em->flush();

            // Mensaje flashbag informando de que se ha creado el mensaje/posts correctamente
            $this->get('session')->getFlashBag()->add('success',
                'El mensaje ha sido editado correctamente'
            );

            return $this->redirect($this->generateUrl(  'topic_show_topic_page',
                array(  'forumSlug'     => $forum->getSlug(),
                        'topicSlug'     => $topic->getSlug(),
                        'topicId'       => $topicId,
                        'pageNumber'    => $pageNumber
                )) . '#' . $post->getNumber());
        }

        // En caso de no introducir los datos de forma valida devuelvo al usuario al formulario de registro
        return $this->render(   'PostBundle:Default:editPostForm.html.twig',
            array(  'form'          => $form->createView(),
                    'topic'         => $topic,
                    'forumSlug'     => $forum->getSlug(),
                    'category'      => $topic->getCategory(),
                    'postId'        => $post->getId(),
                    'pageNumber'    => $pageNumber,
                    'postNumber'    => $post->getNumber(),
                    'criterionSlug' => 'mas-votados'
            )
        );
    }

    /*
     * Función encargada de mostrar el formulario de reporte
     **/
    public function reportFormModalAction(){
        $form = $this->createForm(new ReportPostType());

        // Devuelvo el formulario vacío a la  plantilla
        return $this->render(   '@Post/Default/includes/reportModal.html.twig',
            array(  'reportForm'      => $form->createView(),
            )
        );
    }

}
