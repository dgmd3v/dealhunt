<?php

namespace Deal\PostBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Response;

/*
 * Clase encargada de las acciones relacionadas con los mensajes en las que se usa AJAX y
 * que pueden usar todos los usuarios registrados del foro
 * */
class AjaxController extends Controller
{
    /*
     * Acción encargada de reportar un post/mensaje
     *
     * @param integer $postId Id del mensaje reportado
     * @param integer $postNumber número del post/mensaje reportado
     **/
    public function ajaxReportPostAction()
    {
        $request = $this->getRequest();

        // Si se trata de una petición AJAX serializo los resultados de la consulta a la base de datos
        // y devuelvo la respuesta
        if ($request->isXmlHttpRequest()) {

            // Obtengo el usuario logueado a través del token creado por el componente de seguridad
            $user = $this->get('security.context')->getToken()->getUser();

            // Llamo al serializador para poder utilizarlo
            $serializer = $this->container->get('jms_serializer');

            // Si no se encuentra al usuario, no es un usuario registrado y logueado
            if($user == null || !$this->get('security.context')->isGranted('ROLE_USER')){
                $responseArray = array( "responseCode" => 400,
                                        "errorMsg" => 'Para poder reportar debes estar registrado, logueado');
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Obtengo el objeto del entity manager para realizar consultas
            $em = $this->getDoctrine()->getManager();

            // Solicito los datos pasados a través del script jquery, en este caso el Id del tema, el número del post
            // reportado
            $postId = $request->request->get('postId');
            $postNumber = $request->request->get('postNumber');
            $serializedForm = $request->request->get('form');

            // Paso los datos serializados a un array y los utilizo
            parse_str($serializedForm, $form);

            $form = reset($form);

            $reportReasonId = $form['reportReason'];

            // Usuario que reporta
            $user = $this->getUser();

            // Busco el post a reportar
            $reportedPost = $em->getRepository('PostBundle:Post')->findOnlyPostById($postId);

            // Si no existiese el mensaje reportado
            if (!$reportedPost) {
                $responseArray = array("responseCode" => 400, "errorMsg" => "El post que quieres reportar no existe.");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Busco si existen mas de 10 reportes del mismo usuario en un mismo día
            $todaysReports = $em->getRepository('PostBundle:Report')->findTodaysReportsByUser($user);

            // Si el usuario ya hubiese reportado 10 veces hoy se lo indico
            $globalVariables = $em->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
            $maxReportsByDay = $globalVariables->getMaxReportsPerDay();

            if ($todaysReports == $maxReportsByDay) {
                $responseArray = array( "responseCode" => 400,
                                        "errorMsg" =>   "Ya has reportado " . $maxReportsByDay .
                                                        " veces hoy, no se permiten más reportes por día.");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Busco si existen reportes del mismo usuario al mismo post
            $alreadyReportedByUser = $em->getRepository('PostBundle:Report')->
                                                                        findAlreadyReportedByUser($reportedPost, $user);

            // Si el usuario ya hubiese reportado el post/mensaje se lo indico
            if ($alreadyReportedByUser > 0) {
                $responseArray = array( "responseCode" => 400,
                                        "errorMsg" => "Ya habías reportado este mensaje previamente.");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Busco el motivo del reporte
            $reportReason = $em->getRepository('PostBundle:ReportReason')->findReportReasonById($reportReasonId);

            // Si no existiese la razón de reporte muestro un error
            if (!$reportReason) {
                $responseArray = array("responseCode" => 400, "errorMsg" => "El motivo/razón de reporte ya no existe.");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Si encuentro el post/mensaje lo reporto
            $this->get('deal.postbundle.moderationtools.reportpost')->reportPost($reportedPost, $user, $reportReason);

            $responseArray = array( "responseCode" => 200,
                                    "successMsg" => "El mensaje " . $postNumber . " ha sido reportado con éxito.");
            $jsonContent = $serializer->serialize($responseArray, 'json');

            return new Response($jsonContent);
        }

        // En caso de no ser una petición AJAX retorno un mensaje de error
        return new Response('Esto no es ajax!', 400);
    }


    /*
     * Muestra el contenido de un quote/comentario
     *
     * @param integer $postId Id del post que se está citando
     * @param integer $postNumber número del post/mensaje citado
     **/
    public function ajaxShowQuoteAction()
    {
        $request = $this->getRequest();

        // Si se trata de una petición AJAX serializo los resultados de la consulta a la base de datos
        // y devuelvo la respuesta
        if ($request->isXmlHttpRequest()) {
            // Obtengo el objeto del entity manager para realizar consultas
            $em = $this->getDoctrine()->getManager();

            // Solicito los datos pasados a través del script jquery, en este caso el id del post/mensaje citado
            $postNumber = $request->request->get('postNumber');
            $topicId = $request->request->get('topicId');

            // Busco el mensaje citado
            $post = $em->getRepository('PostBundle:Post')->findPostByNumber($topicId, $postNumber);

            // Llamo al serializador para poder utilizarlo
            $serializer = $this->container->get('jms_serializer');

            // Si no se ha encontrado el post debo mostrar un mensaje de error
            if (!$post) {
                $responseArray = array( "responseCode" => 400,
                                        "errorMsg" => "No se ha encontrado el mensaje solicitado");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Compruebo que no se intente acceder a posts de foros privados
            $forum = $this->get('deal.forumbundle.service.utilities')->
                                                        searchRequestedForum($post->getTopic()->getForum()->getSlug());

            if(!$this->get('security.context')->isGranted('ROLE_ACCESS_FORUM', $forum)){
                $responseArray = array( "responseCode" => 400,
                                        "errorMsg" => "No tienes permisos suficientes para acceder a esta zona.");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Si el post estuviese borrado u oculto muestro un error
            if ($post->getIsDeleted()==1 || $post->getIsHidden()==1) {
                $responseArray = array("responseCode" => 400, "errorMsg" => "El mensaje ha sido borrado/ocultado");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Si encuentro el post lo muestro
            // Le aplico filtros de twig al mensaje citado
            $quoteMsg = $this->container->get('deal.forumbundle.twig.extension.dealextension')
                                                                                        ->bbcode($post->getMessage());
            $quoteMsg = $this->container->get('deal.forumbundle.twig.extension.dealextension')
                                                                                        ->quotes($quoteMsg, $topicId);
            $responseArray = array( "responseCode" => 200,
                                    "quotedUser" => "#" . $postNumber .  " " . $post->getPoster()->getUserName() .
                                                    " ha escrito:", "quoteMsg" => nl2br($quoteMsg));
            $jsonContent = $serializer->serialize($responseArray, 'json');

            return new Response($jsonContent);
        }

        // En caso de no ser una petición AJAX retorno un mensaje de error
        return new Response('Esto no es ajax!', 400);
    }

}
