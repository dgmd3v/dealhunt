<?php

namespace Deal\PostBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Response;

/*
 * Clase encargada de las acciones de administración relacionadas con los temas
 * */
class AdminModController extends Controller
{
    /*
     * Oculta/Des-oculta un mensaje dado su Id
     *
     * @param integer $postId Id del mensaje a ocultar
     **/
    public function ajaxHideUnHidePostAction()
    {
        $request = $this->getRequest();

        // Si se trata de una petición AJAX serializo los resultados de la consulta a la base de datos
        // y devuelvo la respuesta
        if ($request->isXmlHttpRequest()) {

            // Llamo al serializador para poder utilizarlo
            $serializer = $this->container->get('jms_serializer');

            // Obtengo el objeto del entity manager para realizar consultas
            $em = $this->getDoctrine()->getManager();

            // Solicito los datos pasados a través del script jquery, en este caso el id del post/mensaje a ocultar
            $postId = $request->request->get('postId');
            $action = $request->request->get('action');

            // Busco el mensaje a ocultar
            $post = $em->getRepository('PostBundle:Post')->findPostTopicForumAndAccessById($postId);

            // Si no se ha encontrado el post debo mostrar un mensaje de error
            if (!$post) {
                $responseArray = array( "responseCode" => 400,
                                        "errorMsg" => "No se ha encontrado el mensaje solicitado");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Compruebo que pueda moderar en el foro
            if(!$this->get('security.context')->isGranted('ROLE_CAN_MOD_FORUM', $post->getTopic()->getForum())){
                $responseArray = array( "responseCode" => 400,
                    "errorMsg" => "No tienes permisos suficientes para ocultar/des-ocultar mensajes.");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Si estuviese borrado lo aviso
            if ($post->getIsDeleted() == 1) {
                $responseArray = array( "responseCode" => 400,
                                        "errorMsg" =>   "El mensaje está borrado." .
                                                        " Para ocultarlo/des-ocultarlo primero debes des-borrarlo");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            if($action == 'hide'){
                // Si ya estuviese oculto informo de que ya está oculto
                if ($post->getIsHidden()==1) {
                    $responseArray = array( "responseCode" => 200,
                        "successMsg" => "Mensaje #" . $post->getNumber() . " ocultado con éxito. Ya estaba oculto",
                        "postNumber" => $post->getNumber(),
                        "action" => "ajax-post-hide");
                    $jsonContent = $serializer->serialize($responseArray, 'json');
                }

                // Si no estuviese oculto, lo oculto
                else{
                    $this->get('deal.postbundle.moderationtools.posthide')->hide($post);

                    $responseArray = array( "responseCode" => 200,
                        "successMsg" => "Mensaje #" . $post->getNumber() . " ocultado con éxito",
                        "postNumber" => $post->getNumber(),
                        "action" => "ajax-post-hide");
                    $jsonContent = $serializer->serialize($responseArray, 'json');
                }
                return new Response($jsonContent);
            }

            elseif($action == 'unhide'){
                // Si estuviese oculto lo des-oculto
                if ($post->getIsHidden()==1) {
                    $this->get('deal.postbundle.moderationtools.posthide')->unHide($post);

                    $responseArray = array( "responseCode" => 200,
                        "successMsg" => "Mensaje #" . $post->getNumber() . " des-ocultado con éxito",
                        "postNumber" => $post->getNumber(),
                        "action" => "ajax-post-unhide");
                    $jsonContent = $serializer->serialize($responseArray, 'json');
                }

                // Si no estuviese oculto informo de la situación
                else{
                    $responseArray = array( "responseCode" => 200,
                        "successMsg" => "Mensaje #" . $post->getNumber() .
                            " des-ocultado con éxito. Ya estaba des-ocultado",
                        "postNumber" => $post->getNumber(),
                        "action" => "ajax-post-unhide");
                    $jsonContent = $serializer->serialize($responseArray, 'json');
                }
                return new Response($jsonContent);
            }

            else{
                $responseArray = array( "responseCode" => 400,
                    "errorMsg" =>   "Error, esta acción no se puede llevar a cabo");
                $jsonContent = $serializer->serialize($responseArray, 'json');
                return new Response($jsonContent);
            }
        }

        // En caso de no ser una petición AJAX retorno un mensaje de error
        return new Response('Esto no es ajax!', 400);
    }

    /*
     * "Borra" / "Des-Borrar" un post dado su Id
     *
     * @param integer $postId Id del post a borrar/desborrar
     **/
    public function ajaxDeleteUnDeletePostAction()
    {
        $request = $this->getRequest();

        // Si se trata de una petición AJAX serializo los resultados de la consulta a la base de datos
        // y devuelvo la respuesta
        if ($request->isXmlHttpRequest()) {
            // Llamo al serializador para poder utilizarlo
            $serializer = $this->container->get('jms_serializer');

            // Obtengo el objeto del entity manager para realizar consultas
            $em = $this->getDoctrine()->getManager();

            // Solicito los datos pasados a través del script jquery, en este caso el id del post/mensaje a borrar
            $postId = $request->request->get('postId');
            $action = $request->request->get('action');

            // Busco el tema a borrar
            $post = $em->getRepository('PostBundle:Post')->findPostTopicForumAndAccessById($postId);

            // Si no se ha encontrado el post debo mostrar un mensaje de error
            if (!$post) {
                $responseArray = array( "responseCode" => 400,
                                        "errorMsg" => "No se ha encontrado el mensaje solicitado");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Compruebo que pueda moderar en el foro
            if(!$this->get('security.context')->isGranted('ROLE_CAN_MOD_FORUM', $post->getTopic()->getForum())){
                $responseArray = array( "responseCode" => 400,
                    "errorMsg" => "No tienes permisos suficientes para borrar/des-borrar mensajes.");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Si estuviese oculto lo aviso
            if ($post->getIsHidden() == 1) {
                $responseArray = array( "responseCode" => 400,
                                        "errorMsg" =>   "El mensaje está ocultado." .
                                                        " Para borrarlo/des-borrarlo primero debes des-ocultarlo");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            if($action == 'delete'){
                // Si ya estuviese borrado informo de que ya está borrado
                if ($post->getIsDeleted()==1) {
                    $responseArray = array( "responseCode" => 200,
                        "successMsg" => "Mensaje #" . $post->getNumber() . " borrado con éxito. Ya estaba borrado",
                        "postNumber" => $post->getNumber(),
                        "action" => "ajax-post-delete");
                    $jsonContent = $serializer->serialize($responseArray, 'json');
                }

                // Si no estuviese oculto, lo oculto
                else{
                    $this->get('deal.postbundle.moderationtools.postdelete')->delete($post);

                    $responseArray = array( "responseCode" => 200,
                        "successMsg" => "Mensaje #" . $post->getNumber() . " borrado con éxito",
                        "postNumber" => $post->getNumber(),
                        "action" => "ajax-post-delete");
                    $jsonContent = $serializer->serialize($responseArray, 'json');
                }
                return new Response($jsonContent);
            }

            elseif($action == 'undelete'){
                // Si estuviese borrado lo des-borro
                if ($post->getIsDeleted()==1) {
                    $this->get('deal.postbundle.moderationtools.postdelete')->unDelete($post);

                    $responseArray = array( "responseCode" => 200,
                        "successMsg" => "Mensaje #" . $post->getNumber() . " des-borrado con éxito",
                        "postNumber" => $post->getNumber(),
                        "action" => "ajax-post-undelete");
                    $jsonContent = $serializer->serialize($responseArray, 'json');
                }

                // Si no estuviese borrado informo de la situación
                else{
                    $responseArray = array( "responseCode" => 200,
                        "successMsg" => "Mensaje #" . $post->getNumber() .
                                        " des-borrado con éxito. Ya estaba des-borrado",
                        "postNumber" => $post->getNumber(),
                        "action" => "ajax-post-undelete");
                    $jsonContent = $serializer->serialize($responseArray, 'json');
                }
                return new Response($jsonContent);
            }

            else{
                $responseArray = array( "responseCode" => 400,
                    "errorMsg" =>   "Error, esta acción no se puede llevar a cabo");
                $jsonContent = $serializer->serialize($responseArray, 'json');
                return new Response($jsonContent);
            }
        }

        // En caso de no ser una petición AJAX retorno un mensaje de error
        return new Response('Esto no es ajax!', 400);
    }

}
