<?php

namespace Deal\TopicBundle\Service;

use Doctrine\ORM\EntityManager;

/*
 * Clase con utilidades para el manejo del la cache de doctrine
 **/
class PopularDateUpdater {

    // Inyecto el entityManager
    protected $em;

    public function __construct(EntityManager $entityManager) {
        $this->em = $entityManager;
    }

    /*
     * Función encargada de actualizar las fechas de popularidad de los temas tras un cambio en el umbral
     **/
    public function update($newMinVotesToBePop, $oldMinVotesToBePop){

        // Obtengo los temas a actualizar
        $dealTopics = $this->em->getRepository('TopicBundle:TopicDeal')->findAllWithFirstPost();
        $voucherTopics = $this->em->getRepository('TopicBundle:TopicVoucher')->findAllWithFirstPost();
        $freeTopics = $this->em->getRepository('TopicBundle:TopicFree')->findAllWithFirstPost();

        if($newMinVotesToBePop > $oldMinVotesToBePop){
           $this->greaterMinUpdater($dealTopics, $newMinVotesToBePop);
           $this->greaterMinUpdater($voucherTopics, $newMinVotesToBePop);
           $this->greaterMinUpdater($freeTopics, $newMinVotesToBePop);
        }

        else{
            $this->lowerMinUpdater($dealTopics, $newMinVotesToBePop);
            $this->lowerMinUpdater($voucherTopics, $newMinVotesToBePop);
            $this->lowerMinUpdater($freeTopics, $newMinVotesToBePop);
        }

        $this->em->flush();
    }

    /*
     * Función con el bucle encargado de actualizar cuando el mínimo se incrementa
     **/
    protected function greaterMinUpdater($topics, $newMinVotesToBePop){
        foreach($topics as $topic){
            if($topic->getVotes() < $newMinVotesToBePop){
                $topic->setDateMadePopular(null);
                $this->em->persist($topic);
            }
        }
    }

    /*
     * Función con el bucle encargado de actualizar cuando el mínimo se disminuye
     **/
    protected function lowerMinUpdater($topics, $newMinVotesToBePop){
        foreach($topics as $topic){
            if( ($topic->getVotes() >= $newMinVotesToBePop) && ($topic->getDateMadePopular() == null) ){
                // Como fecha de popularidad le doy la fecha en la que fue creado
                $topic->setDateMadePopular($topic->getFirstPost()->getPostingDate());
                $this->em->persist($topic);
            }
        }
    }
} 