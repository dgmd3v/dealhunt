<?php

namespace Deal\TopicBundle\ModerationTools;

use Doctrine\ORM\EntityManager;
use Deal\TopicBundle\Entity\Shop as Shop;

/*
 *
 * Clase encargada de gestionar/preparar el borrado de una tienda
 */
class ShopRemover {

    // Inyecto el EntityManager para poder hacer operaciones en la base de datos
    protected $em;

    /**
     *
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    // Función que cambia la tienda dada en los temas por la tienda Otra
    public function changeTopicNotNullShopToOther (Shop $shop){
        // Busco los temas con la tienda dada
        $dealTopics = $this->em->getRepository('TopicBundle:TopicDeal')->findAllTopicsByShop($shop);
        $freeTopics = $this->em->getRepository('TopicBundle:TopicFree')->findAllTopicsByShop($shop);
        $voucherTopics = $this->em->getRepository('TopicBundle:TopicVoucher')->findAllTopicsByShop($shop);

        if($dealTopics != null){
            $this->changeTopicsShopToOther($dealTopics);
        }

        if($freeTopics != null){
            $this->changeTopicsShopToOther($freeTopics);
        }

        if($voucherTopics != null){
            $this->changeTopicsShopToOther($voucherTopics);
        }
    }

    // Función que cambia la tienda dada en los temas por la tienda Otra
    public function changeTopicNullShopToOther (){
        // Busco los temas con la tienda dada
        $dealTopics = $this->em->getRepository('TopicBundle:TopicDeal')->findAllTopicsWithNullShop();
        $freeTopics = $this->em->getRepository('TopicBundle:TopicFree')->findAllTopicsWithNullShop();
        $voucherTopics = $this->em->getRepository('TopicBundle:TopicVoucher')->findAllTopicsWithNullShop();

        if($dealTopics != null){
            $this->changeTopicsShopToOther($dealTopics);
        }

        if($freeTopics != null){
            $this->changeTopicsShopToOther($freeTopics);
        }

        if($voucherTopics != null){
            $this->changeTopicsShopToOther($voucherTopics);
        }
    }

    // Función que cambia la tienda de un array de temas por la tienda Otra
    protected function changeTopicsShopToOther($topics){
        // Busco la tienda Otra
        $otherShop = $this->em->getRepository('TopicBundle:Shop')->findShopByName('Otra');

        // Si no la encuentro la creo
        if($otherShop == null){
            $newShop = new Shop();
            $newShop->setName('Otra');

            $this->em->persist($newShop);

            // Guardo los cambios en la base de datos
            $this->em->flush();

            $otherShop = $newShop;
        }

        // Cambio la tienda de los temas
        foreach ($topics as $topic) {
            $topic->setShop($otherShop);
            $this->em->persist($topic);
        }

        // Guardo los cambios en la base de datos
        $this->em->flush();
    }


} 