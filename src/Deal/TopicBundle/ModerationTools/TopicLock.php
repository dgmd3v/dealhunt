<?php

namespace Deal\TopicBundle\ModerationTools;

use Doctrine\ORM\EntityManager;
use Deal\TopicBundle\Entity\Topic as Topic;

/*
 *
 * Clase encargada de abrir/cerrar temas/topics
 */
class TopicLock {

    // Inyecto el EntityManager para poder hacer operaciones en la base de datos
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    // Función encargada de cerrar un tema
    public function close(Topic $topic){
        // Cierro el tema dado
        $topic->setIsClosed(true);

        $this->em->persist($topic);

        // Guardo los cambios en la base de datos
        $this->em->flush();
    }

    // Función encargada de abrir un tema
    public function open(Topic $topic){
        // Abro el tema dado
        $topic->setIsClosed(false);

        $this->em->persist($topic);

        // Guardo los cambios en la base de datos
        $this->em->flush();
    }


} 