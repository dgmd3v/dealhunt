<?php

namespace Deal\TopicBundle\ModerationTools;

use Doctrine\ORM\EntityManager;
use Deal\TopicBundle\Entity\Topic as Topic;

/*
 *
 * Clase encargada de borrar temas/topics
 */
class TopicRemover {

    // Inyecto el EntityManager para poder hacer operaciones en la base de datos
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    // Función que actualiza la cuenta de mensajes y temas del usuario antes de hacer el borrado
    public function updateTopicsAndPostsCount (Topic $topic, $flush = true){
        // Actualizo el número de temas/mensajes de los usuarios
        $posts = $this->em->getRepository('PostBundle:Post')->findAllTopicPosts($topic);

        foreach ($posts as $post) {

            $posterProfile = $post->getPoster()->getUserProfile();
            $numberOfPosts = $posterProfile->getPosts();
            $posterProfile->setPosts($numberOfPosts-1);

            if($post->getNumber() == 1){
                $numberOfTopics = $posterProfile->getTopicsStarted();
                $posterProfile->setTopicsStarted($numberOfTopics-1);
            }

            $this->em->persist($post);
        }

        if($flush){
            // Guardo los cambios en la base de datos
            $this->em->flush();
        }

    }

    // Borra el tema por completo (junto con sus mensajes)
    public function remove(Topic $topic){

        $this->updateTopicsAndPostsCount($topic, false);

        // Borro el tema (Internamente la base de datos borra los post del tema y sus relaciones)
        $this->em->remove($topic);

        // Guardo los cambios en la base de datos
        $this->em->flush();
    }

} 