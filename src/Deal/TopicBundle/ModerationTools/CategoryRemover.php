<?php

namespace Deal\TopicBundle\ModerationTools;

use Doctrine\ORM\EntityManager;
use Deal\TopicBundle\Entity\Category as Category;
use Deal\TopicBundle\Entity\Topic as Topic;

/*
 * Clase encargada de gestionar/preparar el borrado de una categoría
 */
class CategoryRemover {

    // Inyecto el EntityManager para poder hacer operaciones en la base de datos
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    // Función que cambia la categoría dada en los temas por la categoría Otros
    public function changeTopicNotNullCategoryToOthers (Category $category){
        // Busco los temas con la categoría dada
        $topics = $this->em->getRepository('TopicBundle:Topic')->findAllTopicsByCategory($category);

        if($topics != null){
            $this->changeTopicsCategoryToOthers($topics);
        }
    }

    // Función que cambia la categoría dada en los temas por la categoría Otros
    public function changeTopicNullCategoryToOthers (){
        // Busco los temas con la categoría dada
        $topics = $this->em->getRepository('TopicBundle:Topic')->findAllTopicsWithNullCategory();

        if($topics != null){
            $this->changeTopicsCategoryToOthers($topics);
        }
    }

    protected function changeTopicsCategoryToOthers($topics){
        // Busco la categoría otros
        $othersCategory = $this->em->getRepository('TopicBundle:Category')->findCategoryByName('Otros');

        // Si no la encuentro la creo
        if($othersCategory == null){
            $newCategory = new Category();
            $newCategory->setName('Otros');

            $this->em->persist($newCategory);

            // Guardo los cambios en la base de datos
            $this->em->flush();

            $othersCategory = $newCategory;
        }

        // Cambio la categoría de los temas
        foreach ($topics as $topic) {
            $topic->setCategory($othersCategory);
            $this->em->persist($topic);
        }

        // Guardo los cambios en la base de datos
        $this->em->flush();
    }


} 