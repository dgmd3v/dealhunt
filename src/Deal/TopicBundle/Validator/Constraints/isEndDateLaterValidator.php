<?php

namespace Deal\TopicBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/*
 * Clase validator que comprueba si dadas dos fechas ambas han sido introducidas y en caso afirmativo, si la fecha
 * de inicio es anterior a la de finalización
 */
class isEndDateLaterValidator extends ConstraintValidator
{
    public function validate($entity, Constraint $constraint)
    {

        $startDate = $entity->getDateStart();
        $endDate = $entity->getDateEnd();

        // Comprobamos primero si se han introducido las dos fechas
        if($startDate == null && $endDate != null){
            $this->context->addViolationAt('dateStart',
                $constraint->messageOneDateBlank
            );
        }

        elseif ($startDate != null && $endDate == null){
            $this->context->addViolationAt('dateEnd',
                $constraint->messageOneDateBlank
            );
        }

        // Comprobamos si la fecha de inicio es posterior a la de finalización
        elseif ($startDate > $endDate ){
            $this->context->addViolationAt('dateEnd',
                $constraint->messageStartBeforeEnd
            );
        }
    }
}
