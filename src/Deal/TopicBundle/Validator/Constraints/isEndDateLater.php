<?php

namespace Deal\TopicBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class isEndDateLater extends Constraint
{
    public $messageStartBeforeEnd = 'La fecha de finalización introducida no puede ser anterior a la fecha de inicio';
    public $messageOneDateBlank = 'Si introduces una fecha la otra no puede estar vacía';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    public function validatedBy()
    {
        return 'isenddatelater_validator';
    }
}
