<?php

namespace Deal\TopicBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TopicBundle extends Bundle
{
    public function getParent()
    {
        return 'FPNTagBundle';
    }
}
