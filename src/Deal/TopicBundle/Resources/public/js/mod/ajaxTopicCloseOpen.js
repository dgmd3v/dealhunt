/*
* Función destinada a ejecutar la acción del controlador encargada abrir/cerrar un tema/topic y mostrar un mensaje
* de información
* */
$(document).on('click', '.ajaxTopicCloseOpen',
    function() {
                var dialogModalAcceptButton = $("#dialogModalAcceptButton");
                var path = dialogModalAcceptButton.attr("href");
                var topicId = dialogModalAcceptButton.attr("data-topicid");
                var action = dialogModalAcceptButton.attr("data-action");

                var resultModal = $('#resultModal');

                $.ajax({
                    type: "POST",
                    url: path,
                    data: {topicId: topicId, action: action },
                    dataType: "json",
                    success: function(response) {
                        if (response.responseCode == 200) {

                            resultModal.find('.modal-body').text(response.successMsg);
                            resultModal.find('.modal-title').text('Felicidades!!!');
                            resultModal.modal();

                            var topicTitle = $('.first-post h3:first-of-type');
                            var topicVotingButtons = $('.voting-buttons').find("a[class^='vote-button-']");

                            if(response.action == "ajaxTopicClose"){
                                $('#dialogModal').removeClass('ajaxTopicClose');
                                dialogModalAcceptButton.removeClass("ajaxTopicClose");

                                var closedButton =
                                    '</div><button type="button" class="btn btn-lg btn-danger topic-closed-button" disabled="disabled"><span class="glyphicon glyphicon-lock"> Tema Cerrado</span></button></div>';
                                $( closedButton ).insertBefore( '#reply-form' );

                                topicTitle.append('<span class="glyphicon glyphicon-lock"></span>');

                                topicVotingButtons.addClass('disabled');

                                var openButton = $('.close-open-button');
                                openButton.attr({
                                    "data-title": "Abrir Tema",
                                    "data-message": "¿Está segur@ de que desea abrir el tema?",
                                    "data-action": "open"
                                });

                                openButton = openButton.find('span');

                                openButton.text(' Abrir');
                                openButton.attr("title", "Abrir Tema");
                                openButton.attr("class", "glyphicon glyphicon-lock");
                            }

                            else if(response.action == "ajaxTopicOpen"){
                                $('#dialogModal').removeClass('ajaxTopicOpen');
                                dialogModalAcceptButton.removeClass("ajaxTopicOpen");

                                $('.topic-closed-button').remove();

                                topicTitle.find('span.glyphicon-lock').remove();

                                if(response.isExpired != true){
                                    topicVotingButtons.removeClass('disabled');
                                }

                                var closeButton = $('.close-open-button');
                                closeButton.attr({
                                    "data-title": "Cerrar Tema",
                                    "data-message": "¿Está segur@ de que desea cerrar el tema?",
                                    "data-action": "close"
                                });

                                closeButton = closeButton.find('span');

                                closeButton.text(' Cerrar');
                                closeButton.attr("title", "Cerrar Tema");
                                closeButton.attr("class", "glyphicon glyphicon-lock");
                            }


                        }

                        else if (response.responseCode == 400) {
                            resultModal.find('.modal-title').text("Error");
                            resultModal.find('.modal-body').text("Error: " + response.errorMsg);
                            resultModal.modal();
                        }
                    }
                });
            }
    );