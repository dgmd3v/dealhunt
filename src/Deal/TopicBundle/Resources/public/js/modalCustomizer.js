/*
 * Script encargado de preparar el modal para las diferentes acciones, abrir/cerrar un tema, ocultar/des-ocultar un
 * mensaje, etc
 * */
$('#dialogModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Botón que lanzó el modal
    var title = button.attr('data-title'); // Extraigo los datos de los atributos data-*
    var message = button.attr('data-message');
    var path = button.attr('data-path');
    var type = button.attr('data-type');

    if(type == 'ajaxDeleteSignature'){
        var modal = $(this);
        var signatureId = button.attr('data-signatureid');
        modal.find('#dialogModalAcceptButton').addClass(type);
        modal.find('.modal-title').text(title);
        modal.find('.modal-body').text(message);
        modal.find("#dialogModalAcceptButton").attr({
            href: path,
            "data-signatureid": signatureId
        });
    }

    else if(type == 'ajaxTopicExpiredStatus'){
        var modal = $(this);
        var topicId = button.attr('data-topicid');
        var topicType = button.attr('data-topictype');
        var action = button.attr('data-action');
        modal.find('#dialogModalAcceptButton').addClass(type);
        modal.find('.modal-title').text(title);
        modal.find('.modal-body').text(message);
        modal.find("#dialogModalAcceptButton").attr({
            href: path,
            "data-topicid": topicId,
            "data-topictype": topicType,
            "data-action": action
        });
    }

    else{
        var modal = $(this);
        modal.find('.modal-title').text(title);
        modal.find('.modal-body').text(message);
        modal.find('#dialogModalAcceptButton').attr("href", path);
        modal.find('#dialogModalAcceptButton').attr("data-dismiss", "");
    }
})

$('#dialogModal').on('hidden.bs.modal', function (event) {

    var modal = $(this);
    modal.find("#dialogModalAcceptButton").attr({
        'class': "btn btn-primary",
        href: "",
        "data-dismiss": "modal",
        "data-topicid": "",
        "data-signatureid": "",
        "data-topictype": "",
        "data-action": ""
    });
})

$('#dialogReportModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Botón que lanzó el modal
    var title = button.attr('data-title'); // Extraigo los datos de los atributos data-*
    var path = button.attr('data-path');

        var modal = $(this);
        var postId = button.attr('data-postid');
        var postNumber = button.attr('data-postnumber');
        modal.find('.modal-title').text(title);
        modal.attr({
            "data-postid": postId,
            "data-postnumber": postNumber
        });
        modal.find('form').attr({
            action: path
        });

})

$('#dialogReportModal').on('hidden.bs.modal', function (event) {

    var modal = $(this);

    modal.attr({
        "data-postid": "",
        "data-postnumber": ""
    });
    modal.find('form').attr({
        action: ""
    });
})