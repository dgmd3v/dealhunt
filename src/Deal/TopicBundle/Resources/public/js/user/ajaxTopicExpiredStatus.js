/*
* Función destinada a ejecutar la acción del controlador encargada marcar un tema/topic como finalizado
* y mostrar un mensaje de información
* */
$(document).on('click', '.ajaxTopicExpiredStatus',
    function() {
                var dialogModalAcceptButton = $("#dialogModalAcceptButton");
                var path = dialogModalAcceptButton.attr("href");
                var topicId = dialogModalAcceptButton.attr("data-topicid");
                var topicType = dialogModalAcceptButton.attr("data-topictype");
                var action = dialogModalAcceptButton.attr("data-action");

                var resultModal = $('#resultModal');

                $.ajax({
                    type: "POST",
                    url: path,
                    data: {topicId: topicId, topicType: topicType, action: action  },
                    dataType: "json",
                    success: function(response) {
                        if (response.responseCode == 200) {

                            resultModal.find('.modal-body').text(response.successMsg);
                            resultModal.find('.modal-title').text('Felicidades!!!');
                            resultModal.modal();

                            $('#dialogModal').removeClass('ajaxTopicExpiredStatus');
                            dialogModalAcceptButton.removeClass("ajaxTopicExpiredStatus");

                            var expiredStatusButton = $('.expired-notexpired-button');

                            var expiredStatusSpan = expiredStatusButton.find('span');

                            var topicTitle = $('.first-post h3:first-of-type');
                            var topicVotingButtons = $('.voting-buttons').find("a[class^='vote-button-']");

                            if(response.expiredStatus=='expired'){
                                expiredStatusButton.attr({
                                    "data-title": "Marcar tema como NO finalizado",
                                    "data-message": "¿Está segur@ de que desea marcar el tema como NO finalizado?",
                                    "data-action": "unexpire"
                                });

                                expiredStatusSpan.text(' No finalizado');
                                expiredStatusSpan.attr("title", "Macar como NO finalizado");

                                topicTitle.addClass('finished');
                                topicVotingButtons.addClass('disabled');
                            }

                            else if(response.expiredStatus=='notexpired'){
                                expiredStatusButton.attr({
                                    "data-title": "Marcar tema como finalizado",
                                    "data-message": "¿Está segur@ de que desea marcar el tema como finalizado?",
                                    "data-action": "expire"
                                });

                                expiredStatusSpan.text(' Finalizado');
                                expiredStatusSpan.attr("title", "Macar como finalizado");

                                topicTitle.removeClass('finished');

                                if(response.isClosed != true){
                                    topicVotingButtons.removeClass('disabled');
                                }
                            }
                        }

                        else if (response.responseCode == 400) {
                            resultModal.find('.modal-title').text("Error");
                            resultModal.find('.modal-body').text("Error: " + response.errorMsg);
                            resultModal.modal();
                        }
                    }
                });
            }
    );