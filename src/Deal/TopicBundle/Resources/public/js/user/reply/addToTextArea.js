/*
 * Script encargado de insertar las etiquetas de los bbcodes y smilies en el textarea cuando se hace click sobre el
 * botón del bbcode. También se encarga de si un usuario logueado presiona sobre el numero de post/mensaje se hará
 * scroll hasta el textarea y se introducirá el numero para poder hacer citas con mayor facilidad.
 * */
$(document).ready(function(){

    //BBCODES

    $("#bold-bbcode").click(function(){
        addBbcode("[b]","[/b]");
    });

    $("#italic-bbcode").click(function(){
        addBbcode("[i]","[/i]");
    });

    $("#underline-bbcode").click(function(){
        addBbcode("[u]","[/u]");
    });

    $("#center-bbcode").click(function(){
        addBbcode("[center]","[/center]");
    });

    $("#image-bbcode").click(function(){
        addBbcode("[img]","[/img]");
    });

    $("#video-bbcode").click(function(){
        addBbcode("[video]","[/video]");
    });

    $("#url-bbcode").click(function(){
        addBbcode("[url=]","[/url]");
    });

    $("#spoiler-bbcode").click(function(){
        addBbcode("[spoiler]","[/spoiler]");
    });

    $("#spoiler-title-bbcode").click(function(){
        addBbcode("[spoiler=]","[/spoiler]");
    });

    $("#title-bbcode").click(function(){
        addBbcode("[title]","[/title]");
    });

    // SMILIES
    $("#show-smilies").click(function(){
        $('#smilies-panel').toggleClass('hidden-content');
    });

    $("#smile").click(function(){
        addSmiley(":smile:");
    });

    $("#laugh").click(function(){
        addSmiley(":laugh:");
    });

    $("#silly").click(function(){
        addSmiley(":silly:");
    });

    $("#wink").click(function(){
        addSmiley(":wink:");
    });

    $("#blush").click(function(){
        addSmiley(":blush:");
    });

    $("#sad").click(function(){
        addSmiley(":sad:");
    });

    $("#cool").click(function(){
        addSmiley(":cool:");
    });

    $("#angry").click(function(){
        addSmiley(":angry:");
    });

    $("#surprised").click(function(){
        addSmiley(":surprised:");
    });

    $("#speechless").click(function(){
        addSmiley(":speechless:");
    });

    $("#geek").click(function(){
        addSmiley(":geek:");
    });

    $("#tease").click(function(){
        addSmiley(":tease:");
    });

    $("#crazy").click(function(){
        addSmiley(":crazy:");
    });

    $("#fools").click(function(){
        addSmiley(":fools:");
    });

    $("#cry").click(function(){
        addSmiley(":cry:");
    });

    $("#xd").click(function(){
        addSmiley(":xd:");
    });

    $("#devil").click(function(){
        addSmiley(":devil:");
    });

    $("#angel").click(function(){
        addSmiley(":angel:");
    });

    $("#ill").click(function(){
        addSmiley(":ill:");
    });

    $("#zipit").click(function(){
        addSmiley(":zipit:");
    });

    $("#annoyed").click(function(){
        addSmiley(":annoyed:");
    });

    $("#please").click(function(){
        addSmiley(":please:");
    });

    $("#hay").click(function(){
        addSmiley(":hay:");
    });

    $("#notguilty").click(function(){
        addSmiley(":notguilty:");
    });

    $("#kissy").click(function(){
        addSmiley(":kissy:");
    });

    $("#zzz").click(function(){
        addSmiley(":zzz:");
    });

    $("#totalshock").click(function(){
        addSmiley(":totalshock:");
    });

    $("#inlove").click(function(){
        addSmiley(":inlove:");
    });

    $("#notonecare").click(function(){
        addSmiley(":notonecare:");
    });

    $("#boring").click(function(){
        addSmiley(":boring:");
    });

    $("#minishock").click(function(){
        addSmiley(":minishock:");
    });

    $("#oh").click(function(){
        addSmiley(":oh:");
    });


    // QUOTES

    $(".post-number").click(function(){
        addQuote($.trim($(this).text()));
    });

    function addBbcode(openingBbcode, closingBbcode)
    {
        // Guardo en la variable input el área de texto
        var input = $("#textarea-container textarea");
        // Longitud del texto
        var length = input.val().length;
        // Posición donde empieza el texto seleccionado (selectionStart es una propiedad de HTMLTextAreaElement)
        var selectionStart = input[0].selectionStart;
        // Posición donde acaba el texto seleccionado
        var selectionEnd = input[0].selectionEnd;
        // Sabiendo donde empieza y acaba el texto, lo selecciono
        var selectedText = input.val().substring(selectionStart, selectionEnd);
        // Modifico el texto seleccionado poniendo las etiquetas del bbcode al principio y al final
        var modifiedText = openingBbcode + selectedText + closingBbcode;
        // Cambio el texto introducido incluyendo el texto con las etiquetas bbcode
        input.val(input.val().substring(0, selectionStart) + modifiedText + input.val().substring(selectionEnd, length));
    }

    function addSmiley(smilie)
    {
        // Guardo en la variable input el área de texto
        var input = $("#textarea-container textarea");
        // Longitud del texto
        var length = input.val().length;
        // Posición donde empieza el texto seleccionado (selectionStart es una propiedad de HTMLTextAreaElement)
        var selectionStart = input[0].selectionStart;
        // Posición donde acaba el texto seleccionado
        var selectionEnd = input[0].selectionEnd;
        // Sabiendo donde empieza y acaba el texto, lo selecciono
        var selectedText = input.val().substring(selectionStart, selectionEnd);
        // Modifico el texto seleccionado poniendo las etiquetas del bbcode al principio y al final
        var modifiedText = smilie + selectedText;
        // Cambio el texto introducido incluyendo el texto con las etiquetas bbcode
        input.val(input.val().substring(0, selectionStart) + modifiedText + input.val().substring(selectionEnd, length));
    }



    function addQuote(postId)
    {
        // Guardo en la variable input el área de texto
        var input = $("#textarea-container textarea");

        // Mensaje citado
        var quote = postId;

        // Cambio el texto introducido incluyendo el texto con las etiquetas bbcode
        input.val(input.val()  + quote );

        // Posición del textarea
        var textareaPos = $('#textarea-container').offset().top;

        // Hago scroll a la posición del textarea
        $('html, body').animate({scrollTop:textareaPos}, 'slow');

        // Evito que el anchor se ejecute como tal
        return false;
    }

});