/*
* Función destinada a ejecutar la acción del controlador encargada cerrar un tema/topic y mostrar un mensaje
* de información
* */
$(document).on('click', '#favorite-unfavorite-button',
    function() {
                var favButton = $(this);
                var topicId = favButton.attr("data-topicid");
                var path = favButton.attr("data-path");
                var markAs = favButton.attr("data-markas");

                $.ajax({
                    type: "POST",
                    url: path,
                    data: {topicId: topicId, markAs: markAs },
                    dataType: "json",
                    success: function(response) {
                        if (response.responseCode == 200) {

                            var favButtonSpan = favButton.find('span');

                            if(markAs == 'favorite'){
                                favButton.attr("title", "Desmarcar como favorito este tema");
                                favButtonSpan.attr("class", "glyphicon glyphicon-star");
                                favButton.attr({
                                    "data-markas": "unfavorite"
                                });
                            }

                            else if(markAs == 'unfavorite'){
                                favButton.attr("title", "Marcar como favorito este tema");
                                favButtonSpan.attr("class", "glyphicon glyphicon-star-empty");
                                favButton.attr({
                                    "data-markas": "favorite"
                                });
                            }
                        }

                        else if (response.responseCode == 400) {
                            var resultModal = $('#resultModal');
                            resultModal.find('.modal-title').text("Error");
                            resultModal.find('.modal-body').text("Error: " + response.errorMsg);
                            resultModal.modal();
                        }
                    }
                });
            }
    );