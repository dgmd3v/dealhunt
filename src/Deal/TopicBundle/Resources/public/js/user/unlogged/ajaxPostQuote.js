/*
 * Función destinada a ejecutar la acción del controlador encargada cerrar un tema/topic y mostrar un mensaje
 * de información
 * */
$(document).on('click', '.quote',
    function (e) {
        e.preventDefault();
        var quote = $(this); // Quote que lanzó el este js
        var path = quote.attr("data-path");

        if(quote.hasClass('toggled')){
            quote.next().remove();
            quote.removeClass('toggled');
        }

        else{
            var postNumber = quote.attr("href");
            postNumber = postNumber.substring(1);
            var topicId = quote.attr("data-topicid");

            $.ajax({
                type: "POST",
                url: path,
                data: {postNumber: postNumber, topicId: topicId },
                dataType: "json",
                success: function(response) {
                    if (response.responseCode == 200) {
                        quote.addClass('toggled');
                        quoteDiv =  "<div>" +
                                        "<cite>" + response.quotedUser + "</cite>" +
                                        "<blockquote>" + response.quoteMsg + "</blockquote>" +
                                    "</div>";
                        $( quoteDiv ).insertAfter( quote );
                    }

                    else if (response.responseCode == 400) {
                        quote.addClass('toggled');
                        $( "<blockquote>" + response.errorMsg + "</blockquote>" ).insertAfter( quote );
                    }
                }
            });
        }
    }
);