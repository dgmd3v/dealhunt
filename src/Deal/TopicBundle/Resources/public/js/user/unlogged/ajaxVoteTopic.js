/*
* Función destinada a ejecutar la acción del controlador encargada los votos de un temas
* */
$(document).on('click', '.vote-button-positive, .vote-button-negative',
    function() {
                var voteButton = $(this);
                var voteButtonClass = voteButton.attr('class').split(' ')[0];
                var topicId = voteButton.attr("data-topicid");
                var path = voteButton.attr("data-path");
                var vote = voteButton.attr("data-vote");
                var topicType = voteButton.attr("data-topictype");

                $.ajax({
                    type: "POST",
                    url: path,
                    data: {topicId: topicId, vote: vote, topicType: topicType, voteButtonClass: voteButtonClass },
                    dataType: "json",
                    success: function(response) {
                        if (response.responseCode == 200) {

                            var topic = $('#topic-' + topicId);

                            topic.find("a[class^='vote-button-']").removeClass('active').addClass('disabled');

                            var votesDiv = topic.find('.votes');

                            if(response.newTotalVotes < 0){
                                var votesUpdate =
                                    '<span class="not-popular-status">' + response.newTotalVotes + '</span>';

                                votesDiv.empty().append(votesUpdate);
                            }

                            else{
                                if(response.newTotalVotes == 0){
                                    var votesUpdate =
                                        '<span class="neither-status">' + response.newTotalVotes + '</span>';

                                    votesDiv.empty().append(votesUpdate);
                                }

                                else{
                                    var votesUpdate =
                                        '<span class="popular-status">' + response.newTotalVotes + '</span>';

                                    votesDiv.empty().append(votesUpdate);
                                }

                            }
                        }

                        else if (response.responseCode == 400) {
                            alert("Error: " + response.errorMsg);
                        }
                    }
                });
            }
    );