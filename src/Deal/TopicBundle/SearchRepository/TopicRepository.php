<?php

namespace Deal\TopicBundle\SearchRepository;

use FOS\ElasticaBundle\Repository;


class TopicRepository extends Repository{

    /*
     * Función encargada de generar la consulta para buscar en todos los foros, todas las categorías,
     * ordenados por relevancia y de cualquier fecha
     *
     * @param string $searchTerm termino a buscar
     * @param array $userRolesArray array con los roles de los que dispone el usuario
     * */
    public function findAllTopics($searchTerm, $userRolesArray){
        $acessRoleTerm = new \Elastica\Query\Terms();
        $acessRoleTerm->setTerms('topic.forum.accessRole.name', $userRolesArray);

        $titleQuery = new \Elastica\Query\Match();
        $titleQuery->setFieldQuery('topic.title', $searchTerm);

        $boolQuery = new \Elastica\Query\Bool();
        $boolQuery->addMust($acessRoleTerm);
        $boolQuery->addMust($titleQuery);

        $query = new \Elastica\Query($boolQuery);

        return $query;

    }

    /*
     * Función encargada de generar la consulta para buscar en todos los foros, todas las categorías,
     * ordenados por fecha y de cualquier fecha
     *
     * @param string $searchTerm termino a buscar
     * @param array $userRolesArray array con los roles de los que dispone el usuario
     * */
    public function findAllTopicsOrderedByDate($searchTerm, $userRolesArray){

        $query = $this->findAllTopics($searchTerm, $userRolesArray);
        $query->setSort(array('postingDate' => array('order' => 'desc')));

        return $query;
    }


    /*
     * Función encargada de generar la consulta para buscar en todos los foros, todas las categorías,
     * ordenados por relevancia y de una fecha concreta hoy, última semana, etc
     *
     * @param string $searchTerm termino a buscar
     * @param array $userRolesArray array con los roles de los que dispone el usuario
     * @param Datetime $dateStart array con la fecha a partir de la cual se buscan resultados
     * */
    public function findAllTopicsByInterval($searchTerm, $userRolesArray, $dateStart){

        $acessRoleTerm = new \Elastica\Query\Terms();
        $acessRoleTerm->setTerms('topic.forum.accessRole.name', $userRolesArray);

        $titleQuery = new \Elastica\Query\Match();
        $titleQuery->setFieldQuery('topic.title', $searchTerm);

        $boolQuery = new \Elastica\Query\Bool();
        $boolQuery->addMust($acessRoleTerm);
        $boolQuery->addMust($titleQuery);

        $filters = new \Elastica\Filter\Bool();
        $filters->addMust(
            new \Elastica\Filter\NumericRange('postingDate', array(
                'gte' => $dateStart->format('Y-m-d\TH:i:s'),
            ))
        );

        $query = new \Elastica\Query\Filtered($boolQuery, $filters);

        return $query;
    }

    /*
     * Función encargada de generar la consulta para buscar en todos los foros, todas las categorías,
     * ordenados por fecha y de una fecha concreta hoy, última semana, etc
     *
     * @param string $searchTerm termino a buscar
     * @param array $userRolesArray array con los roles de los que dispone el usuario
     * @param Datetime $dateStart array con la fecha a partir de la cual se buscan resultados
     * */
    public function findAllTopicsByIntervalOrderedByDate($searchTerm, $userRolesArray, $dateStart){

        $filteredQuery = $this->findAllTopicsByInterval($searchTerm, $userRolesArray, $dateStart);

        $query = new \Elastica\Query($filteredQuery);
        $query->setSort(array('postingDate' => array('order' => 'desc')));

        return $query;
    }

    /*
     * Función encargada de generar la consulta para buscar en todos los foros, de una categoría en concreto,
     * ordenados por relevancia y de cualquier fecha
     *
     * @param string $searchTerm termino a buscar
     * @param integer $category id de la categoría en la cual quiero hacer la búsqueda
     * @param array $userRolesArray array con los roles de los que dispone el usuario
     * */
    public function findAllTopicsByCategory($searchTerm, $category, $userRolesArray){

        $acessRoleTerm = new \Elastica\Query\Terms();
        $acessRoleTerm->setTerms('topic.forum.accessRole.name', $userRolesArray);

        $categoryTerm = new \Elastica\Query\Term();
        $categoryTerm->setTerm('topic.category.id', $category);

        $titleQuery = new \Elastica\Query\Match();
        $titleQuery->setFieldQuery('topic.title', $searchTerm);

        $boolQuery = new \Elastica\Query\Bool();
        $boolQuery->addMust($acessRoleTerm);
        $boolQuery->addMust($categoryTerm);
        $boolQuery->addMust($titleQuery);

        $query = new \Elastica\Query($boolQuery);

        return $query;
    }

    /*
     * Función encargada de generar la consulta para buscar en todos los foros, de una categoría en concreto,
     * ordenados por fecha y de cualquier fecha
     *
     * @param string $searchTerm termino a buscar
     * @param integer $category id de la categoría en la cual quiero hacer la búsqueda
     * @param array $userRolesArray array con los roles de los que dispone el usuario
     * */
    public function findAllTopicsByCategoryOrderedByDate($searchTerm, $category, $userRolesArray){

        $query = $this->findAllTopicsByCategory($searchTerm, $category, $userRolesArray);

        $query->setSort(array('postingDate' => array('order' => 'desc')));

        return $query;
    }



    /*
     * Función encargada de generar la consulta para buscar en todos los foros, de una categoría en concreto,
     * ordenados por relevancia y de una fecha concreta hoy, última semana, etc
     *
     * @param string $searchTerm termino a buscar
     * @param integer $category id de la categoría en la cual quiero hacer la búsqueda
     * @param Datetime $dateStart array con la fecha a partir de la cual se buscan resultados
     * @param array $userRolesArray array con los roles de los que dispone el usuario
     * */
    public function findAllTopicsByCategoryAndInterval($searchTerm, $category, $dateStart, $userRolesArray){

        $acessRoleTerm = new \Elastica\Query\Terms();
        $acessRoleTerm->setTerms('topic.forum.accessRole.name', $userRolesArray);

        $categoryTerm = new \Elastica\Query\Term();
        $categoryTerm->setTerm('topic.category.id', $category);

        $titleQuery = new \Elastica\Query\Match();
        $titleQuery->setFieldQuery('topic.title', $searchTerm);

        $boolQuery = new \Elastica\Query\Bool();
        $boolQuery->addMust($acessRoleTerm);
        $boolQuery->addMust($categoryTerm);
        $boolQuery->addMust($titleQuery);

        $filters = new \Elastica\Filter\Bool();
        $filters->addMust(
            new \Elastica\Filter\NumericRange('postingDate', array(
                'gte' => $dateStart->format('Y-m-d\TH:i:s'),
            ))
        );

        $query = new \Elastica\Query\Filtered($boolQuery, $filters);

        return $query;
    }

    /*
     * Función encargada de generar la consulta para buscar en todos los foros, de una categoría en concreto,
     * ordenados por fecha y de una fecha concreta hoy, última semana, etc
     *
     * @param string $searchTerm termino a buscar
     * @param integer $category id de la categoría en la cual quiero hacer la búsqueda
     * @param Datetime $dateStart array con la fecha a partir de la cual se buscan resultados
     * @param array $userRolesArray array con los roles de los que dispone el usuario
     * */
    public function findAllTopicsByCategoryAndIntervalOrderedByDate($searchTerm, $category, $dateStart, $userRolesArray)
    {

        $filteredQuery = $this->findAllTopicsByCategoryAndInterval($searchTerm, $category, $dateStart, $userRolesArray);

        $query = new \Elastica\Query($filteredQuery);
        $query->setSort(array('postingDate' => array('order' => 'desc')));

        return $query;
    }


    /*
     * Función encargada de generar la consulta para la búsqueda en un foro concreto,
     * en todas las categorías, ordenada por relevancia y de cualquier fecha
     *
     * @param string $searchTerm termino a buscar
     * @param integer $forum id del foro en el cual cual quiero hacer la búsqueda
     * @param array $userRolesArray array con los roles de los que dispone el usuario
     * */
    public function findTopicsByForum($searchTerm, $forum, $userRolesArray){
        $forumTerm = new \Elastica\Query\Term();
        $forumTerm->setTerm('topic.forum.id', $forum);

        $acessRoleTerm = new \Elastica\Query\Terms();
        $acessRoleTerm->setTerms('topic.forum.accessRole.name', $userRolesArray);

        $titleQuery = new \Elastica\Query\Match();
        $titleQuery->setFieldQuery('topic.title', $searchTerm);

        $boolQuery = new \Elastica\Query\Bool();
        $boolQuery->addMust($forumTerm);
        $boolQuery->addMust($acessRoleTerm);
        $boolQuery->addMust($titleQuery);

        $query = new \Elastica\Query($boolQuery);

        return $query;
    }

    /*
     * Función encargada de generar la consulta para la búsqueda en un foro concreto,
     * en todas las categorías, ordenada por fecha y de cualquier fecha
     *
     * @param string $searchTerm termino a buscar
     * @param integer $forum id del foro en el cual cual quiero hacer la búsqueda
     * @param array $userRolesArray array con los roles de los que dispone el usuario
     * */
    public function findTopicsByForumOrderedByDate($searchTerm, $forum, $userRolesArray){

        $query = $this->findTopicsByForum($searchTerm, $forum, $userRolesArray);
        $query->setSort(array('postingDate' => array('order' => 'desc')));

        return $query;
    }


    /*
     * Función encargada de generar la consulta para la búsqueda en un foro concreto,
     * todas las categorías, ordenados por relevancia y de una fecha concreta hoy, última semana, etc
     *
     * @param string $searchTerm termino a buscar
     * @param integer $forum id del foro en el cual cual quiero hacer la búsqueda
     * @param Datetime $dateStart array con la fecha a partir de la cual se buscan resultados
     * @param array $userRolesArray array con los roles de los que dispone el usuario
     * */
    public function findTopicsByForumByInterval($searchTerm, $forum, $dateStart, $userRolesArray){

        $acessRoleTerm = new \Elastica\Query\Terms();
        $acessRoleTerm->setTerms('topic.forum.accessRole.name', $userRolesArray);

        $forumTerm = new \Elastica\Query\Term();
        $forumTerm->setTerm('topic.forum.id', $forum);

        $titleQuery = new \Elastica\Query\Match();
        $titleQuery->setFieldQuery('topic.title', $searchTerm);

        $boolQuery = new \Elastica\Query\Bool();
        $boolQuery->addMust($acessRoleTerm);
        $boolQuery->addMust($forumTerm);
        $boolQuery->addMust($titleQuery);

        $filters = new \Elastica\Filter\Bool();
        $filters->addMust(
            new \Elastica\Filter\NumericRange('postingDate', array(
                'gte' => $dateStart->format('Y-m-d\TH:i:s'),
            ))
        );

        $query = new \Elastica\Query\Filtered($boolQuery, $filters);

        return $query;
    }

    /*
     * Función encargada de generar la consulta para la búsqueda en un foro concreto,
     * todas las categorías, ordenados por fecha y de una fecha concreta hoy, última semana, etc
     *
     * @param string $searchTerm termino a buscar
     * @param integer $forum id del foro en el cual cual quiero hacer la búsqueda
     * @param Datetime $dateStart array con la fecha a partir de la cual se buscan resultados
     * @param array $userRolesArray array con los roles de los que dispone el usuario
     * */
    public function findTopicsByForumIntervalOrderedByDate($searchTerm, $forum, $dateStart, $userRolesArray){

        $filteredQuery = $this->findTopicsByForumByInterval($searchTerm, $forum, $dateStart, $userRolesArray);

        $query = new \Elastica\Query($filteredQuery);
        $query->setSort(array('postingDate' => array('order' => 'desc')));

        return $query;
    }

    /*
     * Función encargada de generar la consulta para la búsqueda en un foro concreto, de una categoría en concreto,
     * ordenados por relevancia y de cualquier fecha
     *
     * @param string $searchTerm termino a buscar
     * @param integer $forum id del foro en el cual cual quiero hacer la búsqueda
     * @param integer $category id de la categoría en la cual quiero hacer la búsqueda
     * @param array $userRolesArray array con los roles de los que dispone el usuario
     * */
    public function findTopicsByForumCategory($searchTerm, $forum, $category, $userRolesArray){
        $forumTerm = new \Elastica\Query\Term();
        $forumTerm->setTerm('topic.forum.id', $forum);

        $categoryTerm = new \Elastica\Query\Term();
        $categoryTerm->setTerm('topic.category.id', $category);

        $acessRoleTerm = new \Elastica\Query\Terms();
        $acessRoleTerm->setTerms('topic.forum.accessRole.name', $userRolesArray);

        $titleQuery = new \Elastica\Query\Match();
        $titleQuery->setFieldQuery('topic.title', $searchTerm);

        $boolQuery = new \Elastica\Query\Bool();
        $boolQuery->addMust($forumTerm);
        $boolQuery->addMust($categoryTerm);
        $boolQuery->addMust($acessRoleTerm);
        $boolQuery->addMust($titleQuery);

        $query = new \Elastica\Query($boolQuery);

        return $query;
    }

    /*
     * Función encargada de generar la consulta para la búsqueda en un foro concreto, de una categoría en concreto,
     * ordenados por fecha y de cualquier fecha
     *
     * @param string $searchTerm termino a buscar
     * @param integer $forum id del foro en el cual cual quiero hacer la búsqueda
     * @param integer $category id de la categoría en la cual quiero hacer la búsqueda
     * @param array $userRolesArray array con los roles de los que dispone el usuario
     * */
    public function findTopicsByForumCategoryOrderedByDate($searchTerm, $forum, $category, $userRolesArray){

        $query = $this->findTopicsByForumCategory($searchTerm, $forum, $category, $userRolesArray);
        $query->setSort(array('postingDate' => array('order' => 'desc')));

        return $query;
    }

    /*
     * Función encargada de generar la consulta para la búsqueda en un foro concreto, de una categoría en concreto,
     * ordenados por relevancia y de una fecha concreta hoy, última semana, etc
     *
     * @param string $searchTerm termino a buscar
     * @param integer $forum id del foro en el cual cual quiero hacer la búsqueda
     * @param integer $category id de la categoría en la cual quiero hacer la búsqueda
     * @param Datetime $dateStart array con la fecha a partir de la cual se buscan resultados
     * @param array $userRolesArray array con los roles de los que dispone el usuario
     * */
    public function findTopicsByForumCategoryAndInterval($searchTerm, $forum, $category, $dateStart, $userRolesArray){
        $acessRoleTerm = new \Elastica\Query\Terms();
        $acessRoleTerm->setTerms('topic.forum.accessRole.name', $userRolesArray);

        $categoryTerm = new \Elastica\Query\Term();
        $categoryTerm->setTerm('topic.category.id', $category);

        $forumTerm = new \Elastica\Query\Term();
        $forumTerm->setTerm('topic.forum.id', $forum);

        $titleQuery = new \Elastica\Query\Match();
        $titleQuery->setFieldQuery('topic.title', $searchTerm);

        $boolQuery = new \Elastica\Query\Bool();
        $boolQuery->addMust($acessRoleTerm);
        $boolQuery->addMust($forumTerm);
        $boolQuery->addMust($categoryTerm);
        $boolQuery->addMust($titleQuery);

        $filters = new \Elastica\Filter\Bool();
        $filters->addMust(
            new \Elastica\Filter\NumericRange('postingDate', array(
                'gte' => $dateStart->format('Y-m-d\TH:i:s'),
            ))
        );

        $query = new \Elastica\Query\Filtered($boolQuery, $filters);

        return $query;
    }

    /*
     * Función encargada de generar la consulta para la búsqueda en un foro concreto, de una categoría en concreto,
     * ordenados por fecha y de una fecha concreta hoy, última semana, etc
     *
     * @param string $searchTerm termino a buscar
     * @param integer $forum id del foro en el cual cual quiero hacer la búsqueda
     * @param integer $category id de la categoría en la cual quiero hacer la búsqueda
     * @param Datetime $dateStart array con la fecha a partir de la cual se buscan resultados
     * @param array $userRolesArray array con los roles de los que dispone el usuario
     * */
    public function findTopicsByForumCategoryAndIntervalOrderedByDate($searchTerm, $forum, $category, $dateStart, $userRolesArray){

        $filteredQuery = $this->findTopicsByForumCategoryAndInterval($searchTerm, $forum, $category, $dateStart, $userRolesArray);

        $query = new \Elastica\Query($filteredQuery);
        $query->setSort(array('postingDate' => array('order' => 'desc')));

        return $query;
    }
}