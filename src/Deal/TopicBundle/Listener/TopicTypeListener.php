<?php

namespace Deal\TopicBundle\Listener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormError;

/*
 * Listener encargado de validar las etiquetas introducidas por el usuario
 * */
class TopicTypeListener {

    public function preSubmit(FormEvent $event)
    {
        $form = $event->getForm();
        $tagsArray = $event->getData()->getTags();

        if(count($tagsArray) > 3){
            $form->get('tags')->addError(new FormError(
                'Solo puedes introducir 3 etiquetas'
            ));
        }

        $minChar = 2;
        $maxChar = 20;

        foreach ($tagsArray as $tag) {

            $tagName = $tag->getName();

            if(strlen($tagName) < $minChar){
                 $form->get('tags')->addError(new FormError(
                    'La longitud de las etiquetas como mínimo ' . $minChar . ' caracteres'
                ));
            }

            if(strlen($tagName) > $maxChar){
                $form->get('tags')->addError(new FormError(
                    'La longitud de las etiquetas como máximo es de ' . $maxChar . ' caracteres'
                ));
            }

            if(  !(preg_match('/^[a-zA-Z0-9ñÑáÁéÉíÍóÓúÚ\-\s]+$/', $tagName))  ){
                $form->get('tags')->addError(new FormError(
                    'Las etiquetas solo pueden contener letras, números, espacios en blanco y guiones (-)'
                ));
            }
        }
    }
} 