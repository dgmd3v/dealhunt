<?php

namespace Deal\TopicBundle\Util;


/*
 * Modificación de la clase SlugifierInterface del bundle FPNTagBundle
 * */
interface SlugifierInterface
{
    function slugify($string, $checkIfSlugInDB, $slugDelimiter);

    function slugifyTagsArray($tagsArray, $checkIfSlugInDB, $slugDelimiter);
}
