<?php

namespace Deal\TopicBundle\Util;

use Doctrine\ORM\EntityManager;

/*
 * Modificación de la clase Slugifier del bundle FPNTagBundle
 * */
class Slugifier implements SlugifierInterface
{
    // Inyecto el EntityManager para poder hacer operaciones en la base de datos
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /*
     * Función que crea un slug dado un texto
     *
     * @param string $text texto para el cual quiero el slug
     * @param boolean $checkIfSlugInDB indica si se quiere comprobar en la base de datos si ya está en uso el slug
     * @param string $slugDelimiter separador/delimitador utilizado para crear el slug
     */
    public function slugify($text, $checkIfSlugInDB = false, $slugDelimiter = '-')
    {
        // Código extraído de:
        // http://cubiq.org/the-perfect-php-clean-url-generator
        setlocale(LC_ALL,'es_ES.UTF8');
        $slug = iconv('UTF-8', 'ASCII//TRANSLIT', $text);
        $slug = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $slug);
        $slug = strtolower(trim($slug, $slugDelimiter));
        $slug = preg_replace("/[\/_|+ -]+/", $slugDelimiter, $slug);

        if($checkIfSlugInDB == true){
            // Obtengo el número de tags que ya tienen el mismo slug
            $numberOfRepeatedSlug = $this->em->getRepository('TopicBundle:Tag')->findNumberOfRepeatedSlug($slug);

            if($numberOfRepeatedSlug > 0) $slug = $slug . '_' . ($numberOfRepeatedSlug+1);
        }
        return $slug;
    }

    /*
     * Función asigna los slugs a un array de etiquetas comprobando que no se crean slugs repetidos
     *
     * @param object array $tagsArray array de etiquetas a las que les voy a dar un slug
     * @param boolean $checkIfSlugInDB indica si se quiere comprobar en la base de datos si ya está en uso el slug
     * @param string $slugDelimiter separador/delimitador utilizado para crear el slug
     */
    public function slugifyTagsArray($tagsArray, $checkIfSlugInDB = false, $slugDelimiter = '-')
    {
        foreach($tagsArray as $tag){
            // Código extraído de:
            // http://cubiq.org/the-perfect-php-clean-url-generator
            setlocale(LC_ALL,'es_ES.UTF8');
            $slug = iconv('UTF-8', 'ASCII//TRANSLIT', $tag->getName());
            $slug = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $slug);
            $slug = strtolower(trim($slug, $slugDelimiter));
            $slug = preg_replace("/[\/_|+ -]+/", $slugDelimiter, $slug);

            if($checkIfSlugInDB == true){
                // Compruebo si ya hay etiquetas con el mismo slug en la base de datos.
                // Obtengo el número de tags que ya tienen el mismo slug
                $numberOfRepeatedSlug = $this->em->getRepository('TopicBundle:Tag')->findNumberOfRepeatedSlug($slug);

                if($numberOfRepeatedSlug > 0) $slug = $slug . '_' . ($numberOfRepeatedSlug+1);
            }

            // Compruebo que el slug asignado a $tag no este ya en uso en otro tag del array de tags
            foreach($tagsArray as $tag2){
                if(    ($tag != $tag2)     &&  ($slug ==  $tag2->getSlug())   ){
                    if((preg_match('/^([a-zA-Z0-9ñáéíóú\-\s]+)_(\d+)$/', $tag2->getSlug(), $matches)) ){
                        $slug = $matches[1] . '_' . ($matches[2]+1);
                    }

                    else{
                        $slug = $slug . '_1';
                    }
                }
            }
            $tag->setSlug($slug);
        }
    }
}
