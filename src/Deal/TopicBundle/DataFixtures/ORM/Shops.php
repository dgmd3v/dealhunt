<?php

namespace Deal\TopicBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Deal\TopicBundle\Entity\Shop;

/*
 * Fixtures/Datos de prueba para la entidad shop
 * En este fixture creo las shops (tiendas), les doy un nombre, etc
 **/
class Shops extends AbstractFixture implements OrderedFixtureInterface
{
    // Orden en el que se carga este fixture
    public function getOrder()
    {
        return 140;
    }
    
    public function load(ObjectManager $manager)
    {
        // Array con los nombres de cada tienda
        $shops = array(         'Amazon.ES',
                                'Amazon.CO.UK',
                                'Amazon.DE',
                                'Amazon.IT',
                                'Amazon.FR',
                                'Ebay',
                                'El Corte Inglés',
                                'Hipercor',
                                'Alcampo',
                                'Carrefour',
                                'Pc Componentes',
                                'CoolMod',
                                'PhoneHouse',
                                'Steam',
                                'Origin',
                                'Uplay',
                                '4frags',
                                'ticketmaster',
                                'entradas.com',
                                'atrapalo.com',
                                'ticketea.com',
                                'Alimerka',
                                'MasYMas',
                                'El Arbol',
                                'Trivago',
                                'muchoviaje.com',
                                'Barcelo Viajes',
                                'McDonalds',
                                'Burger King',
                                'La Chalana',
                                'Tierra Astur',
                                'Otra'
                           );
        
        // Creo nuevas entidades de tipo tienda y relleno con su nombre y 
        // su slug (esto se hace internamente en la entidad)
        foreach ($shops as $shop){
            $entidad = new Shop();
            
            $entidad->setName($shop);
            
            // Hago la entidad persistente
            $manager->persist($entidad);
            
        }// Fin foreach
        
        // Guardo los datos marcados como persistentes
        $manager->flush();
    }
}
