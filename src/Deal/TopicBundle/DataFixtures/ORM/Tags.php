<?php

namespace Deal\TopicBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/*
 * Fixtures/Datos de prueba para la entidad tag (etiqueta)
 * En este fixture creo las tags (etiquetas), les doy un nombre, etc
 **/
class Tags extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    // Contenedor con el cual llamare a los servicios que necesite
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    // Orden en el que se carga este fixture
    public function getOrder()
    {
        return 160;
    }
    
    public function load(ObjectManager $manager)
    {
        // Array con los nombres de cada etiqueta
        $tags = array(         'Audiovisual',
                                'Sonido',
                                'Imagen',
                                'Móviles',
                                'Android',
                                'Ios',
                                'Gaming',
                                'Juegos',
                                'Steam y Origin',
                                'Ordenadores',
                                'Hardware',
                                'Periféricos',
                                'Entretenimiento',
                                'Musica',
                                'Concierto',
                                'Hogar',
                                'Muebles',
                                'Cocina',
                                'Moda',
                                'Ropa',
                                'Rebajas',
                                'Niños',
                                'Juguetes',
                                'Infantil',
                                'Comida',
                                'Verdura',
                                'Fruta',
                                'Viajes',
                                'Vacaciones',
                                'Avión',
                                'Restaurantes',
                                'Menú',
                                'Cena',
                                'Inútil',
                                'Timo',
                                'Regalo',
                                'Tarjeta',
                                'Que me estas container',
                                'Casa',
                                'Bayer',
                                'Aspirinas',
                                'Rappel',
                                'Futuro',
                                'Tesoro',
                                'Mapa',
                                'Arma',
                                'Defensa personal',
                                'Coches',
                                'Neumático',
                                'Arma',
                                'Defensa personal',
                                'China',
                                'Cocina extranjera',
                                'Italia',
                                'Cocina extranjera',
                                'Contundente',
                                'Chigre',
                                'Premium',
                                'Comida basura',
                                'Pizza',
                                'Grasas',
                                'Sándwich',
                                'Islandia',
                                'Escocia',
                                'Rusia',
                                'Alemania',
                                'Mediterráneo',
                                'Crucero',
                                'Berlin',
                                'New York',
                                'Lanzarote',
                                'PortAventura',
                                'Parque de atracciones',
                                'Disney',
                                'Frigo',
                                'Helado',
                                'Danone',
                                'Pascual',
                                'Bebida',
                                'Juver',
                                'Brillante',
                                'Cidacos',
                                'Conservas',
                                'Bimbo',
                                'Coca cola',
                                'Carbonell',
                                'Conservas',
                                'Scalextric',
                                'Infantil',
                                'Playmobil',
                                'Color Baby',
                                'Mattel',
                                'Motorola',
                                'Bebes',
                                'Lego',
                                'Recaro',
                                'Chicco',
                                'Jané',
                                'Casio',
                                'Reloj',
                                'Gafas',
                                'Rayban',
                                'Oakley',
                                'Calzado',
                                'Dr. Martens',
                                'Vans',
                                'Levis',
                                'Deporte',
                                'Adidas',
                                'Lacoste',
                                'Bosch',
                                'Thermomix',
                                'iRobot',
                                'Aspirador',
                                'Black & Decker',
                                'Herramientas',
                                'Philips',
                                'Bombilla',
                                'Taurus',
                                'Tefal',
                                'Tapa Inodoro',
                                'Roca',
                                'Entradas',
                                'Circo',
                                'Tour',
                                'Musical',
                                'Película',
                                'Teatro',
                                'Samsung',
                                'Disco duro',
                                'Intel',
                                'Procesador',
                                'Gigabyte',
                                'Tarjeta gráfica',
                                'Corsair',
                                'Fuente de alimentación',
                                'Asus',
                                'Portátil',
                                'Apple',
                                'Dell',
                                'Monitor',
                                'HP',
                                'Impresora',
                                'Caja ordenador',
                                'Seagate',
                                'Nintendo',
                                'Videojuego',
                                'Xbox One',
                                'PlayStation 4',
                                'Consola',
                                'Valve',
                                'Ozone',
                                'Alfombrilla',
                                'Logitech',
                                'Teclado',
                                'Genius',
                                'Volante',
                                'Logitech',
                                'Ratón',
                                'LG',
                                'Smartphone',
                                'BQ',
                                'Honor',
                                'Smartwatch',
                                'Manos libres',
                                'Teléfono móvil',
                                'Videocámara',
                                'Sony',
                                'Cámara',
                                'Reproductor de Blu-ray',
                                'Nikon',
                                'Mando a distancia',
                                'Televisión',
                                'Home Cinema',
                                'Gafas 3D',
                                'Barra de sonido',
                                'Hardware',
                                'RAM',
                                'Placa Base',
                                'Software',
                                'Antivirus',
                                'Amigo informatico',
                                'Lavavajillas',
                                'Cuchillo',
                                'Mopa',
                                'Dodot',
                                'Colonia',
                                'Gratis',
                                'Viajes largos',


                           );
        
        // Creo nuevas entidades de tipo tag y relleno con su nombre y
        // su slug (esto se hace internamente en la entidad)
        foreach ($tags as $tag){
            $tagManager = $this->container->get('fpn_tag.tag_manager');
            $entity = $tagManager->loadOrCreateTag($tag);
            
            // Hago la entidad persistente
            $manager->persist($entity);
            
        }// Fin foreach
        
        // Guardo los datos marcados como persistentes
        $manager->flush();
    }
}
