<?php

namespace Deal\TopicBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Deal\TopicBundle\Entity\TopicVoucher;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

/*
 * Fixtures/Datos de prueba para la entidad TopicVoucher (temas de cupones descuento)
 * En este fixture creo los topics (temas), les doy un titulo, categoría, etc
 **/
class TopicVouchers extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    // Contenedor con el cual llamare a los servicios que necesite
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    // Orden en el que se carga este fixture
    public function getOrder()
    {
        return 190;
    }

    public function load(ObjectManager $manager)
    {
        // Obtengo las categorías de la base de datos
        $categories = $manager->getRepository('TopicBundle:Category')->findAll();

        // Obtengo las tiendas de la base de datos
        $shops = $manager->getRepository('TopicBundle:Shop')->findAll();

        // Obtengo el foro de cupones descuento
        $forum = $manager->getRepository('ForumBundle:Forum')->findOneBySlug('cupones');

        // Obtengo los foros de la base de datos
        $posts = $manager->getRepository('PostBundle:Post')->findAll();

        // Obtengo los usuarios de la base de datos
        $users = $manager->getRepository('UserBundle:User')->findAll();
        $countUsers = count($users);

        // Array donde guardo los temas que he creado
        $topics = new ArrayCollection();

        // Elijo si quiero que tenga etiquetas o no
        $setTags = true;

        // Directorio raíz del proyecto
        $root = $this->container->get('kernel')->getRootDir();

        // Variable para trabajar con el sistema de ficheros
        $filesystem = new Filesystem();

        // Creo 200 temas
        for ($i=200; $i < 400; $i++){

            // Creo el tema
            $topic = new TopicVoucher();

            // Escojo una categoría al azar
            $category = $categories[array_rand($categories, 1)];

            // Foro al que pertenece el tema
            $topic->setForum($forum);

            // Obtengo datos para personalizar el tema
            $customData = $this->getCustomData($category);

            if($setTags == true){
                $tagManager = $this->container->get('fpn_tag.tag_manager');

                $tag1 = $manager->getRepository('TopicBundle:Tag')->findTagByName($customData['tag1']);
                $tag2 = $manager->getRepository('TopicBundle:Tag')->findTagByName($customData['tag2']);

                // Por si faltase alguna etiqueta
                if($tag1 == null){
                    $tag1 = $manager->getRepository('TopicBundle:Tag')->findTagBySlug($category->getSlug());
                }

                if($tag2 == null){
                    $tag2 = $manager->getRepository('TopicBundle:Tag')->findTagBySlug($category->getSlug());
                }

                // Elijo las etiquetas para el tema
                $tagManager->addTag($tag1, $topic);
                $tagManager->addTag($tag2, $topic);
            }

            // Tienda
            $shop = $customData['shop'];
            $topic->setShop($shop);

            // Título del tema
            $topic->setTitle($customData['title']);

            //Cambio el contenido del primer mensaje
            $firtsPost = $posts[(($i)*$countUsers)];
            $firtsPost->setMessage($customData['message']);

            // Id del primer mensaje
            $topic->setFirstPost($firtsPost);

            // Categoría al que pertenece el tema
            $topic->setCategory($category);

            // Id del primer mensaje
            $topic->setFirstPost($posts[(($i)*$countUsers)]);

            // Id del último mensaje
            $topic->setLastPost($posts[(($i+1)*$countUsers)-1]);

            // Url del la web/tienda donde se puede usar el cupon
            $topic->setUrl($customData['url']);

            // Imagen de la oferta
            $fromHere = $root .'/../web/bundles/topic/images/topicimages/' . $customData['image'];
            $toHere =  $root . '/../web/uploads/images/topic/' . $i . "_" . $customData['image'];
            $filesystem->copy($fromHere, $toHere);

            $topic->setTopicImageName($i . "_" . $customData['image']);

            // Fechas
            $firstPostDate = $firtsPost->getPostingDate();
            $dateStart = clone $firstPostDate;
            $dateStart->setTime(00,00);
            $dateStart->format('Y-m-d H:i:s');
            $dateEnd = clone $firstPostDate;
            $dateEnd->setTime(00,00);
            $dateEnd->format('Y-m-d H:i:s');
            $dateEnd->modify('+' . rand(1, 15) . 'days');

            // Hora de inicio de la mensaje
            $topic->setDateStart($dateStart);

            // Hora en la que finaliza la mensaje
            $topic->setDateEnd($dateEnd);

            // Votos de los usuarios al tema
            for($j=1; $j < count($users); $j++)
            {
                $users[$j]->addTopicVote($topic);

                $var = rand(1,2);

                if($var==1){
                    $topic->setVotes($topic->getVotes()+1);
                }
                else if ($var==2){
                    $topic->setVotes($topic->getVotes()-1);
                }

            }

            // Si el número de votos es mayor o igual que la variable global lo considero popular
            // y guardo la fecha en la que se hizo popular
            $globalVariables = $manager->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
            $minVotesToBePop = $globalVariables->getMinVotesToBePop();

            if($topic->getVotes() >= $minVotesToBePop){
                $datePopular = clone $firstPostDate;
                $datePopular->modify('+' . rand(10, 30) . 'minutes');

                $topic->setDateMadePopular($datePopular);
            }

            // Código descuento
            $topic->setVoucher($customData['code']);

            // Descuento que proporciona el código
            $topic->setVoucherDiscount($customData['discount']);

            // Gasto mínimo para poder usar el descuento
            $topic->setVoucherMinimum($customData['minimum']);

            // Hago la entidad persistente
            $manager->persist($firtsPost);
            $manager->persist($topic);

            // Guardo el tema/topic recien creado en un array
            $topics->add($topic);

        } // Fin for

        // Guardo los datos marcados como persistentes
        $manager->flush();

        if($setTags == true){
            foreach($topics as $topic){
                //Guardo las etiquetas asignadas al topic/tema (solo se puede hacer una vez hecho el persist y el flush)
                foreach($topic->getTags() as $tag){
                    $manager->persist($tagManager->unsafeCreateTagging($tag, $topic));
                }
            }
        }
        $manager->flush();
    }


    /*
     * Genero un titulo, el texto del primer mensaje, la tienda del tema, etc
     *
     * @param category $category del tema
     *
     **/
    private function getCustomData($category)
    {

        switch($category){
            case 'Audiovisual':

                $audiovisualShopNames = array(  'Amazon.ES',
                    'Amazon.CO.UK',
                    'Amazon.DE',
                    'Amazon.IT',
                    'Amazon.FR',
                    'Ebay',
                    'El Corte Inglés',
                    'Hipercor',
                    'Alcampo',
                    'Carrefour',
                    'Pc Componentes',
                    'CoolMod',
                );
                $shopIndex = array_rand($audiovisualShopNames, 1);
                $shopName = $audiovisualShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Móviles':
                $mobileShopNames = array(  'Amazon.ES',
                    'Amazon.CO.UK',
                    'Amazon.DE',
                    'Amazon.IT',
                    'Amazon.FR',
                    'Ebay',
                    'El Corte Inglés',
                    'Hipercor',
                    'Alcampo',
                    'Carrefour',
                    'Pc Componentes',
                    'CoolMod',
                    'PhoneHouse',
                );
                $shopIndex = array_rand($mobileShopNames, 1);
                $shopName = $mobileShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Gaming':
                $gamingShopNames = array(  'Amazon.ES',
                    'Amazon.CO.UK',
                    'Amazon.DE',
                    'Amazon.IT',
                    'Amazon.FR',
                    'Ebay',
                    'El Corte Inglés',
                    'Hipercor',
                    'Alcampo',
                    'Carrefour',
                    'Pc Componentes',
                    'CoolMod',
                    'Steam',
                    'Origin',
                    'Uplay',
                    '4frags',
                );
                $shopIndex = array_rand($gamingShopNames, 1);
                $shopName = $gamingShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Ordenadores':
                $computerShopNames = array(  'Amazon.ES',
                    'Amazon.CO.UK',
                    'Amazon.DE',
                    'Amazon.IT',
                    'Amazon.FR',
                    'Ebay',
                    'El Corte Inglés',
                    'Hipercor',
                    'Alcampo',
                    'Carrefour',
                    'Pc Componentes',
                    'CoolMod',
                    '4frags',
                );
                $shopIndex = array_rand($computerShopNames, 1);
                $shopName = $computerShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Entretenimiento':
                $entertainmentShopNames = array(
                    'El Corte Inglés',
                    'ticketmaster',
                    'entradas.com',
                    'atrapalo.com',
                    'ticketea.com',
                    'Otra',
                );

                $shopIndex = array_rand($entertainmentShopNames, 1);
                $shopName = $entertainmentShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Hogar':
                $homeShopNames = array(  'Amazon.ES',
                    'Amazon.CO.UK',
                    'Amazon.DE',
                    'Amazon.IT',
                    'Amazon.FR',
                    'Ebay',
                    'El Corte Inglés',
                    'Hipercor',
                    'Alcampo',
                    'Carrefour',
                );

                $shopIndex = array_rand($homeShopNames, 1);
                $shopName = $homeShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Moda':
                $fashionShopNames = array(  'Amazon.ES',
                    'Amazon.CO.UK',
                    'Amazon.DE',
                    'Amazon.IT',
                    'Amazon.FR',
                    'Ebay',
                    'El Corte Inglés',
                    'Hipercor',
                    'Alcampo',
                    'Carrefour',
                );

                $shopIndex = array_rand($fashionShopNames, 1);
                $shopName = $fashionShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Niños':
                $kidsShopNames = array(  'Amazon.ES',
                    'Amazon.CO.UK',
                    'Amazon.DE',
                    'Amazon.IT',
                    'Amazon.FR',
                    'Ebay',
                    'El Corte Inglés',
                    'Hipercor',
                    'Alcampo',
                    'Carrefour',
                );
                $shopIndex = array_rand($kidsShopNames, 1);
                $shopName = $kidsShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Comida':
                $foodShopNames = array(
                    'El Corte Inglés',
                    'Hipercor',
                    'Alcampo',
                    'Carrefour',
                    'Alimerka',
                    'MasYMas',
                    'El Arbol'
                );
                $shopIndex = array_rand($foodShopNames, 1);
                $shopName = $foodShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Viajes':
                $travelingShopNames = array(
                    'El Corte Inglés',
                    'Alcampo',
                    'Carrefour',
                    'atrapalo.com',
                    'Trivago',
                    'muchoviaje.com',
                    'Barcelo Viajes'
                );
                $shopIndex = array_rand($travelingShopNames, 1);
                $shopName = $travelingShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Restaurantes':
                $restaurantsShopNames = array(
                    'El Corte Inglés',
                    'Alcampo',
                    'Carrefour',
                    'McDonalds',
                    'Burger King',
                    'La Chalana',
                    'Tierra Astur'
                );
                $shopIndex = array_rand($restaurantsShopNames, 1);
                $shopName = $restaurantsShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            default:
                $otherShopNames = array(
                    'Ebay',
                    'El Corte Inglés',
                    'Hipercor',
                    'Alcampo',
                    'Carrefour',
                );
                $shopIndex = array_rand($otherShopNames, 1);
                $shopName = $otherShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);
        }
    }


    /*
     * Para no ponerlo en el mismo sitio y tener mas ordenado esta función se encarga exclusivamente de generar, titulo
     * mensaje, url e imagen
     *
     * @param string $shopName nombre de la tienda
     * @param category $category categoria de la oferta
     *
     **/
    private function getLeftData($category, $shopName)
    {
        $message = 'Aquí debería haber un texto explicando el descuento, si en alguna tienda no se hace o si hay condiciones para poder usar el descuento que no se pueden introducir en el formulario. También se puede dar una descripción del articulo o artículos que tienen el descuento, o hablar de la tienda, gastos de envío, etc, etc.';
        $code = $this->getRandomVoucher();

        $shopNameEncoded = urlencode($shopName);

        if($category->getName() == 'Audiovisual'){
            $minimum = rand(10, 1000);
            $discount = rand(5, 30);

            $audiovisualArray = array(
                array(
                    "title" => 'Vale descuento para cámaras de vídeo @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'camaradevideovieja.jpg',
                    "tag1" => 'Videocámara',
                    "tag2" => 'Audiovisual',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code

                ),

                array(
                    "title" => 'Descuento del ' . $discount . ' % en Tvs Samsung  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'randomsamsungtv.jpg',
                    "tag1" => 'Televisión',
                    "tag2" => 'Samsung',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code

                ),

                array(
                    "title" =>'Cupón descuento para todos los productos de Audiovisual de LG @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'logolg.jpg',
                    "tag1" => 'Audiovisual',
                    "tag2" => 'LG',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code

                ),

                array(
                    "title" =>'Descuento del ' . $discount . ' % en Cámaras Reflex @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'reflex.jpg',
                    "tag1" => 'Cámara',
                    "tag2" => 'Nikon',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code

                ),

                array(
                    "title" => $discount . ' % en productos de fotografía @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'camaracompacta.jpg',
                    "tag1" => 'Cámara',
                    "tag2" => 'Nikon',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code

                ),
            );

            $offerIndex = array_rand($audiovisualArray, 1);
            return $audiovisualArray[$offerIndex];
        }


        elseif($category->getName() == 'Móviles'){
            $minimum = rand(10, 700);
            $discount = rand(5, 30);

            $mobilesArray = array(
                array(
                    "title" =>'Descuento del ' . $discount . ' % en carcasas para móvil  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'carcasasmovil.jpg',
                    "tag1" => 'Smartphone',
                    "tag2" => 'Móviles',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>'Cupón descuento para todos los smartphones Samsung  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'smartphonesamsung.jpg',
                    "tag1" => 'Smartphone',
                    "tag2" => 'Samsung',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>'Vale por ' . $discount . ' € en la compra de productos LG  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'logolg.jpg',
                    "tag1" => 'Móviles',
                    "tag2" => 'LG',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" => $discount . ' % de descuento en smartwatches Motorola @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'motorolalogo.jpg',
                    "tag1" => 'Smartwatch',
                    "tag2" => 'Motorola',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" => $discount . ' € de descuento en productos de telefonía móvil LG @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'logolg.jpg',
                    "tag1" => 'Smartphone',
                    "tag2" => 'LG',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),

            );

            $offerIndex = array_rand($mobilesArray, 1);
            return $mobilesArray[$offerIndex];
        }


        elseif($category->getName() == 'Gaming'){
            $minimum = rand(10, 400);
            $discount = rand(5, 30);

            $gamingArray = array(
                array(
                    "title" =>'Descuento del ' . $discount . ' % en productos gaming Ozone  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'ozonelogo.jpg',
                    "tag1" => 'Gaming',
                    "tag2" => 'Ozone',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>'Cupón descuento para todos los teclados logitech  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'logitechlogo.png',
                    "tag1" => 'Teclado',
                    "tag2" => 'Logitech',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>'Vale por ' . $discount . ' € en la compra de productos Nintendo  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'orangebox.jpg',
                    "tag1" => 'Videojuego',
                    "tag2" => 'Nintendo',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>$discount . ' % de descuento en la saga Half-life PC @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'logohalflife.jpg',
                    "tag1" => 'Videojuego',
                    "tag2" => 'Valve',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" => $discount . ' € de descuento en juegos de consola  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'juegoconsola.jpg',
                    "tag1" => 'Videojuego',
                    "tag2" => 'Consola',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),
            );

            $offerIndex = array_rand($gamingArray, 1);
            return $gamingArray[$offerIndex];
        }


        elseif($category->getName() == 'Ordenadores'){
            $minimum = rand(10, 2000);
            $discount = rand(5, 30);

            $computersArray = array(

                array(
                    "title" =>'Rebaja del ' . $discount . ' % del precio con este vale en placas base  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'placabase.jpg',
                    "tag1" => 'Placa Base',
                    "tag2" => 'Hardware',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>'Aprovecha la oportunidad, ' . $discount . ' € de descuento en tarjetas gráficas  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'tarjetagraficarandom.jpg',
                    "tag1" => 'Tarjeta gráfica',
                    "tag2" => 'Hardware',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>'Cajas de pc un ' . $discount . ' % menos con este descuento  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'lianli.jpg',
                    "tag1" => 'Caja ordenador',
                    "tag2" => 'Hardware',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>$discount . ' % de descuento en discos duros  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'descuentodiscoduro.jpg',
                    "tag1" => 'Hardware',
                    "tag2" => 'Disco duro',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>'Rebaja de ' . $discount . ' € en el precio de memoria RAM con este cupón  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'memoriaram.jpg',
                    "tag1" => 'Hardware',
                    "tag2" => 'RAM',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),
            );

            $offerIndex = array_rand($computersArray, 1);
            return $computersArray[$offerIndex];
        }


        elseif($category->getName() == 'Entretenimiento'){
            $minimum = rand(7, 60);
            $discount = rand(5, 30);

            $entertainmentArray = array(

                array(
                    "title" =>'Rebaja del ' . $discount . ' % para entradas de cine  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'cineb.jpg',
                    "tag1" => 'Película',
                    "tag2" => 'Entradas',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>'Vale por ' . $discount . ' € de descuento en la próxima compra de entradas para conciertos  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'concierto.jpg',
                    "tag1" => 'Concierto',
                    "tag2" => 'Entradas',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>$discount . ' % de descuento en las entradas de teatro  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'teatro.jpg',
                    "tag1" => 'Teatro',
                    "tag2" => 'Entradas',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>'Cupón descuento para todos los estrenos de cine  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'cinea.jpg',
                    "tag1" => 'Película',
                    "tag2" => 'Entradas',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" => $discount . ' € de descuento para el tour del Real Madrid  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'tourbernabeu.jpg',
                    "tag1" => 'Tour',
                    "tag2" => 'Entradas',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
            "code" => $code
                ),
            );

            $offerIndex = array_rand($entertainmentArray, 1);
            return $entertainmentArray[$offerIndex];
        }


        elseif($category->getName() == 'Hogar'){
            $minimum = rand(5, 60);
            $discount = rand(5, 30);

            $homeArray = array(

                array(
                    "title" =>'Descuento del ' . $discount . ' % en lavadoras Bosch  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'boshlavadora.jpg',
                    "tag1" => 'Cocina',
                    "tag2" => 'Bosch',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>'Cupón descuento para todas los tostadoras Samsung  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'samsunglogo.jpg',
                    "tag1" => 'Cocina',
                    "tag2" => 'Samsung',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>'Vale por ' . $discount . ' € en la compra de productos de cocina LG  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'logolg.jpg',
                    "tag1" => 'Cocina',
                    "tag2" => 'LG',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>$discount . ' % de descuento en microondas Sony  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'sonylogo.jpg',
                    "tag1" => 'Cocina',
                    "tag2" => 'Sony',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" => $discount . ' € de descuento en aparatos de limpieza iRobot  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'irobotlogo.jpg',
                    "tag1" => 'Aspirador',
                    "tag2" => 'iRobot',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),
            );

            $offerIndex = array_rand($homeArray, 1);
            return $homeArray[$offerIndex];
        }


        elseif($category->getName() == 'Moda'){
            $minimum = rand(5, 200);
            $discount = rand(5, 30);

            $fashionArray = array(

                array(
                    "title" =>'Rebaja del ' . $discount . ' % del precio con este vale en Lacoste  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'lacostelogo.jpg',
                    "tag1" => 'Ropa',
                    "tag2" => 'Lacoste',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>'Aprovecha la oportunidad, ' . $discount . ' € de descuento para tu próxima compra de calzado Vans  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'vanslogo.jpg',
                    "tag1" => 'Calzado',
                    "tag2" => 'Vans',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>'Relojes Casio un ' . $discount . ' % menos con este descuento  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'casiologo.png',
                    "tag1" => 'Reloj',
                    "tag2" => 'Casio',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>$discount . ' % de descuento en gafas de sol Rayban  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'raybanlogo.jpg',
                    "tag1" => 'Gafas',
                    "tag2" => 'Rayban',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>'Rebaja de ' . $discount . ' € en el precio de ropa Adidas con este cupón  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'adidaslogo.jpg',
                    "tag1" => 'Deporte',
                    "tag2" => 'Adidas',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),
            );

            $offerIndex = array_rand($fashionArray, 1);
            return $fashionArray[$offerIndex];
        }


        elseif($category->getName() == 'Niños'){
            $minimum = rand(5, 200);
            $discount = rand(5, 30);

            $kidsArray = array(

                array(
                "title" =>'Descuentazo del ' . $discount . ' % en juguetes Lego  @ ' . $shopName,
                "message" => $message,
                "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                "image" => 'logolego.png',
                "tag1" => 'Juguetes',
                "tag2" => 'Lego',
                "discount" => $discount . ' %',
                "minimum" => $minimum,
                    "code" => $code
            ),

                array(
                    "title" =>'Rebaja de ' . $discount . ' € en el precio de monitores de bebe Motorola  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'intercomunicador.jpg',
                    "tag1" => 'Bebes',
                    "tag2" => 'Motorola',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>'Osos de peluche Disney rebajados un ' . $discount . ' € con este cupón  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'logodisney.jpg',
                    "tag1" => 'Juguetes',
                    "tag2" => 'Disney',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>'Rebaja del ' . $discount . ' % del precio con este vale en coches Scalextric  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'scalextric.jpg',
                    "tag1" => 'Juguetes',
                    "tag2" => 'Scalextric',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" => $discount . ' € de descuento en sillitas de bebe Recaro  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'sillarecaro.jpg',
                    "tag1" => 'Bebes',
                    "tag2" => 'Recaro',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),);

            $offerIndex = array_rand($kidsArray, 1);
            return $kidsArray[$offerIndex];
        }


        elseif($category->getName() == 'Comida'){
            $minimum = rand(5, 60);
            $discount = rand(5, 30);

            $foodArray = array(

                array(
                    "title" =>'Vale descuento para conservas Cidacos  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'cidacoslogo.png',
                    "tag1" => 'Conservas',
                    "tag2" => 'Cidacos',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>'Descuento del ' . $discount . ' % en productos Bimbo  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'bimbo.jpg',
                    "tag1" => 'Cocina',
                    "tag2" => 'Bimbo',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>'Cupón descuento para Danonino  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'danonino.jpg',
                    "tag1" => 'Cocina',
                    "tag2" => 'Danone',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>'Descuento del ' . $discount . ' % en helados Frigo  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'frigo.jpg',
                    "tag1" => 'Helado',
                    "tag2" => 'Frigo',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" => $discount . ' % en leches Pascual  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'pascual.jpg',
                    "tag1" => 'Bebida',
                    "tag2" => 'Pascual',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code
                ),
            );

            $offerIndex = array_rand($foodArray, 1);
            return $foodArray[$offerIndex];
        }


        elseif($category->getName() == 'Viajes'){
            $minimum = rand(60, 2000);
            $discount = rand(5, 30);

            $travelingArray = array(

                array(
                    "title" =>'Rebajas del ' . $discount . ' % en destinos a Disney  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'disney.jpg',
                    "tag1" => 'Parque de atracciones',
                    "tag2" => 'Disney',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>'Vuelos a New York un ' . $discount . ' % menos con este descuento  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'ny.jpg',
                    "tag1" => 'Vacaciones',
                    "tag2" => 'New York',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>'Aprovecha la oportunidad, ' . $discount . ' € de descuento cruceros por el mediterráneo  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'mediterraneo.jpg',
                    "tag1" => 'Crucero',
                    "tag2" => 'Mediterráneo',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>$discount . ' % de descuento en viajes a Rusia  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'russia.jpg',
                    "tag1" => 'Vacaciones',
                    "tag2" => 'Rusia',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>$discount . ' € de descuento en tu próximo viaje a Islandia  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'islandia.jpg',
                    "tag1" => 'Vacaciones',
                    "tag2" => 'Islandia',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),
            );

            $offerIndex = array_rand($travelingArray, 1);
            return $travelingArray[$offerIndex];
        }


        elseif($category->getName() == 'Restaurantes'){
            $minimum = rand(5, 60);
            $discount = rand(5, 30);

            $restaurantsArray = array(

                array(
                    "title" => $discount . ' € de descuento en la compra de tu menú  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'mcmenu.png',
                    "tag1" => 'Sándwich',
                    "tag2" => 'Comida basura',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" => $discount . ' % en menús en tu Italiano preferido  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'spaghetti.jpg',
                    "tag1" => 'Cocina extranjera',
                    "tag2" => 'Italia',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>'Aprovecha la oportunidad, ' . $discount . ' € de descuento en tu Pizza  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'dominos.jpeg',
                    "tag1" => 'Pizza',
                    "tag2" => 'Comida basura',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>'Rollitos de primavera un ' . $discount . ' % menos con este descuento  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'rollitos.jpg',
                    "tag1" => 'Cocina extranjera',
                    "tag2" => 'China',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" => $discount . ' % de descuento menú cantábrico  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'carne.jpg',
                    "tag1" => 'Contundente',
                    "tag2" => 'Menú',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),
            );


            $offerIndex = array_rand($restaurantsArray, 1);
            return $restaurantsArray[$offerIndex];
        }


        else{
            $minimum = rand(5, 1000);
            $discount = rand(5, 30);

            $othersArray = array(

                array(
                    "title" =>'Rebaja del ' . $discount . ' % del precio con este vale en bolas de cristal Rappel  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'bolacristal.jpg',
                    "tag1" => 'Futuro',
                    "tag2" => 'Rappel',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>'Aprovecha la oportunidad, ' . $discount . ' € por la compra de una estrella  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'estrella.jpg',
                    "tag1" => 'Timo',
                    "tag2" => 'Inútil',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>'Chicles usados de la Esteban un ' . $discount . ' % menos con este descuento  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'belenesteban.jpg',
                    "tag1" => 'Timo',
                    "tag2" => 'Inútil',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>$discount . ' % de descuento en armas de destrucción masiva  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'cachaba.jpg',
                    "tag1" => 'Arma',
                    "tag2" => 'Defensa personal',
                    "discount" => $discount . ' %',
                    "minimum" => $minimum,
                    "code" => $code
                ),

                array(
                    "title" =>'Rebaja de € en el precio de containers  @ ' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=descuento+' . $shopNameEncoded,
                    "image" => 'container.jpg',
                    "tag1" => 'Casa',
                    "tag2" => 'Que me estas container',
                    "discount" => $discount . ' €',
                    "minimum" => $minimum,
                    "code" => $code
                ),
            );

            $offerIndex = array_rand($othersArray, 1);
            return $othersArray[$offerIndex];
        }

    }

    /*
     * Función encargada de rellenar el array de datos
     *
     * @param string $shopName nombre de la tienda
     * @param category $category categoria de la oferta
     *
     **/
    private function fillDataArray($category, $shopName){
        $dataArray = array( "shop" => "",  "title" => "", "message" => "", "url" => "",
                            "image" => "", "tag1" => "", "tag2" => "", "discount" => "", "code" => "", "minimum" => "");

        $em = $this->container->get('doctrine.orm.entity_manager');

        $shop = $em->getRepository('TopicBundle:Shop')->findShopByName($shopName);
        $leftData = $this->getLeftData($category, $shopName);
        $dataArray['shop'] = $shop;
        $dataArray['title'] = $leftData['title'];
        $dataArray['message'] = $leftData['message'];
        $dataArray['url'] = $leftData['url'];
        $dataArray['image'] = $leftData['image'];
        $dataArray['tag1'] = $leftData['tag1'];
        $dataArray['tag2'] = $leftData['tag2'];
        $dataArray['discount'] = $leftData['discount'];
        $dataArray['code'] = $leftData['code'];
        $dataArray['minimum'] = $leftData['minimum'];
        return $dataArray;
    }

    /*
     * Función de generar un código descuento o devolver "imprimir" si el vale fuese para imprimir
     *
     * @param integer length longitud del código, por defecto 8
     *
     **/
    private function getRandomVoucher($length = 8){

        // Aleatoriamente devuelvo un código o IMPRIMIR
        $print = rand(0,1);

        $randomVoucher = "IMPRIMIR";

        if($print == 0){
            $length = rand(3,15);

            $char = '0123456789QWERTYUIOPASDFGHJKLZXCVBNM';
            $randomVoucher = substr(str_shuffle($char), 0, $length);

            return $randomVoucher;
        }

        else{
            return $randomVoucher;
        }
    }

}