<?php

namespace Deal\TopicBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Deal\TopicBundle\Entity\TopicFree;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

/*
 * Fixtures/Datos de prueba para la entidad TopicFree (Temas de artículos gratuitos)
 * En este fixture creo los topics (temas), les doy un titulo, categoría, etc
 **/
class TopicFrees extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    // Contenedor con el cual llamare a los servicios que necesite
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    // Orden en el que se carga este fixture
    public function getOrder()
    {
        return 200;
    }

    public function load(ObjectManager $manager)
    {
        // Obtengo las categorías de la base de datos
        $categories = $manager->getRepository('TopicBundle:Category')->findAll();

        // Obtengo las tiendas de la base de datos
        $shops = $manager->getRepository('TopicBundle:Shop')->findAll();

        // Obtengo el foro de temas gratuitos
        $forum = $manager->getRepository('ForumBundle:Forum')->findOneBySlug('gratis');

        // Obtengo los foros de la base de datos
        $posts = $manager->getRepository('PostBundle:Post')->findAll();

        // Obtengo los usuarios de la base de datos
        $users = $manager->getRepository('UserBundle:User')->findAll();
        $countUsers = count($users);

        // Array donde guardo los temas que he creado
        $topics = new ArrayCollection();

        // Elijo si quiero que tenga etiquetas o no
        $setTags = true;

        // Directorio raíz del proyecto
        $root = $this->container->get('kernel')->getRootDir();

        // Variable para trabajar con el sistema de ficheros
        $filesystem = new Filesystem();

        // Creo 240 temas
        for ($i=400; $i < 600; $i++){

            // Creo el tema
            $topic = new TopicFree();

            // Escojo una categoría al azar
            $category = $categories[array_rand($categories, 1)];

            // Foro al que pertenece el tema
            $topic->setForum($forum);

            // Obtengo datos para personalizar el tema
            $customData = $this->getCustomData($category);

            if($setTags == true){
                $tagManager = $this->container->get('fpn_tag.tag_manager');

                $tag1 = $manager->getRepository('TopicBundle:Tag')->findTagByName($customData['tag1']);
                $tag2 = $manager->getRepository('TopicBundle:Tag')->findTagByName($customData['tag2']);

                // Por si faltase alguna etiqueta
                if($tag1 == null){
                    $tag1 = $manager->getRepository('TopicBundle:Tag')->findTagBySlug($category->getSlug());
                }

                if($tag2 == null){
                    $tag2 = $manager->getRepository('TopicBundle:Tag')->findTagBySlug($category->getSlug());
                }

                // Elijo las etiquetas para el tema
                $tagManager->addTag($tag1, $topic);
                $tagManager->addTag($tag2, $topic);
            }

            // Tienda
            $shop = $customData['shop'];
            $topic->setShop($shop);

            // Título del tema
            $topic->setTitle($customData['title']);

            //Cambio el contenido del primer mensaje
            $firtsPost = $posts[(($i)*$countUsers)];
            $firtsPost->setMessage($customData['message']);

            // Id del primer mensaje
            $topic->setFirstPost($firtsPost);

            // Categoría al que pertenece el tema
            $topic->setCategory($category);

            // Id del primer mensaje
            $topic->setFirstPost($posts[(($i)*$countUsers)]);

            // Id del último mensaje
            $topic->setLastPost($posts[(($i+1)*$countUsers)-1]);

            // Url del la web/tienda donde se puede usar el cupon
            $topic->setUrl($customData['url']);

            // Imagen de la oferta
            $fromHere = $root .'/../web/bundles/topic/images/topicimages/' . $customData['image'];
            $toHere =  $root . '/../web/uploads/images/topic/' . $i . "_" . $customData['image'];
            $filesystem->copy($fromHere, $toHere);

            $topic->setTopicImageName($i . "_" . $customData['image']);

            // Fechas
            $firstPostDate = $firtsPost->getPostingDate();
            $dateStart = clone $firstPostDate;
            $dateStart->setTime(00,00);
            $dateStart->format('Y-m-d H:i:s');
            $dateEnd = clone $firstPostDate;
            $dateEnd->setTime(00,00);
            $dateEnd->format('Y-m-d H:i:s');
            $dateEnd->modify('+' . rand(1, 15) . 'days');

            // Hora de inicio de la mensaje
            $topic->setDateStart($dateStart);

            // Hora en la que finaliza la mensaje
            $topic->setDateEnd($dateEnd);

            // Votos de los usuarios al tema
            for($j=1; $j < count($users); $j++)
            {
                $users[$j]->addTopicVote($topic);

                $var = rand(1,2);

                if($var==1){
                    $topic->setVotes($topic->getVotes()+1);
                }
                else if ($var==2){
                    $topic->setVotes($topic->getVotes()-1);
                }

            }

            // Si el número de votos es mayor o igual que la variable global lo considero popular
            // y guardo la fecha en la que se hizo popular
            $globalVariables = $manager->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
            $minVotesToBePop = $globalVariables->getMinVotesToBePop();

            if($topic->getVotes() >= $minVotesToBePop){
                $datePopular = clone $firstPostDate;
                $datePopular->modify('+' . rand(10, 30) . 'minutes');

                $topic->setDateMadePopular($datePopular);
            }

            // Hago la entidad persistente
            $manager->persist($firtsPost);
            $manager->persist($topic);

            // Guardo el tema/topic recien creado en un array
            $topics->add($topic);

        } // Fin for

        // Guardo los datos marcados como persistentes
        $manager->flush();

        if($setTags == true){
            foreach($topics as $topic){
                //Guardo las etiquetas asignadas al topic/tema (solo se puede hacer una vez hecho el persist y el flush)
                foreach($topic->getTags() as $tag){
                    $manager->persist($tagManager->unsafeCreateTagging($tag, $topic));
                }
            }
        }
        $manager->flush();
    }


    /*
     * Genero un titulo, el texto del primer mensaje, la tienda del tema, etc
     *
     * @param category $category del tema
     *
     **/
    private function getCustomData($category)
    {

        switch($category){
            case 'Audiovisual':

                $audiovisualShopNames = array(  'Amazon.ES',
                    'Amazon.CO.UK',
                    'Amazon.DE',
                    'Amazon.IT',
                    'Amazon.FR',
                    'Ebay',
                    'El Corte Inglés',
                    'Hipercor',
                    'Alcampo',
                    'Carrefour',
                    'Pc Componentes',
                    'CoolMod',
                );
                $shopIndex = array_rand($audiovisualShopNames, 1);
                $shopName = $audiovisualShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Móviles':
                $mobileShopNames = array(  'Amazon.ES',
                    'Amazon.CO.UK',
                    'Amazon.DE',
                    'Amazon.IT',
                    'Amazon.FR',
                    'Ebay',
                    'El Corte Inglés',
                    'Hipercor',
                    'Alcampo',
                    'Carrefour',
                    'Pc Componentes',
                    'CoolMod',
                    'PhoneHouse',
                );
                $shopIndex = array_rand($mobileShopNames, 1);
                $shopName = $mobileShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Gaming':
                $gamingShopNames = array(  'Amazon.ES',
                    'Amazon.CO.UK',
                    'Amazon.DE',
                    'Amazon.IT',
                    'Amazon.FR',
                    'Ebay',
                    'El Corte Inglés',
                    'Hipercor',
                    'Alcampo',
                    'Carrefour',
                    'Pc Componentes',
                    'CoolMod',
                    'Steam',
                    'Origin',
                    'Uplay',
                    '4frags',
                );
                $shopIndex = array_rand($gamingShopNames, 1);
                $shopName = $gamingShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Ordenadores':
                $computerShopNames = array(  'Amazon.ES',
                    'Amazon.CO.UK',
                    'Amazon.DE',
                    'Amazon.IT',
                    'Amazon.FR',
                    'Ebay',
                    'El Corte Inglés',
                    'Hipercor',
                    'Alcampo',
                    'Carrefour',
                    'Pc Componentes',
                    'CoolMod',
                    '4frags',
                );
                $shopIndex = array_rand($computerShopNames, 1);
                $shopName = $computerShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Entretenimiento':
                $entertainmentShopNames = array(
                    'El Corte Inglés',
                    'ticketmaster',
                    'entradas.com',
                    'atrapalo.com',
                    'ticketea.com',
                    'Otra',
                );

                $shopIndex = array_rand($entertainmentShopNames, 1);
                $shopName = $entertainmentShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Hogar':
                $homeShopNames = array(  'Amazon.ES',
                    'Amazon.CO.UK',
                    'Amazon.DE',
                    'Amazon.IT',
                    'Amazon.FR',
                    'Ebay',
                    'El Corte Inglés',
                    'Hipercor',
                    'Alcampo',
                    'Carrefour',
                );

                $shopIndex = array_rand($homeShopNames, 1);
                $shopName = $homeShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Moda':
                $fashionShopNames = array(  'Amazon.ES',
                    'Amazon.CO.UK',
                    'Amazon.DE',
                    'Amazon.IT',
                    'Amazon.FR',
                    'Ebay',
                    'El Corte Inglés',
                    'Hipercor',
                    'Alcampo',
                    'Carrefour',
                );

                $shopIndex = array_rand($fashionShopNames, 1);
                $shopName = $fashionShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Niños':
                $kidsShopNames = array(  'Amazon.ES',
                    'Amazon.CO.UK',
                    'Amazon.DE',
                    'Amazon.IT',
                    'Amazon.FR',
                    'Ebay',
                    'El Corte Inglés',
                    'Hipercor',
                    'Alcampo',
                    'Carrefour',
                );
                $shopIndex = array_rand($kidsShopNames, 1);
                $shopName = $kidsShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Comida':
                $foodShopNames = array(
                    'El Corte Inglés',
                    'Hipercor',
                    'Alcampo',
                    'Carrefour',
                    'Alimerka',
                    'MasYMas',
                    'El Arbol'
                );
                $shopIndex = array_rand($foodShopNames, 1);
                $shopName = $foodShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Viajes':
                $travelingShopNames = array(
                    'El Corte Inglés',
                    'Alcampo',
                    'Carrefour',
                    'atrapalo.com',
                    'Trivago',
                    'muchoviaje.com',
                    'Barcelo Viajes'
                );
                $shopIndex = array_rand($travelingShopNames, 1);
                $shopName = $travelingShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Restaurantes':
                $restaurantsShopNames = array(
                    'El Corte Inglés',
                    'Alcampo',
                    'Carrefour',
                    'McDonalds',
                    'Burger King',
                    'La Chalana',
                    'Tierra Astur'
                );
                $shopIndex = array_rand($restaurantsShopNames, 1);
                $shopName = $restaurantsShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            default:
                $otherShopNames = array(
                    'Ebay',
                    'El Corte Inglés',
                    'Hipercor',
                    'Alcampo',
                    'Carrefour',
                );
                $shopIndex = array_rand($otherShopNames, 1);
                $shopName = $otherShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);
        }
    }


    /*
     * Para no ponerlo en el mismo sitio y tener mas ordenado esta función se encarga exclusivamente de generar, titulo
     * mensaje, url e imagen
     *
     * @param string $shopName nombre de la tienda
     * @param category $category categoria de la oferta
     *
     **/
    private function getLeftData($category, $shopName)
    {
        $message = 'Aquí debería haber un texto explicando el producto gratuito, si en alguna tienda no se hace o si hay condiciones para poder obtener el articulo gratis que no se pueden introducir en el formulario. También se puede dar una descripción del articulo o artículos gratuitos, o hablar de la tienda, gastos de envío si los hubiese, etc, etc.';

        $shopNameEncoded = urlencode($shopName);

        if($category->getName() == 'Audiovisual'){

            $audiovisualArray = array(
                array(
                    "title" => 'Gratis carretes de fotos Kodak @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'logokodak.jpg',
                    "tag1" => 'Cámara',
                    "tag2" => 'Audiovisual',
                ),

                array(
                    "title" => 'Regalan Gafas 3D Samsung @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'samsunglogo.jpg',
                    "tag1" => 'Gafas 3D',
                    "tag2" => 'Samsung'
                ),

                array(
                    "title" => 'Prueba gratis durante un mes una TV LG @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'logolg.jpg',
                    "tag1" => 'Televisión',
                    "tag2" => 'LG',
                ),

                array(
                    "title" => 'Regalo de kit de limpieza para cámaras Reflex @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'reflex.jpg',
                    "tag1" => 'Cámara',
                    "tag2" => 'Nikon',
                ),

                array(
                    "title" => 'Regalan cámara de fotos por hacerte socio @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'camaradefotosrandom2.jpg',
                    "tag1" => 'Cámara',
                    "tag2" => 'Nikon',
                ),
            );

            $offerIndex = array_rand($audiovisualArray, 1);
            return $audiovisualArray[$offerIndex];
        }


        elseif($category->getName() == 'Móviles'){

            $mobilesArray = array(
                array(
                    "title" => 'Regalan saldo gratis para móvil  @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'saldomovil.jpg',
                    "tag1" => 'Smartphone',
                    "tag2" => 'Teléfono móvil',
                ),

                array(
                    "title" => 'Gratis total!!! memorias SD Samsung @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'memoriasamsungsd.jpg',
                    "tag1" => 'Smartphone',
                    "tag2" => 'Samsung',

                ),

                array(
                    "title" => 'Batería recargable de los chinos gratis!!! @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'bateriadeloschinos.jpg',
                    "tag1" => 'Móviles',
                    "tag2" => 'Smartphone',
                ),

                array(
                    "title" => 'Regalan saldo en Google Play por hacerte socio @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'googleplay.jpg',
                    "tag1" => 'Móviles',
                    "tag2" => 'Smartphone',
                ),

                array(
                    "title" => 'Dos aplicaciones gratis en Amazon App Store por hacerte socio @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'amazonappstore.jpeg',
                    "tag1" => 'Móviles',
                    "tag2" => 'Smartphone',
                ),

            );

            $offerIndex = array_rand($mobilesArray, 1);
            return $mobilesArray[$offerIndex];
        }


        elseif($category->getName() == 'Gaming'){

            $gamingArray = array(
                array(
                    "title" => 'Regalan alfombrillas Ozone por suscribirte a la lista de correo @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'alfombrillaozone2.jpg',
                    "tag1" => 'Gaming',
                    "tag2" => 'Ozone',
                ),

                array(
                    "title" => 'Gratis ratón si el ultimo año has comprado dos productos logitech @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'ratoncutrelogitech.jpg',
                    "tag1" => 'Ratón',
                    "tag2" => 'Logitech',
                ),

                array(
                    "title" => 'Battletoads gratis, juegazo!!! @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'battletoads.jpg',
                    "tag1" => 'Videojuego',
                    "tag2" => 'Nintendo',
                ),

                array(
                    "title" => 'Portal gratis por tiempo limitado @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'portal.jpg',
                    "tag1" => 'Videojuego',
                    "tag2" => 'Valve',
                ),

                array(
                    "title" => 'Suscripción Gold de Playstation gratis durante un mes!!! @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'playgold.jpg',
                    "tag1" => 'Videojuego',
                    "tag2" => 'Consola',
                ),
            );

            $offerIndex = array_rand($gamingArray, 1);
            return $gamingArray[$offerIndex];
        }


        elseif($category->getName() == 'Ordenadores'){

            $computersArray = array(

                array(
                    "title" => 'Regalan memorias RAM DDR usadas!!! @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'ramvieja.jpg',
                    "tag1" => 'Hardware',
                    "tag2" => 'RAM',
                ),

                array(
                    "title" => 'Mini pc RaspBerry Pi gratis al suscribirte @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'rasp.jpg',
                    "tag1" => 'Placa Base',
                    "tag2" => 'Hardware',
                ),

                array(
                    "title" => 'Pilas de PC gratis!!!! @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'pilapc.jpg',
                    "tag1" => 'Placa Base',
                    "tag2" => 'Hardware',
                ),

                array(
                    "title" => 'Antivirus Panda gratis para siempre @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'panda.jpg',
                    "tag1" => 'Software',
                    "tag2" => 'Antivirus',
                ),

                array(
                    "title" => 'Formateo de PC gratuito @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'amigoinformatico.jpeg',
                    "tag1" => 'Software',
                    "tag2" => 'Amigo informatico',
                ),
            );

            $offerIndex = array_rand($computersArray, 1);
            return $computersArray[$offerIndex];
        }


        elseif($category->getName() == 'Entretenimiento'){

            $entertainmentArray = array(

                array(
                    "title" => 'Entradas de cine de cine gratis al registrarte  @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'entradacine.jpg',
                    "tag1" => 'Película',
                    "tag2" => 'Entradas',
                ),

                array(
                    "title" => 'Palomitas gratis al comprar tus entradas en @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'palomitas.jpg',
                    "tag1" => 'Película',
                    "tag2" => 'Entradas',
                ),

                array(
                    "title" => 'Teatro: Hamlet, gratis total!! @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'shakespeare.jpg',
                    "tag1" => 'Teatro',
                    "tag2" => 'Entradas',
                ),

                array(
                    "title" => 'Sorteo de entradas gratuitas @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'entradasgratiscine.jpg',
                    "tag1" => 'Película',
                    "tag2" => 'Entradas',
                ),

                array(
                    "title" => 'Entradas gratis para el tour del Real Madrid @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'tourbernabeu.jpg',
                    "tag1" => 'Tour',
                    "tag2" => 'Entradas',
                ),
            );

            $offerIndex = array_rand($entertainmentArray, 1);
            return $entertainmentArray[$offerIndex];
        }


        elseif($category->getName() == 'Hogar'){

            $homeArray = array(

                array(
                    "title" => 'Muestra de lavavajillas gratis!!! @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'muestralavavajillas.jpg',
                    "tag1" => 'Cocina',
                    "tag2" => 'Lavavajillas',
                ),

                array(
                    "title" => 'Gratis tostadoras Samsung al suscribirte @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'tostadorasamsung.jpg',
                    "tag1" => 'Cocina',
                    "tag2" => 'Samsung',
                ),

                array(
                    "title" => 'Cuchillo gratis con la compra de tu mantel @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'cuchillo.jpg',
                    "tag1" => 'Cocina',
                    "tag2" => 'Cuchillo',
                ),

                array(
                    "title" => 'Gratis recambios de mopa atrapa polvo @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'recambiosmopa.jpg',
                    "tag1" => 'Hogar',
                    "tag2" => 'Mopa',
                ),

                array(
                    "title" => 'Recambios para iRobot gratis @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'recambiosroomba.jpg',
                    "tag1" => 'Aspirador',
                    "tag2" => 'iRobot',
                ),
            );

            $offerIndex = array_rand($homeArray, 1);
            return $homeArray[$offerIndex];
        }


        elseif($category->getName() == 'Moda'){

            $fashionArray = array(

                array(
                    "title" => 'Gratis polos con defectos de la marca Lacoste @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'pololacostefake.jpg',
                    "tag1" => 'Ropa',
                    "tag2" => 'Lacoste',
                ),

                array(
                    "title" => 'Regalan Vans provenientes de un incendio @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'vansburning.jpg',
                    "tag1" => 'Calzado',
                    "tag2" => 'Vans',
                ),

                array(
                    "title" => 'Relojes Casio con la pantalla rota gratis!!! @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'brokencasio.jpg',
                    "tag1" => 'Reloj',
                    "tag2" => 'Casio',
                ),

                array(
                    "title" => 'Gafas Rayban sin cristales gratis!!! @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'brokenrayban.jpg',
                    "tag1" => 'Gafas',
                    "tag2" => 'Rayban',
                ),

                array(
                    "title" => 'Camisetas Adidas gratis al registrarte @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'adidasshirt.jpeg',
                    "tag1" => 'Deporte',
                    "tag2" => 'Adidas',
                ),
            );

            $offerIndex = array_rand($fashionArray, 1);
            return $fashionArray[$offerIndex];
        }


        elseif($category->getName() == 'Niños'){

            $kidsArray = array(

                array(
                    "title" => 'Regalan una pieza de lego, que generosos!!! @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'piezalego.jpg',
                    "tag1" => 'Juguetes',
                    "tag2" => 'Lego',
                ),

                array(
                    "title" => 'Muestra gratis de Dodot @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'muestradodot.jpg',
                    "tag1" => 'Bebes',
                    "tag2" => 'Dodot',
                ),

                array(
                    "title" => 'Mini osos de peluche Disney gratis @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'minibear.jpg',
                    "tag1" => 'Juguetes',
                    "tag2" => 'Disney',
                ),

                array(
                    "title" => 'Muestra de champú para bebe gratis @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'muestrachampu.jpg',
                    "tag1" => 'Bebes',
                    "tag2" => 'Champú',
                ),

                array(
                    "title" => 'Muestra de colonia para bebe gratis @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'muestracolonia.jpg',
                    "tag1" => 'Bebes',
                    "tag2" => 'Colonia',
                ),);

            $offerIndex = array_rand($kidsArray, 1);
            return $kidsArray[$offerIndex];
        }


        elseif($category->getName() == 'Comida'){

            $foodArray = array(

                array(
                    "title" => 'Muestra de mermelada Cidacos gratis!!!! @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'muestramermelada.jpg',
                    "tag1" => 'Conservas',
                    "tag2" => 'Cidacos',
                ),

                array(
                    "title" => 'Regalo de una rebanada de pan Bimbo @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'rebanada.jpg',
                    "tag1" => 'Cocina',
                    "tag2" => 'Bimbo',
                ),

                array(
                    "title" => 'Un Danonino gratis al registrarte @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'undanonino.jpg',
                    "tag1" => 'Cocina',
                    "tag2" => 'Danone',
                ),

                array(
                    "title" => 'Helados Frigo caducados gratis!!!! tonto el último!!!  @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'heladocaducado.jpg',
                    "tag1" => 'Helado',
                    "tag2" => 'Frigo',
                ),

                array(
                    "title" => '1 Litro de leche Pascual por la face  @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'litrogratisleche.jpg',
                    "tag1" => 'Bebida',
                    "tag2" => 'Pascual',
                ),
            );

            $offerIndex = array_rand($foodArray, 1);
            return $foodArray[$offerIndex];
        }


        elseif($category->getName() == 'Viajes'){

            $travelingArray = array(

                array(
                    "title" => 'Viaje de aquí a la esquina gratis!!! @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'esquina.jpg',
                    "tag1" => 'Vacaciones',
                    "tag2" => 'Gratis',
                ),

                array(
                    "title" => 'Vuelos a New York its free!!! @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'aterrizacomopuedas.jpg',
                    "tag1" => 'Vacaciones',
                    "tag2" => 'New York',
                ),

                array(
                    "title" => 'Viajes en cruceros destartalados por el mediterráneo gratis!! (el seguro no es gratis) @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'crucerocutre.jpg',
                    "tag1" => 'Crucero',
                    "tag2" => 'Mediterráneo',
                ),

                array(
                    "title" => 'Viaje de ida gratis a Rusia (para volver a pata) @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'russianbear.jpg',
                    "tag1" => 'Vacaciones',
                    "tag2" => 'Rusia',
                ),

                array(
                    "title" => 'Viajes Gijón-Avilés gratis en Alsa haciéndote cuenta en @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'alsalogo.jpg',
                    "tag1" => 'Vacaciones',
                    "tag2" => 'Viajes largos',
                ),
            );

            $offerIndex = array_rand($travelingArray, 1);
            return $travelingArray[$offerIndex];
        }


        elseif($category->getName() == 'Restaurantes'){

            $restaurantsArray = array(

                array(
                    "title" => 'Menú Sándwich gratis si friegas tu plato y alguno más!!! @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'dirtydishes.jpg',
                    "tag1" => 'Sándwich',
                    "tag2" => 'Comida basura',
                ),

                array(
                    "title" => 'Botella de vino gratis en tu Italiano preferido @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'vinoitaliano.jpg',
                    "tag1" => 'Cocina extranjera',
                    "tag2" => 'Italia',
                ),

                array(
                    "title" => 'Mini pizza gratis en nuestro restaurante @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'minipizza.jpg',
                    "tag1" => 'Pizza',
                    "tag2" => 'Comida basura',
                ),

                array(
                    "title" => 'Rollitos gratis (no garantizamos que estén en perfectas condiciones) @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'rollitos2.jpg',
                    "tag1" => 'Cocina extranjera',
                    "tag2" => 'China',
                ),

                array(
                    "title" => 'Gratis menú cantábrico @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'parrilla.jpg',
                    "tag1" => 'Contundente',
                    "tag2" => 'Menú',
                ),
            );


            $offerIndex = array_rand($restaurantsArray, 1);
            return $restaurantsArray[$offerIndex];
        }


        else{

            $othersArray = array(

                array(
                    "title" => 'Bolas de cristal Rappel gratis total!! (parece que no han tenido éxito) @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'rappelposando.jpg',
                    "tag1" => 'Futuro',
                    "tag2" => 'Rappel',
                ),

                array(
                    "title" => 'Regalan una estrella fugaz!!! @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'estrellafugaz.jpg',
                    "tag1" => 'Timo',
                    "tag2" => 'Inútil',
                ),

                array(
                    "title" => 'Se regalan chicles usados @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'estebanjabba.jpg',
                    "tag1" => 'Timo',
                    "tag2" => 'Inútil',
                ),

                array(
                    "title" => 'Están regalando escopetas de feria por cese de negocio @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'escopetaferia.jpg',
                    "tag1" => 'Arma',
                    "tag2" => 'Defensa personal',
                ),

                array(
                    "title" => 'Containers con un poco de olor a pescado gratis por falta de sitio @' . $shopName,
                    "message" => $message,
                    "url" => 'http://lmgtfy.com/?q=gratis+' . $shopNameEncoded,
                    "image" => 'containersunk.jpg',
                    "tag1" => 'Casa',
                    "tag2" => 'Que me estas container',
                ),
            );

            $offerIndex = array_rand($othersArray, 1);
            return $othersArray[$offerIndex];
        }

    }

    /*
     * Función encargada de rellenar el array de datos
     *
     * @param string $shopName nombre de la tienda
     * @param category $category categoria de la oferta
     *
     **/
    private function fillDataArray($category, $shopName){
        $dataArray = array( "shop" => "",  "title" => "", "message" => "", "url" => "",
            "image" => "", "tag1" => "", "tag2" => "");

        $em = $this->container->get('doctrine.orm.entity_manager');

        $shop = $em->getRepository('TopicBundle:Shop')->findShopByName($shopName);
        $leftData = $this->getLeftData($category, $shopName);
        $dataArray['shop'] = $shop;
        $dataArray['title'] = $leftData['title'];
        $dataArray['message'] = $leftData['message'];
        $dataArray['url'] = $leftData['url'];
        $dataArray['image'] = $leftData['image'];
        $dataArray['tag1'] = $leftData['tag1'];
        $dataArray['tag2'] = $leftData['tag2'];
        return $dataArray;
    }


}










