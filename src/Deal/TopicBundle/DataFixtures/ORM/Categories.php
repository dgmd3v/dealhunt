<?php

namespace Deal\TopicBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Deal\TopicBundle\Entity\Category;

/*
 * Fixtures/Datos de prueba para la entidad category
 * En este fixture creo las categorías, les doy un nombre, etc
 **/
class Categories extends AbstractFixture implements OrderedFixtureInterface
{
    // Orden en el que se carga este fixture
    public function getOrder()
    {
        return 120;
    }
    
    public function load(ObjectManager $manager)
    {
        // Array con los nombres de cada categoría
        $categories = array(    'Audiovisual',
                                'Móviles',
                                'Gaming',
                                'Ordenadores',
                                'Entretenimiento',
                                'Hogar',
                                'Moda',
                                'Niños',
                                'Comida',
                                'Viajes',
                                'Restaurantes',
                                'Otros'
                           );
        
        // Creo nuevas entidades de tipo categoría y relleno con su nombre y
        // su slug (esto se hace internamente en la entidad)
        foreach ($categories as $category){
            $entity = new Category();

            $entity->setName($category);
            
            // Hago la entidad persistente
            $manager->persist($entity);
            
        }// Fin foreach
        
        // Guardo los datos marcados como persistentes
        $manager->flush();
    }
}
