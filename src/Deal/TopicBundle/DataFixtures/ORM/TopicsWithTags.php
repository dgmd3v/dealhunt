<?php

namespace Deal\TopicBundle\DataFixtures\ORM;

use Deal\TopicBundle\Entity\TopicWithTags;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Deal\TopicBundle\Entity\Topic;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Fixtures/Datos de prueba para la entidad topics/temas
 * En este fixture creo los topics (temas), les doy un titulo, categoría, etc
 **/
class TopicsWithTags extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    // Contenedor con el cual llamare a los servicios que necesite
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    // Orden en el que se carga este fixture
    public function getOrder()
    {
        return 210;
    }

    public function load(ObjectManager $manager)
    {
        // Obtengo las categorías de la base de datos
        $categories = $manager->getRepository('TopicBundle:Category')->findAll();

        // Obtengo los foros de la base de datos y los introduzco en un array
        $misc = $manager->getRepository('ForumBundle:Forum')->findOneBySlug('miscelanea');
        $feedback = $manager->getRepository('ForumBundle:Forum')->findOneBySlug('feedback');
        $forums = array($misc, $feedback);

        // Obtengo los foros de la base de datos
        $posts = $manager->getRepository('PostBundle:Post')->findAll();

        // Obtengo las etiquetas de la base de datos
        $tags = $manager->getRepository('TopicBundle:Tag')->findAll();

        // Array donde guardo los temas que he creado
        $topics = new ArrayCollection();

        // Obtengo los usuarios de la base de datos
        $users = $manager->getRepository('UserBundle:User')->findAll();
        $countUsers = count($users);

        // Elijo si quiero que tenga etiquetas o no
        $setTags = true;

        // Creo 200 temas
        for ($i=600; $i < 1000; $i++){

            // Creo el tema
            $topic = new TopicWithTags();

            // Escojo una categoría al azar
            $category = $categories[array_rand($categories, 1)];

            // Escojo un foro al azar
            $forum = $forums[array_rand($forums, 1)];

            // Foro al que pertenece el tema
            $topic->setForum($forum);

            // Genero información para customizar la el tema
            $customData = $this->getCustomData($forum->getName());

            // Título del tema
            $topic->setTitle($customData['title']);

            // Categoría al que pertenece el tema
            $topic->setCategory($category);

            //Cambio el contenido del primer mensaje
            $firtsPost = $posts[(($i)*$countUsers)];
            $firtsPost->setMessage($customData['message']);

            if($setTags == true){
                $tagManager = $this->container->get('fpn_tag.tag_manager');

                // Elijo una etiqueta para el tema
                switch ($category->getSlug()){
                    case 'audiovisual':
                        $tagManager->addTag($tags[rand(0,1)], $topic);
                        $tagManager->addTag($tags[2], $topic);
                        break;
                    case 'moviles':
                        $tagManager->addTag($tags[rand(3,5)], $topic);
                        $tagManager->addTag($tags[2], $topic);
                        break;
                    case 'gaming':
                        $tagManager->addTag($tags[rand(6,8)], $topic);
                        $tagManager->addTag($tags[2], $topic);
                        break;
                    case 'ordenadores':
                        $tagManager->addTag($tags[rand(9,11)], $topic);
                        $tagManager->addTag($tags[2], $topic);
                        break;
                    case 'entretenimiento':
                        $tagManager->addTag($tags[rand(12,14)], $topic);
                        $tagManager->addTag($tags[2], $topic);
                        break;
                    case 'hogar':
                        $tagManager->addTag($tags[rand(15,17)], $topic);
                        break;
                    case 'moda':
                        $tagManager->addTag($tags[rand(18,20)], $topic);
                        break;
                    case 'ninos':
                        $tagManager->addTag($tags[rand(21,23)], $topic);
                        break;
                    case 'comida':
                        $tagManager->addTag($tags[rand(24,26)], $topic);
                        break;
                    case 'viajes':
                        $tagManager->addTag($tags[rand(27,29)], $topic);
                        break;
                    case 'restaurantes':
                        $tagManager->addTag($tags[rand(30,32)], $topic);
                        break;
                }
            }

            // Categoría al que pertenece el tema
            $topic->setCategory($category);

            // Id del primer mensaje
            $topic->setFirstPost($posts[(($i)*$countUsers)]);

            // Id del último mensaje
            $topic->setLastPost($posts[(($i+1)*$countUsers)-1]);

            // Hago la entidad persistente
            $manager->persist($firtsPost);
            $manager->persist($topic);

            // Guardo el tema/topic recien creado en un array
            $topics->add($topic);
        } // Fin for

        // Guardo los datos marcados como persistentes
        $manager->flush();

        if($setTags == true){
            foreach($topics as $topic){
                //Guardo las etiquetas asignadas al topic/tema (solo se puede hacer una vez hecho el persist y el flush)
                foreach($topic->getTags() as $tag){
                    $manager->persist($tagManager->unsafeCreateTagging($tag, $topic));
                }
            }
        }
        $manager->flush();
    }


    /*
 * Genero un titulo, el texto del primer mensaje, etc
 *
 * @param string $forumName nombre del foro a la que pertenece el topic/tema
 *
 **/
    private function getCustomData($forumName)
    {
        $customData = array('title' => '', 'message' => '');

        if($forumName == 'Feedback'){
            $titlesArray = array(   'Uso internet explorer 6 y no me va muy bien la web, como mejorarlo?',
                                    'Más bbcodes? Queremos colores para llenar los hilos de colorines y provocar cegera',
                                    'Se podrían poner mas servidores de vídeo en los vídeos?',
                                    'Esta permitido hacer SPAM?',
                                    'Tengo un 486 y a veces la web me va lenta, soluciones?',
                                    'Como hago para insertar imágenes en los temas?',
                                    'Que tipo de imágenes puedo usar en las ofertas?',
                                    'Se pueden crear temas repetidos?',
            );

            $randomTitle = array_rand($titlesArray, 1);

            $customData['title'] = $titlesArray[$randomTitle];

            $message = "Aquí debería haber un texto pidiendo alguna mejora en la aplicación web, o si el usuario ha encontrado un bug información del mismo y de como ha llegado a el, sugerencias en general sobre la aplicación web.";
            $customData['message'] = $message;

        }

        else{
            $titlesArray = array(   'Se pueden crear temas repetidos?',
                                    'Barcelona vs Madrid, opiniones?',
                                    'Política: elecciones 2015',
                                    'Algún amigo informático me hace un presupuesto para un ordenador?',
                                    'Es buen momento para comprarse una casa?',
                                    'Halt and catch fire (peligro spoilers)',
                                    'Cine: Interstellar (2014)',
            );

            $randomTitle = array_rand($titlesArray, 1);

            $customData['title'] = $titlesArray[$randomTitle];

            $message = "Aquí debería haber un texto explicando de que va el hilo, en plan offtopic, fútbol, política, cine, series, ordenadores, videojuegos, decoración, etc, etc";
            $customData['message'] = $message;
        }

        return  $customData;
    }

}
