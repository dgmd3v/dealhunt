<?php

namespace Deal\TopicBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Deal\TopicBundle\Entity\TopicDeal;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

/*
 * Fixtures/Datos de prueba para la entidad TopicDeal (Temas de ofertas)
 * En este fixture creo los topics (temas), les doy un titulo, categoría, etc
 **/
class TopicDeals extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    // Contenedor con el cual llamare a los servicios que necesite
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    // Orden en el que se carga este fixture
    public function getOrder()
    {
        return 180;
    }

    public function load(ObjectManager $manager)
    {
        // Obtengo las categorías de la base de datos
        $categories = $manager->getRepository('TopicBundle:Category')->findAll();

        // Obtengo el foro de temas gratuitos
        $forum = $manager->getRepository('ForumBundle:Forum')->findOneBySlug('ofertas');

        // Obtengo los foros de la base de datos
        $posts = $manager->getRepository('PostBundle:Post')->findAll();

        // Obtengo los usuarios de la base de datos
        $users = $manager->getRepository('UserBundle:User')->findAll();
        $countUsers = count($users);

        // Array donde guardo los temas que he creado
        $topics = new ArrayCollection();

        // Elijo si quiero que tenga etiquetas o no
        $setTags = true;

        // Directorio raíz del proyecto
        $root = $this->container->get('kernel')->getRootDir();

        // Variable para trabajar con el sistema de ficheros
        $filesystem = new Filesystem();

        // Creo la carpeta donde se guardaran las imágenes de los temas
        $topicsImageFolder = $root . '/../web/uploads/images/topic/';
        if($filesystem->exists($topicsImageFolder)){
            $filesystem->remove($topicsImageFolder);
        }
        $filesystem>mkdir($topicsImageFolder, 0775);
        $filesystem->chown($topicsImageFolder, 'www-data', true);
        $filesystem->chgrp($topicsImageFolder, 'uploadsmaster', true);

        // Creo 200 temas
        for ($i=0; $i < 200; $i++){

            // Creo el tema
            $topic = new TopicDeal();

            // Escojo una categoría al azar
            $category = $categories[array_rand($categories, 1)];

            // Foro al que pertenece el tema
            $topic->setForum($forum);

            // Categoría al que pertenece el tema
            $topic->setCategory($category);

            // Obtengo datos para personalizar el tema
            $customData = $this->getCustomData($category);

            if($setTags == true){
                $tagManager = $this->container->get('fpn_tag.tag_manager');

                $tag1 = $manager->getRepository('TopicBundle:Tag')->findTagByName($customData['tag1']);
                $tag2 = $manager->getRepository('TopicBundle:Tag')->findTagByName($customData['tag2']);

                // Por si faltase alguna etiqueta
                if($tag1 == null){
                    $tag1 = $manager->getRepository('TopicBundle:Tag')->findTagBySlug($category->getSlug());
                }

                if($tag2 == null){
                    $tag2 = $manager->getRepository('TopicBundle:Tag')->findTagBySlug($category->getSlug());
                }

                // Elijo las etiquetas para el tema
                $tagManager->addTag($tag1, $topic);
                $tagManager->addTag($tag2, $topic);
            }

            // Tienda
            $shop = $customData['shop'];
            $topic->setShop($shop);

            // Título del tema
            $topic->setTitle($customData['title']);

            //Cambio el contenido del primer mensaje
            $firtsPost = $posts[(($i)*$countUsers)];
            $firtsPost->setMessage($customData['message']);

            // Id del primer mensaje
            $topic->setFirstPost($firtsPost);

            // Id del último mensaje
            $topic->setLastPost($posts[(($i+1)*$countUsers)-1]);

            // Url de la oferta
            $topic->setUrl($customData['url']);

            // Imagen de la oferta
            $fromHere = $root .'/../web/bundles/topic/images/topicimages/' . $customData['image'];
            $toHere =  $root . '/../web/uploads/images/topic/' . $i . "_" . $customData['image'];
            $filesystem->copy($fromHere, $toHere);

            $topic->setTopicImageName($i . "_" . $customData['image']);

            // Fechas
            $firstPostDate = $firtsPost->getPostingDate();
            $dateStart = clone $firstPostDate;
            $dateStart->setTime(00,00);
            $dateStart->format('Y-m-d H:i:s');
            $dateEnd = clone $firstPostDate;
            $dateEnd->setTime(00,00);
            $dateEnd->format('Y-m-d H:i:s');
            $dateEnd->modify('+' . rand(1, 15) . 'days');

            // Hora de inicio de la mensaje
            $topic->setDateStart($dateStart);

            // Hora en la que finaliza la mensaje
            $topic->setDateEnd($dateEnd);

            // Votos de los usuarios al tema
            for($j=1; $j < count($users); $j++)
            {
                $users[$j]->addTopicVote($topic);

                $var = rand(1,2);

                if($var==1){
                    $topic->setVotes($topic->getVotes()+1);
                }
                else if ($var==2){
                    $topic->setVotes($topic->getVotes()-1);
                }

            }

            // Si el número de votos es mayor o igual que la variable global lo considero popular
            // y guardo la fecha en la que se hizo popular
            $globalVariables = $manager->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
            $minVotesToBePop = $globalVariables->getMinVotesToBePop();

            if($topic->getVotes() >= $minVotesToBePop){
                $datePopular = clone $firstPostDate;
                $datePopular->modify('+' . rand(10, 30) . 'minutes');

                $topic->setDateMadePopular($datePopular);
            }

            // Hago la entidad persistente
            $manager->persist($firtsPost);
            $manager->persist($topic);

            // Guardo el tema/topic recien creado en un array
            $topics->add($topic);

        } // Fin for

        // Guardo los datos marcados como persistentes
        $manager->flush();

        if($setTags == true){
            foreach($topics as $topic){
                //Guardo las etiquetas asignadas al topic/tema (solo se puede hacer una vez hecho el persist y el flush)
                foreach($topic->getTags() as $tag){
                    $manager->persist($tagManager->unsafeCreateTagging($tag, $topic));
                }
            }
        }
        $manager->flush();
    }


    /*
     * Genero un titulo, el texto del primer mensaje, la tienda del tema, etc
     *
     * @param category $category del tema
     *
     **/
    private function getCustomData($category)
    {

        switch($category){
            case 'Audiovisual':

                $audiovisualShopNames = array(  'Amazon.ES',
                                                'Amazon.CO.UK',
                                                'Amazon.DE',
                                                'Amazon.IT',
                                                'Amazon.FR',
                                                'Ebay',
                                                'El Corte Inglés',
                                                'Hipercor',
                                                'Alcampo',
                                                'Carrefour',
                                                'Pc Componentes',
                                                'CoolMod',
                );
                $shopIndex = array_rand($audiovisualShopNames, 1);
                $shopName = $audiovisualShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Móviles':
                $mobileShopNames = array(  'Amazon.ES',
                    'Amazon.CO.UK',
                    'Amazon.DE',
                    'Amazon.IT',
                    'Amazon.FR',
                    'Ebay',
                    'El Corte Inglés',
                    'Hipercor',
                    'Alcampo',
                    'Carrefour',
                    'Pc Componentes',
                    'CoolMod',
                    'PhoneHouse',
                );
                $shopIndex = array_rand($mobileShopNames, 1);
                $shopName = $mobileShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Gaming':
                $gamingShopNames = array(  'Amazon.ES',
                    'Amazon.CO.UK',
                    'Amazon.DE',
                    'Amazon.IT',
                    'Amazon.FR',
                    'Ebay',
                    'El Corte Inglés',
                    'Hipercor',
                    'Alcampo',
                    'Carrefour',
                    'Pc Componentes',
                    'CoolMod',
                    'Steam',
                    'Origin',
                    'Uplay',
                    '4frags',
                );
                $shopIndex = array_rand($gamingShopNames, 1);
                $shopName = $gamingShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Ordenadores':
                $computerShopNames = array(  'Amazon.ES',
                    'Amazon.CO.UK',
                    'Amazon.DE',
                    'Amazon.IT',
                    'Amazon.FR',
                    'Ebay',
                    'El Corte Inglés',
                    'Hipercor',
                    'Alcampo',
                    'Carrefour',
                    'Pc Componentes',
                    'CoolMod',
                    '4frags',
                );
                $shopIndex = array_rand($computerShopNames, 1);
                $shopName = $computerShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Entretenimiento':
                $entertainmentShopNames = array(
                    'El Corte Inglés',
                    'ticketmaster',
                    'entradas.com',
                    'atrapalo.com',
                    'ticketea.com',
                    'Otra',
                );

                $shopIndex = array_rand($entertainmentShopNames, 1);
                $shopName = $entertainmentShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Hogar':
                $homeShopNames = array(  'Amazon.ES',
                    'Amazon.CO.UK',
                    'Amazon.DE',
                    'Amazon.IT',
                    'Amazon.FR',
                    'Ebay',
                    'El Corte Inglés',
                    'Hipercor',
                    'Alcampo',
                    'Carrefour',
                );

                $shopIndex = array_rand($homeShopNames, 1);
                $shopName = $homeShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Moda':
                $fashionShopNames = array(  'Amazon.ES',
                    'Amazon.CO.UK',
                    'Amazon.DE',
                    'Amazon.IT',
                    'Amazon.FR',
                    'Ebay',
                    'El Corte Inglés',
                    'Hipercor',
                    'Alcampo',
                    'Carrefour',
                );

                $shopIndex = array_rand($fashionShopNames, 1);
                $shopName = $fashionShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Niños':
                $kidsShopNames = array(  'Amazon.ES',
                    'Amazon.CO.UK',
                    'Amazon.DE',
                    'Amazon.IT',
                    'Amazon.FR',
                    'Ebay',
                    'El Corte Inglés',
                    'Hipercor',
                    'Alcampo',
                    'Carrefour',
                );
                $shopIndex = array_rand($kidsShopNames, 1);
                $shopName = $kidsShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Comida':
                $foodShopNames = array(
                    'El Corte Inglés',
                    'Hipercor',
                    'Alcampo',
                    'Carrefour',
                    'Alimerka',
                    'MasYMas',
                    'El Arbol'
                );
                $shopIndex = array_rand($foodShopNames, 1);
                $shopName = $foodShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Viajes':
                $travelingShopNames = array(
                    'El Corte Inglés',
                    'Alcampo',
                    'Carrefour',
                    'atrapalo.com',
                    'Trivago',
                    'muchoviaje.com',
                    'Barcelo Viajes'
                );
                $shopIndex = array_rand($travelingShopNames, 1);
                $shopName = $travelingShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            case 'Restaurantes':
                $restaurantsShopNames = array(
                    'El Corte Inglés',
                    'Alcampo',
                    'Carrefour',
                    'McDonalds',
                    'Burger King',
                    'La Chalana',
                    'Tierra Astur'
                );
                $shopIndex = array_rand($restaurantsShopNames, 1);
                $shopName = $restaurantsShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);

            default:
                $otherShopNames = array(
                    'Ebay',
                    'El Corte Inglés',
                    'Hipercor',
                    'Alcampo',
                    'Carrefour',
                );
                $shopIndex = array_rand($otherShopNames, 1);
                $shopName = $otherShopNames[$shopIndex];

                return $this->fillDataArray($category, $shopName);
        }
    }


/*
 * Para no ponerlo en el mismo sitio y tener mas ordenado esta función se encarga exclusivamente de generar, titulo
 * mensaje, url e imagen
 *
 * @param string $shopName nombre de la tienda
 * @param category $category categoria de la oferta
 *
 **/
    private function getLeftData($category, $shopName)
    {

        if($category->getName() == 'Audiovisual'){
            $price = rand(10, 1000);

            $audiovisualArray = array(
                array(
                    "title" => 'Samsung UE22H5000AW - Televisor (22"), Full HD, 1920 x 1080 @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Disfruta de imágenes más realistas y brillantes gracias a la resolución Full HD del televisor Samsung. Además, podrás ver todos tus contenidos cómodamente gracias a la conexión USB integrada.

                Disfruta como nunca de imágenes más realistas, nítidas y llenas de detalles. El televisor de Samsung, gracias a su resolución dos veces superior a la de una televisión HD estándar, te proporciona una increíble experiencia visual en la que sentirás cómo te sumerges en la pantalla. Con la riqueza y la textura de sus imágenes Full HD, tus programas y series favoritas no volverán a ser lo mismo. Descubre una nueva realidad con un televisor Full HD de Samsung.

                Con el Modo Fútbol podrás disfrutar al máximo de tus deportes favoritos, con colores más reales y un brillo único. Gracias al efecto de sonido multi-surround crearás una atmósfera en tu salón que poco tendrá que envidiar a la del estadio. Además, podrás hacer zoom en la areas que selecciones en la pantalla para mejorar la visibilidad. Prepárate para disfrutar de tu deporte favorito con tus amigos en el salón de tu casa.

                La tecnología Wide Color Enhancer Plus de Samsung utiliza un avanzado algoritmo para mejorar visiblemente la calidad de la imagen, consiguiendo tonos y detalles que otras televisiones son incapaces de reproducir. Por fin podrás ver los colores como realmente son con Wide Color Enhancer Plus.

                Gracias a Connect Share Movie, basta con conectar una memoria USB o un disco duro a tu televisión y tendrás al instante todas tus películas, fotos y canciones. Ahora puedes disfrutar de todo tu contenido en la televisión sin tener que moverte del sofá.
                ',
                    "url" => 'http://www.samsung.com/es/consumer/av/televisions/plano/UE22H5000AWXXC',
                    "image" => 'tvsamsung.jpeg',
                    "tag1" => 'Televisión',
                    "tag2" => 'Samsung'

                ),

                array(
                    "title" => 'Sony TV LED W600B Full HD - Televisor Full HD  @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Las imágenes Full HD con detalles nítidos se ven correspondidas con un sonido claro y realista que mejora la música y los diálogos. Haz que tu televisor sea el centro doméstico de entretenimiento online. Disfruta de una experiencia de navegación perfecta y encuentra el contenido perfecto para todas tus necesidades diarias mediante la nueva y elegante interfaz de usuario.

                Experimenta una calidad de imagen sorprendente, desde los Blu-ray Disc™ y las retransmisiones habituales de televisión hasta los vídeos de Internet y del smartphone. El exclusivo procesador de imagen de Sony analiza, limpia y perfecciona las imágenes para ofrecer unos resultados hermosos con una drástica reducción del ruido de la imagen, lo cual aporta la auténtica calidad del Full HD.

                Haz que la escucha sea tan real como la visualización. ClearAudio+ ajusta el sonido del televisor para ofrecer una experiencia envolvente. Escucha la música, los diálogos y los efectos con mayor claridad y separación, veas lo que veas.

                Escucha mucho más con una caja de altavoces compacta. El altavoz Bass Reflex produce sonidos enriquecidos y nítidos. El diseño aislado de la caja garantiza una reproducción del sonido muy eficaz para que se oiga con claridad.

                Controla tu mundo de contenidos con la interfaz intuitiva y rápida de BRAVIA®. Cambia entre canales de televisión, vídeos recomendados, fotos en tu red o incluso contenido de YouTube™ y otros canales online. Encuentra tu contenido favorito al momento con el rápido encendido del televisor y la rápida ejecución de las aplicaciones1.

                Enciende la TV al instante, normalmente en menos de dos segundos. Luego, tendrás acceso a Internet en otro segundo. Cada acción y cada aplicación es más rápida que nunca, para que puedas disfrutar y explorar tu contenido siempre que quieras.

                ',
                    "url" => 'http://www.sony.es/electronics/televisores/w600b-series',
                    "image" => 'tvsony.jpg',
                    "tag1" => 'Televisión',
                    "tag2" => 'Sony'

                ),

                array(
                    "title" =>'Samsung HW-H450 - Barra de sonido de 290W @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'La nueva generación de barras de sonido Samsung te ofrece multitud de características que te permitirán mejorar el sonido de tu televisión o disfrutar de tu música, compartirla y seleccionarla fácilmente desde tu smartphone y sin ayuda de cables.

                 Descubre la flexibilidad y la independencia que te aporta tu barra de sonido Samsung con conectividad Bluetooth. Con Bluetooth PowerOn siempre que los altavoces estén conectados a la red eléctrica puedes ponerlo en marcha con tu smartphone y disfrutar de tu música con la mejor calidad.

                Conecta tu televisor y tu barra de sonido inalámbricamente vía Bluetooth para disfrutar de un mejor sonido con TV Sound Connect. La ausencia de cables de la tele a la barra te permite disfrutar de más potencia de sonido ahorrando cables. Además, puedes controlar tu TV y los altavoces desde el mando a distancia de tu televisor gracias a Bluetooth y a la conectividad HDMI. ※ TV Sound Connect está disponible en todos los televisores Samsung 3D 2013, excepto en la serie LED F6100.


                ',
                    "url" => 'http://www.samsung.com/es/consumer/av/barras-de-sonido/barras-de-sonido/HW-H450/ZF',
                    "image" => 'barrasonidosamsung.jpg',
                    "tag1" => 'Barra de sonido',
                    "tag2" => 'Samsung'

                ),

                array(
                    "title" =>'Pack de 2 Gafas 3D SSG-5100GB (150 horas batería) @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Las nuevas gafas superpuestas 3D te ofrecen una experiencia audiovisual totalmente realista y con una calidad de imagen antes nunca vista

                Mira cómo las imágenes cobran vida con las gafas 3D de Samsung. Gracias a su diseño, son perfectas para que aquellos que ya usan gafas puedan llevarlas superpuestas a las suyas. Las gafas 3D son mucho más cómodas, proporcionándote una mejor experiencia tridimensional. Además, podrás disfrutar mucho más de tu televisor 3D gracias a su batería mejorada, capaz de durar hasta 150 horas con una sola carga. Esta es la experiencia 3D que siempre has querido tener.

                Gracias a un efecto 3D avanzado que mejora la velocidad LCD y el ratio de sombras, las gafas 3D SGG-5100GB de Samsung te ofrecen la mejor experiencia audiovisual. Ambas lentes, derecha e izquierda, trabajan en perfecta armonía, consiguiendo que percibas imágenes totalmente realistas. Además, con el visor Samsung podrás sumergirte en cualquier imagen sin tener que preocuparte por la molesta luz exterior. Disfruta de hasta 150 horas de entretenimiento en 3D con una sola carga. Vincula tus gafas con el televisor a través de la estable conexión de radiofrecuencia y ya estarás preparado para entrar en un nuevo mundo de entretenimiento. * Las gafas 3D se venden de forma separada en distintas regiones.

                ',
                    "url" => 'http://www.samsung.com/es/consumer/av/televisions/accesories/SSG-P51002/XC?subsubtype=other',
                    "image" => 'gafassamsung3d.jpg',
                    "tag1" => 'Gafas 3D',
                    "tag2" => 'Samsung'

                ),

                array(
                    "title" =>'Philips HTD3510 - Equipo de Home Cinema 5.1 de 300 W @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'La experiencia de entretenimiento en casa se optimiza con el sistema de cine en casa de Philips. Los altavoces al aire ofrecen un potente sonido Surround de 300 W y el escalado visual de DVD a 1080p mediante HDMI permite obtener imágenes cercanas a la alta definición.

                Puesto que Dolby Digital, el principal estándar de audio digital multicanal del mundo, se basa en la forma en que el oído humano procesa el sonido de manera natural, disfrutarás de un sonido Surround de excepcional calidad con un entorno espacial muy realista.

                La entrada de audio te permite reproducir fácilmente música directamente del iPod/iPhone/iPad, reproductor de MP3 o portátil mediante una sencilla conexión al sistema de cine en casa. Solo tienes que conectar el dispositivo de audio a la toma de entrada de audio para disfrutar de la música con la calidad de sonido superior del sistema de cine en casa de Philips.

                EasyLink te permite controlar varios dispositivos como reproductores de DVD, Blu-ray, televisores, etc. con un solo mando a distancia. Utiliza el protocolo HDMI CEC estándar del sector para compartir funcionalidad entre dispositivos a través del cable HDMI. Con solo pulsar un botón podrás manejar simultáneamente todos los equipos compatibles con HDMI CEC que estén conectados. Ahora podrás utilizar con mayor comodidad funciones como el modo de espera y la reproducción.

                HDMI es una conexión digital directa que puede transmitir vídeo de alta definición digital, así como audio digital multicanal. Al eliminar la conversión a señal analógica, ofrece una calidad de imagen y sonido perfectos, completamente libres de ruido. Ahora podrás disfrutar de películas con definición estándar en una resolución de alta definición real, que te ofrece imágenes más reales con más detalles
                ',
                    "url" => 'http://www.philips.es/c-p/HTD3510_12/sistema-de-cine-en-casa-5.1',
                    "image" => 'homecinemaphillips.jpg',
                    "tag1" => 'Home Cinema',
                    "tag2" => 'Philips'

                ),

                array(
                    "title" =>'LG 42LB5610 - Televisor LED de 42"  @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Conseguirás una alta definición de colores, brillo y nitidez. Porque la tecnología IPS de LG dispone de más regularidad y de menos cambios en la temperatura de color, alcanzando una temperatura de color en un nivel próximo a los 6500K. Por eso ofrece una impresión idéntica a la de la imagen original. Y olvídate de la vista cansada, esta tecnología proporciona una imagen tan nítida y real que evita la fatiga ocular, lo que la convierte en la idónea para fans del deporte, cine y videojuegos.

                Con la tecnología IPS , disfrutarás de una calidad de imagen excepcional, ya estés de pie, sentado o tumbado. Supone una gran ángulo de visión, 178º horizontal y 178º vertical: desde el frente, los lados, abajo… cualquier ángulo que imagines se te mostrará con gran calidad.

                Los televisores con panel IPS son superiores a los de panel convencional en la claridad de las imágenes que muestran durante los movimientos rápidos. Gracias a su avanzada estructura de cristal líquido, la tecnología IPS puede generar imágenes precisas sin distorsiones en los movimientos rápidos.

                Conecta un Smartphone compatible mediante el cable HDMI-MHL. Así podrás, ver todo lo que hagas en tu Smartphone (correo, juegos, aplicaciones, navegar por internet, etc.) directamente en la pantalla de tu televisor.

                Además, podrás cargar el teléfono mientras esté conectado. El cable MHL no está incluido en el televisor.


                ',
                    "url" => 'http://www.lg.com/es/television/lg-42LB5610',
                    "image" => 'tvlg.jpg',
                    "tag1" => 'Televisión',
                    "tag2" => 'LG'

                ),

                array(
                    "title" =>'Philips SRP2008B - Mando a distancia universal @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Excelente relación calidad-precio
                Sirve para sustituir hasta 8 mandos a distancia

                Un cómodo mando a distancia universal de Philips que puede controlar hasta 8 dispositivos.

                Fácil de usar
                - Botones bien agrupados para un acceso fácil a funciones específicas

                Compatibilidad total con tus dispositivos
                - Funciona con más de 800 dispositivos
                ',
                    "url" => 'http://www.philips.es/c-p/SRP2008B_86/perfect-replacement-mando-a-distancia-universal',
                    "image" => 'mandouniversalphillips.jpg',
                    "tag1" => 'Mando a distancia',
                    "tag2" => 'Philips'

                ),

                array(
                    "title" =>'Nikon D3200 - Cámara réflex digital de 24 Mp @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Perpetúe sus recuerdos con la potente, pero increíblemente fácil de usar, D3200.

                Provista de un sensor CMOS de 24,2 megapíxeles y el potente sistema de procesamiento de imágenes EXPEED 3 de Nikon, esta SLR permite obtener extraordinarias fotos y vídeos D-movie en máxima definición (Full HD).

                Empezar a utilizarla es muy fácil gracias al Modo guía de Nikon, que le muestra cómo lograr resultados asombrosos en unos sencillos pasos. Con la ayuda de las imágenes de muestra y las claras instrucciones, podrá capturar el tipo de fotos y vídeos de alta calidad que siempre deseó.

                La D3200 es una forma fantástica de capturar la vida con todo lujo de detalles que además le permite enviar imágenes directamente desde la cámara a un dispositivo inteligente* para que pueda compartir sus fotos y vídeos al instante.
                ',
                    "url" => 'http://www.nikon.es/es_ES/product/digital-cameras/slr/consumer/d3200',
                    "image" => 'nikond3200.png',
                    "tag1" => 'Cámara',
                    "tag2" => 'Nikon'

                ),

                array(
                    "title" =>'LG BP 135 - Reproductor de Blu-ray @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Los reproductores Blu-ray de LG están pensados para reproducir la más alta calidad de imagen, además de los nuevos formatos de video en 3D. Nuestro trabajo esta encaminado en ofrecerte productos de muy alta calidad que te provean de sensaciones únicas. Para ello creamos productos complementarios como las TV LED extra finas y con marco reducido, los reproductores Blu Ray 3D, los Home cinema 3D sound y la última tecnología en Smart TV, esta última para proporcionarte contenidos para toda la familia, además de una mejorada usabilidad, nunca conocida hasta ahora en un televisor.

                En LG pensamos en ti, y escuchamos tus necesidades, por eso desarrollamos continuamente nuestros productos, adaptándolos periódicamente a tus nuevas necesidades.

                ',
                    "url" => 'http://www.lg.com/es/reproductores-video/lg-BP135',
                    "image" => 'bluraylg.jpg',
                    "tag1" => 'Reproductor de Blu-ray',
                    "tag2" => 'LG'

                ),

                array(
                    "title" =>'Sony Cyber-SHOT DSC-HX300 @  - Cámara compacta de 20.4 MP' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Explora la libertad creativa de los controles manuales combinada con un diseño similar al de las cámaras réflex que cabe en la palma de la mano, y acerca cualquier escena con el zoom óptico de 50x.

                Olvida eso de cambiar de lente; todas las posibilidades fotográficas están cubiertas con una lente de zoom óptico de 50x. Captura imágenes gran angular, o haz zoom para obtener primeros planos mientras la cámara se mantiene firme.

                Graba vídeos con detalles, fluidos y realistas en Full HD y a una velocidad de 30 fotogramas por segundo. Graba en formato MP4 para una edición y carga sencillas.

                El procesador BIONZ™ recoge la información capturada por el sensor CMOS y la procesa para mantener la realidad, la textura y la profundidad, y te permite hacer 10 disparos por segundo.

                Optical SteadyShot contrarresta de forma dinámica el movimiento para mantener la imagen enfocada, con lo que dispones de libertad para tomar imágenes con claridad cristalina sobre la marcha y sin trípode.

                La función de barrido panorámico te permite capturar imágenes gran angular. Basta con realizar un barrido por la escena con la cámara y esta hace una serie de fotos y las une en una foto panorámica.

                ',
                    "url" => 'http://www.sony.es/electronics/camaras-compactas-cyber-shot/dsc-hx300',
                    "image" => 'camarasony.jpg',
                    "tag1" => 'Cámara',
                    "tag2" => 'Sony'

                ),

                array(
                    "title" =>'Samsung F90 - Videocámara (5.0 Mp) @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Sabemos que un potente zoom óptico es fundamental para cualquier videocámara; por esta razón, la F90 de Samsung cuenta con intelli-zoom de 70x (zoom óptico de 52x). Acércate más a la acción cuando lo necesites, asegurándote de que cada imagen sea tal cual la imaginas. Además, para crear aún más efecto zoom, hemos añadido píxeles adicionales al zoom óptico existente desde el sensor de imagen.

                Disfruta de la misma nitidez, riqueza de colores y naturalidad de la que disfrutas cuando grabas películas en alta definición, en tus propios vídeos. La grabación en HD 1.280 x 720 y 30p te ofrece una resolución 2,5 veces mejor que las definiciones de las videocámaras convencionales. Además, como es capaz de grabar 30 fotogramas por segundo, disfrutarás de presentaciones cinematográficas, muy útiles cuando estás reproduciendo tus vídeos en tu televisión o monitor HD.

                Zoom óptico 52X (70x intelli zoom) Sabemos que un potente zoom óptico es fundamental para cualquier videocámara; por esta razón, la F90 de Samsung cuenta con intelli-zoom de 70x (zoom óptico de 52x). Acércate más a la acción cuando lo necesites, asegurándote de que cada imagen sea tal cual la imaginas. Además, para crear aún más efecto zoom, hemos añadido píxeles adicionales al zoom óptico existente desde el sensor de imagen.

                Reproduce tus momentos favoritos de una noche con amigos o de una caminata por el bosque sin tener que perder el tiempo rebobinando. La función My Clip te permite marcar cualquier escena en tu vídeo para que puedas revivir al instante los mejores momentos, las mejores vistas o lo que tú quieras, sin tener que realizar ninguna edición adicional.

                 Ya se trate del césped, las nubes, el océano o un vestido de fiesta, siempre capturarás con precisión todos los colores. Tu videocámara F90 cuenta con un sensor CMOS, capaz de filtrar la luz en los colores rojo, verde y azul, para ofrecer una reproducción más profunda, rica y natural que cualquier imagen obtenida por un sensor CCD. Disfruta con imágenes más nítidas.

                ',
                    "url" => 'http://www.samsung.com/es/support/model/HMX-F90BP/EDC',
                    "image" => 'camaravideosamsung.jpg',
                    "tag1" => 'Videocámara',
                    "tag2" => 'Samsung'

                ),
            );

            $offerIndex = array_rand($audiovisualArray, 1);
            return $audiovisualArray[$offerIndex];
        }


        elseif($category->getName() == 'Móviles'){
            $price = rand(10, 700);

            $mobilesArray = array(
                array(
                    "title" => 'Samsung GT-E1270 @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>' Disfruta de las cosas sencillas. Su diseño compacto te facilita llevarlo contigo. El nuevo teléfono cuenta con nuevas características que mejorarán tu vida. Disfruta de una mejor experiencia visual con su pantalla LCD y escucha tu música preferida con su radio FM.Todo lo que necesitas al alcance de tus dedos.

                                    El nuevo t GT-E1270 ha sido diseñado para encajar a la perfección en tu mano, teniendo siempre en mente los conceptos de portabilidad y estilo. Su sencillo diseño te permitirá guardarlo en cualquier bolsillo sin que tengas que preocuparte por el espacio. Además, su dieño redondeadole proporciona un agarre mucho más cómodo Elige el color que mejor encaja contigo y disfruta de la sencillez en tu vida cotidiana.

                                    Saca el máximo provecho a las prácticas funcionalidades del nuevo Samsung GT-E1270, un teléfono diseñado con pantalla LCD que incluye Radio FM para que escuches cuando quieras tus emisoras favoritas, y al que podrás conectar, gracias a su conector microUSB, otros dispositivos compatibles. Samsung GT-E1270 es la solución ideal para aquellos que desean un teléfono móvil muy sencillo y funcional.

                    ',
                    "url" => 'http://www.samsung.com/es/consumer/mobile-phone/phones/classic/GT-E1270RWAPHE',
                    "image" => 'samsungtapa.jpg',
                    "tag1" => 'Teléfono móvil',
                    "tag2" => 'Samsung'

                ),

                array(
                    "title" => 'Samsung HM1200 - Manos libres Bluetooth para móvil (Control de volumen) @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Compacto y moderno diseño.
                                La última versión de Bluetooth Headset.
                                Tecnología multipunto.
                                Conexión Dual.
                                Tiempo en espera: 8 - 300 horas.
                                Bluetooth Versión: 3.0.
                                Características principales: Vinculación activa, control del nivel de la batería, llamada de marcación de recepción / Última llamada, volumen hacia arriba y hacia abajo.
                                Componentes: auricular Bluetooth, adaptador de viaje, manual del usuario.
                                Compatibilidad: Compatible con la mayoría de Bluetooth - teléfonos habilitados.

',
                    "url" => 'http://www.samsung.com/hk_en/consumer/mobile/mobile-phones/accessories/BHM1200UBEGWDT?subsubtype=bluetooth-headset',
                    "image" => 'manoslibressamsung.jpg',
                    "tag1" => 'Manos libres',
                    "tag2" => 'Samsung'

                ),

                array(
                    "title" => 'Motorola Moto 360 - Smartwatch  (pantalla 1.56", 4 GB, 512 MB RAM) @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Moto 360 mostrará notificaciones periódicas según donde te encuentres y lo que estés haciendo, por lo que siempre estará conectado. Información meteorológica, alertas de vuelos, tráfico, etc., todo ello ofrecido por Android Wear. Y solo con un simple vistazo a tu muñeca

                                El control de voz hace que sea fácil hacer las cosas, aun en el caso de que tengas las manos ocupadas. Solo tienes que decir “Ok Google” y podrás enviar textos, programar un recordatorio, ver la información meteorológica e incluso pedir indicaciones.

                                Solo la aplicación de actividad cardíaca de Moto 360 realiza un seguimiento de tu progreso, utilizando referencias diarias y semanales, para que siempre sepas en qué punto te encuentras.

                                Disfruta de una batería que dura todo el día. Tan solo tendrás que colocar el Moto 360 en su base de carga por la noche. Mientras el reloj se carga muestra la hora, lo que convierte la base en un elemento perfecto para escritorios y mesillas de noche.
                                ',
                    "url" => 'http://www.motorola.es/Motorola-Moto-360/Moto-360-es.html',
                    "image" => 'moto360.jpg',
                    "tag1" => 'Smartwatch',
                    "tag2" => 'Motorola'

                ),

                array(
                    "title" => 'LG G Watch R - Smartwatch  (1.3", 4 GB, 1.2 GHz, 512 MB RAM) @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Es perfectamente curva y ultra delgada, y su diseño redondo y clásico encajará con cualquier estilo. Su diámetro de 1,3 pulgadas permite tener una pantalla de reloj completa y muy fácil de usar. Te sorprenderá su color y contraste ilimitado, visible en un grosor de menos de 0,6 milímetros. Siempre estará encendida, siempre lista para que puedas consultarla y sorprender a los que te rodean.

                                Diseño metálico que aporta elegacia y resistencia, será capaz de seguir tu ritmo diario.

                                Háblale y él buscará. Con el comando de voz “OK Google” se pondrá en marcha para mostrarte los resultados directamente en la pantalla. Enseguida podrás conocer la respuesta a preguntas tan comunes como cómo llegar a determinado sitio, cómo quedó el resultado del partido, o cuáles son las últimas actualizaciones de tus apps favoritas, como Whatsapp, Gmail o Facebook. También te sugerirá información que pueda interesarte en ese momento.
                    ',
                    "url" => 'http://www.lg.com/es/wearables/lg-LGW110-g-watch-r',
                    "image" => 'lggwatch.jpg',
                    "tag1" => 'Smartwatch',
                    "tag2" => 'LG'

                ),

                array(
                    "title" => 'Honor 3C - Dual SIM , 5.0", cámara 8 Mp, 8 GB, Quad-Core, 2 GB RAM @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>' El smartphone Honor 3C le proporciona todo lo necesario para la era digital.

                                    Con grandes características como una pantalla táctil de 5.0" LTPS de alta definición que le permite ver más detalles, una cámara principal de 8 megapíxeles y una cámara frontal de 5 megapíxeles para fotos más claras, doble Micro SIM, y la tecnología de ahorro de energía para ampliar duración de la batería, lo que realmente le transforma en una alternativa capaz a un precio excepcional.

                                     El Honor 3C tiene una cámara principal de 8 Mp y una secundaria (frontal) de 5Mp.

                                    No sólo que las dos cámaras ofrecen imágenes de gran calidad, pero la cámara secundaria tiene la característica adicional de tomar mejores selfies en "modo belleza

                                     La pantalla de 5.0 " de alta definición de Honor 3C tiene colores más vívidos y envolventes gracias a su resolución de 294 píxeles por pulgada.

                                    Con 16 millones de colores y amplio ángulo de visión, técnica de calibración OTP, el Honor 3C ofrece unos colores mejores, juegos 3D mas rapidos y visión más suave de la películas.

                                    ',
                    "url" => 'http://www.huaweidevice.co.in/?q=huawei-honor-3C',
                    "image" => 'honor3c.jpg',
                    "tag1" => 'Smartphone',
                    "tag2" => 'Honor'

                ),

                array(
                    "title" => 'Samsung Galaxy S3 Neo (4.8", cámara 8 Mp, 16 GB, Quad, 1.5 GB RAM) @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Con la innovadora función Smart Stay, tu GALAXY S III Neo reconoce automáticamente cuando estás mirando la pantalla, ya sea para leer un libro electrónico o navegar por internet. Su cámara frontal percibe tus ojos y mantiene la pantalla iluminada mientras note que siguen fijos en ella, evitando interrupciones en tus lecturas. Una idea brillante.

                    Compartir contenido con tus amigos debe ser fácil, rápido y, por qué no, divertido. La función S Beam te permite hacerlo al instante, para que puedas transferir documentos, contactos, imágenes, música, vídeos... lo que quieras. Tan sólo acerca la parte trasera de dos GALAXY S III Neo para conectarte, compartir y disfrutar

                    Ahora es mucho más fácil y divertido hacer varias cosas al mismo tiempo en la pantalla Super AMOLED HD de 4,8" de GALAXY S III Neo. Gracias a la función Pop up Play y su potente procesador Quad Core, podrás ver un vídeo HD en una ventana al mismo tiempo que escribes un mensaje de texto o email. Arrástra la ventana del video a donde desees en la pantalla, chatea con tus contactos o navega por internet. No te pierdas nada.


                    ',
                    "url" => 'http://www.samsung.com/es/consumer/mobile-phone/smartphones/galaxy/GT-I9301MBIPHE',
                    "image" => 'samsungs3neo.jpg',
                    "tag1" => 'Smartphone',
                    "tag2" => 'Samsung'

                ),

                array(
                    "title" => 'BQ Aquaris E4.5 (4.5", cámara 8 Mp, 8 GB, Quad-Core, 1 GB RAM @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Con Aquaris E hemos conseguido lo que llevábamos buscando durante años, el compromiso perfecto entre tamaño y especificaciones: un producto de máxima calidad, diseñado en España e íntegramente original. Podría ser más fino, pero dejaríamos de ofrecerte una batería que siga tu ritmo. Podría ser más corto, pero no queríamos suprimir la micro SD externa, ni dejar de ofrecerte dual micro-SIM para que tengas 2 teléfonos en 1. Además, hemos integrado todos los elementos externos, como la cámara y la entrada de audio para garantizar su protección.

                                Ahora podrás hacer fotos de hasta 8Mpx con la cámara trasera y de hasta 5Mpx con la frontal. Y como no todo son megapíxeles, hemos equipado las cámara con lentes Largan de alta calidad usadas en los smartphones de más alta gama.
                    ',
                    "url" => 'http://www.bq.com/es/productos/aquaris-e4-5.html',
                    "image" => 'bqe45.jpg',
                    "tag1" => 'Smartphone',
                    "tag2" => 'BQ'

                ),

                array(
                    "title" => 'Motorola Nuevo Moto G (5", 8 Mp, 8 GB, Quad-Core, 1 GB RAM) @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Smartphone increíble. Precio sorprendente.

                    Un buen Smartphone no debería de ser un privilegio, sino una elección. Tiene una pantalla brillante, un sonido estéreo, una batería para todo el día y el SO Android más reciente; todo esto a un precio excepcional. El nuevo Moto G está mejor que nunca, te da todo lo que necesitas para todo lo que haces.
                    Batería para todo tu día.

                    Haz todas las actividades cotidianas con la batería que dura todo el día. No te preocupes nunca por detenerte a cargarla.
                    ',
                    "url" => 'http://www.motorola.es/Moto-G-de-Motorola/Moto-g-gen2-es.html',
                    "image" => 'motog2014.jpg',
                    "tag1" => 'Smartphone',
                    "tag2" => 'Motorola'

                ),

                array(
                    "title" => 'Motorola Moto E (4.3", 5 Mpx, 4 GB, Dual-Core, 1 GB RAM) @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Pantalla táctil de 4.3 pulgadas 540 x 960
                                Procesador Snapdragon 200 Dual-Core a 1.2 GHz, con 1 GB de RAM
                                Cámara trasera de 5 megapixels
                                Sistema operativo Android 4.4 KitKat; Conectividad 3G, WiFi, Bluetooth, GSM

                                Cuenta con la pantalla más nítida en su clase, con Corning Gorilla Glass y una protección contra salpicaduras resistente al agua para ofrecer una mayor durabilidad, y una batería que dura todo el día. Moto E tiene una calidad duradera y un precio para todos.

                                Tu Moto E trae aplicaciones integradas que te ayudan en la transición de tu antiguo teléfono al nuevo y que hacen más fácil compartir todo lo que desees

                    ',
                    "url" => 'http://www.motorola.es/consumers/mobile-phones/Moto-E/moto-e-es.html',
                    "image" => 'motoe.jpg',
                    "tag1" => 'Smartphone',
                    "tag2" => 'Motorola'

                ),

                array(
                    "title" => 'LG G2 (pantalla 5.2", cámara 13 Mp, 16 GB, Quad-Core 2.3 GHz) @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Más pantalla y batería en el mismo tamaño ¿Es posible?

                            Smartphone 4G android con botón trasero, pantalla 5.2" Full HD IPS, procesador ultra rápido y batería de larga duración de 3.000 mAh
                            LG G2 D802

                            El único Smartphone del mundo con botón trasero para un uso más intuitivo. Más por menos. Pantalla más grande en el mismo tamaño (5.2” Full HD IPS). Mayor autonomía de batería para disfrutarlo más de un día entero (3.000 MAH SIO+). Máxima rapidez y potencia con su procesador de cuatro nucleos SNAPDRAGON 800 2.26 Ghz Cámara de 13Mpx, 16 ó 32 GB de memoria y peso de 143 gramos
                            ',
                    "url" => 'http://www.lg.com/es/telefonos-moviles/lg-G2-D802',
                    "image" => 'lgg2.jpg',
                    "tag1" => 'Smartphone',
                    "tag2" => 'LG'

                )
            );

            $offerIndex = array_rand($mobilesArray, 1);
            return $mobilesArray[$offerIndex];
        }


        elseif($category->getName() == 'Gaming'){
            $price = rand(10, 400);

            $gamingArray = array(
                array(
                    "title" => 'Logitech G500s - Ratón láser @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Logitech G500s FPS Laser Gaming Mouse

                                Logitech G500s es un ratón muy personalizable y ofrece precisión láser para juegos FPS. Peso y equilibrio personalizables permiten ajustar el ratón a las preferencias del usuario. Programe cualquiera de los 10 controles mediante Logitech Gaming Software opcional para el acceso inmediato a comandos simples o combinaciones multicomando. Un botón rueda con dos modos y desplazamiento superrápido ofrece tanto precisión clic a clic como velocidad con giro libre. Construcción duradera, con microinterruptores para 20 millones de clics, un cable trenzado y pies de baja fricción capaces de recorrer 250 kilómetros. Y materiales de superficie avanzados para hacer de G500s una solución totalmente preparada para jugar.
                                Contenido de la caja

                                Ratón para juego, Bandeja de lastre ajustable, Pesas y estuche, Documentación del usuario, 3 años de garantía del fabricante y asistencia técnica telefónica
                    ',
                    "url" => 'http://gaming.logitech.com/es-roam/product/g500s-laser-gaming-mouse',
                    "image" => 'g500s.jpg',
                    "tag1" => 'Ratón',
                    "tag2" => 'Logitech'

                ),

                array(
                    "title" => 'Genius TwinWheel FX - Volante (PC, PS3) @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'TwinWheel FXE es el nuevo volante de carreras con tacto de piel de Genius que le permite jugar con cualquier juego de carreras para ordenadores o PlayStation 3. Su diseño plegable le ahorra espacio y le ofrece siete ángulos de ajuste diferentes. Los baches, las colisiones y los choques y la tecnología TouchSense patentada por Immersion le permite sentir las sensaciones de la conducción durante el juego. Además, la TwinWheel FXE incluye 12 botones programables, cuatro ventosas y una abrazadera en forma de C.

                                Para más información, consulte a su representante local',
                    "url" => 'http://www.geniusnet.com/Genius/wSite/ct?xItem=19501&ctNode=1310',
                    "image" => 'volantegenius.jpg',
                    "tag1" => 'Volante',
                    "tag2" => 'Genius'

                ),

                array(
                    "title" => 'Logitech G710+ - Teclado (70 MB, 2.2 Kg, QWERTY Español), negro @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Teclas mecánicas táctiles de respuesta rápida

                                Largo alcance

                                Las teclas mecánicas de G710+ son perfectas para juegos, con respuesta táctil silenciosa superior a la de las teclas con membrana de goma. Están optimizadas con una fuerza de actuación de 45 g y una distancia de 4 mm, para agilizar la entrada de comandos. Y las pruebas de duración han demostrado un ciclo de vida de 50 millones de pulsaciones.

                                Teclas silenciosas

                                Concentración en el juego

                                Un mecanismo de teclas sin clic y un anillo amortiguador bajo cada tecla contribuyen a reducir considerablemente el ruido producido al pulsar, sin que disminuya la acción de respuesta.

                                Retroiluminación de dos zonas ajustable

                                El saber, la iluminación

                                Facilita el uso de las teclas, incluso con luz escasa. El teclado tiene retroiluminación LED blanca ajustable en uno de cuatro niveles de brillo, además de una opción de desactivación. Para mejorar la visibilidad de las teclas WASD y de flecha, el brillo de éstas se puede ajustar de forma independiente del resto del teclado.
                                ',
                    "url" => 'http://gaming.logitech.com/es-es/product/g710plus-mechanical-gaming-keyboard',
                    "image" => 'g710.jpg',
                    "tag1" => 'Teclado',
                    "tag2" => 'Logitech'

                ),

                array(
                    "title" => 'Ozone Ground Level S - Alfombrilla de ratón @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Ozone Ground Level S - Alfombrilla de ratón

                            - Alfombrilla diseñada especificamente para los deportes electrónicos. Con una superficie perfecta para gran velocidad de deslizamiento, adecuada para reducir el ruido y proporcionar un alto confort.

                            - Base de espuma de goma, para asegurar el mejor agarre sin necesidad de mover o corregir tu posición cuando estás en el centro de la acción. Diseño con el logotipo de Ozone y la firma en la esquina inferior.

                            - Gran ligereza para mejorar la portabilidad. Su altura de 2mm corrige las pequeñas imperfecciones que la mesa pueda tener. Tamaño S de 250mm x 210mm.

                            - Disponible en cuatro tamaños diferentes.
',
                    "url" => 'http://www.ozonegaming.com/es/producto/ground-level/',
                    "image" => 'alfombrillaozone.jpg',
                    "tag1" => 'Alfombrilla',
                    "tag2" => 'Ozone'

                ),

                array(
                    "title" => 'Half Life 2: The Orange Box (PC DVD) @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'The Orange Box, creado por Valve, es un paquete de videojuegos que contiene Half-Life 2, Half-Life 2: Episode One, Half-Life 2: Episode Two, Portal y Team Fortress 2. Ha sido lanzado para PC, Xbox 360 y PlayStation 3.

                                En la versión en Steam de este paquete, incluye tres juegos extra: Half-Life 2: Deathmatch, Half-Life 2: Lost Coast y Peggle Extreme.',
                    "url" => 'http://store.steampowered.com/sub/469/?l=spanish',
                    "image" => 'orangebox.jpg',
                    "tag1" => 'Videojuego',
                    "tag2" => 'Valve'

                ),

                array(
                    "title" => 'PlayStation 4 - Consola 500 GB @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'PlayStation 4, la nueva generación de sistemas de entretenimiento, redefine el mundo de los videojuegos gracias a su espectacular experiencia de juego, rapidez, gráficos impresionantes, personalización inteligente, capacidades sociales sumamente integradas e innovadoras funciones para segundas pantallas.
                                Orientada a los jugadores, inspirada por los desarrolladores

                                La futura experiencia de juego que ofrecerá PlayStation 4 está inspirada por los deseos de los clientes y las ideas de los desarrolladores y se caracteriza por un compromiso inquebrantable con la creación de una experiencia de juego excelente. La arquitectura del sistema permite que puedas conectarte a un mundo de experiencias más amplio y acceder fácilmente a la gran oferta de PlayStation: a otras consolas, a plataformas móviles y a la red de PlayStation. PS4 sigue cinco principios de diseño de experiencia de usuario que van a caracterizar a todos sus juegos: simples, directos, sociales, integrados y personalizados.',
                                                    "url" => 'https://www.playstation.com/es-es/explore/ps4/',
                    "image" => 'ps4.jpg',
                    "tag1" => 'Consola',
                    "tag2" => 'PlayStation 4'

                ),

                array(
                    "title" => 'Xbox One - Consola + Assassins Creed: Unity + Extras @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Contiene:
                                • Consola Xbox One 500 GB
                                • Mando Inalámbrico Xbox One
                                • Código para descargar el juego Assassin‘s Creed: Unity
                                • Código para descargar el juego Assassin ́s Creed IV: Black Flag
                                • Código para descargar el juego Rayman Legends
                                • Auricular Xbox One
                                • Cable HDMI
                                • Código de prueba de 14 días de Xbox Live Gold ',
                    "url" => 'http://www.xbox.com/es-ES/xbox-one',
                    "image" => 'xbone.jpg',
                    "tag1" => 'Consola',
                    "tag2" => 'Xbox One'

                ),

                array(
                    "title" => 'Grand Theft Auto V PS4 @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Otras características:
                                Editor: Rockstar Games
                                Explorador: Rockstar North
                                Fecha de lanzamiento: 18/11/2014
                                Género: Acción / Aventura
                                Juego de edición: Básico
                                Modo multijugador: Si
                                Plataforma: PlayStation 4
                                Rango ESRB: M (Maduro)
                                Tipo de modo multijugador: En línea
                                Versión de idioma: DEU',
                    "url" => 'http://www.rockstargames.com/V/es',
                    "image" => 'gtav.jpg',
                    "tag1" => 'Videojuego',
                    "tag2" => 'PlayStation 4'

                ),

                array(
                    "title" => 'Assassins Creed: Unity - Edición Especial @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>' La ya legendaria saga Assassins Creed regresa con un episodio exclusivo de la nueva generación, el cual también aparece en Windows PC para disfrute de todos los fans del enfrentamiento entre assassins y templarios. Ubisoft traslada a los usuarios a la Revolución Francesa, histórica época que tiene mucho que ofrecer a los aventureros dispuestos a profundizar en ella.

                                El juego Assassins Creed: Unity incorpora una gran dosis de contenido adicional. En Revolución Química los jugadores tienen 30 minutos de juego individual en el cual los jugadores deberán ayudar a Antoine Laurent de Lavoisier, científico reconocido como el padre de la química moderna. Tras el descubrimiento de la bomba venenosa, el científico es secuestrado y con él también desaparece su fórmula, por lo que será fundamental acudir en su rescate. ',
                    "url" => 'http://assassinscreed.ubi.com/es-es/games/assassins-creed-unity.aspx',
                    "image" => 'assassin.jpg',
                    "tag1" => 'Videojuego',
                    "tag2" => 'Xbox One'

                ),

                array(
                    "title" => 'Super Mario: Galaxy 2 @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Únete al único e irrepetible Mario en un viaje por extraños planetas en los que te esperan nuevas trampas y peligros, en Super Mario Galaxy 2.

                                Ha llegado de nuevo el momento de celebrar el Festival de las Estrellas, que se celebra una vez cada cien años y en el que una lluvia de estrellas baña el Reino Champiñón. Tras recibir una invitación de la Princesa Peach, Mario se encamina hacia el castillo acompañado de una pequeña estrella extraviada que encontró vagando por el cosmos. Pero cuando Mario llega al castillo, ya es demasiado tarde. Bowser ha aprovechado el poder de las estrellas y se ha llevado a la princesa Peach consigo. Una misión cósmica épica para rescatar a Peach está a punto de comenzar...

                                Explora las profundidades del espacio y sigue a Mario y a Yoshi en una aventura increíble. Con decenas de galaxias por explorar -a cada cual más creativa-, los nuevos potenciadores y la libertad para jugar solo o con un amigo, ¡la aventura es de lo más espectacular y divertida! Descubre el placer de pasear por las nubes o rodar con los nuevos trajes de Mario y utiliza las características únicas de Yoshi para elevarte, salir corriendo o balancearte por las estrellas.
                                ',
                    "url" => 'https://www.nintendo.es/Juegos/Wii/Super-Mario-Galaxy-2-529982.html',
                    "image" => 'mariogalaxy.jpg',
                    "tag1" => 'Videojuego',
                    "tag2" => 'Nintendo'

                )
            );

            $offerIndex = array_rand($gamingArray, 1);
            return $gamingArray[$offerIndex];
        }


        elseif($category->getName() == 'Ordenadores'){
            $price = rand(100, 2000);

            $computersArray = array(
                array(
                    "title" => 'Seagate STBV4000200 - Disco duro externo de 4 TB (USB 3.0) @ ' . $shopName . ' - ' . $price . ' €',
                    "message" => 'Almacenamiento adicional sencillo

                                La unidad de disco duro externa Expansion™ de Seagate® ofrece una solución sencilla para añadir almacenamiento adicional de forma instantánea a su ordenador.

                                La solución de almacenamiento fácil de usar.

                                La unidad de disco duro externa Expansion se instala fácilmente conectando dos cables. Podrá empezar a guardar sus archivos digitales en la unidad desde el primer instante.

                                Capacidad de almacenamiento adicional al instante.

                                Los archivos de fotos digitales, vídeo y música pueden poner a prueba el almacenamiento de su ordenador y disminuir su rendimiento a medida que la unidad de disco duro interna llega a su límite de capacidad.',
                    "url" => 'http://www.seagate.com/es/es/external-hard-drives/desktop-hard-drives/expansion-hard-drive/',
                    "image" => 'discoduro.jpeg',
                    "tag1" => 'Disco duro',
                    "tag2" => 'Seagate'

                ),

                array(
                    "title" => 'Corsair Graphite CC600TM  @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Alto rendimiento, espectacular diseño, montaje muy sencillo

                                La serie Graphite 600T permite ensamblar fácilmente un sistema para juegos de primera clase. Con el excelente sistema de refrigeración que proporcionan los ventiladores dobles de 200 mm, así como con las ocho ranuras de expansión PCI-E para múltiples tarjetas gráficas y espacio abundante para componentes de mayor tamaño y consumo, el modelo Graphite Series 600T supera sin problemas prácticamente cualquier exigencia.
                                 Aunque por su tamaño el modelo Graphite Series 600T es una semitorre, por dentro es increíblemente espacioso. Se adapta con facilidad a las tarjetas gráficas de mayor tamaño del sector gracias a las bahías para discos duros reconfigurables.

                                El mayor espacio que se encuentra detrás de la placa base permite la distribución de forma ordenada de los cables. Hasta seis bandejas para discos duros, con espacio para discos SSD y discos mecánicos estándar, situados debajo de las cuatro bahías para discos ópticos de 5,25" que no requieren herramientas.

                                Y si esto no fuera suficiente, puede retirar el ventilador de 200 mm del panel superior e instalar un radiador de 240 mm y ventiladores dobles de 120 mm para incorporar soluciones de refrigeración líquida.

                                ',
                    "url" => 'http://www.corsair.com/it-it/graphite-series-600t',
                    "image" => '600t.jpg',
                    "tag1" => 'Caja ordenador',
                    "tag2" => 'Corsair'

                ),

                array(
                    "title" => 'HP Laserjet PRO CP 1025 - Impresora láser @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Mejore los documentos con color y cree material de marketing desde su propia oficina, impresión en blanco y negro
                                La impresora color más pequeña del mundo también es una de las que mayor ahorro energético proporcionan
                                Características fáciles de usar para mantener su productividad.
                            ',
                    "url" => 'http://store.hp.com/SpainStore/Merch/Product.aspx?id=CF346A&opt=B19&sel=BPRN',
                    "image" => 'impresorahp.jpg',
                    "tag1" => 'Impresora',
                    "tag2" => 'HP'

                ),

                array(
                    "title" => 'Dell UltraSharp U2414H - Monitor LED de 23.8", negro @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Tamaño de imagen visible de 23.8"
                                Carcasa ultra compacta y borde negro de la pantalla
                                4 puertos UBS 3.0
                                DisplayPort 1.2, admite High Bit Rate 2
                                Conector HDMI (MHL)
                            ',
                    "url" => 'http://accessories.us.dell.com/sna/productdetail.aspx?c=us&cs=04&l=en&sku=860-BBCG',
                    "image" => 'monitordell.jpg',
                    "tag1" => 'Monitor',
                    "tag2" => 'Dell'

                ),

                array(
                    "title" => 'Apple MacBook Pro 15" (Retina Quad Core i7, 2.2 GHz, 16 GB, 256 GB HD) @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Espectacular pantalla de retina

                                A millones de píxeles por delante.

                                El modelo de 15 pulgadas tiene más de cinco millones de píxeles, y el modelo de 13 más de cuatro millones. Con este ejército de píxeles puedes retocar fotos y montar vídeos en HD con una definición asombrosa. Y como el texto es tan nítido, da gusto navegar por Internet y leer documentos. Una pantalla así solo la puedes esperar del portátil más avanzado del mundo.


                                Nuevas tecnologías de alto rendimiento

                                El MacBook Pro con pantalla Retina te ofrece un rendimiento sin precedentes gracias a sus procesadores Intel de cuarta generación con dos y cuatro núcleos, gráficos avanzados, almacenamiento flash PCIe, Wi‑Fi 802.11ac* y Thunderbolt 2. Da igual si navegas por una web o la diseñas, si ves un vídeo o lo editas: al MacBook Pro no hay nada que se le resista.

                                Mucha potencia en poco espacio. Eso es lo que ofrece el MacBook Pro. Nuestros ingenieros y diseñadores se han replanteado todos los componentes para maximizar el rendimiento y minimizar el tamaño. Un trabajo fino. El más fino, ligero y potente que hemos hecho nunca.

                                ',
                    "url" => 'http://store.apple.com/es/buy-mac/macbook-pro',
                    "image" => 'macpro.jpg',
                    "tag1" => 'Portátil',
                    "tag2" => 'Apple'

                ),

                array(
                    "title" => 'ASUS X SERIES X553MA-BING-SX451B @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Los portátiles de la Serie X de ASUS son la opción perfecta para trabajar y disfrutar de contenido multimedia. Su resistente diseño presenta un acabado en círculos concéntricos disponible en colores que van desde la sobriedad del negro y el blanco hasta tonos más expresivos como el Rosa pasión o el Púrpura lavanda. En el interior de la Serie X encontramos tecnologías de mejora audiovisual como ASUS Splendid y SonicMaster, que te ofrecen imágenes en la que te sumergirás y una calidad de sonido perfecta.

                                Equipado con todas las características necesarias para ofrecerte el rendimiento que tú buscas, la Serie X reinventa tu experiencia informática cotidiana. Es un equipo perfecto para trabajar y para disfrutar del contenido que quieras. La tecnología Instant On reanuda el ordenador en 2 segundos para que puedas volver a tu tarea inmediatamente, y la conexión USB 3.0 ofrece velocidades de transferencia de datos 10 veces superiores a la del USB 2.0. La Serie X de ASUS pone a tu servicio la velocidad y el rendimiento que necesitas para disfrutar de una experiencia de informática multitarea y multimedia sobresaliente en un equipo con diseño elegante y ergonómico.
                    ',
                    "url" => 'http://www.asus.com/es/Notebooks_Ultrabooks/X553MA/',
                    "image" => 'portatilasus.jpg',
                    "tag1" => 'Portátil',
                    "tag2" => 'Asus'

                ),

                array(
                    "title" => 'Corsair AX860 80Plus Platinum (860 W) @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Funcionamiento silencioso, menor consumo y montaje sencillo

                                La fuente de alimentación AX860 ofrece un suministro de energía respetuosa con el medio ambiente para proteger los componentes críticos de su PC. El eficiente diseño utiliza menos energía y funciona silenciosamente a cargas medias y bajas, y el conjunto modular de cables ayuda hacer el montaje rápido y fácil, con lo que mejora el aspecto general

                                Los condensadores de alta calidad, un riel específico de +12 V y un valor de PF de 0,99 le brindan una potencia confiable con un nivel mínimo de ondulación y ruido. Protección frente a sobrevoltaje, caídas de voltaje, sobreintensidad y cortocircuitos que garantizan la seguridad de los componentes clave de su equipo. Asimismo, AX760 se ha diseñado para alcanzar la potencia máxima con una calificación de temperatura de 50º C del servidor.

                                ',
                    "url" => 'http://www.corsair.com/en/ax860-atx-power-supply-860-watt-80-plus-platinum-certified-fully-modular-psu',
                    "image" => 'fuentecorsair.jpg',
                    "tag1" => 'Fuente de alimentación',
                    "tag2" => 'Corsair'

                ),

                array(
                    "title" => 'Gigabyte G1 Gaming - Tarjeta gráfica de 4 GB nVIDIA GTX 970 @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Arquitetura Maxwell
                                Tecnologia Flex Display
                                Tecnologia GPU Gauntlet de super over clocking
                                Sistema de resfriamento WINDFORCE 3X
                                Tecnologia GIGABYTE Dual Bios
                                Componentes Ultra DurÃ¡veis
                                Tecnologia OC Guru II com GPU Boost 2.0
                                4GB de memÃ³ria de grande capacidade para uma experiÃªncia gamer 4K Ultra HD
                                6 controladores de fases
                                Base traseira (Back Plate) metalica.',
                    "url" => 'http://www.gigabyte.com/products/product-page.aspx?pid=5209#ov',
                    "image" => '970gtx.jpg',
                    "tag1" => 'Tarjeta gráfica',
                    "tag2" => 'Gigabyte'

                ),

                array(
                    "title" => 'Intel Core i5-4690K Box - Procesador (3.5 GHz) @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'    Nº de procesador: i5-4690K.
                                    Cache: 6 MBs
                                    DMI2: 5Gt/s.
                                    64-Bits.
                                    Extensión de conjunto de instrucciones: SSE 4.1/4.2, AVX 2.0
                                    Núcleos: 4.
                                    Subprocesadores: 4
                                    Frecuencia del procesador: 3,5 GHz.
                                    Frecuencia Turbo: 3,9Ghz.
                                    Tamaño de memoria máx. (depende del tipo de memoria): 32GB.
                                    Tipo de memoria: DDR3-1333/1600.
                                    Nº de canales: 2.
                                    Intel HD Graphics 4600.

                                ',
                    "url" => 'http://ark.intel.com/es-es/products/80811/Intel-Core-i5-4690K-Processor-6M-Cache-up-to-3_90-GHz',
                    "image" => 'procesadorintel.jpg',
                    "tag1" => 'Procesador',
                    "tag2" => 'Intel'

                ),

                array(
                    "title" => 'Samsung Serie 850 EVO - Disco duro sólido (500 GB) @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Tamaño de búfer: 512 MB
                                    Velocidad de transferencia de datos: 6 Gbit/s
                                    Velocidad de escritura: 520 MB/s y lectura: 540 MB/s
                                ',
                    "url" => 'http://www.samsung.com/es/news/local/samsung-serie-850-evo',
                    "image" => 'sddsamsung.jpg',
                    "tag1" => 'Disco duro',
                    "tag2" => 'Samsung'

                )
            );

            $offerIndex = array_rand($computersArray, 1);
            return $computersArray[$offerIndex];
        }


        elseif($category->getName() == 'Entretenimiento'){
            $price = rand(5, 60);

            $entertainmentArray = array(
                array(
                    "title" => 'Entradas Selma @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'SELMA retrata la lucha histórica de Martin Luther King para garantizar el derecho de voto de todos los ciudadanos y la peligrosa y terrorífica campaña que se cerró con una larga marcha desde la ciudad de Selma hasta la de Montgomery, en Alabama, y que llevó al presidente Johnson a firmar la ley sobre el derecho al voto en 1965.',
                    "url" => 'http://www.filmaffinity.com/es/film583944.html',
                    "image" => 'selma.jpg',
                    "tag1" => 'Película',
                    "tag2" => 'Entradas'

                ),

                array(
                    "title" => 'RAPHAEL: GIRA DE AMOR Y DESAMOR @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'"De amor & desamor" es el sugerente título que da nombre al nuevo disco y a la gira de Raphael.
                                    El artista vuelve a sorprender con nuevas versiones (nueva voz, nuevos arreglos, nueva producción) de temas históricos en su carrera profesional y, desde luego, en la música popular en español del último medio siglo.
                                    A punto de cumplir la mágica cifra de 55 años sobre los escenarios, nos vuelve a brindar algunas de sus canciones imprescindibles. ',
                    "url" => 'http://raphaelnet.com/index.html',
                    "image" => 'raphael.jpg',
                    "tag1" => 'Concierto',
                    "tag2" => 'Entradas'

                ),

                array(
                    "title" => 'Concierto Julio Iglesias @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'El concierto es el 6 de junio en el Palacio de Vista Alegre (Madrid).

                                Julio es un peligro y lo sabes',
                    "url" => 'http://www.julioiglesias.com/',
                    "image" => 'julioiglesias.jpg',
                    "tag1" => 'Concierto',
                    "tag2" => 'Entradas'

                ),

                array(
                    "title" => 'Tricicle - Bits - Entradas @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'En este espectáculo los integrantes de Tricicle se han convertido en bits y como tales, a golpe de ratón saltan de sketch, de tema, de decorado, de personajes, de gafas, de sexo incluso recuperan a viejos conocidos para hacerse un autohomenaje con el fin de aumentar su pasión enfermiza por el humor.',
                    "url" => 'http://www.tricicle.com/sp/index.php',
                    "image" => 'tricicle.jpg',
                    "tag1" => 'Teatro',
                    "tag2" => 'Entradas'

                ),

                array(
                    "title" => 'Ex-Machina (Digital) @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Un programador multimillonario contrata a Caleb, un joven empleado para que pase una semana en un lugar remoto, con el objetivo de que participe en un test en la que estará involucrada su última creación: un robot mujer donde la inteligencia artificial lo es todo.',
                    "url" => 'http://www.filmaffinity.com/es/film132582.html',
                    "image" => 'exmachina.jpg',
                    "tag1" => 'Película',
                    "tag2" => 'Entradas'

                ),

                array(
                    "title" => 'El Francotirador @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'De la mano del director Clint Eastwood, llega El Francotirador, protagonizada por Bradley Cooper como Chris Kyle, el francotirador más letal de la historia del ejército de los Estados Unidos. Pero hay algo más en él aparte de ser un buen tirador. El marine del grupo de operaciones especiales de la Marina de Estados Unidos, Chris Kyle, es enviado a Irak con una sola misión: proteger a sus compañeros de ejército. Su precisión milimétrica salva incontables vidas en el campo de batalla y, a medida que se extienden sus valientes hazañas, se gana el apodo de Leyenda. Sin embargo, su reputación también crece detrás de las líneas enemigas, de manera que ponen precio a su cabeza y se convierte en objetivo prioritario de los insurgentes. También se está enfrentando a otra batalla en casa: ser un buen marido y padre desde el otro lado del mundo. A pesar del peligro, y del precio que, debido a su actividad, tiene que pagar su familia en su país, Chris sirve durante cuatro angustiosas misiones en Irak, personificando el emblema del credo de los SEAL: no dejar a ningún hombre atrás. Pero al volver a su casa,con su mujer, Taya Renae Kyle (Siena Miller) y con sus hijos, Chris descubre que lo que no puede dejar atrás es la guerra.',
                    "url" => 'http://www.filmaffinity.com/es/film941942.html',
                    "image" => 'elfrancotirador.jpg',
                    "tag1" => 'Película',
                    "tag2" => 'Entradas'

                ),

                array(
                    "title" => 'La Conspiración de Noviembre @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Peter Devereaux es un agente de la CIA extremadamente peligroso y altamente entrenado, que es persuadido para salir de su tranquilo retiro para realizar una misión muy personal. Debe proteger a una testigo muy valiosa, Alice Fournier, que podría revelar la verdad detrás de un antiguo caso de conspiración. Pronto descubre que esta misión le convierte en el objetivo de su antiguo amigo y protegido en la CIA, David Mason. Con crecientes sospechas de la presencia de un infiltrado en la agencia, no hay nadie en quien Devereaux pueda confiar, nadie en absoluto.',
                    "url" => 'http://www.filmaffinity.com/es/film218143.html',
                    "image" => 'conspiracionnoviembre.jpg',
                    "tag1" => 'Película',
                    "tag2" => 'Entradas'

                ),

                array(
                    "title" => 'Nino Bravo - Su vida, su música. El musical @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Tarifa 15% descuento para Mayores de 65 años.(Al final del proceso de compra utilizar el desplegable en el precio y seleccionar la tarifa correspondiente).
                                Nino Bravo, El Musical realiza un recorrido cronológico por la vida y la carrera artística de Nino Bravo , con motivo del 40 aniversario de su fallecimiento. Imágenes inéditas cedidas por TVE, entrevistas a diferentes personajes de la época, compositores de sus temas y amigos, y por supuesto, sus canciones. ',
                    "url" => 'http://www.eliteproducciones.com/musicales-8-nino-bravo-el-musical-su-vida-su-musica.html',
                    "image" => 'logoninobravo.jpg',
                    "tag1" => 'Musical',
                    "tag2" => 'Entradas'

                ),

                array(
                    "title" => 'Tour Bernabéu @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'¿Te imaginas poder recorrer el Bernabéu a tu aire, visitando todos aquellos míticos lugares que sólo conocías por fotografías? Y hacerlo sin prisas, invirtiendo el tiempo que consideres necesario, y en cualquier día del año. El sueño de todo madridista ya es realidad gracias al Tour del Bernabéu.',
                    "url" => 'http://www.realmadrid.com/entradas/tour-bernabeu',
                    "image" => 'tourbernabeu.jpg',
                    "tag1" => 'Tour',
                    "tag2" => 'Entradas'

                ),

                array(
                    "title" => 'AMALUNA CIRQUE DU SOLEIL @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Amaluna invita a la audiencia a una isla misteriosa, gobernada por las diosas y guiada por los ciclos de la luna.

                                    Su reina, Prospera, dirige la ceremonia de mayoría de edad de su hija en un rito que honra la femineidad, la renovación, el renacimiento y el equilibrio, y que marca el traspaso de estas ideas y valores de una generación a la siguiente.

                                    Tras una tormenta causada por Prospera, un grupo de hombres jóvenes llegan a la isla, lo que desencadena una épica y emotiva historia de amor entre la hija de Prospera y un valiente joven pretendiente. Pero es un amor que deberá superar muchas pruebas. La pareja deberá enfrentar numerosos y difíciles desafíos, y superar sobrecogedores contratiempos antes de lograr tener confianza, fe y armonía el uno con el otro.',
                    "url" => 'https://www.cirquedusoleil.com/es-ES/home/shows.aspx',
                    "image" => 'amaluna.jpg',
                    "tag1" => 'Circo',
                    "tag2" => 'Entradas'

                )
            );

            $offerIndex = array_rand($entertainmentArray, 1);
            return $entertainmentArray[$offerIndex];
        }


        elseif($category->getName() == 'Hogar'){
            $price = rand(5, 200);

            $homeArray = array(
                array(
                    "title" => 'Roca - Tapa Inodoro Lacado con Bisagra de Acero @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'
    Asiento para el inodoro del modelo victoria de roca
    Asiento lacado para inodoro victoria de roca.: asiento para el inodoro del modelo victoria de roca.asientos marca roca
',
                    "url" => 'http://www.roca.es/',
                    "image" => 'taparoca.jpg',
                    "tag1" => 'Tapa Inodoro',
                    "tag2" => 'Roca'

                ),

                array(
                    "title" => 'Bosch IXO (0.603.981.000) - Atornillador de litio (3,6 W, 3,6 V) color verde @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'CARACTERÍSTICAS PRINCIPALES DEL PRODUCTO

                                No tiene efecto memoria ni se autodescarga, siempre lista para la acción gracias a la Tecnología de Litio. PowerLight, gracias al diodo integrado, las piezas de trabajo se encuentran siempre bien iluminadas. Indicador del sentido de giro y del estado de carga por luz LED.
                                OTRAS VENTAJAS

                                Bloqueo automático del husillo para apretar o soltar tornillos a mano. El cargador es al mismo tiempo un lugar de colocación óptimo. Empuñadura Softgrip para una sujeción mejor y más cómoda.',
                    "url" => 'http://www.bosch-ixo.com/es/es/',
                    "image" => 'boschdestornillador.jpg',
                    "tag1" => 'Herramientas',
                    "tag2" => 'Bosch'

                ),

                array(
                    "title" => 'Tefal Actifry Snacking - Freidora @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Te imaginas que pudieras cocinar 1 kg de alimentos con tan sólo una cucharada de aceite? Con Actifry Snacking de Tefal, tu robot de cocina sano, lo puedes hacer.

Al necesitar muy poco aceite, se cocina con mucha menos grasa, por lo que sus ventajas nutricionales son evidentes.

El poco consumo en aceite te permite usar aceite limpio cada vez que cocines, probar diferentes tipos de aceite y por supuesto supone una ventaja económica.

Actifry Snacking es el robot de cocina sano para todos tus platos de cada dia, porque gracias a su nuevo plato de cocción saludable, podrás cocinar todo tipo de alimentos frágiles, porciones enteras y congelados.

Su tapa transparente permite ver el proceso de cocción y saber cuándo está lista la comida, además de evitar las típicas salpicaduras de aceite. Con Actifry Snacking cocinar es fácil y limpio porque no necesitas más utensilios.

El recipiente de cocción es antiadherente para que ningún alimento se pegue.

Su limpieza es muy sencilla, la tapa, la bandeja y el recipiente de cocción se pueden extraer y lavar en el lavavajillas. Así ahorrarás tiempo y esfuerzo.

La freidora sana Actifry Snacking incluye un recetario muy completo e interesante para que realices recetas sanas y deliciosas: gambas, carne, pollo, verduras, pescado, salsas, etc..

Porque la nutrición no es sólo cuestión de ingredientes. ',
                    "url" => 'http://www.tefal.es/All+Products/Cooking+appliances/Actifry/Products/Actifry+Snacking/Actifry+Snacking.htm',
                    "image" => 'freidora.jpg',
                    "tag1" => 'Cocina',
                    "tag2" => 'Tefal'

                ),

                array(
                    "title" => 'Taurus Toast & Go - Sandwichera @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'El Toast & Co es un nuevo concepto en la cocina, una mezcla entre Sandwichera y grill de pequeño formato. Su diseño está inspirado por las típicas parrillas americanas, sobre todo su asa y su placa superior decorada con relieves lineales. Ambas placas del Toast & Co son antiadherentes y la superior además es basculante. Además ambas placas cuentan con relieves lineales para conseguir marcar a la perfección los sándwiches. También cuenta con un asa de toque frío y un gancho fijo de cierre.',
                    "url" => 'http://www.tefal.es/All+Products/Cooking+appliances/Actifry/Products/Actifry+Snacking/Actifry+Snacking.htm',
                    "image" => 'tostadora.jpg',
                    "tag1" => 'Cocina',
                    "tag2" => 'Taurus'

                ),

                array(
                    "title" => 'Bosch MSM67170 - Batidora de mano (750 W), color negro @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Motor de 750W
                                Pie de batidora con 4 cuchillas de acero inoxidable
                                Ligera, con asa ergonómica y recubrimiento SoftTouch
                                12 velocidades más botón turbo
                                Accesorios: picador para hierbas aromáticas, frutos secos o queso, varillas para crema o claras de huevo
                                Vaso de mezclas graduado con tapa
                            ',
                    "url" => 'http://www.bosch-home.es/productos/preparacion-de-alimentos/batidoras/batidoras-de-mano/MSM67170.html',
                    "image" => 'batidorabosch.jpg',
                    "tag1" => 'Cocina',
                    "tag2" => 'Bosch'

                ),

                array(
                    "title" => 'Philips 621009 Master PL-C26W - Bombilla compacta (26 W)  @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Estándares de calidad alemana, certificado GS (Geprufte Sicherheit)
                                Lámpara compacta de descarga de mercurio a baja presión
                                Diseñada para alumbrado general o auxiliar en ambientes profesionales o domésticos

                            ',
                    "url" => 'http://www.philips.es/c-m-li/bombillas-fluorescentes-compactas',
                    "image" => 'bombilla.jpg',
                    "tag1" => 'Bombilla',
                    "tag2" => 'Philips'

                ),

                array(
                    "title" => 'Black & Decker EMAX32S cortadora de césped (1200W) @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'EMAX eléctrico Segadora de Negro & Decker
                            Corte hasta el borde, cortar sin escape, Tecnología E -Drive
                            Es naranja
                        ',
                    "url" => 'http://www.blackanddecker.es/gardentools/productdetails/catno/EMax32s/',
                    "image" => 'cortacesped.jpg',
                    "tag1" => 'Herramientas',
                    "tag2" => 'Black & Decker'

                ),

                array(
                    "title" => 'iRobot Roomba 650 - Robot aspirador @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Olvídate de la limpieza tradicional y deja que el robot aspirador iRobot Roomba 650 haga la limpieza por ti. Nuestros robots son una extraordinaria mezcla entre desarrollo, tecnología y diseño, y todo esto significa que tus suelos estarán limpios cada día sin mover un solo dedo.

                                El robot aspirador iRobot Roomba 650 limpia tal y como tú lo harías, elimina el polvo, la suciedad y es capaz de ajustar su cabezal de limpieza a los diferentes tipos de suelo de forma automática. Los modelos de la serie 600 utilizan la avanzada tecnología Aerovac para maximizar el flujo de aire y succionar así la suciedad de los cepillos y llevarla al depósito. Del mismo modo, su avanzado carro de cepillos permite una limpieza más fácil alargando así su vida útil. ',
                    "url" => 'http://www.irobot.es/tienda/robots/roomba/serie-600/robot-de-limpieza-aspirador-irobot-roomba-651',
                    "image" => 'roomba.jpg',
                    "tag1" => 'Aspirador',
                    "tag2" => 'iRobot'

                ),

                array(
                    "title" => 'Thermomix TM31 @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Thermomix ® es más que un ayudante de cocina - es un universo de recetas en constante expansión, de comunidades online y offline y de recursos esenciales para ayudarte a sacar el máximo partido a tu Thermomix ®.',
                    "url" => 'http://thermomix.vorwerk.es/home/',
                    "image" => 'thermomix.jpg',
                    "tag1" => 'Cocina',
                    "tag2" => 'Thermomix'

                ),

                array(
                    "title" => 'Bosch TAS2002 - Cafetera Tassimo @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Capacidad del depósito de agua extraible: 1.5L
                            Potencia: 1300 W
                            Sistema multibebida que prepara distintas variedades de bebidas calientes: espresso, cappuccino, bebida con sabor a chocolate, té, latte macchiato...
                            Totalmente automática, con un solo botón. Incluye LEDs indicadores del estado de la máquina
                            Reconocimiento inteligente de T DISCS mediante lector de código de barras: regula la cantidad de agua, la temperatura y el tiempo de preparación
                        ',
                    "url" => 'http://www.bosch-home.es/productos/cafeteras/maquinas-multibebidas-tassimo/TAS2002.html',
                    "image" => 'cafetera.jpg',
                    "tag1" => 'Cocina',
                    "tag2" => 'Bosch'

                )
            );

            $offerIndex = array_rand($homeArray, 1);
            return $homeArray[$offerIndex];
        }


        elseif($category->getName() == 'Moda'){
            $price = rand(5, 200);

            $fashionArray = array(
                array(
                    "title" => 'Casio Reloj Vintage @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Pulsera: Plástico Negro
    Cierre: Hebilla
    Material de la caja: Plástico
    Color de la esfera: Plateado
    Material de la esfera: Plástico

',
                    "url" => 'http://www.casio-europe.com/es/',
                    "image" => 'casio.jpg',
                    "tag1" => 'Reloj',
                    "tag2" => 'Casio'

                ),

                array(
                    "title" => 'Lacoste AH7912-00 - Jersey, con cuello pico para hombre @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'100% algodón. Lavado a máquina, 40 grados. Estilo del cuello: pico',
                    "url" => 'http://global.lacoste.com/es/homepage',
                    "image" => 'lacoste.jpg',
                    "tag1" => 'Ropa',
                    "tag2" => 'Lacoste'

                ),

                array(
                    "title" => 'adidas Stan Smith - zapatilla deportiva de piel unisex @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'La Stan Smith triunfó en las pista de tenis durante los años 70. Y gracias a su elegante diseño no tardó en convertirse en todo un icono de la moda urbana. Esta zapatilla para mujer luce un toque renovado con un punto transgresor en forma de piel blanca cuarteada y grietas tintadas. Presenta un refuerzo en contraste en el talón, 3 bandas perforadas y una suela de goma cosida.

    Parte superior de piel cuarteada y capa tintada inferior visible a través de las grietas
    Forro de piel sintética
    Contrafuerte y refuerzo del talón en contraste
    Suela de goma cosida

                    ',
                    "url" => 'http://www.adidas.es',
                    "image" => 'zapasadidas.jpg',
                    "tag1" => 'Deporte',
                    "tag2" => 'Adidas'

                ),

                array(
                    "title" => 'adidas FEF PRE Suit - Chándal de fútbol para hombre, color negro @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Este conjunto de chaqueta y pantalón para hombre está inspirado en el distintivo rojo de la primera equipación que inspiró el nombre de La Furia Roja.

                                Tiro de la entrepierna de 78,5 cm (talla 50).
                                Bolsillos frontales con cremallera en la chaqueta y el pantalón.
                                Completamente forrado de malla para mayor comodidad y transpirabilidad.
                                Chaqueta: Cremallera frontal y cuello alzado; puños elásticos para un ajuste cómodo; escudo de la Federación Española de Fútbol a la izquierda del pecho.
                                Pantalón: Cintura elástica con cordón.
                                Tejido dobby 100% poliéster.',
                    "url" => 'http://www.adidas.es',
                    "image" => 'chandaladidas.jpg',
                    "tag1" => 'Deporte',
                    "tag2" => 'Adidas'

                ),

                array(
                    "title" => 'Levis 511 Slim Fit - Vaqueros slim fit para hombre @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Sube, vive y trabaja con los mismos vaqueros. Diseñado para el ciclista urbano, nuestros vaqueros Commuter™ 511™ Slim Fit Jeans son súper prácticos, te dan seguridad, libertad de movimiento y protección sin importar el ritmo al que te muevas. ',
                    "url" => 'http://www.levi.com',
                    "image" => 'levis.jpeg',
                    "tag1" => 'Ropa',
                    "tag2" => 'Levis'

                ),

                array(
                    "title" => 'VANS Zapatillas @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Material exterior: cuero
    Revestimiento: tela
    Material de la suela: goma
    Cierre: cordones
    Tipo de tacón: plano
    Tipo de talla: talla estándar
',
                    "url" => 'http://shop.vans.es',
                    "image" => 'vans.png',
                    "tag1" => 'Calzado',
                    "tag2" => 'Vans'

                ),

                array(
                    "title" => 'Dr. Martens 1460 - Botas con cordones @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Material exterior: Cuero
    Revestimiento: Sin forro
    Material de la suela: Tela
    Cierre: Cordones
    Altura del tacón: 2.5 centímetros
    Tipo de tacón: Plano
    Composición: ver descripción
    Diámetro de eje: normal centímetros

',
                    "url" => 'http://www.drmartens.com/uk/',
                    "image" => 'martens.jpg',
                    "tag1" => 'Calzado',
                    "tag2" => 'Dr. Martens'

                ),

                array(
                    "title" => 'Oakley - Gafas de sol Holbrook @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'HOLBROOK es un clásico de todos los tiempos que se hace eco de los héroes de la pantalla de las décadas de 1940, 50 y 60, los iconos que inspiraron a la juventud con su propia marca de desafío. ¿Quién era la leyenda? En ocasiones era un solitario y en ocasiones un líder, un hombre que vivía por un código, que luchaba por lo que era correcto y que hacía todo lo que tenía que hacerse. No lo llamaría un hombre entre hombres porque esa chorrada de macho no significaría nada para él. Lo que veía era lo que había, incluido el polvo de las carreteras abiertas y una apariencia que decía que podría haberse ido al día siguiente porque nada podría retenerle.',
                    "url" => 'http://es.oakley.com/',
                    "image" => 'oakley.jpg',
                    "tag1" => 'Gafas',
                    "tag2" => 'Oakley'

                ),

                array(
                    "title" => 'Rayban Aviator - Gafas de sol, talla única, multicolor @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'No polarizada
    Ancho de las lentes: 6.2 centímetros
    Alto de las lentes: 5.4 centímetros
    Puente: 14 milímetros
    Dimensiones:: 62-14-135 FEED_BULLET_MEASURE
    Item reference number: RB3025 001/51
    Model: Varón

',
                    "url" => 'http://www.ray-ban.com',
                    "image" => 'raybanaviator.jpeg',
                    "tag1" => 'Gafas',
                    "tag2" => 'Rayban'

                ),

                array(
                    "title" => 'Casio Reloj Vintage Dorado @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Pulsera: Acero inoxidable Dorado
    Material de la caja: Acero inoxidable
    Color de la esfera: Dorado
    Movimiento: Cuarzo
    Otras características: Cronógrafo

',
                    "url" => 'http://www.casio-europe.com/es/',
                    "image" => 'casiodorado.jpg',
                    "tag1" => 'Reloj',
                    "tag2" => 'Casio'

                )
            );

            $offerIndex = array_rand($fashionArray, 1);
            return $fashionArray[$offerIndex];
        }


        elseif($category->getName() == 'Niños'){
            $price = rand(5, 200);
            $kidsArray = array(
                array(
                    "title" => 'Silla de paseo Epic Pushchair Black @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Deportivo, compacto y dinámico son los tres adjetivos que definen al nuevo Epic de Jané. Su diseño urbano y vanguardista destaca por su chasis tubular que le confiere un carácter resistente e innovador. Además, Epic ofrece una versatilidad extraordinaria: neumáticos de PU de alta resistencia y poder amortiguador, suspensión independiente de largo recorrido en las ruedas traseras, excelente maniobrabilidad gracias a su equilibrado tren de ruedas delantero consiguiendo un giro suave y preciso, doble seguro de freno y plegado más compacto con hamaca extraíble y reversible. Su nuevo chasis tubular fabricado en aluminio de alto límite elástico lo convierte en uno de los coches más ligeros de su categoría. Pensando hasta en el último detalle, Epic también incluye una práctica funda para el manillar que evita el desgaste por el uso continuo de la sillita y que además le aporta un toque muy personal. Como todos los cochecitos de Jané, Epic ofrece todas las características y rasgos propios de la marca: ligereza, conducción suave, comodidad para el bebé, sistema pro fix y un diseño único. ¡Epic es un gran coche lleno de novedades!',
                    "url" => 'http://www.jane.es/es/',
                    "image" => 'sillajane.jpg',
                    "tag1" => 'Bebes',
                    "tag2" => 'Jané'

                ),

                array(
                    "title" => 'Chicco Oasys 2-3 FixPlus - Silla de coche, grupo 2/3, color negro @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Oasys 2-3 es la novísima silla de auto homologada según la normativa ECE R44/04 para el transporte de niños de 15 a 36 Kg (Grupo 2-3); de 3 a 12 años aproximadamente.

                                    Se instala utilizando los cinturones del coche siguiendo las guías rojas. Además, la silla puede utilizarse utilizando los conectores rígidos FixPlus para garantizar la máxima estabilidad. ',
                    "url" => 'http://www.chicco.es/ProdottiChicco/SchedaProdotto/tabid/172/art/06079245950000/Default.aspx',
                    "image" => 'sillachicco.jpg',
                    "tag1" => 'Bebes',
                    "tag2" => 'Chicco'

                ),

                array(
                    "title" => 'Recaro Young Sport - Silla de coche, grupo 1/2/3, color grafito @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Para niños que ya van al colegio, la Recaro Young Sport ofrece los niveles más altos de seguridad y ergonomía desde los 36 kg (aprox. 9 meses a 12 años).Tecnológicamente avanzada, con espuma interior ergonómica desarrollada para la industria automotriz, que proporciona confort total y mantiene la circulación del aire.Incluye arnés regulable en altura con elementos que absorben la energía, un regulador central del arnés, tensores del cinturón patentados y una práctica asa de transporte.

                                Sólo colocar en el automóvil e instalar en su sitio con el cinturón de seguridad de tres puntos del automóvil. Es todo lo que hay que hacer!

                                Beneficios:

                                -Una silla de seguridad para niños desde aprox. los 9 meses de edad y hasta los c. 12 años (36 Kg), lo que la hace una silla infantil especialmentediseñada para usarse durante mucho tiempo

                                -La silla es muy fácil de instalar en el vehículo. Permite continuar ajustando el cinturón de seguridad del automóvil aún con la hebilla cerrada

                                -Extremadamente confortable por su óptimo sistema de circulación del aire, como resultado de su espuma ergonómica
                                ',
                    "url" => 'http://www.recaro.com/',
                    "image" => 'sillarecaro.jpg',
                    "tag1" => 'Bebes',
                    "tag2" => 'Recaro'

                ),

                array(
                    "title" => 'Simba 6315872630 Disney Winnie the Pooh - Peluche de Winnie the Pooh básico (25 cm) @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Dimensiones : 25 cm, Material: Felpa',
                    "url" => 'http://www.disneystore.es/winnie-the-pooh-y-sus-amigos/winnie-the-pooh?CMP=KNC-Google&att=Franquicia-WinniethePooh_PeluchesWinniethePooh&gclid=CLy6qMTzrMQCFYLItAodinkA6w',
                    "image" => 'peluche.jpg',
                    "tag1" => 'Bebes',
                    "tag2" => 'Disney'

                ),

                array(
                    "title" => 'LEGO Duplo - Nuevo tren, set de inicio (10507) @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Introduce a tu hijo en el mundo de los trenes y la construcción con Mi Primer Set de Trenes LEGO DUPLO. Crea el divertido tren de vapor y disfruta con los efectos de sonido, el vagón de pasajeros, la estación con taquilla, la bomba de repostaje y la señal. Un set con un gran número de ladrillos DUPLO y más de 55 cm de vías, perfecto para jóvenes constructores, que pasarán horas disfrutando del juego creativo en torno a los trenes. Incluye figuras DUPLO de un maquinista y un niño. ',
                    "url" => 'http://www.lego.com/es-es/duplo',
                    "image" => 'lego.jpg',
                    "tag1" => 'Bebes',
                    "tag2" => 'Lego'

                ),

                array(
                    "title" => 'Motorola MBP 33 - Intercomunicador con Cámara Digital @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Pantalla digital, Función de responder al bebé, Alerta de fuera de cobertura, Función de infrarrojos, Pantalla de LCD, Digital, Monitor a color, Alcance: hasta 300 metros, Señal encriptada, Pantalla LCD, Sensor de la temperatura de la habitación, Sólo audio, Sensor de visión nocturna, Sensor de vídeo, Sensor de audio',
                    "url" => 'https://www.motorola.com/us/monitors/MBP33-Wireless-Video-Baby-Monitor/95184.html',
                    "image" => 'intercomunicador.jpg',
                    "tag1" => 'Bebes',
                    "tag2" => 'Motorola'

                ),

                array(
                    "title" => 'Princesas Disney Frozen sparkle - Muñeca Elsa (Mattel Y9960) @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'La muñeca Elsa Purpurina luce un precioso vestido largo azul brillante con detalles invernales mientras que Kristoff, el valiente aventurero, lleva el mismo traje que en la película de dibujos animados. Se venden por separado. ',
                    "url" => 'http://www.mattel.com/',
                    "image" => 'frozen.jpg',
                    "tag1" => 'Infantil',
                    "tag2" => 'Mattel'

                ),

                array(
                    "title" => 'Color Baby - Coche de rally Citroën con radiocontrol, 24 x 17 cm, escala 1:28 (41045) @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Luces delanteras y traseras
                                Suspensión especial
                                Vehículo full function
                                Mando 2 x AA, coche 2 x AA
                                Frecuencia 27.145 MHz
                                Coche de juguete
                                Es teledirigido mediante un mando radiocontrol
                                Cuenta con luces traseras y delanteras',
                    "url" => 'http://www.colorbaby.es/',
                    "image" => 'cocheteledir.jpg',
                    "tag1" => 'Infantil',
                    "tag2" => 'Color Baby'

                ),

                array(
                    "title" => 'Playmobil - Barco pirata de ataque, set de juego (5135) @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Estos piratas Playmobil surcan los mares en busca de un gran tesoro. Siguiendo las pautas de un mapa, llegarán a una isla en la que esperan encontrar cofres llenos de oro ... ¿Serán capaces? ¡Acompáñales en su aventura! Con dos cañones. Se le puede adaptar motor submarino. ',
                    "url" => 'http://www.playmobil.es/',
                    "image" => 'barcoplaymobil.jpg',
                    "tag1" => 'Infantil',
                    "tag2" => 'Playmobil'

                ),

                array(
                    "title" => 'Scalextric Compact - Coche Compact Porsche 911 GT3 "Vallejo" (Fábrica de Juguetes 90226) @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Sistema compact.    Resistentes y con más agarre de lo normal para hacer más divertida la carrera.    Pensado para los más pequeños',
                    "url" => 'http://scalextric.es/inicio',
                    "image" => 'scalextric.jpg',
                    "tag1" => 'Infantil',
                    "tag2" => 'Scalextric'

                )
            );

            $offerIndex = array_rand($kidsArray, 1);
            return $kidsArray[$offerIndex];
        }


        elseif($category->getName() == 'Comida'){
            $price = rand(1, 6);
            $foodArray = array(
                array(
                    "title" => 'Atún claro en aceite vegetal Calvo pack de 6x52 g.  @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'El atún claro Calvo en aceite vegetal es el resultado de más de 70 años de experiencia seleccionando y elaborando el mejor atún claro. En Calvo preparamos este producto seleccionando la parte más sabrosa del atún, el solomillo, para que disfrutes de un sabor y textura inigualables. - See more at: http://calvo.es/productos/general/atun-claro-en-aceite-vegetal/#sthash.9yJZreUJ.dpuf',
                    "url" => 'http://calvo.es/',
                    "image" => 'atuncalvo.png',
                    "tag1" => 'Conservas',
                    "tag2" => 'Calvo'

                ),

                array(
                    "title" => 'Aceite de oliva virgen Carbonell 1 l.  @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Durante más de 145 años Carbonell ha estado presente en nuestras cocinas y ha elaborado su aceite cuidando detalladamente cada paso. Hoy, fruto de esa experiencia, Carbonell elabora su aceite con el Método Exclusivo Carbonell® por el que se seleccionan las mejores aceitunas en base al origen, variedad y momento de recogida y se sigue un proceso único de elaboración con el que se conserva la esencia de la aceituna. De este modo, se obtienen , de manera natural , aceites con beneficios específicos. Por eso, no todos los aceites son iguales.',
                    "url" => 'http://www.carbonell.es/',
                    "image" => 'aceitecarbonell.png',
                    "tag1" => 'Cocina',
                    "tag2" => 'Carbonell'

                ),

                array(
                    "title" => 'Coca cola 2 litros @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'La chispa de la vida … ',
                    "url" => 'http://www.cocacola.es/',
                    "image" => 'cocacola.jpg',
                    "tag1" => 'Bebida',
                    "tag2" => 'Coca cola'

                ),

                array(
                    "title" => 'BIMBO Buenísimo pan de molde sin corteza bolsa 450 g @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'HARINA DE TRIGO, AGUA, LEVADURA, AZÚCAR, ACEITE DE OLIVA REFINADO (1,1%), SAL, EMULGENTES (E-471, E-472e, E-481), GLUTEN DE TRIGO, VINAGRE, HARINA DE SOJA, CONSERVADORES (E-200, E-282), CORRECTOR DE ACIDEZ (E-341i), ALMIDÓN DE MAIZ, HARINA DE HABAS. PUEDE CONTENER TRAZAS DE SEMILLAS DE SÉSAMO.',
                    "url" => 'http://www.bimbo.es/',
                    "image" => 'panbimbo.jpg',
                    "tag1" => 'Cocina',
                    "tag2" => 'Bimbo'

                ),

                array(
                    "title" => 'CIDACOS espárragos blancos 6-12 piezas extra gruesos frasco 205 g neto escurrido @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Esparrago, agua, sal, azucar. Antoxidantes: acido citrico y acido ascorbico (vitamina C)',
                    "url" => 'https://www.cidacos.com/',
                    "image" => 'cidacos.jpg',
                    "tag1" => 'Conservas',
                    "tag2" => 'Cidacos'

                ),

                array(
                    "title" => 'BRILLANTE arroz redondo tradicional cocido para guarnición pack 2 envase 125 g @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Arroz redondo (arroz cocido 97%), agua, aceite de girasol, sal, lecitina de soja.Contiene soja y derivados. Yo me lo guiso, yo me lo como.',
                    "url" => 'http://www.brillante.es/',
                    "image" => 'brillante.jpg',
                    "tag1" => 'Cocina',
                    "tag2" => 'Brillante'

                ),

                array(
                    "title" => 'JUVER disfruta cóctel de frutas sin azúcares añadidos pack 2 latas 215 g neto escurrido @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Fruta (melocotón, pera, piña, uva, cerezas sin hueso coloreadas con E-127), agua, acidulante: ácido cítrico y edulcorantes (aspartamo, acesulfamo K). Contiene una fuente de fenilalanina.',
                    "url" => 'http://www.juver.com/juver/',
                    "image" => 'juver.jpg',
                    "tag1" => 'Bebida',
                    "tag2" => 'Juver'

                ),

                array(
                    "title" => 'Pascual leche entera con calcio envase 1 l @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'La Vitamina D contribuye a la absorción normal del calcio. El calcio es necesario para tus huesos. Con Vitamina K, te ayuda a mantener tus huesos en buen estado. En el contexto de una dieta variada y equilibrada y un estilo de vida saludable. ',
                    "url" => 'http://www.lechepascual.es/',
                    "image" => 'pascualleche.jpg',
                    "tag1" => 'Bebida',
                    "tag2" => 'Pascual'

                ),

                array(
                    "title" => 'Danone Danonino petit pack 6 unidades 50 g @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Leche, azúcar, puré de fresa (5,4%) o fresa 4,7% y plátano 0,6%, fructo-oligosacáridos o fructosa, almidón modificado de tapioca, sales de calcio, fermentos lácticos, vitamina D, aromas y colorantes naturales: carmín (fresa), luteína y carmín (fresa-plátano) y caramelo (galleta).',
                    "url" => 'http://www.alimentasonrisas.es/es/danonino/',
                    "image" => 'danonino.jpg',
                    "tag1" => 'Cocina',
                    "tag2" => 'Danone'

                ),

                array(
                    "title" => 'Frigo Magnun Mini helados surtidos Magnum @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Directo a las lorzas. Leche desnatada rehidratada, azúcar, manteca de cacao¹,Leche desnatada en polvo, suero de leche concentrado, almendras, grasa de coco, jarabe de glucosa, mantequilla concentrada, jarabe de glucosa-fructosa, pasta de cacao¹, emulgentes (E471, E442, E476), estabilizantes (E410, E412, E407), vainas de vainilla, aromas, colorante (E160a). Puede contener soja, otros frutos secos y cacahuetes. Sin gluten. ¹Certificado Rainforest Alliance?.',
                    "url" => 'http://www.frigo.es/',
                    "image" => 'magnun.jpg',
                    "tag1" => 'Helado',
                    "tag2" => 'Frigo'

                )
            );

            $offerIndex = array_rand($foodArray, 1);
            return $foodArray[$offerIndex];
        }


        elseif($category->getName() == 'Viajes'){
            $price = rand(60, 2000);
            $travelingArray = array(
                array(
                    "title" => 'Disney Santa Fe @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Incluye: 4 noches + 5 días de entradas. Fiel al ambiente típico de la Route 66, el hotel dispone de habitaciones renovadas tematizadas con la película Cars con cuarto de baño, teléfono, televisión con canales internacionales, radio, un ventilador de techo y caja fuerte.

                                Family Rooms disponibles para 6 personas.

                                Elige una habitación Rio Grande o Eldorado si quieres más tranquilidad o si prefieres alojarte más cerca de los servicios de hotel.',
                    "url" => 'http://www.disneylandparis.es/',
                    "image" => 'santafe.jpg',
                    "tag1" => 'Parque de atracciones',
                    "tag2" => 'Disney'

                ),

                array(
                    "title" => 'PortAventura 5 noches 2 adultos @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Acceso ilimitado a PortAventura Park
                                Reserva preferencial en los servicios a mesa del parque PortAventura desde el Guest Service
                                Servicio de recogida en las tiendas del parque. Envío al hotel de las compras realizadas en PortAventura Park
                                Wifi gratuito en todo el hotel
                                Aparcamiento gratuito (plazas limitadas, sujetas a disponibilidad)
                            ',
                    "url" => 'http://www.portaventura.es/',
                    "image" => 'portaventuralogo.jpg',
                    "tag1" => 'Parque de atracciones',
                    "tag2" => 'PortAventura'

                ),

                array(
                    "title" => 'Viaje Lanzarote @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Viaja a Lanzarote con Rumbo: combina un vuelo de tu elección, de linea regular o lowcost, con un hotel seleccionado entre los proveedores y cadenas hoteleras más importantes. Construye tu paquete de viaje en función de la tipología de servicio que estés buscando y disfruta de unas vacaciones a Lanzarote - Islas Canarias sin complicaciones!',
                    "url" => 'http://www.turismolanzarote.com/',
                    "image" => 'lanzarote.jpg',
                    "tag1" => 'Vacaciones',
                    "tag2" => 'Lanzarote'

                ),

                array(
                    "title" => 'Vuelos a NY desde Madrid @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Muy buena oferta, yo fui por el doble el año pasado. El precio incluye estancia en primera clase y unas croquetas para el camino.',
                    "url" => 'http://www.nycgo.com/es',
                    "image" => 'ny.jpg',
                    "tag1" => 'Vacaciones',
                    "tag2" => 'New York'

                ),

                array(
                    "title" => '7 días de vacaciones en Berlin @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Toda una ganga yo ya me he cogido mi viaje para hartarme a cerveza alemana!!!',
                    "url" => 'http://www.visitberlin.de/es',
                    "image" => 'berlin.jpg',
                    "tag1" => 'Vacaciones',
                    "tag2" => 'Berlin'

                ),

                array(
                    "title" => 'Crucero Maravilloso Mediterráneo @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Arrrr marineros!!! 7 noches a bordo del Costa Diadema
Salida desde Barcelona de Diciembre/15 a Marzo/16
Tasas de embarque incluidas
Reserva por 50€ por persona
Pensión Completa a bordo
',
                    "url" => 'http://www.costacruceros.es/B2C/E/Pages/Default.aspx',
                    "image" => 'mediterraneo.jpg',
                    "tag1" => 'Crucero',
                    "tag2" => 'Mediterráneo'

                ),

                array(
                    "title" => 'Selva negra alemana @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'El entorno natural de Suiza, combina suaves prados verdes y lagos color esmeralda, con poblaciones donde sus pintorescas casas parecen sacadas de un cuento infantil. ',
                    "url" => 'http://www.selvanegra.info/',
                    "image" => 'selva.jpg',
                    "tag1" => 'Vacaciones',
                    "tag2" => 'Alemania'

                ),

                array(
                    "title" => 'Tesoros de Rusia Imperial Verano 015 @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Rusia, inmenso país de marcados contrastes y riqueza cultural extraordinaria, ha sido durante gran parte de su historia casi un misterio para Occidente. Si San Petersburgo es la ventana de Europa, Moscú es el corazón de Rusia. ',
                    "url" => 'http://www.turismoderusia.es/',
                    "image" => 'russia.jpg',
                    "tag1" => 'Vacaciones',
                    "tag2" => 'Rusia'

                ),

                array(
                    "title" => 'Maravillas de Escocia @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Un verdadero punto de encuentro para el arte y la cultura, con una compleja historia impregnada de tradición. Romántica y controvertida, estas tierras de contraste y magia albergan algunos de los parajes más bellos y sobrecogedores de ',
                    "url" => 'http://www.visitscotland.com/es-es/',
                    "image" => 'scotland.jpg',
                    "tag1" => 'Vacaciones',
                    "tag2" => 'Escocia'

                ),

                array(
                    "title" => 'Circuito Islandia Tierra de hielo y fuego @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Islandia es una tierra joven, aún formándose. Los volcanes, los glaciares, las coladas de lava, los acantilados y las cascadas nos presentan toda una paleta de colores. ',
                    "url" => 'http://es.visiticeland.com/',
                    "image" => 'islandia.jpg',
                    "tag1" => 'Vacaciones',
                    "tag2" => 'Islandia'

                )
            );

            $offerIndex = array_rand($travelingArray, 1);
            return $travelingArray[$offerIndex];
        }


        elseif($category->getName() == 'Restaurantes'){
            $price = rand(5, 60);
            $restaurantsArray = array(
                array(
                    "title" => 'Menú Rebajado @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'- Un sándwich a elegir*

- Patatas fritas medianas, patatas Deluxe o ensalada.

- Una botella de 50 cl de agua Aquabona, un refresco de 40 cl (Coca-Cola, Coca-Cola Zero, Coca-Cola Light, Sprite, Lipton Ice Tea, Trina Limón), una cerveza Mahou de 30cl o una cerveza sin alcohol Mahou de 30cl',
                    "url" => 'http://www.mcdonalds.es/productos/nuestros-menus/mcmenu',
                    "image" => 'mcmenu.png',
                    "tag1" => 'Sándwich',
                    "tag2" => 'Comida basura'

                ),

                array(
                    "title" => 'Menú rebajado a la mitad  @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Yo creo que con estos precios no merece ni comer en casa, eso si me voy a poner como un animal.',
                    "url" => 'http://lmgtfy.com/?q=comida+grasienta',
                    "image" => 'greasyfood.jpg',
                    "tag1" => 'Grasas',
                    "tag2" => 'Comida basura'

                ),

                array(
                    "title" => '2x1 en pizza!!! @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Dos por uno perfecto para el día de partido con los amigos, aprovechad la oferta!!!',
                    "url" => 'http://lmgtfy.com/?q=pizza',
                    "image" => 'dominos.jpeg',
                    "tag1" => 'Pizza',
                    "tag2" => 'Comida basura'

                ),

                array(
                    "title" => 'Tremendo menú @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Nuestra carta se completa con una sabrosísima ternera asturiana, vinos de gran solera
y sidra autóctona de las mejores marcas.

El más atento servicio y un ambiente diseñado para conseguir el mayor bienestar tienen
como fin la satisfacción absoluta de nuestros clientes',
                    "url" => 'http://lmgtfy.com/?q=ternera+asturiana',
                    "image" => 'terneraasturiana.jpg',
                    "tag1" => 'Contundente',
                    "tag2" => 'Menú'

                ),

                array(
                    "title" => 'Menú cantábrico @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Brocheta de pulpo de pedreru y langostinos con su bilbaina
Calamares frescos fritos
Mousse de perdiz
Picadillo de jabalí con su huevo frito y patatas paja
Arroz caldoso con almejas y pixín /rape del Cantábrico
Carrilleras de bellota al vino tinto',
                    "url" => 'http://lmgtfy.com/?q=menu+del+norte',
                    "image" => 'carne.jpg',
                    "tag1" => 'Contundente',
                    "tag2" => 'Menú'

                ),

                array(
                    "title" => 'Menú premium @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Brocheta de pulpo de pedreru y langostinos con su bilbaina
Calamares frescos fritos
Mousse de perdiz
Arroz con bogavante',
                    "url" => 'http://lmgtfy.com/?q=menu+premium',
                    "image" => 'marisco.jpg',
                    "tag1" => 'Premium',
                    "tag2" => 'Menú'

                ),

                array(
                    "title" => 'Menú sidrero @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Escalope de ternera, tortilla de setas y patatas fritas. Entrecot de ternera con patatas fritas y pimientos. Filete de merluza a la romana, espárragos con mayonesa, croquetas y ensalada . Postre, pan y vino  ',
                    "url" => 'http://lmgtfy.com/?q=menu+sidreria',
                    "image" => 'sidreria.jpg',
                    "tag1" => 'Chigre',
                    "tag2" => 'Menú'

                ),

                array(
                    "title" => 'Menú tradicional @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'En nuestra amplia carta podrás encontrar una gran variedad de platos tradicionales: fabadas y potes asturianos, carnes y pescados a la parrilla, guisos, platos de pueblo y nuestras parrilladas y tablones para compartir.',
                    "url" => 'http://lmgtfy.com/?q=fabada',
                    "image" => 'tierraastur.jpeg',
                    "tag1" => 'Contundente',
                    "tag2" => 'Menú'

                ),

                array(
                    "title" => 'Promoción cocina italiana a buen precio @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'La mejor cocina italiana al mejor precio, spaghetti, pizza, ensaladas italianas, limoncello, etc, etc',
                    "url" => 'http://lmgtfy.com/?q=spaghetti',
                    "image" => 'spaghetti.jpg',
                    "tag1" => 'Cocina extranjera',
                    "tag2" => 'Italia'

                ),

                array(
                    "title" => 'Rollitos de primaver para llevar @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Esta promoción la podéis encontrar en todos los establecimientos de ',
                    "url" => 'http://lmgtfy.com/?q=rollitos+de+primavera+' . $shopName . '+aprovechadla!!!',
                    "image" => 'rollitos.jpg',
                    "tag1" => 'Cocina extranjera',
                    "tag2" => 'China'

                )
            );


            $offerIndex = array_rand($restaurantsArray, 1);
            return $restaurantsArray[$offerIndex];
        }


        else{
            $price = rand(5, 1000);
            $othersArray = array(
                array(
                    "title" => 'Cachabas rebajadas @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Hay muchos gamberros en tu barrio? Esto impedirá que se acerquen sin al menos pensárselo dos veces a que esperas!!! madera de la buena!!! y barata!!!',
                    "url" => 'http://lmgtfy.com/?q=cachaba',
                    "image" => 'cachaba.jpg',
                    "tag1" => 'Defensa personal',
                    "tag2" => 'Arma'

                ),

                array(
                    "title" => 'Neumático Blizzak LM001 @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'El nuevo neumático Blizzak LM001, que combina las últimas innovaciones en cuanto a tecnologías aplicadas a la banda de rodadura y los materiales, le ofrece el rendimiento que necesita para hacer frente a las condiciones meteorológicas actuales, cada vez más impredecibles. El neumático Blizzak LM001 está completamente certificado por TÜV, lo que le garantiza el nivel de seguridad que busca en sus neumáticos y le da la confianza necesaria para superar todos los obstáculos que el invierno ponga en su camino.',
                    "url" => 'http://www.bridgestone.es/',
                    "image" => 'neumatico.jpg',
                    "tag1" => 'Neumático',
                    "tag2" => 'Coches'

                ),

                array(
                    "title" => 'BERETTA 682 GOLD E TRAP @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Si tu vecino te molesta no hay nada mejor que una de estas y ademas ahora estan a buen precio, las regalan!!!

Después de años de victorias en los campos de tiro de medio mundo, las superpuestas de competición Beretta 682 Gold E han sido objeto de un moderno restyling y de numerosos retoques proyectuales que han hecho todavía mas cautivadora su línea y mas competitivas sus prestaciones.
La báscula de las nuevas 682 Gold E, estilizada y elegante pero extremadamente robusta, ofrece un exclusivo acabado “dualcolor” que, gracias al marcado contraste entre las zonas brillantes, las opacas y los detalles en “oro” proporciona un extraordinario efecto estético al que también contribuye un moderno diseño gráfico de elipses.
El tratamiento especial a base de níquel garantiza una elevada protección a los agenes atmosféricos.',
                    "url" => 'http://lmgtfy.com/?q=BERETTA+682+GOLD+E+TRAP',
                    "image" => 'escopeta.jpg',
                    "tag1" => 'Defensa personal',
                    "tag2" => 'Arma'

                ),

                array(
                    "title" => 'Mapa de carreteras @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'No te va la tecnología? No sabes que es eso de un smartphone? GPQue? Tu solución esta en este tema',
                    "url" => 'http://lmgtfy.com/?q=mapa+carreteras+papel',
                    "image" => 'mapadecarreteras.jpg',
                    "tag1" => 'Mapa',
                    "tag2" => 'Tesoro'

                ),

                array(
                    "title" => 'Bola de cristal Rappel @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Quieres saber tu futuro? El de tu prima Pepi? Pues aquí llega la nueva y patentada Bola de cristal Rappel (no incluye tangas dorados), ahora de oferta',
                    "url" => 'http://lmgtfy.com/?q=bola+de+cristal',
                    "image" => 'bolacristal.jpg',
                    "tag1" => 'Futuro',
                    "tag2" => 'Rappel'

                ),

                array(
                    "title" => '2 x 1 en aspirinas @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Tu PFC se eterniza? Te duele la cabeza? No puedes dormir por esos dolores? Cómprate un carro de aspirinas ahora en 2x1',
                    "url" => 'http://lmgtfy.com/?q=aspirinas',
                    "image" => 'aspirinas.jpg',
                    "tag1" => 'Aspirinas',
                    "tag2" => 'Bayer'

                ),

                array(
                    "title" => 'Containers habitables @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Problemas para independizarte y encontrar una vivienda digna a un precio razonable? Estos bonitos containers harán posible lo imposible, por limpieza de stock ahora en oferta!!!',
                    "url" => 'http://lmgtfy.com/?q=container',
                    "image" => 'container.jpg',
                    "tag1" => 'Casa',
                    "tag2" => 'Que me estas container'

                ),

                array(
                    "title" => 'Tarjeta regalo un 30% más barata @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Cansado de no saber que comprarle a tu novia/padre/madre/hermana? Compra una de estas tarjetas regalo y que se busquen la vida!!!!',
                    "url" => 'http://lmgtfy.com/?q=tarjeta+regalo',
                    "image" => 'tarjetaregalo.jpg',
                    "tag1" => 'Tarjeta',
                    "tag2" => 'Regalo'

                ),

                array(
                    "title" => 'Se venden estrellas reales @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'No sabes en que quemar tu dinero? Ya puedes apuntarte otra manera absurda para gastártelo, compra una estrella a cambio de tu dinero recibirás un papel que ha costado 5 céntimos ',
                    "url" => 'http://lmgtfy.com/?q=comprar+estrella',
                    "image" => 'estrella.jpg',
                    "tag1" => 'Timo',
                    "tag2" => 'Inútil'

                ),

                array(
                    "title" => 'Chicles usados de Belén Esteban @ ' . $shopName . ' - ' . $price . ' €',
                    "message" =>'Yo ya me he comprado 100, me voy a hacer de oro en ebay, aprovechad!!!!',
                    "url" => 'http://lmgtfy.com/?q=belen+esteban',
                    "image" => 'belenesteban.jpg',
                    "tag1" => 'Timo',
                    "tag2" => 'Inútil'

                )


            );

            $offerIndex = array_rand($othersArray, 1);
            return $othersArray[$offerIndex];
        }

    }

/*
 * Función encargada de rellenar el array de datos
 *
 * @param string $shopName nombre de la tienda
 * @param category $category categoria de la oferta
 *
 **/
    private function fillDataArray($category, $shopName){
        $dataArray = array( "shop" => "",  "title" => "", "message" => "", "url" => "", "image" => "", "tag1" => "", "tag2" => "");

        $em = $this->container->get('doctrine.orm.entity_manager');

        $shop = $em->getRepository('TopicBundle:Shop')->findShopByName($shopName);
        $leftData = $this->getLeftData($category, $shopName);
        $dataArray['shop'] = $shop;
        $dataArray['title'] = $leftData['title'];
        $dataArray['message'] = $leftData['message'];
        $dataArray['url'] = $leftData['url'];
        $dataArray['image'] = $leftData['image'];
        $dataArray['tag1'] = $leftData['tag1'];
        $dataArray['tag2'] = $leftData['tag2'];
        return $dataArray;
    }
}

