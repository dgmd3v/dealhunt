<?php

namespace Deal\TopicBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Deal\TopicBundle\Entity\Topic;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/*
 * Fixtures/Datos de prueba para la entidad topics/temas
 * En este fixture creo los topics (temas), les doy un titulo, categoría, etc
 **/
class Topics extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    // Contenedor con el cual llamare a los servicios que necesite
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    // Orden en el que se carga este fixture
    public function getOrder()
    {
        return 230;
    }

    public function load(ObjectManager $manager)
    {
        // Obtengo las categorías de la base de datos
        $categories = $manager->getRepository('TopicBundle:Category')->findAll();

        // Obtengo los foros de la base de datos y los introduzco en un array
        $mod = $manager->getRepository('ForumBundle:Forum')->findOneBySlug('moderacion');
        $admin = $manager->getRepository('ForumBundle:Forum')->findOneBySlug('administracion');
        $forums = array($mod, $admin);

        // Obtengo los usuarios de la base de datos
        $users = $manager->getRepository('UserBundle:User')->findAll();
        $countUsers = count($users);

        // Obtengo los foros de la base de datos
        $posts = $manager->getRepository('PostBundle:Post')->findAll();

        // Creo 240 temas
        for ($i=1000; $i < 1400; $i++){

            // Creo el tema
            $topic = new Topic();

            // Escojo una categoría al azar
            $category = $categories[array_rand($categories, 1)];

            // Escojo un foro al azar
            $forum = $forums[array_rand($forums, 1)];

            // Foro al que pertenece el tema
            $topic->setForum($forum);

            // Genero información para customizar la el tema
            $customData = $this->getCustomData($forum->getName());

            // Título del tema
            $topic->setTitle($customData['title']);

            // Categoría al que pertenece el tema
            $topic->setCategory($category);

            //Cambio el contenido del primer mensaje
            $firtsPost = $posts[(($i)*$countUsers)];
            $firtsPost->setMessage($customData['message']);

            // Id del primer post
            $topic->setFirstPost($posts[(($i)*$countUsers)]);

            // Id del último post
            $topic->setLastPost($posts[(($i+1)*$countUsers)-1]);

            // Hago la entidad persistente
            $manager->persist($firtsPost);
            $manager->persist($topic);


        } // Fin for

        // Guardo los datos marcados como persistentes
        $manager->flush();
    }


    /*
     * Genero un titulo, el texto del primer mensaje, etc
     *
     * @param string $forumName nombre del foro a la que pertenece el topic/tema
     *
     **/
    private function getCustomData($forumName)
    {
        $customData = array('title' => '', 'message' => '');

        if($forumName == 'Moderación'){
            $titlesArray = array(   'Usuarios insultando a otros',
                                    'Spam descarado de un usuario',
                                    'Usuario escribiendo todos sus posts en mayúsculas',
                                    'Usuario reportado por varios usuarios',
                                    'Baneamos a este usuario?',
                                    'Un usuario me pide que le quitemos en ban que le hemos puesto durante 10 años',
                                    'Usuario troll',
                                    'Debemos cerrar este hilo?',
                                    'Quejas por ocultar mensajes'
            );

            $randomTitle = array_rand($titlesArray, 1);

            $customData['title'] = $titlesArray[$randomTitle];

            $message = "Aquí debería haber un texto explicando de que va el hilo, si se quiere banear a un usuario, el motivo.  Si hay problemas en un hilo explicarlo y poner un enlace, todo lo que tenga que ver con usuarios no cumpliendo las normas ...";
            $customData['message'] = $message;

        }

        else{
            $titlesArray = array(   'Deberíamos crear un foro exclusivo de deportes?',
                                    'Creo que deberíamos borrar la cache, todos los problemas tontos son por la cache',
                                    'Incluimos mas BBCodes? Sugerencias?',
                                    'En el futuro pondremos mas servidores de vídeo disponibles, cuales?',
                                    'Este hosting no va muy bien, debemos cambiar a otro?',
                                    'Cambiamos Apache por nginx?',
                                    'Activamos la cache APC también en el modo dev?',
            );

            $randomTitle = array_rand($titlesArray, 1);

            $customData['title'] = $titlesArray[$randomTitle];

            $message = "Aquí debería haber un texto explicando de que va el hilo, si hay problemas con el servidor, posibles bugs, ampliaciones que se pueden hacer en el foro, en general problemas de funcionamiento de la aplicación web";
            $customData['message'] = $message;
        }

        return  $customData;
    }

}
