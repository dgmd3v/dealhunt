<?php

namespace Deal\TopicBundle\Entity;

use \FPN\TagBundle\Entity\Tag as BaseTag;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Deal\TopicBundle\Entity\Tag
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Deal\TopicBundle\Entity\TagRepository")
 * @UniqueEntity( fields="slug", message="Ya existe una etiqueta con el mismo slug.")
 * @UniqueEntity( fields="name", message="Ya existe una etiqueta con el mismo nombre.")
 */
class Tag extends BaseTag
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /*
     * Nombre de la etiqueta
     * */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity="Tagging", mappedBy="tag", fetch="EAGER")
     **/
    protected $tagging;

    /**
     * Sets the tag's name
     *
     * @param string $name Name to set
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns tag's name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Metodo magico que retorna el nombre de la etiqueta (util para comparar etiquetas)
     *
     * @return string
     */
    public function __toString() {
        return $this->name;
    }
}