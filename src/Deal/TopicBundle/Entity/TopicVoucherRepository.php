<?php

namespace Deal\TopicBundle\Entity;

use Doctrine\ORM\EntityRepository;

class TopicVoucherRepository extends EntityRepository
{
    /*
     * Devuelve todos los topics/temas de tipo voucher
     *
     * @param integer $forum id del foro en el cual vamos a buscar los temas
     **/
    public function findAllWithFirstPost()
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  '  SELECT t, fp
                     FROM TopicBundle:TopicVoucher t
                     JOIN t.firstPost fp');

        $query = $em->createQuery($dql);

        // Retorno el resultado de la consulta
        return $query->getResult();
    }

    /*
     * Devuelve el número de topics/temas más votados de un foro
     *
     * @param integer $forum id del foro del cual queremos buscar los temas más votados
     * @param integer $forum $minVotesToBePop mínimo de votos positivos que debe tener un topic (tema) para ser popular
     **/
    public function findNumberOfMostVotedTopics($forum, $minVotesToBePop)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT COUNT(t)
                      FROM TopicBundle:TopicVoucher t
                      JOIN t.forum f
                     WHERE t.votes >= :minVotes
                       AND f.id = :forum');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'forum'     => $forum,
            'minVotes'  => $minVotesToBePop,
        ));

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }


    /*
     * Devuelve el número de topics/temas más votados de un foro y una categoría concreta
     *
     * @param integer $forum id del foro del cual queremos buscar los temas más votados
     * @param string $category slug de la categoría de la cual queremos buscar los temas recientemente comentados
     * @param integer  $minVotesToBePop mínimo de votos positivos que debe tener un topic (tema) para ser popular
     **/
    public function findNumberOfMostVotedTopicsByCategory($forum, $category, $minVotesToBePop)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT COUNT(t)
                      FROM TopicBundle:TopicVoucher t
                      JOIN t.forum f
                      JOIN t.category cat
                     WHERE f.id = :forum
                       AND cat.id = :category
                       AND t.votes >= :minVotes');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'forum'     => $forum,
            'category'  => $category,
            'minVotes'  => $minVotesToBePop
        ));

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }

    /*
     * Devuelve el número de topics/temas de un foro
     *
     * @param integer $forum id del foro del cual queremos buscar los temas
     **/
    public function findNumberOfTopicsByForumId($forum)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT COUNT(t)
                      FROM TopicBundle:TopicVoucher t
                      JOIN t.forum f
                     WHERE f.id = :forum');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'forum' => $forum
        ));

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }


    /*
     * Devuelve los topics/temas de un foro que superen un umbral de votos y por la fecha en la que se hizo popular,
     * además de eso devuelve datos como el creador del tema, el foro al que pertenece,
     * el Último usuario en postear, etc, etc
     *
     * @param integer $forum id del foro del cual queremos buscar los temas más votados
     * @param integer $minVotesToBePop mínimo de votos positivos que debe tener un topic (tema) para ser popular
     **/
    public function findMostVotedTopics($forum, $minVotesToBePop)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();     
        
        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT t, fP, lP, creator, lastPoster, f
                      FROM TopicBundle:TopicVoucher t
                      JOIN t.firstPost fP
                      JOIN t.lastPost lP
                      JOIN fP.poster creator
                      JOIN lP.poster lastPoster
                      JOIN t.forum f
                     WHERE t.votes >= :minVotes
                       AND f.id = :forum
                  ORDER BY t.dateMadePopular DESC');        

        $query = $em->createQuery($dql);
        
        // Parámetros de la consulta
        $query->setParameters(array(
            'forum'     => $forum,
            'minVotes'  => $minVotesToBePop
        ));

        // Seteo el modo de hidratación
        $query->setHydrationMode(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        // Retorno la consulta
        return $query;
    }

    /*
     * Devuelve los topics/temas de un foro ordenados por la fecha de creación (de más a menos reciente),
     * además de eso devuelve datos como el creador del tema, el foro al que pertenece, el Último usuario en postear,
     * etc, etc
     *
     * @param integer $forum id del foro del cual queremos buscar los temas más nuevos
    **/
    public function findNewestTopics($forum)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();     
        
        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT t, fP, lP, creator, lastPoster, f
                      FROM TopicBundle:TopicVoucher t
                      JOIN t.firstPost fP
                      JOIN t.lastPost lP
                      JOIN fP.poster creator
                      JOIN lP.poster lastPoster
                      JOIN t.forum f
                     WHERE f.id = :forum
                  ORDER BY fP.postingDate DESC');        

        $query = $em->createQuery($dql);
        
        // Parámetros de la consulta
        $query->setParameters(array(
                'forum' => $forum
        ));

        // Seteo el modo de hidratación
        $query->setHydrationMode(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        // Retorno la consulta
        return $query;
    }

    /*
     * Devuelve los topics/temas de un foro ordenados por la fecha del último comentario realizado en el tema
     * (de más a menos reciente), además de eso devuelve datos como el creador del tema, el foro al que pertenece,
     * el último usuario en postear, etc, etc
     *
     * @param integer $forum id del foro del cual queremos buscar los temas recientemente comentados
    **/
    public function findRecentlyCommentedTopics($forum)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();     
        
        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT t, fP, lP, creator, lastPoster, f
                      FROM TopicBundle:TopicVoucher t
                      JOIN t.firstPost fP
                      JOIN t.lastPost lP
                      JOIN fP.poster creator
                      JOIN lP.poster lastPoster
                      JOIN t.forum f
                     WHERE f.id = :forum
                  ORDER BY lP.postingDate DESC');        

        $query = $em->createQuery($dql);
        
        // Parámetros de la consulta
        $query->setParameters(array(
                'forum' => $forum
        ));

        // Seteo el modo de hidratación
        $query->setHydrationMode(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        // Retorno la consulta
        return $query;
    }

    /*
     * Devuelve los topics/temas de un foro y una categoría concreta ordenados por la fecha del último comentario
     * realizado en el tema (de más a menos reciente),además de eso devuelve datos como el creador del tema,
     * el foro al que pertenece, el Último usuario en postear, etc, etc
     *
     * @param integer $forum id del foro del cual queremos buscar los temas recientemente comentados
     * @param integer $category id de la categoría de la cual queremos buscar los temas recientemente comentados
     **/
    public function findRecentlyCommentedTopicsByCategory($forum, $category)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();     
        
        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT t, fP, lP, creator, lastPoster, f
                      FROM TopicBundle:TopicVoucher t
                      JOIN t.firstPost fP
                      JOIN t.lastPost lP
                      JOIN fP.poster creator
                      JOIN lP.poster lastPoster
                      JOIN t.forum f
                      JOIN t.category cat
                     WHERE f.id = :forum
                       AND cat.id = :category
                  ORDER BY lP.postingDate DESC');        

        $query = $em->createQuery($dql);
        
        // Parámetros de la consulta
        $query->setParameters(array(
                'forum'     => $forum,
                'category'  => $category
        ));

        // Seteo el modo de hidratación
        $query->setHydrationMode(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        // Retorno la consulta
        return $query;
    }

    /*
     * Devuelve el número de topics/temas de un foro
     *
     * @param integer $forum id del foro del cual queremos buscar los temas más votados
     * @param integer $category id de la categoría de la cual queremos buscar los temas
     **/
    public function findNumberOfTopicsByCategory($forum, $category)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT COUNT(t)
                      FROM TopicBundle:TopicVoucher t
                      JOIN t.forum f
                      JOIN t.category cat
                     WHERE f.id = :forum
                       AND cat.id = :category');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'forum'     => $forum,
            'category'  => $category
        ));

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }

    /*
     * Devuelve la consulta que obtiene los topics/temas de un foro y una categoría concreta que superen un umbral
     * de votos, ordenados por fecha de popularidad, además de eso devuelve datos como
     * el creador del tema, el foro al que pertenece, el Último usuario en postear, etc, etc
     *
     * @param integer $forum id del foro del cual queremos buscar los temas más votados
     * @param integer $category id de la categoría de la cual queremos buscar los temas más votados
     * @param integer  $minVotesToBePop mínimo de votos positivos que debe tener un topic (tema) para ser popular
     **/
    public function findMostVotedTopicsByCategory($forum, $category, $minVotesToBePop)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();     
        
        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT t, fP, lP, creator, lastPoster, f, cat
                      FROM TopicBundle:TopicVoucher t
                      JOIN t.firstPost fP
                      JOIN t.lastPost lP
                      JOIN fP.poster creator
                      JOIN lP.poster lastPoster
                      JOIN t.forum f
                      JOIN t.category cat
                     WHERE f.id = :forum
                       AND cat.id = :category
                       AND t.votes >= :minVotes
                  ORDER BY t.dateMadePopular DESC');        

        $query = $em->createQuery($dql);
        
        // Parámetros de la consulta
        $query->setParameters(array(
                'forum'     => $forum,
                'category'  => $category,
                'minVotes'  => $minVotesToBePop,
        ));

        // Seteo el modo de hidratación
        $query->setHydrationMode(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        // Retorno la consulta
        return $query;
    }

    /*
     * Devuelve los topics/temas de un foro y una categoría concreta ordenados por la fecha de creación
     * (de más a menos reciente), además de eso devuelve datos como el creador del tema, el foro al que pertenece,
     * el Último usuario en postear, etc, etc
     *
     * @param integer $forum id del foro del cual queremos buscar los temas más recientes
     * @param integer $category id de la categoría de la cual queremos buscar los temas más recientes
    **/
    public function findNewestTopicsByCategory($forum, $category)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();     
        
        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT t, fP, lP, creator, lastPoster, f, cat
                      FROM TopicBundle:TopicVoucher t
                      JOIN t.firstPost fP
                      JOIN t.lastPost lP
                      JOIN fP.poster creator
                      JOIN lP.poster lastPoster
                      JOIN t.forum f
                      JOIN t.category cat
                     WHERE f.id = :forum
                       AND cat.id = :category
                  ORDER BY fP.postingDate DESC');        

        $query = $em->createQuery($dql);
        
        // Parámetros de la consulta
        $query->setParameters(array(
                'forum'     => $forum,
                'category'  => $category
        ));

        // Seteo el modo de hidratación
        $query->setHydrationMode(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        // Retorno la consulta
        return $query;
    }

    /*
     * Devuelve los 10 topics/temas más populares (con mayor número de votos) de un foro
     * ordenados por el número de votos (de más a menos votos), además devuelve el foro al que pertenecen los temas.
     * Todos ellos se ha vuelto populares en el último día/semana/mes, depende del periodo que queramos.
     *
     * @param integer $forum id del foro del cual queremos buscar los 10 populares
     * @param string $fromTime cadena de fecha/hora en la cual indico desde cuando empiezo a contar el día/semana/mes
     * @param integer $minVotesToBePop mínimo de votos que debe tener un tema para ser considerador popular
    **/
    public function findAllTopTopicsByTime($forum, $fromTime, $minVotesToBePop)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();     
        
        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (   'SELECT t, f
                      FROM TopicBundle:TopicVoucher t
                      JOIN t.forum f
                     WHERE f.id =:forum
                       AND t.votes >=:minVotesToBePop
                       AND t.dateMadePopular >= :min
                  ORDER BY t.votes DESC');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'forum'             => $forum,
            'minVotesToBePop'   => $minVotesToBePop,
            'min'               => new \DateTime($fromTime)
        ));

        // Limito los resultados
        $query->setMaxResults(10);

        // Seteo el modo de hidratación
        $query->setHydrationMode(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        // Retorno el resultado de la consulta
        return $query->getResult();
    }

    /*
     * Devuelve los 10 topics/temas más populares (con mayor número de votos) de un foro y una categoría concreta
     * ordenados por el número de votos (de más a menos votos), además devuelve el foro al que pertenecen los temas.
     * Todos ellos se ha vuelto populares en el último día/semana/mes, depende del periodo que queramos.
     *
     * @param string $forum slug del foro del cual queremos buscar los 10 populares
     * @param integer $category id la categoría de la cual queremos buscar los 10 populares
     * @param string $fromTime cadena de fecha/hora en la cual indico desde cuando empiezo a contar el día/semana/mes
     * @param integer $minVotesToBePop mínimo de votos que debe tener un tema para ser considerador popular
     **/
    public function findTopTopicsByTime($forum, $category, $fromTime, $minVotesToBePop)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT t, f
                     FROM TopicBundle:TopicVoucher t
                      JOIN t.forum f
                      JOIN t.category cat
                     WHERE f.id = :forum
                       AND cat.id = :category
                       AND t.votes >= :minVotesToBePop
                       AND t.dateMadePopular >= :min
                  ORDER BY t.votes DESC');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'forum'             => $forum,
            'category'          => $category,
            'minVotesToBePop'   => $minVotesToBePop,
            'min'               => new \DateTime($fromTime)
        ));

        // Limito los resultados
        $query->setMaxResults(10);

        // Seteo el modo de hidratación
        $query->setHydrationMode(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        // Retorno el resultado de la consulta
        return $query->getResult();
    }

    /*
     * Devuelve el topic/tema solicitado por su id
     *
     * @param integer $topicId id del topic/tema del cual queremos información
     *
     **/
    public function findTopicById($topicId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT t, f, cat, shop, fp, creator
                     FROM TopicBundle:TopicVoucher t
                      JOIN t.forum f
                 LEFT JOIN t.category cat
                 LEFT JOIN t.shop shop
                      JOIN t.firstPost fp
                      JOIN fp.poster creator
                     WHERE t.id = :topicId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'topicId' => $topicId
        ));

        // Limito los resultados
        $query->setMaxResults(1);


        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }

    /*
     * Devuelve el topic/tema solicitado por su id y además el perfil del usuario del primer mensaje
     *
     * @param integer $topicId id del topic/tema del cual queremos información
     *
     **/
    public function findTopicByIdAndFPProfile($topicId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT t, f, cat, shop, fp, creator, up
                     FROM TopicBundle:TopicVoucher t
                      JOIN t.forum f
                 LEFT JOIN t.category cat
                 LEFT JOIN t.shop shop
                      JOIN t.firstPost fp
                      JOIN fp.poster creator
                      JOIN creator.userProfile up
                     WHERE t.id = :topicId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'topicId' => $topicId
        ));

        // Limito los resultados
        $query->setMaxResults(1);


        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }

    /*
     * Devuelve el número de topics/temas que están en un foro y en un array de ids dado
     *
     * @param array $ids ids de los topics/temas de los cuales queremos información
     * @param integer $forum id del foro del cual queremos buscar los temas
     **/
    public function findNumberOfTopicsInArray($ids, $forum)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT count(t)
                      FROM TopicBundle:TopicVoucher t
                      JOIN t.firstPost fP
                      JOIN t.lastPost lP
                      JOIN fP.poster creator
                      JOIN lP.poster lastPoster
                      JOIN t.forum f
                     WHERE f.id = :forum
                       AND t.id IN (:ids)
                  ORDER BY fP.postingDate DESC');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'ids'   => $ids,
            'forum' => $forum,
        ));

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }

    /*
     * Devuelve los topics/temas solicitados en el array de ids que pertenecen a un foro dado
     *
     * @param array $ids ids de los topics/temas de los cuales queremos información
     * @param integer $forum id del foro del cual queremos buscar los temas
     *
     **/
    public function findTopicsByIdsAndForum($ids, $forum)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT t, fP, lP, creator, lastPoster, f, ar
                      FROM TopicBundle:TopicVoucher t
                      JOIN t.firstPost fP
                      JOIN t.lastPost lP
                      JOIN fP.poster creator
                      JOIN lP.poster lastPoster
                      JOIN t.forum f
                      JOIN f.accessRole ar
                     WHERE f.id = :forum
                       AND t.id IN (:ids)
                  ORDER BY fP.postingDate DESC');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'ids'   => $ids,
            'forum' => $forum,
        ));

        // Retorno la consulta
        return $query;
    }

    /*
     * Devuelve el número de topics/temas que pertenecen a una tienda dada
     *
     * @param integer $shop id de la tienda de la cual queremos buscar los temas
     * @param integer $forum id del foro en el cual vamos a buscar los temas
     **/
    public function findNumberOfTopicsByShop($shop, $forum)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT COUNT(t)
                      FROM TopicBundle:TopicVoucher t
                      JOIN t.forum f
                      JOIN t.shop s
                     WHERE f.id = :forumId
                       AND s.id = :shopId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'forumId'   => $forum,
            'shopId'    => $shop
        ));

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }

    /*
     * Devuelve los topics/temas de un foro concreto que tengan en común la tienda
     *
     * @param integer $shop id de la tienda de la cual queremos buscar los temas
     * @param integer $forum id del foro en el cual vamos a buscar los temas
     **/
    public function findTopicsByShop($shop, $forum)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT t, fP, lP, creator, lastPoster, f, s
                     FROM TopicBundle:TopicVoucher t
                      JOIN t.shop s
                      JOIN t.firstPost fP
                      JOIN t.lastPost lP
                      JOIN fP.poster creator
                      JOIN lP.poster lastPoster
                      JOIN t.forum f
                     WHERE f.id = :forumId
                       AND s.id = :shopId
                  ORDER BY fP.postingDate DESC');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'forumId'   => $forum,
            'shopId'    => $shop,
        ));

        // Seteo el modo de hidratación
        $query->setHydrationMode(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        // Retorno la consulta
        return $query;
    }

    /*
     * Devuelve los topics/temas de un foro concreto
     *
     * @param integer $forum id del foro en el cual vamos a buscar los temas
     **/
    public function findAllTopicsByForum($forum)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT t
                     FROM TopicBundle:TopicVoucher t
                      JOIN t.forum f
                     WHERE f.id = :forumId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'forumId' => $forum,
        ));

        // Retorno el resultado de la consulta
        return $query->getResult();
    }

    /*
     * Devuelve los topics/temas (de todos los foros) que tengan en común la tienda
     *
     * @param integer $shop id de la tienda de la cual queremos buscar los temas
     **/
    public function findAllTopicsByShop($shop)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT t
                      FROM TopicBundle:TopicVoucher t
                      JOIN t.shop s
                     WHERE s.id = :shop');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'shop' => $shop
        ));

        // Retorno el resultado de la consulta
        return $query->getResult();
    }

    /*
     * Devuelve los topics/temas (de todos los foros) que tengan en común que su tienda es NULL
     *
     **/
    public function findAllTopicsWithNullShop()
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT t
                      FROM TopicBundle:TopicVoucher t
                     WHERE t.shop IS NULL');

        $query = $em->createQuery($dql);

        // Retorno el resultado de la consulta
        return $query->getResult();
    }

}
