<?php

namespace Deal\TopicBundle\Entity;

use Doctrine\ORM\EntityRepository;

class TagRepository extends EntityRepository
{
    /*
     * Devuelve el numero de etiquetas que ya tienen el slug dado
     *
     * @param string $slug slug a buscar
    **/
    public function findNumberOfRepeatedSlug($slug)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT COUNT(t)
                      FROM TopicBundle:Tag t
                     WHERE t.slug = :slug
                        OR t.slug LIKE :extraSlug');

        $query = $em->createQuery($dql);

        $extraSlug = $slug . '\_%';

        // Parámetros de la consulta
        $query->setParameters(array(
            'slug'      => $slug,
            'extraSlug' => $extraSlug
        ));

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }

    /*
     * Devuelve la etiqueta que tiene el slug solicitado
     *
     * @param string $tagSlug slug de la etiqueta a buscar
     **/
    public function findTagBySlug($tagSlug)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT tag
                      FROM TopicBundle:Tag tag
                     WHERE tag.slug = :tagSlug');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'tagSlug' => $tagSlug,
        ));

        // Limito los resultados
        $query->setMaxResults(1);

        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }


    /*
     * Devuelve las etiquetas asociadas a un tema
     *
     * @param integer $topicId id del tema del cual queremos obtener las etiquetas
     **/
    public function findTopicTags($topicId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT tag, tagging
                      FROM TopicBundle:Tag tag
                      JOIN tag.tagging tagging
                     WHERE tagging.resourceId = :topicId
                  ORDER BY tag.updatedAt ASC');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'topicId' => $topicId,
        ));

        // Retorno el resultado de la consulta
        return $query->getResult();
    }

    /*
     * Devuelve los id de los temas (de un tipo concreto) asociados a una etiqueta
     *
     * @param string $taggableType tipo de recurso con tags
     * @param integer $tagSlug id de la etiqueta
     **/
    public function findTopicIdsByTypeAndTag($taggableType, $tag)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT tagging.resourceId
                      FROM TopicBundle:Tagging tagging
                      JOIN tagging.tag tag
                     WHERE tagging.resourceType = :taggableType
                       AND tag.id = :tag');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'taggableType' => $taggableType,
            'tag' => $tag,
        ));

        $queryResult = $query->getArrayResult();

        $idsArray = null;

        // Retorno el array de ids
        foreach ($queryResult as $index) {
            $idsArray[] = $index['resourceId'];
        }

        return $idsArray;
    }

    /*
     * Devuelve los id de los temas asociados a una etiqueta
     *
     * @param string $tagSlug slug de la etiqueta
     **/
    public function findTopicIdsByTag($tagSlug)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT tagging.resourceId
                      FROM TopicBundle:Tagging tagging
                      JOIN tagging.tag tag
                     WHERE tag.slug = :tagSlug');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'tagSlug' => $tagSlug,
        ));

        $queryResult = $query->getArrayResult();

        $idsArray = null;

        // Retorno el array de ids
        foreach ($queryResult as $index) {
            $idsArray[] = $index['resourceId'];
        }

        return $idsArray;
    }

    /*
     * Devuelve la etiqueta que tiene el nombre solicitado
     *
     * @param string $tagName nombre de la etiqueta a buscar
     **/
    public function findTagByName($tagName)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT tag
                      FROM TopicBundle:Tag tag
                     WHERE tag.name = :tagName');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'tagName' => $tagName,
        ));

        // Limito los resultados
        $query->setMaxResults(1);

        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }

}
