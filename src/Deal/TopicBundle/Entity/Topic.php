<?php

namespace Deal\TopicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Deal\ForumBundle\Util\Util;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Deal\TopicBundle\Validator\Constraints as MyAssert;
use FOS\ElasticaBundle\Annotation\Search as Search;

/**
 * Topic
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Deal\TopicBundle\Entity\TopicRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"Topic" = "Topic", "TopicWithTags" = "TopicWithTags", "Deal" = "TopicDeal", "Free" = "TopicFree", "Voucher" = "TopicVoucher"})
 * @Search(repositoryClass="Deal\TopicBundle\SearchRepository\TopicRepository")
 * @Serializer\ExclusionPolicy("all")
 */
class Topic
{
    // *************** COLUMNAS DE LA TABLA ***************  

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose
     */
    // ID del topic (tema)
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(max = 100, maxMessage = "El número máximo de caracteres que se pueden usar para el titulo es de {{ limit }}")
     * @Serializer\Expose
     */
    // Title (Título) del topic
    // Reglas de validación: No puede estar vacío, no puede sobrepasar los 90 caracteres
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     * @Serializer\Expose
     * @Assert\Length(max = 100, maxMessage = "El número máximo de caracteres que se pueden escribir para el slug son {{ limit }}")
     */
    // Slug del tema
    protected $slug;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_closed", type="boolean")
     */
    // Variable booleana en la que se indica si el tema (topic) está cerrado, por defecto a falso.
    protected $isClosed;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Deal\ForumBundle\Entity\Forum")
     * @ORM\JoinColumn(name="forum_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     * @Serializer\Expose
     */
    // Foro al que pertenece el tema. Relación Forum-Topic (1-N), un topic solo 
    // puede pertenecer a un Forum, mientras que un forum puede tener N topics
    protected $forum;

    /**
     * @var integer
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Deal\TopicBundle\Entity\Category")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="SET NULL")
     */
    // Categoría al que pertenece el tema (electrónica, juguetes, ropa, etc). 
    // Relación Category-Topic (1-N), un topic solo puede tener una categoría
    // mientras que una categoría puede ser asociada a N posts
    // Reglas de validación: No puede estar vacío.
    protected $category;

    /**
     * @var integer
     * @Assert\NotBlank()
     * @Assert\Valid
     * @ORM\OneToOne(targetEntity="Deal\PostBundle\Entity\Post", cascade={"persist"})
     * @ORM\JoinColumn(name="first_post_id", referencedColumnName="id", onDelete="SET NULL")
     */
    // Primer mensaje del tema. Relación Post-Topic (1-1), un topic solo puede tener un único post como primer post,
    // mientras que un Post puede ser 0 o 1 veces el primero de un Topic por eso propago la clave de Post a Topic
    // Reglas de validación: No puede estar vacío.
    protected $firstPost;

    /**
     * @var integer
     *
     * @ORM\OneToOne(targetEntity="Deal\PostBundle\Entity\Post")
     * @ORM\JoinColumn(name="last_post_id", referencedColumnName="id", onDelete="SET NULL")
     */
    // Último mensaje del tema. Relación Post-Topic (1-1), un topic solo puede tener un único post como último post,
    // mientras que un Post puede ser 0 o 1 veces el último de un Topic por eso propago la clave de Post  a Topic
    protected $lastPost;

    public function __construct() {
        $this->firstPost;
        $this->isClosed = false;
    }

    // *************** SETTERS Y GETTERS ***************     

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Topic
     */
    public function setTitle($title)
    {
        $this->title = $title;

        // A la vez que le doy nombre al topic le pongo un slug, solo si no lo tuviese ya
        if($this->slug == null){
          $this->slug = Util::getSlug($title);
        }


        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Topic
     */
    public function setSlug($slug)
    {
        if($slug == null){
            $this->slug = Util::getSlug($this->title);
        }

        else{
            $this->slug = Util::getSlug($slug);
        }

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Get isClosed
     *
     * @return boolean
     */
    public function getIsClosed()
    {
        return $this->isClosed;
    }

    /**
     * Set isClosed
     *
     * @param boolean $isClosed
     * @return Topic
     */
    public function setIsClosed($isClosed)
    {
        $this->isClosed = $isClosed;

        return $this;
    }

    /**
     * Set forum
     *
     * @param Deal\ForumBundle\Entity\Forum $forum
     * @return Topic
     */
    public function setForum(\Deal\ForumBundle\Entity\Forum $forum)
    {
        $this->forum = $forum;

        return $this;
    }

    /**
     * Get forum
     *
     * @return integer
     */
    public function getForum()
    {
        return $this->forum;
    }

    /**
     * Set category
     *
     * @param Deal\TopicBundle\Entity\Category $category
     * @return Topic
     */
    public function setCategory(\Deal\TopicBundle\Entity\Category $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Deal\TopicBundle\Entity\Category $category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set firstPost
     *
     * @param integer Deal\PostBundle\Entity\Post $firstPost
     * @return Topic
     */
    public function setFirstPost(\Deal\PostBundle\Entity\Post $firstPost)
    {
        $this->firstPost = $firstPost;

        return $this;
    }

    /**
     * Get firstPost
     *
     * @return integer
     */
    public function getFirstPost()
    {
        return $this->firstPost;
    }

    /**
     * Set lastPost
     *
     * @param integer Deal\PostBundle\Entity\Post $lastPost
     * @return Topic
     */
    public function setLastPost(\Deal\PostBundle\Entity\Post $lastPost)
    {
        $this->lastPost = $lastPost;

        return $this;
    }

    /**
     * Get lastPost
     *
     * @return integer
     */
    public function getLastPost()
    {
        return $this->lastPost;
    }

    // Método mágico para que PHP sepa como convertir la entidad en una cadena de texto
    public function __toString()
    {
        if(is_null($this->getTitle())) {
            return 'NULL';
        }

        return $this->getTitle();
    }

    /**
     * Get topicType
     *
     * @return String
     */
    public function getTopicType()
    {
        return 'Topic';
    }
}