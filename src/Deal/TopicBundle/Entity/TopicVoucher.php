<?php

namespace Deal\TopicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Deal\TopicBundle\Validator\Constraints as MyAssert;
use DoctrineExtensions\Taggable\Taggable;
use Doctrine\Common\Collections\ArrayCollection;

/*
 * Clase que representa los temas (topics) de tipo cupón descuento
 * */

/**
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Deal\TopicBundle\Entity\TopicVoucherRepository")
 * @Serializer\ExclusionPolicy("all")
 * @Vich\Uploadable
 * @MyAssert\isEndDateLater
 */
class TopicVoucher extends Topic implements Taggable
{
    // *************** COLUMNAS DE LA TABLA ***************

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_made_popular", type="datetime", nullable=true)
     */
    // Fecha en el que alcanzó los votos suficientes para ser popular. Si no es popular puede ser null
    protected $dateMadePopular;

    /**
     * @var integer
     *
     * @ORM\Column(name="votes", type="integer")
     * @Serializer\Expose
     */
    // Suma de los votos positivos y negativos recibidos
    protected $votes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_expired", type="boolean")
     */
    // Variable booleana en la que se guarda el estado de la oferta, expirada
    // o activa. Por defecto a falso
    protected $isExpired;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Url()
     * @ORM\Column(name="url", type="string", length=255)
     * @Assert\Length(max = 255, maxMessage = "El número máximo de caracteres que se pueden escribir para la url son {{ limit }}")
     */
    // Enlace a la web del descuento
    // Reglas de validación: No puede estar vacío. Debe tener el aspecto de una Url valida. Longitud Máxima
    protected $url;

    /**
     * @var File
     * @Assert\NotBlank(groups={"newTopic"}, message="No puedes publicar este tema sin incluir una imagen")
     * @Vich\UploadableField(mapping="topicImage", fileNameProperty="topicImageName")
     * @Assert\Image(maxSize = "3M",
     *               mimeTypes = {"image/jpeg", "image/png", "image/gif"},
     *               mimeTypesMessage= "El archivo no es una imagen válida. Solo puedes subir imágenes jpeg, png y gif")
     */
    // En este campo guardo el objeto UploadedFile después de haber enviado el formulario
    // Reglas de validación: No puede estar vacío. La imagen subida debe cumplir unos criterios
    protected $topicImageFile;

    /**
     * @var string
     *
     * @ORM\Column(name="topic_image_name", type="string", length=255, nullable=false)
     * @Assert\Length(max = 255, maxMessage = "El número máximo de caracteres que se pueden escribir para el nombre de la imagen son {{ limit }}")
     */
    // Nombre de la imagen del topic/tema
    protected $topicImageName;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     *
     * @var \DateTime $updatedAt
     */
    // Hora a la que se actualizo la imagen del topic/tema, necesario para subir correctamente la imagen
    protected $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_start", type="datetime", nullable=true)
     */
    // Fecha de inicio de la oferta/descuento, puede ser null si no existe fecha de inicio de la oferta
    protected $dateStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_end", type="datetime", nullable=true)
     */
    // Fecha de fin de la oferta/descuento, puede ser null si no existe fecha de fin de oferta
    protected $dateEnd;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="voucher", type="string", length=100, nullable=false)
     * @Assert\Length(max = 100, maxMessage = "El número máximo de caracteres que se pueden escribir para el cupón son {{ limit }}")
     */
    // Código descuento, si el post no es un descuento puede ser null
    // Reglas de validación: No puede estar vacío. Longitud Máxima
    protected $voucher;

    /**
     * @var string
     * @ORM\Column(name="voucher_discount", type="string", length=100, nullable=true)
     * @Assert\Length(max = 11, maxMessage = "El número máximo de caracteres que se pueden usar para descuento es de {{ limit }}")
     */
    // Descuento proporcionado por el código descuento, si el post no es un descuento puede ser null
    // Reglas de validación: No puede estar vacío, no puede sobrepasar los 11 caracteres
    protected $voucherDiscount;

    /**
     * @var string
     *
     * @ORM\Column(name="voucher_minimum", type="string", length=100, nullable=true)
     * @Assert\Length(max = 100, maxMessage = "El número máximo de caracteres que se pueden escribir para el gasto mínimo son {{ limit }}")
     */
    // Gasto mínimo para poder hacer uso del código descuento, si el post no es un descuento puede ser null
    protected $voucherMinimum;

    /**
     * @var integer
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Deal\TopicBundle\Entity\Shop")
     * @ORM\JoinColumn(name="shop_id", referencedColumnName="id", onDelete="SET NULL")
     */
    // Tienda que ofrece la oferta/código descuento. Relación Shop-Topic (1-N),
    // un topic (oferta/código descuento) solo puede pertenecer a una tienda,
    // mientras que una tienda, puede tener N topics (oferta/código descuento)
    // Reglas de validación: No puede estar vacío.
    protected $shop;

    protected $tags;

    public function __construct() {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dateMadePopular = null;
        $this->votes = 0;
        $this->isExpired = false;
        parent::__construct();
    }

    // *************** SETTERS Y GETTERS ***************

    /**
     * Get getTaggableType
     *
     * @return String
     */
    public function getTaggableType()
    {
        return 'TopicVoucherTag';
    }

    public function getTags()
    {
        $this->tags = $this->tags ?: new ArrayCollection();

        return $this->tags;
    }

    public function getTaggableId()
    {
        return $this->getId();
    }

    public function setTags($tags) {

        // Borro las etiquetas del tema
        $this->tags->clear();

        // Añado al tema las nuevas etiquetas introducidas si se ha introducido alguna
        if($tags != null){
            foreach($tags as $tag) {
                $this->tags->add($tag);
            }
        }

        return $this;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return TopicVoucher
     */
    public function setDateMadePopular($date)
    {
        $this->dateMadePopular = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDateMadePopular()
    {
        return $this->dateMadePopular;
    }

    /**
     * Set votes
     *
     * @param integer $votes
     * @return TopicVoucher
     */
    public function setVotes($votes)
    {
        $this->votes = $votes;

        return $this;
    }

    /**
     * Get votes
     *
     * @return integer
     */
    public function getVotes()
    {
        return $this->votes;
    }

    /**
     * Set isExpired
     *
     * @param boolean $isExpired
     * @return TopicVoucher
     */
    public function setIsExpired($isExpired)
    {
        $this->isExpired = $isExpired;

        return $this;
    }

    /**
     * Get isExpired
     *
     * @return boolean
     */
    public function getIsExpired()
    {
        return $this->isExpired;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return TopicVoucher
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setTopicImageFile(File $image)
    {
        $this->topicImageFile = $image;

        if ($image) {
            // Es necesario que al menos un campo cambie si se usa doctrine,
            // en otro caso el listener no será llamado y se perderá el fichero
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    // Devuelvo el objeto UploadedFile
    public function getTopicImageFile()
    {
        return $this->topicImageFile;
    }

    /**
     * @param string $topicImageName
     */
    // Función para "setear" el nombre de la imagen
    public function setTopicImageName($topicImageName)
    {
        $this->topicImageName = $topicImageName;
    }

    /**
     * @return string
     */
    // Función que devuelve el nombre de la imagen
    public function getTopicImageName()
    {
        return $this->topicImageName;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     * @return TopicVoucher
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     * @return TopicVoucher
     */
    public function setdateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getdateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set voucher
     *
     * @param string $voucher
     * @return TopicVoucher
     */
    public function setVoucher($voucher)
    {
        $this->voucher = $voucher;

        return $this;
    }

    /**
     * Get voucher
     *
     * @return string
     */
    public function getVoucher()
    {
        return $this->voucher;
    }

    /**
     * Set voucherDiscount
     *
     * @param string $voucherDiscount
     * @return TopicVoucher
     */
    public function setVoucherDiscount($voucherDiscount)
    {
        $this->voucherDiscount = $voucherDiscount;

        return $this;
    }

    /**
     * Get voucherDiscount
     *
     * @return string
     */
    public function getVoucherDiscount()
    {
        return $this->voucherDiscount;
    }

    /**
     * Set voucherMinimum
     *
     * @param string $voucherMinimum
     * @return TopicVoucher
     */
    public function setVoucherMinimum($voucherMinimum)
    {
        $this->voucherMinimum = $voucherMinimum;

        return $this;
    }

    /**
     * Get voucherMinimum
     *
     * @return string
     */
    public function getVoucherMinimum()
    {
        return $this->voucherMinimum;
    }

    /**
     * Set shop
     *
     * @param integer Deal\TopicBundle\Entity\Shop $shop
     * @return TopicVoucher
     */
    public function setShop(\Deal\TopicBundle\Entity\Shop $shop)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop
     *
     * @return integer
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Get topicType
     *
     * @return String
     */
    public function getTopicType()
    {
        return 'TopicVoucher';
    }

}






