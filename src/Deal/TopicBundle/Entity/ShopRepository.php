<?php

namespace Deal\TopicBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ShopRepository extends EntityRepository
{
    /*
     * Devuelve una tienda dada una id
     *
     * @param integer $shopId id de la tienda a buscar
    **/
    public function findShopById($shopId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();     
        
        // Creo la consulta
        $dql = (  ' SELECT sho
                      FROM TopicBundle:Shop sho
                     WHERE sho.id = :shopId');

        $query = $em->createQuery($dql);
        
        // Parámetros de la consulta
        $query->setParameters(array(
                'shopId' => $shopId
        ));

        // Uso la cache para guardar este resultado (durante 604800s = 1 semana )
        $query->useResultCache(true, 604800);
        
        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }

    /*
     * Devuelve una tienda dado un nombre
     *
     * @param string $shopName nombre de la tienda a buscar
    **/
    public function findShopByName($shopName)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT sho
                      FROM TopicBundle:Shop sho
                     WHERE sho.name = :shopName');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
                'shopName' => $shopName
        ));

        // Uso la cache para guardar este resultado (durante 604800s = 1 semana )
        $query->useResultCache(true, 604800);

        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }

    /*
     * Devuelve una tienda dado un slug
     *
     * @param string $shopSlug slug de la tienda a buscar
    **/
    public function findShopBySlug($shopSlug)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT sho
                      FROM TopicBundle:Shop sho
                     WHERE sho.slug = :shopSlug');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
                'shopSlug' => $shopSlug
        ));

        // Uso la cache para guardar este resultado (durante 604800s = 1 semana )
        $query->useResultCache(true, 604800);

        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }

}
