<?php

namespace Deal\TopicBundle\Entity;

use DoctrineExtensions\Taggable\TagManager as BaseTagManager;
use Doctrine\ORM\EntityManager;
use Deal\TopicBundle\Util\SlugifierInterface;

use Doctrine\ORM\Query\Expr;
use DoctrineExtensions\Taggable\Taggable;


/*
 * Modificación de la clase TagManager del bundle FPNTagBundle
 * */
class TagManager extends BaseTagManager
{
    protected $slugifier;

    /**
     * @see DoctrineExtensions\Taggable\TagManager::__construct()
     */
    public function __construct(EntityManager $em, $tagClass = null, $taggingClass = null, SlugifierInterface $slugifier)
    {
        parent::__construct($em, $tagClass, $taggingClass);
        $this->slugifier = $slugifier;
    }

    /*
     * Función similar a createTags incluida en el bundle FPNTagBundle, crea etiquetas a partir de un nombre/texto dado
     *
     * @param array $name nombre de la etiqueta
     * @param array $checkIfSlugInDB booleano donde se indica si se comprueba en la base de datos si existe el slug
     * @param array $slugDelimiter caracter utilizado como separador en el slug
     *
     * @return Tag[]
     */
    protected function createTag($name, $checkIfSlugInDB = false, $slugDelimiter = '-')
    {
            $tag = parent::createTag($name);
            $tag->setSlug($this->slugifier->slugify($name, $checkIfSlugInDB, $slugDelimiter));

        return $tag;
    }

    /*
     * Modificación de la función loadOrCreateTags del bundle FPNTagBundle. A partir de un array de etiquetas guarda en
     * la base de datos aquellas que no estén (previamente se les ha asignando un slug) y devuelve las etiquetas
     * resultantes
     *
     * @param array $tagsArg array de etiquetas
     * @return $tagsInDB[]
     */
    public function loadOrCreateTagsFromTagsArray(array $tagsArg)
    {
        if (empty($tagsArg)) {
            return array();
        }

        $tags = array_unique($tagsArg);

        foreach($tags as $tag){
            $names[] = $tag->getName();
        }

        $builder = $this->em->createQueryBuilder();
        $tagsInDB = $builder
            ->select('t')
            ->from($this->tagClass, 't')
            ->where($builder->expr()->in('t.name', $names))
            ->getQuery()
            ->getResult()
        ;

        $missingTags = array_udiff($tags, $tagsInDB,
            function( $tag, $loadedTag){
                return strcasecmp($tag->getName(), $loadedTag->getName());
            }
        );

        if (sizeof($missingTags)) {
            // Seteo el slug de las etiquetas que faltan en la base de datos
            $this->slugifier->slugifyTagsArray($missingTags, true, '-');
            foreach ($missingTags as $tag) {

                $this->em->persist($tag);
                $tagsInDB[] = $tag;
            }
            $this->em->flush();
        }
        return $tagsInDB;
    }



    /**
     * Modificación de la función de la clase de la que hereda. Crea las etiquetas que no existan en la base de datos,
     * pero no las guarda en la base de datos para poder validarlas antes
     *
     * @param array $names Array con los nombres de las etiquetas a crear
     * @param integer $maxTags número máximo de etiquetas a crear
     *
     * @return Tag[]
     */
    public function LoadOrCreateTagsWithOutFlush(array $names, $maxTags)
    {
        if (empty($names)) {
            return array();
        }
        $names = array_unique($names);

        $i=0;
        foreach ($names as $name) {
            if($i == $maxTags) break;
            $tags[] = parent::createTag($name);
            $i++;
        }

        return $tags;
    }

    /**
     * Modificación de la función de la clase de la que hereda. Crea el vinculo entre la etiqueta y el objeto deseado.
     * Este método se utiliza para aligerar el creado de etiquetas de los fixtures y se debe usar con precaución porque
     * no hace ninguna comprobación.
     *
     * @param Tag       $tag        Tag object
     * @param Taggable  $resource   Taggable resource object
     * @return Tagging
     */
    public function unsafeCreateTagging(Tag $tag, Taggable $resource)
    {
        return new $this->taggingClass($tag, $resource);
    }
}
