<?php

namespace Deal\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * BaseNotification
 *
 * @ORM\Entity
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"Notification" = "Notification", "Moderation" = "Moderation"})
 */
class Notification
{
    // *************** COLUMNAS DE LA TABLA ***************

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    // ID de la notificación
    protected $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isNotified", type="boolean")
     */
    // Campo que indica si el usuario ha sido notificado. Por defecto a falso
    protected $isNotified = false;


    // *************** SETTERS Y GETTERS ***************

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isNotified
     *
     * @param boolean $isNotified
     * @return BaseNotification
     */
    public function setIsNotified($isNotified)
    {
        $this->isNotified = $isNotified;

        return $this;
    }

    /**
     * Get isNotified
     *
     * @return boolean
     */
    public function getIsNotified()
    {
        return $this->isNotified;
    }

}


/**
 * ModerationNotification
 *
 * @ORM\Entity
 */
class Moderation extends Notification
{
    // *************** COLUMNAS DE LA TABLA ***************

    /**
     * @ORM\ManyToOne(targetEntity="Deal\PostBundle\Entity\Post")
     */
    // Tenemos una relación N-1 entre el aviso de moderación (ModerationNotification) y el comentario (Post)
    // al cual se refiere la notificación. Podemos tener N notificaciones de moderación sobre un post.
    protected $post;

    // *************** SETTERS Y GETTERS ***************

    /**
     * Set post
     *
     * @param Deal\PostBundle\Entity\Post $post
     * @return ModerationNotification
     */
    public function setPost(\Deal\PostBundle\Entity\Post $post)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return integer
     */
    public function getPost()
    {
        return $this->post;
    }

}


///**
// * QuoteNotification
// *
// * @ORM\Entity
// */
//class QuoteNotification extends BaseNotification
//{
//    // *************** COLUMNAS DE LA TABLA ***************
//
//    /**
//     * @ORM\ManyToOne(targetEntity="Deal\UserBundle\Entity\User")
//     */
//    // Tenemos una relación N-1 entre el usuario al que se le notifica (user) y la notificación.
//    // Un usuario puede tener N notificaciones, pero una notificación solo puede ser para un usuario notificado.
//    protected $notifiedUser;
//
//    /**
//     * @ORM\ManyToOne(targetEntity="Deal\PostBundle\Entity\Post")
//     */
//    // Tenemos una relación N-1 entre la cita (quote) y el mensaje (Post) al cual se refiere la cita (quote).
//    // Se pueden hacer N citas sobre un único mensaje.
//    protected $post;
//
//
//    // *************** SETTERS Y GETTERS ***************
//
//    /**
//     * Set notifiedUser
//     *
//     * @param Deal\UserBundle\Entity\User $user
//     * @return QuoteNotification
//     */
//    public function setNotifiedUser(\Deal\UserBundle\Entity\User $notifiedUser)
//    {
//        $this->notifiedUser = $notifiedUser;
//
//        return $this;
//    }
//
//    /**
//     * Get notifiedUser
//     *
//     * @return Deal\UserBundle\Entity\User $user
//     */
//    public function getNotifiedUser()
//    {
//        return $this->notifiedUser;
//    }
//
//    /**
//     * Set post
//     *
//     * @param Deal\PostBundle\Entity\Post $post
//     * @return QuoteNotification
//     */
//    public function setPost(\Deal\PostBundle\Entity\Post $post)
//    {
//        $this->post = $post;
//
//        return $this;
//    }
//
//    /**
//     * Get post
//     *
//     * @return integer
//     */
//    public function getPost()
//    {
//        return $this->post;
//    }
//
//}
//
///**
// * SignatureNotification
// *
// * @ORM\Entity
// */
//class SignatureNotification extends BaseNotification
//{
//    // *************** COLUMNAS DE LA TABLA ***************
//
//    /**
//     * @ORM\OneToOne(targetEntity="Deal\UserBundle\Entity\Signature")
//     */
//    // Tenemos una relación 1-1 entre la notificación de una firma (SignatureNotification) y la firma.
//    // Una notificación de una firma solo puede pertenecer a una firma y una firma solo puede pertenecer
//    // a una notificación.
//    protected $signature;
//
//    // *************** SETTERS Y GETTERS ***************
//
//    /**
//     * Set signature
//     *
//     * @param Deal\UserBundle\Entity\Signature $signature
//     * @return SignatureNotification
//     */
//    public function setSignature(\Deal\UserBundle\Entity\Signature $signature)
//    {
//        $this->signature = $signature;
//
//        return $this;
//    }
//
//    /**
//     * Get signature
//     *
//     * @return Deal\UserBundle\Entity\Signature $signature
//     */
//    public function getSignature()
//    {
//        return $this->signature;
//    }
//
//}

