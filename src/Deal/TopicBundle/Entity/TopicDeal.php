<?php

namespace Deal\TopicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Deal\TopicBundle\Validator\Constraints as MyAssert;
use DoctrineExtensions\Taggable\Taggable;
use Doctrine\Common\Collections\ArrayCollection;

/*
 * Clase que representa los temas (topics) de tipo oferta
 * */

/**
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Deal\TopicBundle\Entity\TopicDealRepository")
 * @Serializer\ExclusionPolicy("all")
 * @Vich\Uploadable
 * @MyAssert\isEndDateLater
 */
class TopicDeal extends Topic implements Taggable
{
    // *************** COLUMNAS DE LA TABLA ***************

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_made_popular", type="datetime", nullable=true)
     */
    // Fecha en el que alcanzó los votos suficientes para ser popular. Si no es popular puede ser null
    protected $dateMadePopular;

    /**
     * @var integer
     *
     * @ORM\Column(name="votes", type="integer")
     * @Serializer\Expose
     */
    // Suma de los votos positivos y negativos recibidos
    protected $votes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_expired", type="boolean")
     */
    // Variable booleana en la que se guarda el estado de la oferta, expirada
    // o activa. Por defecto a falso
    protected $isExpired;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Url()
     * @ORM\Column(name="url", type="string", length=255)
     * @Assert\Length(max = 255, maxMessage = "El número máximo de caracteres que se pueden escribir para la url son {{ limit }}")
     */
    // Enlace a la web de la oferta.
    // Reglas de validación: No puede estar vacío. Debe tener el aspecto de una Url valida.
    protected $url;

    /**
     * @var File
     * @Assert\NotBlank(groups={"newTopic"}, message="No puedes publicar este tema sin incluir una imagen")
     * @Vich\UploadableField(mapping="topicImage", fileNameProperty="topicImageName")
     * @Assert\Image(maxSize = "3M",
     *               mimeTypes = {"image/jpeg", "image/png", "image/gif"},
     *               mimeTypesMessage= "El archivo no es una imagen válida. Solo puedes subir imágenes jpeg, png y gif")
     */
    // En este campo guardo el objeto UploadedFile después de haber enviado el formulario
    protected $topicImageFile;

    /**
     * @var string
     *
     * @ORM\Column(name="topic_image_name", type="string", length=255, nullable=false)
     * @Assert\Length(max = 255, maxMessage = "El número máximo de caracteres que se pueden escribir para el nombre de la imagen del tema son {{ limit }}")
     */
    // Nombre de la imagen del topic/tema
    protected $topicImageName;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     *
     * @var \DateTime $updatedAt
     */
    // Hora a la que se actualizo la imagen del topic/tema, necesario para subir correctamente la imagen
    protected $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_start", type="datetime", nullable=true)
     */
    // Fecha de inicio de la oferta, puede ser null si no existe fecha de inicio de la oferta
    protected $dateStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_end", type="datetime", nullable=true)
     */
    // Fecha de fin de la oferta, puede ser null si no existe fecha de fin de oferta
    protected $dateEnd;

    /**
     * @var integer
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Deal\TopicBundle\Entity\Shop")
     * @ORM\JoinColumn(name="shop_id", referencedColumnName="id", onDelete="SET NULL")
     */
    // Tienda que ofrece la oferta. Relación Shop-Topic (1-N),
    // un topic (oferta) solo puede pertenecer a una tienda,
    // mientras que una tienda, puede tener N topics (ofertas)
    // Reglas de validación: No puede estar vacío.
    protected $shop;

    protected $tags;

    public function __construct() {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dateMadePopular = null;
        $this->votes = 0;
        $this->isExpired = false;
        parent::__construct();
    }


    // *************** SETTERS Y GETTERS ***************

    /**
     * Get getTaggableType
     *
     * @return String
     */
    public function getTaggableType()
    {
        return 'TopicDealTag';
    }

    public function getTags()
    {
        $this->tags = $this->tags ?: new ArrayCollection();

        return $this->tags;
    }

    public function getTaggableId()
    {
        return $this->getId();
    }

    public function setTags($tags) {

        // Borro las etiquetas del tema
        $this->tags->clear();

        // Añado al tema las nuevas etiquetas introducidas si se ha introducido alguna
        if($tags != null){
            foreach($tags as $tag) {
                $this->tags->add($tag);
            }
        }

        return $this;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return TopicDeal
     */
    public function setDateMadePopular($date)
    {
        $this->dateMadePopular = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDateMadePopular()
    {
        return $this->dateMadePopular;
    }

    /**
     * Set votes
     *
     * @param integer $votes
     * @return TopicDeal
     */
    public function setVotes($votes)
    {
        $this->votes = $votes;

        return $this;
    }

    /**
     * Get votes
     *
     * @return integer
     */
    public function getVotes()
    {
        return $this->votes;
    }

    /**
     * Set isExpired
     *
     * @param boolean $isExpired
     * @return TopicDeal
     */
    public function setIsExpired($isExpired)
    {
        $this->isExpired = $isExpired;

        return $this;
    }

    /**
     * Get isExpired
     *
     * @return boolean
     */
    public function getIsExpired()
    {
        return $this->isExpired;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return TopicDeal
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setTopicImageFile(File $image)
    {
        $this->topicImageFile = $image;

        if ($image) {
            // Es necesario que al menos un campo cambie si se usa doctrine,
            // en otro caso el listener no será llamado y se perderá el fichero
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    // Devuelvo el objeto UploadedFile
    public function getTopicImageFile()
    {
        return $this->topicImageFile;
    }

    /**
     * @param string $topicImageName
     */
    // Función para "setear" el nombre de la imagen
    public function setTopicImageName($topicImageName)
    {
        $this->topicImageName = $topicImageName;
    }

    /**
     * @return string
     */
    // Función que devuelve el nombre de la imagen
    public function getTopicImageName()
    {
        return $this->topicImageName;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     * @return TopicDeal
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     * @return TopicDeal
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set shop
     *
     * @param integer Deal\TopicBundle\Entity\Shop $shop
     * @return TopicDeal
     */
    public function setShop(\Deal\TopicBundle\Entity\Shop $shop)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop
     *
     * @return integer
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Get topicType
     *
     * @return String
     */
    public function getTopicType()
    {
        return 'TopicDeal';
    }

}