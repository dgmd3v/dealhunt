<?php

namespace Deal\TopicBundle\Entity;

use Doctrine\ORM\EntityRepository;

class TopicRepository extends EntityRepository
{
    /**
     * Devuelve el número de topics/temas de un foro
     *
     * @param integer $forum id del foro del cual queremos buscar los temas
     **/
    public function findNumberOfTopicsByForumId($forum)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT COUNT(t)
                      FROM TopicBundle:Topic t
                      JOIN t.forum f
                     WHERE f.id = :forum');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'forum' => $forum
        ));

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }

    /**
     * Devuelve el número de topics/temas de un foro
     *
     * @param integer $forum id del foro del cual queremos buscar los temas más votados
     * @param integer $category id de la categoría de la cual queremos buscar los temas
     **/
    public function findNumberOfTopicsByCategory($forum, $category)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT COUNT(t)
                      FROM TopicBundle:Topic t
                      JOIN t.forum f
                      JOIN t.category cat
                     WHERE f.id = :forum
                       AND cat.id = :category');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'forum'     => $forum,
            'category'  => $category
        ));

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }

    /**
     * Devuelve los topics/temas de un foro ordenados por la fecha de creación (de más a menos reciente),
     * además de eso devuelve datos como el creador del tema, el foro al que pertenece, el Último usuario en postear,
     * etc, etc
     *
     * @param integer $forum id del foro del cual queremos buscar los temas más nuevos
    **/
    public function findNewestTopics($forum)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT t, fP, lP, creator, lastPoster, f, ar
                      FROM TopicBundle:Topic t
                      JOIN t.firstPost fP
                      JOIN t.lastPost lP
                      JOIN fP.poster creator
                      JOIN lP.poster lastPoster
                      JOIN t.forum f
                      JOIN f.accessRole ar
                     WHERE f.id = :forum
                  ORDER BY fP.postingDate DESC');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
                'forum' => $forum,
        ));

        // Seteo el modo de hidratación
        $query->setHydrationMode(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        // Retorno la consulta
        return $query;
    }

    /**
     * Devuelve los topics/temas de un foro ordenados por la fecha del último comentario realizado en el tema
     * (de más a menos reciente), además de eso devuelve datos como el creador del tema, el foro al que pertenece,
     * el Último usuario en postear, etc, etc
     *
     * @param integer $forum id del foro del cual queremos buscar los temas recientemente comentados
    **/
    public function findRecentlyCommentedTopics($forum)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();     
        
        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT t, fP, lP, creator, lastPoster, f, ar
                      FROM TopicBundle:Topic t
                      JOIN t.firstPost fP
                      JOIN t.lastPost lP
                      JOIN fP.poster creator
                      JOIN lP.poster lastPoster
                      JOIN t.forum f
                      JOIN f.accessRole ar
                     WHERE f.id = :forum
                  ORDER BY lP.postingDate DESC');        

        $query = $em->createQuery($dql);
        
        // Parámetros de la consulta
        $query->setParameters(array(
                'forum' => $forum
        ));

        // Seteo el modo de hidratación
        $query->setHydrationMode(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        // Retorno la consulta
        return $query;
    }

    /**
     * Devuelve los topics/temas de un foro y una categoría concreta ordenados por la fecha del último comentario
     * realizado en el tema (de más a menos reciente),además de eso devuelve datos como el creador del tema,
     * el foro al que pertenece, el Último usuario en postear, etc, etc
     *
     * @param integer $forum id del foro del cual queremos buscar los temas recientemente comentados
     * @param integer $category id de la categoría de la cual queremos buscar los temas recientemente comentados
     **/
    public function findRecentlyCommentedTopicsByCategory($forum, $category)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();     
        
        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT t, fP, lP, creator, lastPoster, f, ar
                      FROM TopicBundle:Topic t
                      JOIN t.firstPost fP
                      JOIN t.lastPost lP
                      JOIN fP.poster creator
                      JOIN lP.poster lastPoster
                      JOIN t.forum f
                      JOIN f.accessRole ar
                      JOIN t.category cat
                     WHERE f.id = :forum
                       AND cat.id = :category
                  ORDER BY lP.postingDate DESC');        

        $query = $em->createQuery($dql);
        
        // Parámetros de la consulta
        $query->setParameters(array(
                'forum'     => $forum,
                'category'  => $category
        ));

        // Seteo el modo de hidratación
        $query->setHydrationMode(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        // Retorno la consulta
        return $query;
    }


    /**
     * Devuelve los topics/temas de un foro y una categoría concreta ordenados por la fecha de creación
     * (de más a menos reciente), además de eso devuelve datos como el creador del tema, el foro al que pertenece,
     * el Último usuario en postear, etc, etc
     *
     * @param integer $forum id del foro del cual queremos buscar los temas más recientes
     * @param integer $category id de la categoría de la cual queremos buscar los temas más recientes
    **/
    public function findNewestTopicsByCategory($forum, $category)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();     
        
        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT t, fP, lP, creator, lastPoster, f, ar, cat
                      FROM TopicBundle:Topic t
                      JOIN t.firstPost fP
                      JOIN t.lastPost lP
                      JOIN fP.poster creator
                      JOIN lP.poster lastPoster
                      JOIN t.forum f
                      JOIN f.accessRole ar
                      JOIN t.category cat
                     WHERE f.id = :forum
                       AND cat.id = :category
                  ORDER BY fP.postingDate DESC');        

        $query = $em->createQuery($dql);
        
        // Parámetros de la consulta
        $query->setParameters(array(
                'forum'     => $forum,
                'category'  => $category
        ));

        // Seteo el modo de hidratación
        $query->setHydrationMode(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        // Retorno la consulta
        return $query;
    }

    /**
     * Devuelve el topic/tema solicitado por su id
     *
     * @param int $topicId id del topic/tema del cual queremos información
     *
     **/
    public function findTopicById($topicId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT t, f, ar, cat, fp, creator
                     FROM TopicBundle:Topic t
                      JOIN t.forum f
                      JOIN f.accessRole ar
                 LEFT JOIN t.category cat
                      JOIN t.firstPost fp
                      JOIN fp.poster creator
                     WHERE t.id = :topicId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'topicId' => $topicId
        ));

        // Limito los resultados
        $query->setMaxResults(1);

        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }

    /**
     * Devuelve el número de topics/temas que está en un foro público (ROLE_USER) y también en un array dado
     *
     * @param array $ids ids de los topics/temas de los cuales queremos información
     * @param integer $forum id del foro del cual queremos buscar los temas
     **/
    public function findNumberOfPublicTopicsInArray($ids)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT count(t)
                      FROM TopicBundle:Topic t
                      JOIN t.firstPost fP
                      JOIN t.lastPost lP
                      JOIN fP.poster creator
                      JOIN lP.poster lastPoster
                      JOIN t.forum f
                      JOIN f.accessRole ar
                     WHERE ar.name = :ROLE_USER
                       AND t.id IN (:ids)
                  ORDER BY fP.postingDate DESC');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'ids'       => $ids,
            'ROLE_USER' => 'ROLE_USER',
        ));

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }

    /**
     * Devuelve los topics/temas solicitados en el array de ids (de los foros públicos)
     *
     * @param array $ids ids de los topics/temas de los cuales queremos información
     *
     **/
    public function findTopicsByIds($ids)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT t, fP, lP, creator, lastPoster, f, ar
                      FROM TopicBundle:Topic t
                      JOIN t.firstPost fP
                      JOIN t.lastPost lP
                      JOIN fP.poster creator
                      JOIN lP.poster lastPoster
                      JOIN t.forum f
                      JOIN f.accessRole ar
                     WHERE ar.name = :ROLE_USER
                       AND t.id IN (:ids)
                  ORDER BY fP.postingDate DESC');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'ids'       => $ids,
            'ROLE_USER' => 'ROLE_USER',
        ));

        // Retorno la consulta
        return $query;
    }

    /**
     * Devuelve los topics/temas solicitados en el array de ids que pertenecen a un foro dado
     *
     * @param array $ids ids de los topics/temas de los cuales queremos información
     * @param integer $forum id del foro del cual queremos buscar los temas
     *
     **/
    public function findTopicsByIdsAndForum($ids, $forum)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT t, fP, lP, creator, lastPoster, f, ar
                      FROM TopicBundle:Topic t
                      JOIN t.firstPost fP
                      JOIN t.lastPost lP
                      JOIN fP.poster creator
                      JOIN lP.poster lastPoster
                      JOIN t.forum f
                      JOIN f.accessRole ar
                     WHERE f.id = :forum
                       AND t.id IN (:ids)
                  ORDER BY fP.postingDate DESC');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'ids'   => $ids,
            'forum' => $forum,
        ));

        // Retorno la consulta
        return $query;
    }

    /**
     * Devuelve el número de topics/temas que están en un foro y en un array de ids dado
     *
     * @param array $ids ids de los topics/temas de los cuales queremos información
     * @param integer $forum id del foro del cual queremos buscar los temas
     **/
    public function findNumberOfTopicsInArray($ids, $forum)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT count(t)
                      FROM TopicBundle:Topic t
                      JOIN t.firstPost fP
                      JOIN t.lastPost lP
                      JOIN fP.poster creator
                      JOIN lP.poster lastPoster
                      JOIN t.forum f
                     WHERE f.id = :forum
                       AND t.id IN (:ids)
                  ORDER BY fP.postingDate DESC');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'ids'   => $ids,
            'forum' => $forum,
        ));

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }

    /**
     * Devuelve solo topic/tema solicitado por su id
     *
     * @param int $topicId id del topic/tema del cual queremos información
     *
     **/
    public function findOnlyTopicById($topicId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT t
                      FROM TopicBundle:Topic t
                     WHERE t.id = :topicId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'topicId' => $topicId
        ));

        // Limito los resultados
        $query->setMaxResults(1);

        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }

    /**
     * Devuelve el topic/tema solicitado por su id además de su foro y el rol de acceso de este
     *
     * @param int $topicId id del topic/tema del cual queremos información
     *
     **/
    public function findTopicForumAndAccessById($topicId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT t, f, ar
                     FROM TopicBundle:Topic t
                      JOIN t.forum f
                      JOIN f.accessRole ar
                     WHERE t.id = :topicId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'topicId' => $topicId
        ));

        // Limito los resultados
        $query->setMaxResults(1);

        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }

    /**
     * Devuelve solo topic/tema solicitado por su id y su último post
     *
     * @param int $topicId id del topic/tema del cual queremos información
     *
     **/
    public function findOnlyTopicAndLastPostById($topicId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT t, lp
                     FROM TopicBundle:Topic t
                     JOIN t.lastPost lp
                     WHERE t.id = :topicId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'topicId' => $topicId
        ));

        // Limito los resultados
        $query->setMaxResults(1);

        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }

    /**
     * Devuelve el número de topics/temas de todos los foros públicos (ROLE_USER) que tengan en común la tienda
     *
     * @param integer $shop id de la tienda de la cual queremos buscar los temas
     **/
    public function findNumberOfTopicsByShopInPublicForums($shop)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT COUNT(t)
                      FROM TopicBundle:Topic t
                      JOIN t.forum f
                      JOIN f.accessRole ar
                     WHERE ar.name =:ROLE_USER
                       AND t.id IN (SELECT td.id FROM TopicBundle:TopicDeal td JOIN td.shop s WHERE s.id =:shopId)
                        OR t.id IN (SELECT tv.id FROM TopicBundle:TopicVoucher tv JOIN tv.shop sv WHERE sv.id =:shopId)
                        OR t.id IN (SELECT tf.id FROM TopicBundle:TopicFree tf JOIN tf.shop sf WHERE sf.id =:shopId)'
                );

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'ROLE_USER' => 'ROLE_USER',
            'shopId'    => $shop,
        ));

        // Retorno el resultado de la consulta
        return $query->getSingleScalarResult();
    }

    /**
     * Devuelve los topics/temas de todos los foros públicos (ROLE_USER) que tengan en común la tienda
     *
     * @param integer $shop id de la tienda de la cual queremos buscar los temas
     **/
    public function findTopicsByShopInPublicForums($shop)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Hago un fetch JOIN para reducir el número de consultas a realizar
        $dql = (  ' SELECT t, fP, lP, creator, lastPoster, f, ftt
                      FROM TopicBundle:Topic t
                      JOIN t.firstPost fP
                      JOIN t.lastPost lP
                      JOIN fP.poster creator
                      JOIN lP.poster lastPoster
                      JOIN t.forum f
                      JOIN f.forumTopicsType ftt
                      JOIN f.accessRole ar
                     WHERE ar.name =:ROLE_USER
                       AND t.id IN (SELECT td.id FROM TopicBundle:TopicDeal td JOIN td.shop s WHERE s.id =:shopId)
                        OR t.id IN (SELECT tv.id FROM TopicBundle:TopicVoucher tv JOIN tv.shop sv WHERE sv.id =:shopId)
                        OR t.id IN (SELECT tf.id FROM TopicBundle:TopicFree tf JOIN tf.shop sf WHERE sf.id =:shopId)
                  ORDER BY fP.postingDate DESC');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'ROLE_USER' => 'ROLE_USER',
            'shopId'    => $shop,
        ));

        // Retorno la consulta
        return $query;
    }

    /**
     * Devuelve los topics/temas de un foro concreto
     *
     * @param integer $forum id del foro en el cual vamos a buscar los temas
     **/
    public function findAllTopicsByForum($forum)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT t
                     FROM TopicBundle:Topic t
                     JOIN t.forum f
                     WHERE f.id = :forumId');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'forumId' => $forum,
        ));

        // Retorno el resultado de la consulta
        return $query->getResult();
    }

    /**
     * Devuelve los topics/temas (de todos los foros) que tengan en común la categoría
     *
     * @param integer $category id de la categoría de la cual queremos buscar los temas
     **/
    public function findAllTopicsByCategory($category)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT t
                      FROM TopicBundle:Topic t
                      JOIN t.category cat
                     WHERE cat.id = :category');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'category' => $category
        ));

        // Retorno el resultado de la consulta
        return $query->getResult();
    }

    /**
     * Devuelve los topics/temas (de todos los foros) que tengan en común que su categoría es NULL
     *
     **/
    public function findAllTopicsWithNullCategory()
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT t
                      FROM TopicBundle:Topic t
                     WHERE t.category IS NULL');

        $query = $em->createQuery($dql);

        // Retorno el resultado de la consulta
        return $query->getResult();
    }

}
