<?php

namespace Deal\TopicBundle\Entity;

use Doctrine\ORM\EntityRepository;

class CategoryRepository extends EntityRepository
{
    /*
     * Devuelve una categoría dada una id
     *
     * @param integer $categoryId id de la categoría a buscar
    **/
    public function findCategoryById($categoryId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();     
        
        // Creo la consulta
        $dql = (  ' SELECT cat
                      FROM TopicBundle:Category cat
                     WHERE cat.id = :categoryId');

        $query = $em->createQuery($dql);
        
        // Parámetros de la consulta
        $query->setParameters(array(
                'categoryId' => $categoryId
        ));

        // Uso la cache para guardar este resultado (durante 604800s = 1 semana )
        $query->useResultCache(true, 604800);
        
        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }

    /*
     * Devuelve una categoría dado su nombre
     *
     * @param string $categoryName nombre de la categoría a buscar
    **/
    public function findCategoryByName($categoryName)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Creo la consulta
        $dql = (  ' SELECT cat
                      FROM TopicBundle:Category cat
                     WHERE cat.name = :categoryName');

        $query = $em->createQuery($dql);

        // Limito los resultados
        $query->setMaxResults(1);

        // Parámetros de la consulta
        $query->setParameters(array(
                'categoryName' => $categoryName
        ));

        // Uso la cache para guardar este resultado (durante 604800s = 1 semana )
        $query->useResultCache(true, 604800);

        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }

    /*
     * Devuelve todas las categorías existentes
     **/
    public function findAllCategories()
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Genero la consulta
        $dql = (  ' SELECT c
                      FROM TopicBundle:Category c
                      ORDER BY c.name ASC');

        $query = $em->createQuery($dql);

        // Uso la cache para guardar este resultado (durante 604800s = 1 semana )
        $query->useResultCache(true, 604800);

        // Retorno el resultado de la consulta
        return $query->getResult();
    }

}
