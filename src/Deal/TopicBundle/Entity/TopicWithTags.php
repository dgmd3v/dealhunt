<?php

namespace Deal\TopicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Deal\TopicBundle\Validator\Constraints as MyAssert;
use DoctrineExtensions\Taggable\Taggable;
use Doctrine\Common\Collections\ArrayCollection;

/*
 * Clase que representa los temas (topics) "normales" con etiquetas
 * */

/**
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Deal\TopicBundle\Entity\TopicWithTagsRepository")
 * @Serializer\ExclusionPolicy("all")
 */
class TopicWithTags extends Topic implements Taggable
{
    protected $tags;


    public function __construct() {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
        parent::__construct();
    }


    // *************** SETTERS Y GETTERS ***************

    /**
     * Get getTaggableType
     *
     * @return String
     */
    public function getTaggableType()
    {
        return 'TopicWithTagsTag';
    }

    public function getTags()
    {
        $this->tags = $this->tags ?: new ArrayCollection();

        return $this->tags;
    }

    public function getTaggableId()
    {
        return $this->getId();
    }

    public function setTags($tags) {

        // Borro las etiquetas del tema
        $this->tags->clear();

        // Añado al tema las nuevas etiquetas introducidas si se ha introducido alguna
        if($tags != null){
            foreach($tags as $tag) {
                $this->tags->add($tag);
            }
        }

        return $this;
    }

    /**
     * Get topicType
     *
     * @return String
     */
    public function getTopicType()
    {
        return 'TopicWithTags';
    }
}