<?php

namespace Deal\TopicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Deal\ForumBundle\Util\Util;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Category
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Deal\TopicBundle\Entity\CategoryRepository")
 * @Serializer\ExclusionPolicy("all")
 * @UniqueEntity( fields="slug", message="Ya existe una categoría con el mismo slug.")
 * @UniqueEntity( fields="name", message="Ya existe una categoría con el mismo nombre.")
 */
class Category
{
    // *************** COLUMNAS DE LA TABLA ***************      
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    // ID de la categoría
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     * @Serializer\Expose
     * @Assert\Length(max = 100, maxMessage = "El número máximo de caracteres que se pueden escribir para el nombre son {{ limit }}")
     */
    // Nombre de la categoría
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=100)
     * @Serializer\Expose
     * @Assert\Length(max = 100, maxMessage = "El número máximo de caracteres que se pueden escribir para el slug son {{ limit }}")
     */
    // Slug de la categoría
    private $slug;

    // *************** SETTERS Y GETTERS ***************      

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;
        
        // A la vez que le doy nombre al foro le pongo un slug
        $this->slug = Util::getSlug($name);
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Category
     */
    public function setSlug($slug)
    {
        if($slug == null){
            $this->slug = Util::getSlug($this->name);
        }

        else{
            $this->slug = Util::getSlug($slug);
        }

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }
    
    // Método mágico para que PHP sepa como convertir la entidad en una cadena de texto
    public function __toString()
    {
        return $this->getName();
    }    
}
