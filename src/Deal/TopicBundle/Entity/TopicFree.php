<?php

namespace Deal\TopicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Deal\TopicBundle\Validator\Constraints as MyAssert;

/*
 * Clase que representa los temas (topics) de tipo gratis
 * */

/**
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Deal\TopicBundle\Entity\TopicFreeRepository")
 * @Serializer\ExclusionPolicy("all")
 * @Vich\Uploadable
 *
 */
class TopicFree extends TopicDeal
{
    // *************** COLUMNAS DE LA TABLA ***************



    // *************** SETTERS Y GETTERS ***************

    /**
     * Get getTaggableType
     *
     * @return String
     */
    public function getTaggableType()
    {
        return 'TopicFreeTag';
    }

    /**
     * Get topicType
     *
     * @return String
     */
    public function getTopicType()
    {
        return 'TopicFree';
    }

}