<?php

namespace Deal\TopicBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

/*
 * Clase encargada de transformar texto en etiquetas/tags y etiquetas/tags en texto
 * */
class TextToTagTransformer implements DataTransformerInterface
{
    /**
     * @var tagManager
     */
    private $tagManager;

    /**
     * @param $tagManager
     */
    public function __construct($tagManager)
    {
        $this->tagManager = $tagManager;
    }

    /**
     * Transforma objetos tag a string/texto
     *
     * @param  $tags|null $tags
     * @return string
     */
    public function transform($tags)
    {
        if (null === $tags) {
            return "";
        }
        return join(', ', $tags->toArray());

    }

    /**
     * Transforma texto a objetos de tipo tag
     *
     * @param  string $stringTags
     *
     * @return stringTags|null
     *
     */
    public function reverseTransform($stringTags)
    {
        if (!$stringTags) {
            return null;
        }

        // Paso a un array las etiquetas introducidas y cojo solo las 3 primeras
        $tagsArray = $this->tagManager->splitTagNames($stringTags);

        // Por si e introdujesen solo espacios en blanco (   ,  , )
        if (!$tagsArray) {
            return null;
        }

        return $this->tagManager->LoadOrCreateTagsWithOutFlush($tagsArray, 3);
    }

} 