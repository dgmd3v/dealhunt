<?php

namespace Deal\TopicBundle\Form\Backend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

/*
 * Formulario para la creación del primer posts/mensaje del topic en el backend
 **/
class BackendTopicFirstPostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('message', 'textarea', array(
            'attr'  => array( 'placeholder' => "Escribe aquí el mensaje del tema", 'rows' => 7),
            'label' => false,
            'constraints' =>  new NotBlank(),
            'error_bubbling' => false
        ));
    }

    /*
     * Esta función indica el namespace de la entidad que vamos a modificar con este formulario.
     **/
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array('data_class'=> 'Deal\PostBundle\Entity\Post'));
    }

    public function getName()
    {
        return 'deal_topicbundle_backend_topicfirstposttype';
    }
} 