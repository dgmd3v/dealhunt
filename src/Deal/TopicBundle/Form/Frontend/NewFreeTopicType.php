<?php

namespace Deal\TopicBundle\Form\Frontend;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Formulario para la creación de topics/temas (Cosas gratis)
 **/
class NewFreeTopicType extends NewTopicWithTagsType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->remove('title',   'text')
            ->add('title',   'text', array(
                'label'      => 'Título',
                'attr'       => array( 'placeholder' => "Escribe aquí el título del tema sobre el artículo gratuito"),
                'help_block' => 'Ej: muestra gratis @ en esta tienda'
            ))
            ->add('url',  'url', array(
                'label' => 'Enlace',
                'attr'  => array( 'placeholder' => "Escribe la url/enlace al articulo gratuito")
            ))
            ->add('topicImageFile', 'file', array(
                'required'  => true,
                'label'     => 'Imagen del artículo gratuito',
            ))
            ->add('shop', 'zenstruck_ajax_entity', array(
                'class'             => 'TopicBundle:Shop',
                'use_controller'    => true,
                'property'          => 'name',
                'help_block'        => 'Si no encuentras la tienda introduce "Otra" ',
                'label'             => 'Tienda',
                'placeholder'       => 'Selecciona una tienda',
                'attr'              => array('class' => 'col-sm-12', 'style' => 'padding: 0%')

            ))
            ->add('dateStart', 'date', array(
                'widget'                => 'choice',
                'required'              => false,
                'label'                 => 'Empieza',
                'empty_value'           => array('year' => 'Año', 'month' => 'Mes', 'day' => 'Día'),
                'render_optional_text'  => false
            ))
            ->add('dateEnd', 'date', array(
                'widget'                => 'choice',
                'required'              => false,
                'label'                 => 'Finaliza',
                'empty_value'           => array('year' => 'Año', 'month' => 'Mes', 'day' => 'Día'),
                'render_optional_text'  => false
            ));

    }

    /*
     * Esta función indica el namespace de la entidad que vamos a modificar con este formulario.
     **/
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(   'data_class'=> 'Deal\TopicBundle\Entity\TopicFree',
                                        'validation_groups' => array('Default', 'newTopic'),
            )
        );
    }

    public function getName()
    {
        return 'deal_topicbundle_newfreetopictype';
    }
}