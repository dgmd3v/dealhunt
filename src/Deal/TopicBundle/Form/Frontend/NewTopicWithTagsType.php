<?php

namespace Deal\TopicBundle\Form\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\FormEvents;
use Deal\TopicBundle\Listener\TopicTypeListener;


/*
 * Formulario para la creación de topics/temas con etiquetas
 **/
class NewTopicWithTagsType extends NewTopicType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('tags', 'deal_topicbundle_tagtype', array(
                'label'         => 'Etiquetas',
                'attr'          => array( 'placeholder' => "Escribe aquí las etiquetas relacionadas con este tema"),
                'help_block'    => 'Escribe un máximo de tres etiquetas, cada una separada por comas ( , ). <br>
                                    Ej: Cámara de fotos, Reflex, Fotografía'
            ));

        // Listener utilizado para validar las etiquetas
        $listener = new TopicTypeListener();
        $builder->addEventListener(FormEvents::SUBMIT, array($listener,'preSubmit'));


    }

    /*
     * Esta función indica el namespace de la entidad que vamos a modificar con este formulario.
     **/
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(   'data_class'=> 'Deal\TopicBundle\Entity\TopicWithTags',
                                        'validation_groups' => array('Default'),
            )
        );
    }

    public function getName()
    {
        return 'deal_topicbundle_newtopicwithtagstype';
    }
} 