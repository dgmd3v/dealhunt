<?php

namespace Deal\TopicBundle\Form\Frontend;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Formulario para la creación de topics/temas (ofertas)
 **/
class NewDealTopicType extends NewTopicWithTagsType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->remove('title',   'text')
            ->add('title',   'text', array(
                'label'         => 'Título',
                'attr'          => array( 'placeholder' => "Escribe aquí el título de la oferta"),
                'help_block'    => 'Ej: camiseta azul @ amazon.es - 2€'
            ))
            ->add('url',  'url', array(
                'label' => 'Enlace',
                'attr'  => array( 'placeholder' => "Escribe la url/enlace a la oferta")
            ))
            ->add('topicImageFile', 'file', array(
                'required'  => true,
                'label'     => 'Imagen del la oferta',
            ))
            ->add('shop', 'zenstruck_ajax_entity', array(
                'class'             => 'TopicBundle:Shop',
                'use_controller'    => true,
                'property'          => 'name',
                'help_block'        => 'Si no encuentras la tienda introduce "Otra" ',
                'label'             => 'Tienda',
                'placeholder'       => 'Selecciona una tienda',
                'attr'              => array('class' => 'col-sm-12', 'style' => 'padding: 0%')

            ))
            ->add('dateStart', 'date', array(
                'widget'                => 'choice',
                'required'              => false,
                'label'                 => 'Empieza',
                'empty_value'           => array('year' => 'Año', 'month' => 'Mes', 'day' => 'Día'),
                'render_optional_text'  => false
            ))
            ->add('dateEnd', 'date', array(
                'widget'                => 'choice',
                'required'              => false,
                'label'                 => 'Finaliza',
                'empty_value'           => array('year' => 'Año', 'month' => 'Mes', 'day' => 'Día'),
                'render_optional_text'  => false
            ));

    }
    /**
     * Esta función indica el namespace de la entidad que vamos a modificar con este formulario.
    **/
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(   'data_class'=> 'Deal\TopicBundle\Entity\TopicDeal',
                                        'validation_groups' => array('Default', 'newTopic'),
                                    )
                              );
    }

    public function getName()
    {
        return 'deal_topicbundle_newdealtopictype';
    }
} 