<?php

namespace Deal\TopicBundle\Form\Frontend;

use Deal\TopicBundle\Form\DataTransformer\TextToTagTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use DoctrineExtensions\Taggable\TagManager;
use Symfony\Component\DependencyInjection\ContainerInterface;


/*
 * Formulario para la creación de tags/etiquetas
 **/
class TagType extends AbstractType
{
    public function __construct(TagManager $tagManager, ContainerInterface $container )
    {
        $this->tagManager = $tagManager;
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new TextToTagTransformer($this->tagManager, $this->container);

        $builder->addModelTransformer($transformer);
    }

    public function getParent()
    {
        return 'text';
    }

    public function getName()
    {
        return 'deal_topicbundle_tagtype';
    }
} 