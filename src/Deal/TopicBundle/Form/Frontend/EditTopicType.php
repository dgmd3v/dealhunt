<?php

namespace Deal\TopicBundle\Form\Frontend;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/*
 * Formulario para la edicion de topics/temas
 **/
class EditTopicType extends NewTopicType
{
    // Nota: Se modifica category por un bug de symfony en el cual empty_data devuelve siempre null, lo cual provoca
    // errores en nuestra aplicación
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->remove('category', 'entity')
            ->add('category', 'entity', array(
                'class'         => 'TopicBundle:Category',
                'property'      => 'name',
                'label'         => 'Categoría',
                'attr'          => array('class' => 'col-sm-12 select2-field', 'style' => 'padding: 0%')
            ));

    }

    /*
     * Esta función indica el namespace de la entidad que vamos a modificar con este formulario.
     **/
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(   'data_class'=> 'Deal\TopicBundle\Entity\Topic',
                                        'validation_groups' => array('Default'),
            )
        );
    }

    public function getName()
    {
        return 'deal_topicbundle_edittopictype';
    }
} 