<?php

namespace Deal\TopicBundle\Form\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;


/*
 * Formulario para la creación de topics/temas
 **/
class NewTopicType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title',   'text', array(
                'label' => 'Título',
                'attr'  => array( 'placeholder' => "Escribe aquí el título del tema"),
            ))
            ->add('firstPost', new TopicFirstPostType())
            ->add('category', 'entity', array(
                'class'         => 'TopicBundle:Category',
                'property'      => 'name',
                'label'         => 'Categoría',
                'empty_data'    => null,
                'empty_value'   => 'Selecciona una categoría',
                'attr'          => array('class' => 'col-sm-12 select2-field', 'style' => 'padding: 0%')
            ));


    }

    /*
     * Esta función indica el namespace de la entidad que vamos a modificar con este formulario.
     **/
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(   'data_class'=> 'Deal\TopicBundle\Entity\Topic',
                                        'validation_groups' => array('Default'),
            )
        );
    }

    public function getName()
    {
        return 'deal_topicbundle_newtopictype';
    }
} 