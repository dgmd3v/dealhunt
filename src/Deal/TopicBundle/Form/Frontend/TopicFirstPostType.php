<?php

namespace Deal\TopicBundle\Form\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

/*
 * Formulario para la creación del primer posts/mensaje del topic
 **/
class TopicFirstPostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('message', 'textarea', array(
            'label'             => 'Mensaje',
            'attr'              => array( 'placeholder' => "Escribe aquí el mensaje del tema", 'rows' => 7),
            'label_render'      => true,
            'error_bubbling'    => false
        ));
    }

    /*
     * Esta función indica el namespace de la entidad que vamos a modificar con este formulario.
     **/
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(   'data_class'=> 'Deal\PostBundle\Entity\Post',
                                        'validation_groups' => array('Default'),
            )
        );
    }

    public function getName()
    {
        return 'deal_topicbundle_topicfirstposttype';
    }
} 