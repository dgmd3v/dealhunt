<?php

namespace Deal\TopicBundle\Controller;

use Deal\TopicBundle\Entity\TopicFree;
use Deal\TopicBundle\Entity\TopicDeal;
use Deal\TopicBundle\Entity\TopicVoucher;
use Deal\TopicBundle\Entity\TopicWithTags;
use Deal\TopicBundle\Form\Frontend\NewTopicWithTagsType;
use Deal\TopicBundle\Form\Frontend\EditTopicWithTagsType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Deal\TopicBundle\Entity\Topic;
use Deal\PostBundle\Entity\Post;
use Deal\TopicBundle\Form\Frontend\NewTopicType;
use Deal\TopicBundle\Form\Frontend\EditTopicType;
use Deal\TopicBundle\Form\Frontend\NewDealTopicType;
use Deal\TopicBundle\Form\Frontend\NewFreeTopicType;
use Deal\TopicBundle\Form\Frontend\NewVoucherTopicType;
use Deal\TopicBundle\Form\Frontend\EditDealTopicType;
use Deal\TopicBundle\Form\Frontend\EditFreeTopicType;
use Deal\TopicBundle\Form\Frontend\EditVoucherTopicType;
use Deal\PostBundle\Form\Frontend\NewPostType;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/*
 * Clase encargada de las acciones “habituales” de los temas
 **/
class DefaultController extends Controller
{
    /**
     * Muestra en una plantilla los mensajes de un tema/topic el cual pertenece a un foro y tiene una id concreta.
     *
     * @param string $forumSlug slug del foro al cual pertenece el tema
     * @param integer $topicSlug slug del tema/topic
     * @param integer $topicID id del tema/topic
     * @param integer $pageNumber página de los comentarios que vamos a mostrar, por defecto 1
     **/
    public function showTopicAction($forumSlug, $topicSlug, $topicId, $pageNumber = 1)
    {
        // Compruebo que el número de página sea mayor que 0
        if($pageNumber < 1){
            return new RedirectResponse(
                $this->generateUrl( 'topic_show_topic_page',
                    array ( 'forumSlug' => $forumSlug,
                            'topicSlug' => $topicSlug,
                            'topicId' => $topicId,
                            'pageNumber'=> 1))
            );
        }

        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getDoctrine()->getManager();

        $forum = $this->get('deal.forumbundle.service.utilities')->searchRequestedForum($forumSlug);

        // Por si el foro no existiese
        if(!$forum){
            throw $this->createNotFoundException(   'No se ha encontrado '
                . 'el foro solicitado');
        }

        if(!$this->get('security.context')->isGranted('ROLE_ACCESS_TOPIC', $forum)){
            Throw new AccessDeniedHttpException('No tienes permisos suficientes para acceder a este tema.');
        }

        $forumTopicsType = $forum->getForumTopicsType()->getName();

        $repository = 'TopicBundle:' . $forumTopicsType;

        // Primero busco el tema
        switch($forumTopicsType){
            case 'TopicDeal':
                $topic = $em->getRepository($repository)->findTopicById($topicId);
                $criterionSlug = 'mas-votados';
                break;
            case 'TopicFree':
                $topic = $em->getRepository($repository)->findTopicById($topicId);
                $criterionSlug = 'mas-votados';
                break;
            case 'TopicVoucher':
                $topic = $em->getRepository($repository)->findTopicById($topicId);
                $criterionSlug = 'mas-votados';
                break;
            case 'TopicWithTags':
                $topic = $em->getRepository($repository)->findTopicById($topicId);
                $criterionSlug = 'recientemente-comentados';
                break;
            default:
                $topic = $em->getRepository($repository)->findTopicById($topicId);
                $criterionSlug = 'recientemente-comentados';
                break;
        }

        // Si por algún motivo no encuentro el tema muestro un mensaje de error
        if(!$topic){
            throw $this->createNotFoundException(   'No se ha encontrado '
                . 'el tema solicitado');
        }

        if(!$this->get('security.context')->isGranted('ROLE_ACCESS_TOPIC', $topic->getForum())){
            Throw new AccessDeniedHttpException('No tienes permisos suficientes para acceder a este tema.');
        }

        $postCount = $em->getRepository('PostBundle:Post')->findNumberOfPosts($topicId);

        // Ahora busco los mensajes del tema
        $comments = $em->getRepository('PostBundle:Post')->findTopicPosts($topicId);

        // Extraigo el primer mensaje del hilo de los mensajes del tema, dejando en $comments solo
        // los comentarios del tema
        $firstPost = $topic->getFirstPost();

        // Pagino los resultados con la ruta personalizada
        $globalVariables = $em->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
        $maxPostsPerTopicPage = $globalVariables->getMaxPostsPerTopicPage();

        // Si se solicita una página de un tema sin resultados, devuelvo la última página existente.
        // (Ej: tengo 30 mensajes con un limite de mensajes por página de 10 y solicito la página 4 cuando solo debería
        // existir hasta la 3, por lo que devuelvo la última existe es decir la 3)

        // Etiquetas del tema (solo si tiene)
        if($forumTopicsType != 'Topic'){
            $tags = $em->getRepository('TopicBundle:Tag')->findTopicTags($topicId);
        }

        $maxResultsPages = ceil(($postCount-1)/$maxPostsPerTopicPage);

        if($maxResultsPages == 0) $maxResultsPages = 1;

        if( $pageNumber > $maxResultsPages){

            $pageNumber = $maxResultsPages;

            return new RedirectResponse(
                $this->generateUrl('topic_show_topic_page', array (
                    'forumSlug'     => $forumSlug,
                    'topicSlug'     => $topicSlug,
                    'topicId'       => $topicId,
                    'pageNumber'    => $pageNumber,
                ))
            );
        }

        $comments->setHint('knp_paginator.count', $postCount-1);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate( $comments,
                                            $this->get('request')->query->get('page', $pageNumber),
                                            $maxPostsPerTopicPage,
                                            array('distinct' => false));

        // Ruta y plantilla de paginación personalizadas
        $pagination->setUsedRoute('topic_show_topic_page');
        $pagination->setTemplate('TopicBundle:Default:pagination/MOD_twitter_bootstrap_v3_pagination.html.twig');

        // Formulario vacío para nuevos mensajes/posts
        $form = $this->createForm(new NewPostType());

        // Obtengo el usuario logueado a través del token creado por el componente de seguridad
        $user = $this->get('security.context')->getToken()->getUser();

        $isVoted = false;

        // Si no se encuentra al usuario o no es un usuario registrado el tema no puede estar marcado como favorito
        if($user == null || !$this->get('security.context')->isGranted('ROLE_USER')){
            $isFavorite = 0;
        }

        else{
            $isFavorite = $em->getRepository('UserBundle:Visited')->findIfTopicIsFavorite($topicId, $user);

            // Si es un usuario registrado y además es un tema susceptible de ser votado compruebo si ha votado el tema
            if($forumTopicsType == 'TopicDeal' ||$forumTopicsType == 'TopicVoucher' ||$forumTopicsType == 'TopicFree'){
                $userVotedTopic =  $em->getRepository('UserBundle:User')->findAlreadyVotedTopic($user, $topic->getId());
                if($userVotedTopic == 1) $isVoted = true;
            }
        }

        // Retorno una plantilla generada con twig a la que le paso los mensajes
        // buscados, el tema de los mensajes, el foro al que pertenece y la categoría al que pertenece el tema
        if($forumTopicsType != 'Topic'){
            return $this->render(   'TopicBundle:Default:topic.html.twig',
                array(
                    'form'              => $form->createView(),
                    'firstPost'         => $firstPost,
                    'pagination'        => $pagination,
                    'topic'             => $topic,
                    'forumSlug'         => $forumSlug,
                    'tags'              => $tags,
                    'category'          => $topic->getCategory(),
                    'pageNumber'        => $pageNumber,
                    'criterionSlug'     => $criterionSlug,
                    'isFavorite'        => $isFavorite,
                    'isVoted'           => $isVoted,
                    'forumTopicsType'   => $forumTopicsType,
                    'lastPageNumber'    => $maxResultsPages,
                )
            );
        }

        else{
            return $this->render(   'TopicBundle:Default:topic.html.twig',
                array(
                    'form'              => $form->createView(),
                    'firstPost'         => $firstPost,
                    'pagination'        => $pagination,
                    'topic'             => $topic,
                    'forumSlug'         => $forumSlug,
                    'category'          => $topic->getCategory(),
                    'pageNumber'        => $pageNumber,
                    'criterionSlug'     => $criterionSlug,
                    'isFavorite'        => $isFavorite,
                    'isVoted'           => $isVoted,
                    'forumTopicsType'   => $forumTopicsType,
                    'lastPageNumber'    => $maxResultsPages,
                )
            );
        }

    }

    /*
     * Crea un tema con los datos introducidos en un formulario
     *
     * @param string $forumSlug slug del foro en el cual estamos creando el tema
     **/
    public function newTopicAction($forumSlug)
    {
        // Obtengo el usuario logueado a través del token creado por el componente de seguridad
        $user = $this->get('security.context')->getToken()->getUser();

        // Si se está intentando crear un topic/tema sin ser usuario muestro un mensaje informando del error y
        // redirijo al login
        if($user == null || !$this->get('security.context')->isGranted('ROLE_USER')){
            $this->get('session')->getFlashBag()->add('info',
                'Para publicar un nuevo tema debes haberte registrado y estar logueado'
            );

            $request = $this->container->get('request');
            $session = $request->getSession();
            $session->set('requestedPage', $request->getPathInfo());

            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $forum = $this->get('deal.forumbundle.service.utilities')->searchRequestedForum($forumSlug);

        // Por si el foro no existiese
        if(!$forum){
            throw $this->createNotFoundException(   'No se ha encontrado '
                . 'el foro donde quieres crear el mensaje');
        }

        // Compruebo si puede acceder al foro donde se quiere crear el tema
        if(!$this->get('security.context')->isGranted('ROLE_ACCESS_TOPIC', $forum)){
            Throw new AccessDeniedHttpException('No tienes permisos suficientes para crear un tema en este foro.');
        }

        // Creo un nuevo mensaje/post que sera el primero del tema
        $post = new Post();

        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getDoctrine()->getManager();

        // Dependiendo del foro cambia el tipo de topic/tema (Ofertas, cupones, gratis, etc)
        switch($forum->getForumTopicsType()){
            case 'TopicDeal':
                // Creo un nuevo topic/tema y le asigno el primer post
                $topic = new TopicDeal();
                $topic->setFirstPost($post);

                $form = $this->createForm(new NewDealTopicType(), $topic);

                $formTitle = "Nueva Oferta";
                $blockId = "new-deal";
                $criterionSlug = 'mas-votados';
                
                break;

            case 'TopicVoucher':
                // Creo un nuevo topic/tema y le asigno el primer post
                $topic = new TopicVoucher();
                $topic->setFirstPost($post);

                $form = $this->createForm(new NewVoucherTopicType(), $topic);

                $formTitle = "Nuevo cupón descuento";
                $blockId = "new-voucher";
                $criterionSlug = 'mas-votados';

                break;

            case 'TopicFree':
                // Creo un nuevo topic/tema y le asigno el primer post
                $topic = new TopicFree();
                $topic->setFirstPost($post);

                $form = $this->createForm(new NewFreeTopicType(), $topic);

                $formTitle = "Nuevo artículo gratuito";
                $blockId = "new-free";
                $criterionSlug = 'mas-votados';

                break;

            case 'TopicWithTags':
                // Creo un nuevo topic/tema CON TAGS y le asigno el primer post
                $topic = new TopicWithTags();
                $topic->setFirstPost($post);

                $form = $this->createForm(new NewTopicWithTagsType(), $topic);

                $formTitle = "Nuevo tema";
                $blockId = "new-topic-with-tags";
                $criterionSlug = 'recientemente-comentados';

                break;

            default:
                // Creo un nuevo topic/tema SIN TAGS y le asigno el primer post
                $topic = new Topic();
                $topic->setFirstPost($post);

                $form = $this->createForm(new NewTopicType(), $topic);

                $formTitle = "Nuevo tema";
                $blockId = "new-topic";
                $criterionSlug = 'recientemente-comentados';

                break;
        }

        // Relaciono el formulario con los datos introducidos por el usuario en el mismo
        $request = $this->getRequest();
        $form->handleRequest($request);

        // Compruebo que los datos enviados son correctos
        if($form->isValid()){
            // "Seteo" los datos del topic/tema

            // los temas de tipo 'topic' no llevan etiquetas
            if($topic->getTopicType() != 'Topic'){
                $tags = $form["tags"]->getData();

                // Invoco al gestor de tags
                $tagManager = $this->get('fpn_tag.tag_manager');

                // Si se introducen etiquetas en el formulario las guardo en la base de datos y las reemplazo en el
                // objeto topic/tema
                if($tags != null) {
                    $newTags = $tagManager->loadOrCreateTagsFromTagsArray($tags);
                    $tagManager->replaceTags($newTags, $topic);
                }
            }

            // Busco la categoría introducida
            $categoryId = $form["category"]->getData();
            $category = $em->getRepository('TopicBundle:Category')->findCategoryById($categoryId);

            // Por si la categoría no existiese
            if(!$category){
                throw $this->createNotFoundException(   'No se ha encontrado '
                    . 'la categoría donde quieres crear el mensaje');
            }

            $topic->setCategory($category);
            $topic->setForum($forum);

            // "Seteo" el mensaje, el usuario que lo publica, llamando a un servicio que lo hace
            $this->container->get('deal.postbundle.service.setfirstpost')->set($topic, $post);

            // A partir de aquí seteo los campos específicos según el tipo de topic/tema
            $topicType = $topic->getTopicType();
            if( $topicType == 'TopicDeal' ||  $topicType == 'TopicVoucher' || $topicType == 'TopicFree'){
                // Busco la tienda introducida
                $shopId = $form["shop"]->getData();
                $shop = $em->getRepository('TopicBundle:Shop')->findShopById($shopId);

                // Por si la tienda no existiese
                if(!$shop){
                    throw $this->createNotFoundException(   'No se ha encontrado '
                        . 'la tienda a la que pertenecería este mensaje');
                }

                $topic->setShop($shop);

                // Busco la fecha de inicio/Fin introducida
                $dateStart = $form["dateStart"]->getData();
                $dateEnd = $form["dateEnd"]->getData();

                $topic->setDateStart($dateStart);
                $topic->setDateEnd($dateEnd);

            }

            if($topicType == 'TopicVoucher'){
                $voucher = $form["voucher"]->getData();
                $voucherDiscount = $form["voucherDiscount"]->getData();
                $voucherMinimum = $form["voucherMinimum"]->getData();

                $topic->setVoucher($voucher);
                $topic->setVoucherDiscount($voucherDiscount);
                $topic->setVoucherMinimum($voucherMinimum);
            }

            $em->persist($topic);
            $em->persist($post);
            $em->flush();

            // los temas de tipo 'topic' no llevan etiquetas
            if($topic->getTopicType() != 'Topic'){
                // Guardo las etiquetas y las cargo para mostrarlas en la vista
                $tagManager->saveTagging($topic);
            }

            // Mensaje flashbag informando de que se ha creado el mensaje/posts correctamente
            $this->get('session')->getFlashBag()->add('success',
                'Tema creado correctamente'
            );

            return $this->redirect($this->generateUrl(  'topic_show_topic_page',
                    array(  'forumSlug' => $topic->getForum()->getSlug(),
                        'topicSlug' => $topic->getSlug(),
                        'topicId'   => $topic->getId(),
                        'pageNumber'=> 1
                    )) . '#1');
        }

        // En caso de no introducir los datos de forma valida devuelvo al usuario al formulario de creación de temas
        return $this->render(   'TopicBundle:Default:newTopicForm.html.twig',
            array(  'form'          => $form->createView(),
                    'forumSlug'     => $forumSlug,
                    'formTitle'     => $formTitle,
                    'criterionSlug' => $criterionSlug,
                    'blockId'       => $blockId,
            )
        );
    }

    /*
     * Edito los datos de un tema ya creado a través de los datos introducidos en un formulario
     *
     * @param string $forumSlug slug del foro en el cual estamos editando el tema
     * @param integer $topicId Id del tema a editar
     **/
    public function editTopicAction($forumSlug, $topicId)
    {
        // Obtengo el usuario logueado a través del token creado por el componente de seguridad
        $user = $this->get('security.context')->getToken()->getUser();

        // Si se está intentando editar un topic/tema sin ser usuario muestro un mensaje informando del error y
        // redirijo al login
        if($user == null || !$this->get('security.context')->isGranted('ROLE_USER')){
            $this->get('session')->getFlashBag()->add('info',
                'Para editar un mensaje debes haberte registrado y estar logueado'
            );

            $request = $this->container->get('request');
            $session = $request->getSession();
            $session->set('requestedPage', $request->getPathInfo());

            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $forum = $this->get('deal.forumbundle.service.utilities')->searchRequestedForum($forumSlug);

        // Por si el foro no existiese
        if(!$forum){
            throw $this->createNotFoundException(   'No se ha encontrado '
                . 'el foro donde quieres editar el mensaje');
        }

        // Si intenta editar un tema que está en un foro al que no puede acceder el usuario ...
        if(!$this->get('security.context')->isGranted('ROLE_ACCESS_TOPIC', $forum)){
            Throw new AccessDeniedHttpException('No tienes permisos suficientes para editar un tema en este foro.');
        }

        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getDoctrine()->getManager();

        // Dependiendo del foro cambia el tipo de topic/tema (Ofertas, cupones, gratis, etc)
        switch($forum->getForumTopicsType()){
            case 'TopicDeal':
                // Busco el topic que se va a editar
                $topic = $em->getRepository('TopicBundle:TopicDeal')->findTopicById($topicId);
                // Si por algún motivo no encuentro el topic muestro un mensaje de error
                if(!$topic){
                    throw $this->createNotFoundException(   'No se ha encontrado '
                        . 'el tema que quieres editar');
                }
                $tagManager = $this->get('fpn_tag.tag_manager');
                $tagManager->loadTagging($topic);

                $form = $this->createForm(new EditDealTopicType(), $topic);
                $formTitle = "Editar Oferta";
                $blockId = "edit-deal";
                break;

            case 'TopicVoucher':
                // Busco el tema que se va a editar
                $topic = $em->getRepository('TopicBundle:TopicVoucher')->findTopicById($topicId);
                // Si por algún motivo no encuentro el tema muestro un mensaje de error
                if(!$topic){
                    throw $this->createNotFoundException(   'No se ha encontrado '
                        . 'el tema que quieres editar');
                }
                $tagManager = $this->get('fpn_tag.tag_manager');
                $tagManager->loadTagging($topic);

                $form = $this->createForm(new EditVoucherTopicType(), $topic);
                $formTitle = "Editar cupón descuento";
                $blockId = "edit-voucher";
                break;

            case 'TopicFree':
                // Busco el topic que se va a editar
                $topic = $em->getRepository('TopicBundle:TopicFree')->findTopicById($topicId);
                // Si por algún motivo no encuentro el tema muestro un mensaje de error
                if(!$topic){
                    throw $this->createNotFoundException(   'No se ha encontrado '
                        . 'el tema que quieres editar');
                }
                $tagManager = $this->get('fpn_tag.tag_manager');
                $tagManager->loadTagging($topic);

                $form = $this->createForm(new EditFreeTopicType(), $topic);
                $formTitle = "Editar artículo gratuito";
                $blockId = "edit-free";
                break;

            case 'TopicWithTags':
                // Busco el tema que se va a editar
                $topic = $em->getRepository('TopicBundle:TopicWithTags')->findTopicById($topicId);
                // Si por algún motivo no encuentro el tema muestro un mensaje de error
                if(!$topic){
                    throw $this->createNotFoundException(   'No se ha encontrado '
                        . 'el tema que quieres editar');
                }
                $tagManager = $this->get('fpn_tag.tag_manager');
                $tagManager->loadTagging($topic);

                $form = $this->createForm(new EditTopicWithTagsType(), $topic);
                $formTitle = "Editar tema";
                $blockId = "edit-topic-with-tags";
                break;

            default:
                // Busco el tema que se va a editar
                $topic = $em->getRepository('TopicBundle:Topic')->findTopicById($topicId);
                // Si por algún motivo no encuentro el tema muestro un mensaje de error
                if(!$topic){
                    throw $this->createNotFoundException(   'No se ha encontrado '
                        . 'el tema que quieres editar');
                }
                $form = $this->createForm(new EditTopicType(), $topic);
                $formTitle = "Editar tema";
                $blockId = "edit-topic";
                break;
        }

        if(!$this->get('security.context')->isGranted('ROLE_ACCESS_TOPIC', $topic->getForum())){
            Throw new AccessDeniedHttpException('No tienes permisos suficientes para acceder a este tema.');
        }

        // Para editar un tema tienes que ser el usuario "propietario" del tema, que el tema este abierto y
        // estar logueado, o ser moderador.
        if(!$this->get('security.context')->isGranted('ROLE_EDIT_TOPIC', $topic)){
            $this->get('session')->getFlashBag()->add('info',
                "Para editar este tema debes ser " .
                "el usuario que lo creo o un moderador y que el tema no este cerrado"
            );

            return $this->redirect($this->generateUrl(  'topic_show_topic_page',
                array(  'forumSlug'     => $topic->getForum()->getSlug(),
                        'topicSlug'     => $topic->getSlug(),
                        'topicId'       => $topicId,
                        'pageNumber'    => 1
                    )));
        }

        // Antiguo mensaje del tema
        $oldFirstPostMessage = $topic->getFirstPost()->getMessage();

        $topicType = $topic->getTopicType();

        // Relaciono el formulario con los datos introducidos por el usuario en el mismo
        $request = $this->getRequest();
        $form->handleRequest($request);

        // Compruebo que los datos enviados son correctos
        if($form->isValid()){

            // los temas de tipo 'topic' no llevan etiquetas
            if($topicType != 'Topic'){
                $tags = $form["tags"]->getData();

                // Si se introducen etiquetas en el formulario las guardo en la base de datos y las reemplazo en el
                // objeto topic/tema
                if($tags != null) {
                    $newTags = $tagManager->loadOrCreateTagsFromTagsArray($tags);
                    $tagManager->replaceTags($newTags, $topic);
                }
            }

            // "Seteo" la hora de edición del mensaje/post
            $topic->getFirstPost()->setEditDate(new \DateTime('now'));

            // Guardo los datos en la base de datos
            $em = $this->getDoctrine()->getManager();
            $em->persist($topic);

            // Creo las notificaciones por las citas del tema si las hubiese
            $this->container->get('deal.postbundle.service.postquote')->editPostQuotes( $topic->getFirstPost(),
                                                                                        $oldFirstPostMessage);

            $em->flush();

            if($topicType != 'Topic'){
                // Guardo las etiquetas y las cargo para mostrarlas en la vista
                $tagManager->saveTagging($topic);
            }

            // Mensaje flashbag informando de que se ha editado el mensaje/posts correctamente
            $this->get('session')->getFlashBag()->add('success',
                'El tema ha sido editado correctamente'
            );

            return $this->redirect($this->generateUrl(  'topic_show_topic_page',
                    array(  'forumSlug' => $topic->getForum()->getSlug(),
                            'topicSlug' => $topic->getSlug(),
                            'topicId'   => $topic->getId(),
                            'pageNumber'=> 1
                    )) . '#1');
        }

        // En caso de no introducir los datos de forma valida devuelvo al usuario al formulario de creación de temas
        return $this->render(   'TopicBundle:Default:editTopicForm.html.twig',
            array(
                    'form'      => $form->createView(),
                    'topic'     => $topic,
                    'post'      => $topic->getFirstPost(),
                    'formTitle' => $formTitle,
                    'blockId'   => $blockId,
            )
        );
    }
}
