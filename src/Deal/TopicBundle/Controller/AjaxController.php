<?php

namespace Deal\TopicBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;


/*
 * Clase encargada de las acciones relacionadas con los temas en las que se usa AJAX y
 * que pueden usar todos los usuarios registrados del foro
 * */
class AjaxController extends Controller
{
    /*
     * Marcar un tema como favorito
     *
     * @param integer $topicId Id del tema a marcar como favorito
     **/
    public function ajaxFavoriteTopicAction()
    {
        $request = $this->getRequest();

        // Si se trata de una petición AJAX serializo los resultados de la consulta a la base de datos
        // y devuelvo la respuesta
        if ($request->isXmlHttpRequest()) {

            // Obtengo el usuario logueado a través del token creado por el componente de seguridad
            $user = $this->get('security.context')->getToken()->getUser();

            // Llamo al serializador para poder utilizarlo
            $serializer = $this->container->get('jms_serializer');

            // Si se está intentando marcar como favorito un topic/tema sin ser usuario muestro un
            // mensaje informando del error
            if($user == null || !$this->get('security.context')->isGranted('ROLE_USER')){

                $responseArray = array( "responseCode" => 400,
                                        "errorMsg" =>   "Para marcar como favorito un tema debes" .
                                                        " haberte registrado y estar logueado");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Obtengo el objeto del entity manager para realizar consultas
            $em = $this->getDoctrine()->getManager();

            // Solicito los datos pasados a través del script jquery, en este caso el id del post/mensaje a borrar
            $topicId = $request->request->get('topicId');
            $markAs = $request->request->get('markAs');

            // Busco el tema visitado a marcar como favorito
            $favoriteTopic = $em->getRepository('UserBundle:Visited')->findOneTopicVisitedByUser($topicId, $user);

            // Si no se ha encontrado el tema dentro de los temas visitados debo mostrar un mensaje de error
            if (!$favoriteTopic) {
                $responseArray = array( "responseCode" => 400,
                                        "errorMsg" => "Para marcar como favorito un tema debes haberlo visitado antes");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Compruebo que no se intente acceder a temas de foros privados
            $forum = $this->get('deal.forumbundle.service.utilities')
                        ->searchRequestedForum($favoriteTopic->getLastVisitedPost()->getTopic()->getForum()->getSlug());

            if(!$this->get('security.context')->isGranted('ROLE_ACCESS_FORUM', $forum)){
                $responseArray = array( "responseCode" => 400,
                                        "errorMsg" => "No tienes permisos suficientes para acceder a esta zona.");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Si encuentro el tema lo marco/desmarco como favorito y lo guardo
            if($markAs == 'favorite'){
                $favoriteTopic->setIsFavorite(true);
            }

            elseif($markAs == 'unfavorite'){
                $favoriteTopic->setIsFavorite(false);
            }

            $em->persist($favoriteTopic);
            $em->flush();

            $responseArray = array("responseCode" => 200);
            $jsonContent = $serializer->serialize($responseArray, 'json');

            return new Response($jsonContent);
        }

        // En caso de no ser una petición AJAX retorno un mensaje de error
        return new Response('Esto no es ajax!', 400);
    }

    /*
     * Votar un tema
     *
     * @param integer $topicId Id del tema a votar
     **/
    public function ajaxVoteTopicAction()
    {
        $request = $this->getRequest();

        // Si se trata de una petición AJAX serializo los resultados de la consulta a la base de datos
        // y devuelvo la respuesta
        if ($request->isXmlHttpRequest()) {

            // Obtengo el usuario logueado a través del token creado por el componente de seguridad
            $user = $this->get('security.context')->getToken()->getUser();

            // Llamo al serializador para poder utilizarlo
            $serializer = $this->container->get('jms_serializer');

            // Si se está intentando votar un topic/tema sin ser usuario muestro un mensaje informando del error
            if($user == null || !$this->get('security.context')->isGranted('ROLE_USER')){

                $responseArray = array( "responseCode" => 400,
                                        "errorMsg" => "Para votar un tema debes haberte registrado y estar logueado");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Obtengo el objeto del entity manager para realizar consultas
            $em = $this->getDoctrine()->getManager();

            // Solicito los datos pasados a través del script jquery, en este caso el id del post/mensaje a borrar
            $topicId = $request->request->get('topicId');
            $topicType = $request->request->get('topicType');
            $voteButtonClass = $request->request->get('voteButtonClass');

            // Busco el tema a votar
            switch($topicType){
                case 'TopicDeal':
                    $topic = $em->getRepository('TopicBundle:TopicDeal')->findTopicByIdAndFPProfile($topicId);
                    break;
                case 'TopicFree':
                    $topic = $em->getRepository('TopicBundle:TopicFree')->findTopicByIdAndFPProfile($topicId);
                    break;
                case 'TopicVoucher':
                    $topic = $em->getRepository('TopicBundle:TopicVoucher')->findTopicByIdAndFPProfile($topicId);
                    break;
                default:
                    $topic = null;
            }

            // Si no se ha encontrado la firma debo mostrar un mensaje de error
            if (!$topic) {
                $responseArray = array( "responseCode" => 400,
                                        "errorMsg" => "No se ha encontrado el tema que quieres votar");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Compruebo que no se intente acceder a temas de foros privados
            $forum = $this->get('deal.forumbundle.service.utilities')
                                                                ->searchRequestedForum($topic->getForum()->getSlug());

            if(!$this->get('security.context')->isGranted('ROLE_ACCESS_FORUM', $forum)){
                $responseArray = array( "responseCode" => 400,
                                        "errorMsg" => "No tienes permisos suficientes para acceder a esta zona.");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Si el tema está finalizado/expirado muestro un error
            $today = new \DateTime('today');
            $finishingTopicDate = $topic->getDateEnd();

            if($finishingTopicDate != null){
                if (($topic->getIsExpired()) || ($today > $finishingTopicDate)) {
                    $responseArray = array( "responseCode" => 400,
                        "errorMsg" => "No puedes votar este tema, ya está finalizado o expirado");
                    $jsonContent = $serializer->serialize($responseArray, 'json');

                    return new Response($jsonContent);
                }
            }

            // Si el tema está cerrado muestro un error
            if ($topic->getIsClosed()) {
                $responseArray = array( "responseCode" => 400,
                    "errorMsg" => "No puedes votar este tema, está cerrado");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Si el tema ya había sido votado muestro un error
            $isTopicVoted = $em->getRepository('UserBundle:User')->findAlreadyVotedTopic($user, $topicId);

            if ($isTopicVoted) {
                $responseArray = array("responseCode" => 400, "errorMsg" => "Ya has votado este tema");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Añado el voto y sumo el voto al tema
            $user->addTopicVote($topic);

            if($voteButtonClass == "vote-button-negative"){
                $newTotalVotes = $topic->getVotes() - 1;
                $userProfile = $topic->getFirstPost()->getPoster()->getUserProfile();
                $userNegativeVotes = $userProfile->getDealsNegative();
                $userProfile->setDealsNegative($userNegativeVotes+1);
            }

            else{
                $newTotalVotes = $topic->getVotes() + 1;
                $userProfile = $topic->getFirstPost()->getPoster()->getUserProfile();
                $userPositiveVotes = $userProfile->getDealsPositive();
                $userProfile->setDealsPositive($userPositiveVotes+1);
            }

            $globalVariables = $em->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
            $minVotesToBePop = $globalVariables->getMinVotesToBePop();

            if($newTotalVotes >= $minVotesToBePop && $topic->getDateMadePopular() == null){
                $topic->setDateMadePopular(new \DateTime('now'));
            }

            elseif($newTotalVotes < $minVotesToBePop && $topic->getDateMadePopular() != null){
                $topic->setDateMadePopular(null);
            }

            $topic->setVotes($newTotalVotes);

            $em->persist($user);
            $em->persist($topic);
            $em->flush();

            $responseArray = array("responseCode" => 200, "newTotalVotes" => $newTotalVotes);

            $jsonContent = $serializer->serialize($responseArray, 'json');

            return new Response($jsonContent);
        }

        // En caso de no ser una petición AJAX retorno un mensaje de error
        return new Response('Esto no es ajax!', 400);
    }


    /*
     * Marco un tema como expirado (finalizado)
     *
     * @param integer $topicId Id del tema a marcar como expirado
     **/
    public function ajaxTopicExpiredStatusAction()
    {
        $request = $this->getRequest();

        // Si se trata de una petición AJAX serializo los resultados de la consulta a la base de datos
        // y devuelvo la respuesta
        if ($request->isXmlHttpRequest()) {

            // Obtengo el usuario logueado a través del token creado por el componente de seguridad
            $user = $this->get('security.context')->getToken()->getUser();

            // Llamo al serializador para poder utilizarlo
            $serializer = $this->container->get('jms_serializer');

            // Si se está intentando marcar un tema como expirado sin ser usuario
            if($user == null || !$this->get('security.context')->isGranted('ROLE_USER') ){

                $responseArray = array( "responseCode" => 400,
                                        "errorMsg" =>   "Para marcar un tema como finalizado/no finalizado" .
                                                        " primero debes estar registrado y logueado");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Obtengo el objeto del entity manager para realizar consultas
            $em = $this->getDoctrine()->getManager();

            // Solicito los datos pasados a través del script jquery, en este caso el id del topic a cerrar
            $topicId = $request->request->get('topicId');
            $topicType = $request->request->get('topicType');
            $action = $request->request->get('action');

            // Busco el tema a marcar como expirado
            switch($topicType){
                case 'TopicDeal':
                    $topic = $em->getRepository('TopicBundle:TopicDeal')->findTopicById($topicId);
                    break;
                case 'TopicFree':
                    $topic = $em->getRepository('TopicBundle:TopicFree')->findTopicById($topicId);
                    break;
                case 'TopicVoucher':
                    $topic = $em->getRepository('TopicBundle:TopicVoucher')->findTopicById($topicId);
                    break;
                default:
                    $topic = null;
            }

            // Si no se ha encontrado el tema debo mostrar un mensaje de error
            if (!$topic) {
                $responseArray = array( "responseCode" => 400,
                    "errorMsg" =>   "No se ha encontrado el tema solicitado o " .
                        " no puede ser marcado como finalizado/des-finalizado");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Si intenta marcar como finalizado un tema que está en un foro al que no puede acceder el usuario ...
            $forumAccess = $this->get('security.context')->isGranted('ROLE_ACCESS_FORUM', $topic->getForum());
            if(!$forumAccess){
                $responseArray = array( "responseCode" => 400,
                                        "errorMsg" => "No tienes permisos suficientes para acceder a este tema.");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Para marcar como finalizado un tema tienes que ser el usuario "propietario" del tema y estar logueado,
            // o ser moderador.
            elseif ($forumAccess){
                if(!$this->get('security.context')->isGranted('ROLE_MARK_TOPIC_AS_EXPIRED', $topic)){
                    $responseArray = array( "responseCode" => 400,
                                            "errorMsg" =>   "Solo se puede marcar un tema como finalizado si estas" .
                                                            " registrado y eres el creador del tema o un moderador");
                    $jsonContent = $serializer->serialize($responseArray, 'json');

                    return new Response($jsonContent);
                }
            }

            // Si hubiese caducado/expirado por fecha lo marco como finalizado independientemente de la acción indicada
            $today = new \DateTime('today');
            $finishingTopicDate = $topic->getDateEnd();

            if (($today > $finishingTopicDate) && ($finishingTopicDate != null) ) {
                $topic->setIsExpired(true);
                $em->persist($topic);
                $em->flush();

                $responseArray = array(
                    "responseCode" => 200,
                    "successMsg" => "ATENCIÓN: El tema ha sido marcado como finalizado debido a que por fecha el tema".
                                    " ha caducado",
                    "expiredStatus" => "expired"
                );
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            if($action == 'expire'){
                // Si ya estuviese marcado como finalizado, informo de la situacion
                if ($topic->getIsExpired()==1) {
                    $responseArray = array( "responseCode" => 200,
                        "successMsg" => "El tema ha sido marcado como finalizado con éxito." .
                                        "Ya estaba marcado como finalizado",
                        "expiredStatus" => "expired");
                }

                // Si no estuviese marcado como finalizado, lo marco como finalizado
                else{

                    $topic->setIsExpired(true);
                    $responseArray = array( "responseCode" => 200,
                        "successMsg" => "El tema ha sido marcado como finalizado con éxito",
                        "expiredStatus" => "expired");
                }
            }

            elseif($action == 'unexpire'){

                $isClosed = $topic->getIsClosed();

                $closedInfo = '';

                if($isClosed){
                    $closedInfo = ' (El tema está cerrado)';
                }

                // Si ya estuviese marcado como no finalizado informo de la situación
                if ($topic->getIsExpired()==0) {
                    $responseArray = array( "responseCode" => 200,
                        "successMsg" => "El tema ha sido marcado como NO finalizado con éxito." .
                                        "Ya estaba marcado como NO finalizado" . $closedInfo,
                                        "expiredStatus" => "notexpired",
                                        "isClosed" => $isClosed);
                }

                // Si no estuviese marcado como finalizado, lo marco como finalizado
                else{
                    $topic->setIsExpired(false);
                    $responseArray = array( "responseCode" => 200,
                                            "successMsg" => "El tema ha sido marcado como NO finalizado con éxito" .
                                                            $closedInfo,
                                            "expiredStatus" => "notexpired",
                                            "isClosed" => $isClosed);
                }
            }

            else{
                $responseArray = array( "responseCode" => 400,
                                        "errorMsg" =>   "Error, esta acción no se puede llevar a cabo");
                $jsonContent = $serializer->serialize($responseArray, 'json');
                return new Response($jsonContent);
            }

            $em->persist($topic);
            $em->flush();

            $jsonContent = $serializer->serialize($responseArray, 'json');

            return new Response($jsonContent);
        }

        // En caso de no ser una petición AJAX retorno un mensaje de error
        return new Response('Esto no es ajax!', 400);
    }

}
