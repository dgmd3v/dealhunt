<?php

namespace Deal\TopicBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;

/*
 * Clase para evitar que al borrar desde el listado se produzca un bug, y es que no se ejecutan las funciones
 * preRemove y postRemove (esta última puede seguir bugueada) encargadas de realizar acciones antes y
 * después de borrar un objeto de la lista
 * */
class CRUDController extends Controller
{
    /*
     * Función que ejecuta las funciones preRemove y postRemove cuando se ejecuta una acción en forma de batch
    **/
    public function batchActionDelete(ProxyQueryInterface $query)
    {
        if (method_exists($this->admin, 'preRemove')) {
            foreach ($query->getQuery()->iterate() as $object) {
                $this->admin->preRemove($object[0]);
            }
        }

        $response = parent::batchActionDelete($query);

        if (method_exists($this->admin, 'postRemove')) {
            foreach ($query->getQuery()->iterate() as $object) {
                $this->admin->postRemove($object[0]);
            }
        }

        return $response;
    }

}