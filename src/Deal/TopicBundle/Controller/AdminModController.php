<?php

namespace Deal\TopicBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

/*
 * Clase encargada de las acciones de administración relacionadas con los temas
 * */
class AdminModController extends Controller
{
    /*
     * Borro un tema dada su Id
     *
     * @param integer $topicId Id del tema a borrar
     **/
    public function deleteTopicAction($topicId)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getDoctrine()->getManager();

        // Busco el tema a borrar
        $topic = $em->getRepository('TopicBundle:Topic')->findTopicForumAndAccessById($topicId);

        if(!$topic){
            // Mensaje flashbag informando de que se ha borrado el tema con éxito
            $this->get('session')->getFlashBag()->add('success',
                'El tema ya había sido borrado previamente o no existía'
            );

            return new RedirectResponse(
                $this->generateUrl('forum_front_page')
            );
        }

        // Compruebo que pueda moderar en el foro
        if(!$this->get('security.context')->isGranted('ROLE_CAN_MOD_FORUM', $topic->getForum())){
            $this->get('session')->getFlashBag()->add('error',
                'No tienes permisos para borrar este tema.'
            );

            return $this->redirect($this->getRequest()->headers->get('referer'));
        }

        // Borro las etiquetas del tema
        // los temas de tipo 'topic' no llevan etiquetas
        if($topic->getTopicType() != 'Topic'){
            $tagManager = $this->get('fpn_tag.tag_manager');
            $tagManager->deleteTagging($topic);
        }

        // Llama al servicio que borra el tema
        $this->get('deal.topicbundle.moderationtools.topicremover')->remove($topic);

        // Mensaje flashbag informando de que se ha borrado el tema con éxito
        $this->get('session')->getFlashBag()->add('success',
            'El tema ha sido eliminado correctamente'
        );

        return new RedirectResponse(
            $this->generateUrl('forum_front_page')
        );
    }

    /*
     * Abro/Cierro un tema dado su Id
     *
     * @param integer $topicId Id del tema a abrir
     **/
    public function ajaxCloseOpenTopicAction()
    {
        $request = $this->getRequest();

        // Si se trata de una petición AJAX serializo los resultados de la consulta a la base de datos
        // y devuelvo la respuesta
        if ($request->isXmlHttpRequest()) {
            // Llamo al serializador para poder utilizarlo
            $serializer = $this->container->get('jms_serializer');

            // Obtengo el objeto del entity manager para realizar consultas
            $em = $this->getDoctrine()->getManager();

            // Solicito los datos pasados a través del script jquery, en este caso el id del topic a abrir/cerrar
            $topicId = $request->request->get('topicId');
            $action = $request->request->get('action');

            // Busco el tema a abrir/cerrar
            $topic = $em->getRepository('TopicBundle:Topic')->findTopicForumAndAccessById($topicId);

            // Si no se ha encontrado el post debo mostrar un mensaje de error
            if (!$topic) {
                $responseArray = array("responseCode" => 400, "errorMsg" => "No se ha encontrado el tema solicitado");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Compruebo que pueda moderar en el foro
            if(!$this->get('security.context')->isGranted('ROLE_CAN_MOD_FORUM', $topic->getForum())){
                $responseArray = array( "responseCode" => 400,
                    "errorMsg" => "No tienes permisos suficientes para abrir/cerrar temas.");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            if($action == 'close'){
                // Si estuviese abierto lo cierro
                if ($topic->getIsClosed()==0) {
                    $this->get('deal.topicbundle.moderationtools.topiclock')->close($topic);

                    $responseArray = array( "responseCode" => 200,
                                            "successMsg" => "El tema ha sido cerrado con éxito",
                                            "action" => "ajaxTopicClose");
                    $jsonContent = $serializer->serialize($responseArray, 'json');
                }

                // Si ya estuviese cerrado informo de la situación
                else{
                    $responseArray = array( "responseCode" => 200,
                        "successMsg" => "El tema ha sido cerrado con éxito. Ya estaba cerrado.",
                        "action" => "ajaxTopicClose");
                    $jsonContent = $serializer->serialize($responseArray, 'json');
                }
                return new Response($jsonContent);
            }

            elseif($action == 'open'){

                $expiredInfo = '';
                $isExpired = false;

                // Compruebo si el tipo de tema es de los que puede finalizarse
                $topicType = $topic->getTopicType();
                if( ($topicType == 'TopicDeal') || ($topicType == 'TopicVoucher') || ($topicType == 'TopicFree')){
                    $isExpired = $topic->getIsExpired();

                    if($isExpired){
                        $expiredInfo = ' (El tema está expirado)';
                    }
                }

                // Si estuviese cerrado lo abro
                if ($topic->getIsClosed()==1) {
                    $this->get('deal.topicbundle.moderationtools.topiclock')->open($topic);

                    $responseArray = array( "responseCode" => 200,
                                            "successMsg" => "El tema ha sido abierto con éxito" . $expiredInfo,
                                            "action" => "ajaxTopicOpen",
                                            "isExpired" => $isExpired);
                    $jsonContent = $serializer->serialize($responseArray, 'json');
                }

                // Si ya estuviese abierto informo de la situación
                else{
                    $responseArray = array( "responseCode" => 200,
                                            "successMsg" => "El tema ha sido abierto con éxito. Ya estaba abierto" .
                                                            $expiredInfo,
                                            "action" => "ajaxTopicOpen",
                                            "isExpired" => $isExpired);
                    $jsonContent = $serializer->serialize($responseArray, 'json');
                }
                return new Response($jsonContent);
            }

            else{
                $responseArray = array( "responseCode" => 400,
                                        "errorMsg" =>   "Error, esta acción no se puede llevar a cabo");
                $jsonContent = $serializer->serialize($responseArray, 'json');
                return new Response($jsonContent);
            }
        }

        // En caso de no ser una petición AJAX retorno un mensaje de error
        return new Response('Esto no es ajax!', 400);
    }
}
