<?php

namespace Deal\TopicBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\CoreBundle\Validator\ErrorElement;

/*
 * Clase de configuración para la administración de los temas con etiquetas en el panel de administración
 * */
class TopicWithTagsAdmin extends TopicAdmin {

    // Campos que se van a mostrar en los formularios crear/editar entidades
    protected function configureFormFields(FormMapper $formMapper)
    {
        // Cargo las etiquetas
       $tagManager = $this->getTagManager();

        $tags = $this->hasSubject()
            ? $tagManager->loadTagging($this->getSubject())
            : array();

        parent::configureFormFields($formMapper);
        $formMapper
            ->with('Datos No Obligatorios')
            ->add('tags', 'entity', array(  'class' => 'Deal\TopicBundle\Entity\Tag',
                                            'choices'=> $tags,
                                            'required'=> false,
                                            'multiple'=>true,
                                            'label' => 'Etiquetas'))
        ->end()
        ;
    }

    // Limito el número de etiquetas a 3
    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
            ->with('tags')
            ->assertCount(array('max' => 3))
            ->end()
        ;
    }

    // Después del persist guardo el etiquetado
    public function postPersist($object) {
        $tagManager = $this->getTagManager();
        $tagManager->saveTagging($object);
    }

    // Después del update guardo el etiquetado
    public function postUpdate($object) {
        $tagManager = $this->getTagManager();
        $tagManager->saveTagging($object);
    }

    // Antes del remove borro el etiquetado y actualizo el número de temas/mensajes de los usuarios
    public function preRemove($object) {
        $tagManager = $this->getTagManager();
        $tagManager->deleteTagging($object);
        $container = $this->getConfigurationPool()->getContainer();
        $container->get('doctrine.orm.entity_manager')->flush();
        $container->get('deal.topicbundle.moderationtools.topicremover')->updateTopicsAndPostsCount($object);
    }

    // Devuelvo el tag manager
    protected function getTagManager(){
        return $this->getConfigurationPool()->getContainer()->get('fpn_tag.tag_manager');
    }
} 