<?php

namespace Deal\TopicBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\CoreBundle\Validator\ErrorElement;

/*
 * Clase de configuración para la administración de los temas de tipo oferta en el panel de administración
 * */
class TopicDealAdmin extends TopicAdmin {

    // Campos que se van a mostrar en los formularios crear/editar entidades
    protected function configureFormFields(FormMapper $formMapper)
    {
        // Cargo las etiquetas
        $tagManager = $this->getTagManager();

        $tags = $this->hasSubject()
            ? $tagManager->loadTagging($this->getSubject())
            : array();

        parent::configureFormFields($formMapper);
        $formMapper
            ->with('Datos Obligatorios')
            ->add('shop', null, array('class' => 'Deal\TopicBundle\Entity\Shop', 'label' => 'Tienda'))
            ->add('votes', 'number', array('label' => 'Suma de los votos'))
            ->add('url', 'url', array('label' => 'url'))
            ->end()
        ->with('Datos No Obligatorios')
            ->add('isExpired', 'checkbox', array('required'=> false, 'label' => '¿Finalizado?'))
            ->add('tags', 'entity', array(  'class' => 'Deal\TopicBundle\Entity\Tag',
                                        'choices'=> $tags,
                                        'required'=> false,
                                        'multiple'=>true,
                                        'label' => 'Etiquetas'))
            ->add('dateStart', 'datetime', array('required'=> false, 'label' => 'Fecha de inicio'))
            ->add('dateEnd', 'datetime', array('required'=> false, 'label' => 'Fecha de finalización'))
            ->add('dateMadePopular', 'datetime', array( 'required'=> false,
                                                        'label' => 'Fecha en la que se hizo popular'))
        ->end()
        ;

        $subject = $this->getSubject();

        // Si estoy creando el tema la imagen debe ser obligatoria
        if(!$subject->getId() > 0) {
            $formMapper
                ->with('Datos Obligatorios')
                ->add('topicImageFile', 'file', array( 'label' => 'Imagen'))
                ->end();
        }

        else{
            $formMapper
            ->with('Datos No Obligatorios')
            ->add('topicImageFile', 'file', array('required'=> false, 'label' => 'Imagen'))
            ->end();
        }
    }

    // Limito el número de etiquetas a 3
    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
            ->with('tags')
            ->assertCount(array('max' => 3))
            ->end()
        ;
    }

    // Campos a mostrar en los formularios para filtrar entidades
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        parent::configureDatagridFilters($datagridMapper);
        $datagridMapper
            ->add('isExpired', null, array('label' => '¿Finalizado?'))
            ->add('shop', null, array('class' => 'Deal\TopicBundle\Entity\Shop', 'label' => 'Tienda'))
            ;
    }

    // Campos para ser mostrados en los listados de entidades
    protected function configureListFields(ListMapper $listMapper)
    {
        parent::configureListFields($listMapper);
        $listMapper
            ->add('isExpired', 'boolean', array('label' => '¿Finalizado?'))
            ->add('shop', null, array('sortable'=>true, 'sort_field_mapping'=> array('fieldName'=>'id'),'sort_parent_association_mappings' => array(array('fieldName'=>'shop')), 'class' => 'Deal\TopicBundle\Entity\Shop', 'label' => 'Tienda'))
            ->add('dateMadePopular', 'datetime', array('label' => 'Fecha Pop.'))
            ->add('dateStart', 'datetime', array('label' => 'Fecha de inicio'))
            ->add('dateEnd', 'datetime', array('label' => 'Fecha de finalización'))
        ;
    }

    // Antes de guardar el tema compruebo si se ha cambiado la suma de las votos para actualizar la fecha de popularidad
    public function preUpdate($object)
    {
        parent::preUpdate($object);

        $container = $this->getConfigurationPool()->getContainer();
        $em = $container->get('doctrine.orm.entity_manager');

        $globalVariables = $em->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
        $minVotesToBePop = $globalVariables->getMinVotesToBePop();

        if($object->getVotes() >= $minVotesToBePop && $object->getDateMadePopular() == null){
            $object->setDateMadePopular(new \DateTime('now'));
        }

        elseif($object->getVotes() < $minVotesToBePop && $object->getDateMadePopular() != null){
            $object->setDateMadePopular(null);
        }
    }

    // Después del persist guardo el etiquetado
    public function postPersist($object) {
        $tagManager = $this->getConfigurationPool()->getContainer()->get('fpn_tag.tag_manager');
        $tagManager->saveTagging($object);
    }

    // Después del update guardo el etiquetado
    public function postUpdate($object) {
        $tagManager = $this->getConfigurationPool()->getContainer()->get('fpn_tag.tag_manager');
        $tagManager->saveTagging($object);
    }

    // Antes del remove borro el etiquetado y actualizo los temas/mensajes de los usuarios
    public function preRemove($object) {
        $container = $this->getConfigurationPool()->getContainer();
        $tagManager = $container->get('fpn_tag.tag_manager');
        $tagManager->deleteTagging($object);
        $container->get('doctrine.orm.entity_manager')->flush();
        $container->get('deal.topicbundle.moderationtools.topicremover')->updateTopicsAndPostsCount($object);
    }

    // Devuelvo el tag manager
    protected function getTagManager(){
        return $this->getConfigurationPool()->getContainer()->get('fpn_tag.tag_manager');
    }
}