<?php

namespace Deal\TopicBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;

/*
 * Clase de configuración para la administración de las categorías en el panel de administración
 * */
class CategoryAdmin extends Admin {

    // Campos que se van a mostrar en los formularios crear/editar entidades
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Datos Obligatorios')
            ->add('name', "text", array('label' => 'Nombre de la categoría'))
            ->end()
            ->with('Datos No Obligatorios')
            ->add('slug',"text", array('required'=> false, 'label' => 'Slug de la categoría'))
            ->end()
        ;
    }

    // Campos a mostrar en los formularios para filtrar entidades
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, array('label' => 'Id'))
            ->add('name', null, array('label' => 'Nombre de la categoría'))
        ;
    }

    // Campos para ser mostrados en los listados de entidades
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, array('label' => 'Id'))
            ->addIdentifier('name', null, array('label' => 'Nombre de la categoría'))
            ->add('slug',"text", array('label' => 'Slug de la categoría'))
        ;
    }

    // Después del remove actualizo los temas con la categoría Otros y borro la cache de doctrine
    public function postRemove($object) {
        $container = $this->getConfigurationPool()->getContainer();
        $container->get('deal.topicbundle.moderationtools.categoryremover')->changeTopicNullCategoryToOthers();

        // Borro la cache de datos
        $container->get('deal.forumbundle.service.doctrinecache')->remove();
    }

    // Después del persist borro la cache de doctrine
    public function postPersist($object) {
        $this->removeDoctrineCache();
    }

    // Después del update borro la cache de doctrine
    public function postUpdate($object) {
        $this->removeDoctrineCache();
    }

    // Funcion encargada de borrar la cache de doctrine
    protected function removeDoctrineCache(){
        $container = $this->getConfigurationPool()->getContainer();

        // Borro la cache de datos
        $container->get('deal.forumbundle.service.doctrinecache')->remove();
    }

    // Evito que se puedan borrar las categorías por batch
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('batch');
    }

} 