<?php

namespace Deal\TopicBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;

/*
 * Clase de configuración para la administración de los temas de tipo cupón descuento en el panel de administración
 * */
class TopicVoucherAdmin extends TopicDealAdmin {

    // Campos que se van a mostrar en los formularios crear/editar entidades
    protected function configureFormFields(FormMapper $formMapper)
    {
        parent::configureFormFields($formMapper);
        $formMapper
            ->with('Datos Obligatorios')
            ->add('voucher', 'text', array('label' => 'Cupón'))
            ->end()
        ->with('Datos No Obligatorios')
            ->add('voucherDiscount', 'text', array('required'=> false, 'label' => 'Descuento'))
            ->add('voucherMinimum', 'text', array('required'=> false, 'label' => 'Gasto Mínimo'))
        ->end()
        ;
    }

    // Campos para ser mostrados en los listados de entidades
    protected function configureListFields(ListMapper $listMapper)
    {
        parent::configureListFields($listMapper);
        $listMapper
            ->add('voucher', 'text', array('label' => 'Cupón'))
            ->add('voucherDiscount', 'text', array('label' => 'Descuento'))
            ->add('voucherMinimum', 'text', array('label' => 'Gasto Mínimo'))
        ;
    }

}