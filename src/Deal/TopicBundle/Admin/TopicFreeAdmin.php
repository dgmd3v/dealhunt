<?php

namespace Deal\TopicBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;

/*
 * Clase de configuración para la administración de los temas de tipo gratis en el panel de administración
 * */
class TopicFreeAdmin extends TopicDealAdmin {

// Esta clase hereda de la clase TopicDealAdmin
}