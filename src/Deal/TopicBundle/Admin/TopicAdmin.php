<?php

namespace Deal\TopicBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Doctrine\ORM\EntityRepository;

/*
 * Clase de configuración para la administración de los temas en el panel de administración
 * */
class TopicAdmin extends Admin {

    // Campos que se van a mostrar en los formularios crear/editar entidades
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Datos Obligatorios')
            ->add('title', 'text', array('label' => 'Título del tema'))
            ->add('forum', null, array(
                                    'class' => 'Deal\ForumBundle\Entity\Forum',
                                    'label' => 'Foro',
                                    'query_builder' =>
                                        function(EntityRepository $qb) {
                                            $topicType = $this->getSubject()->getTopicType();
                                            $query =
                                                $qb->createQueryBuilder('f')
                                                    ->join('f.forumTopicsType', 'ftt')
                                                    ->where('ftt.name =:forumTopicsType')
                                                    ->setParameter('forumTopicsType', $topicType)
                                            ;

                                            return $query;
                                        }
            ))
            ->add('category', null, array('class' => 'Deal\TopicBundle\Entity\Category', 'label' => 'Categoría'))
            ->add('firstPost', 'deal_topicbundle_backend_topicfirstposttype', array( 'label' => 'Texto del tema'))
            ->end()
            ->with('Datos No Obligatorios')
            ->add('isClosed', 'checkbox', array('required'=> false, 'label' => '¿Cerrado?'))
            ->add('slug', 'text', array('required'=> false, 'label' => 'Slug del tema'))
            ->end()
        ;
    }

    // Campos a mostrar en los formularios para filtrar entidades
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, array('label' => 'Id'))
            ->add('title', null, array('label' => 'Título'))
            ->add('forum', null, array('label' => 'Foro'))
            ->add('category', null, array('label' => 'Categoría'))
            ->add('isClosed', null, array('label' => '¿Cerrado?'))
        ;
    }

    // Campos para ser mostrados en los listados de entidades
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, array('label' => 'Id'))
            ->addIdentifier('title', null, array('label' => 'Título del tema'))
            ->add('forum', null, array( 'sortable'=>true,
                                        'sort_field_mapping'=> array('fieldName'=>'id'),
                                        'sort_parent_association_mappings' => array(array('fieldName'=>'forum')),
                                        'class' => 'Deal\ForumBundle\Entity\Forum',
                                        'label' => 'Foro'))
            ->add('category', null, array(  'sortable'=>true,
                                            'sort_field_mapping'=> array('fieldName'=>'id'),
                                            'sort_parent_association_mappings' => array(array('fieldName'=>'category')),
                                            'class' => 'Deal\TopicBundle\Entity\Category',
                                            'label' => 'Categoría'))
            ->add('slug', 'text', array('label' => 'Slug del tema'))
            ->add('isClosed', 'boolean', array('label' => '¿Cerrado?'))
        ;
    }

    // Extraigo el tipo de tema a partir de la clase usada
    public function getTopicTypeFromClass(){
        $class = $this->getClass();
        $pattern = '/(?:.+\\\\)+(.+)/i';

        preg_match($pattern, $class, $match);
        return $match[1];
    }

    // Sobrescribo esta función para que en el listado solo aparezcan temas del tipo solicitado
    public function createQuery($context = 'list')
    {
        $topicType = $this->getTopicTypeFromClass();

        $query = parent::createQuery($context);
        if($topicType == 'Topic'){
            $query->join('o.forum', 'f')
                ->join('f.forumTopicsType', 'ftt')
                ->andWhere('ftt.name =:type');
            $query->setParameter('type', $topicType);
        }

        return $query;
    }

    // Antes de guardar el tema debo rellenar el primer mensaje con la información necesaria, lo hago a través de un
    // servicio
    public function prePersist($object)
    {
        $container = $this->getConfigurationPool()->getContainer();

        $container->get('deal.postbundle.service.setfirstpost')->set($object, $object->getFirstPost());
    }

    // Antes del remove actualizo los post/temas del usuario
    public function preRemove($object) {
        $container = $this->getConfigurationPool()->getContainer();
        $container->get('deal.topicbundle.moderationtools.topicremover')->updateTopicsAndPostsCount($object);
    }

    // Antes de actualizar el mensaje compruebo si hay citas para crear las notificaciones pertinentes
    public function preUpdate($object){
        $container = $this->getConfigurationPool()->getContainer();
        $originalEntity = $container->get('doctrine.orm.entity_manager')->getUnitOfWork()->getOriginalEntityData($object->getFirstPost());
        $newFirstPost = $object->getFirstPost();
        $oldFirstPostMessage = $originalEntity['message'];

        $container->get('deal.postbundle.service.postquote')->editPostQuotes( $newFirstPost,
            $oldFirstPostMessage);
    }

} 