<?php

namespace Deal\TopicBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;

/*
 * Clase de configuración para la administración de las etiquetas en el panel de administración
 * */
class TagAdmin extends Admin {

    // Campos que se van a mostrar en los formularios crear/editar entidades
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Datos Obligatorios')
            ->add('name', "text", array('label' => 'Nombre de la etiqueta'))
            ->end()
            ->with('Datos No Obligatorios')
            ->add('slug',"text", array('required'=> false, 'label' => 'Slug de la etiqueta'))
            ->end()
        ;
    }

    // Campos a mostrar en los formularios para filtrar entidades
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, array('label' => 'Id'))
            ->add('name', null, array('label' => 'Nombre de la etiqueta'))
        ;
    }

    // Campos para ser mostrados en los listados de entidades
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, array('label' => 'Id'))
            ->addIdentifier('name', null, array('label' => 'Nombre de la etiqueta'))
            ->add('slug',"text", array('label' => 'Slug de la etiqueta'))
        ;
    }

    // Antes de guardar la etiqueta debo asignarle un slug, lo hago a través de un
    // servicio
    public function prePersist($object)
    {
        $tagName = $object->getName();

        $container = $this->getConfigurationPool()->getContainer();

        $tagSlug = $container->get('fpn_tag.slugifier.default')->slugify($tagName);

        $object->setSlug($tagSlug);
    }
} 