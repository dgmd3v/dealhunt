<?php

namespace Deal\ForumBundle\ModerationTools;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Deal\ForumBundle\Entity\Forum as Forum;

/*
 * Clase encargada de borrar foros enteros
 **/
class ForumRemover {

    // Inyecto el EntityManager para poder hacer operaciones en la base de datos
    protected $em;

    // Inyecto el contenedor de dependencias para acceder a servicios
    protected $container;

    public function __construct(Container $container, EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->container = $container;
    }

    /*
     * Función encargada de borrar los temas de un foro dado
     *
     * @param Forum $forum foro del cual queremos borrar los temas
     * */
    public function removeForumTopics(Forum $forum){

        $repository = 'TopicBundle:' . $forum->getForumTopicsType();

        //Busco todos los temas del foro
        $topics = $this->em->getRepository($repository)->findAllTopicsByForum($forum);

        // Si hay temas en los foros los borro
        if($topics != null){
            foreach ($topics as $topic) {
                // Llama al servicio que actualiza la cuenta de los mensajes y temas antes de borrar
                $this->container->get('deal.topicbundle.moderationtools.topicremover')
                                                                             ->updateTopicsAndPostsCount($topic, false);
            }
        }

        // No borro el foro aquí, lo borra el panel de administración

        // Guardo los cambios en la base de datos
        $this->em->flush();
    }


} 