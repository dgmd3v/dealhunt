<?php

namespace Deal\ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Role
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Deal\ForumBundle\Entity\RoleRepository")
 * @UniqueEntity(fields={"name"}, message="Ya existe un rol con el mismo nombre.")
 */
class Role
{
    // *************** COLUMNAS DE LA TABLA ***************

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30)
     * @Assert\Length(max = 30, maxMessage = "El número máximo de caracteres que se pueden escribir para el nombre del rol son {{ limit }}")
     */
    private $name;

    // *************** SETTERS Y GETTERS ***************

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Role
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    // Método mágico para que PHP sepa como convertir la entidad en una cadena de texto
    public function __toString()
    {
        return (string) $this->getName();
    }
}
