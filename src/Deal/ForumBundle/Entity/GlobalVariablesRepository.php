<?php

namespace Deal\ForumBundle\Entity;

use Doctrine\ORM\EntityRepository;

/*
 * Clase que sirve como repositorio para las consultas relacionadas con las variables globales
 * */
class GlobalVariablesRepository extends EntityRepository
{
    /*
     * Devuelve todas las variables globales
     **/
    public function findAllGlobalVariables()
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Genero la consulta
        $dql = (  ' SELECT g
                      FROM ForumBundle:GlobalVariables g');

        $query = $em->createQuery($dql);

        // Limito los resultados
        $query->setMaxResults(1);

        // Uso la cache para guardar este resultado (durante 604800s = 1 semana )
        $query->useResultCache(true, 604800);

        // Retorno el resultado de la consulta
        return $query->getSingleResult();
    }
}
