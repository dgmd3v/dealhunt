<?php

namespace Deal\ForumBundle\Entity;

use Doctrine\ORM\EntityRepository;

/*
 * Clase que sirve como repositorio para las consultas relacionadas con los foros
 * */
class ForumRepository extends EntityRepository
{
    /*
     * Devuelve todos los foros existentes
     **/
    public function findAllForums()
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Genero la consulta
        $dql = (  ' SELECT f, ar, ftt
                      FROM ForumBundle:Forum f
                      JOIN f.forumTopicsType ftt
                      JOIN f.accessRole ar
                  ORDER BY f.id ASC');

        $query = $em->createQuery($dql);

        // Uso la cache para guardar este resultado (durante 604800s = 1 semana )
        $query->useResultCache(true, 604800);

        // Retorno el resultado de la consulta
        return $query->getResult();
    }

    /*
     * Devuelve todos los foros existentes a los que puede acceder el usuario dependiendo de sus roles
     *
     * @param array $roles array con los roles del usuario
     **/
    public function findAllForumsByRoles($roles)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Genero la consulta
        $dql = (  ' SELECT f, ar
                      FROM ForumBundle:Forum f
                      JOIN f.accessRole ar
                     WHERE ar.name IN (:roles)
                  ORDER BY f.id ASC');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'roles' => $roles,
        ));

        // Uso la cache para guardar este resultado (durante 604800s = 1 semana )
        $query->useResultCache(true, 604800);

        // Retorno el resultado de la consulta
        return $query->getResult();
    }
}