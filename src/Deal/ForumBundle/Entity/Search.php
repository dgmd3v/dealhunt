<?php

namespace Deal\ForumBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

/*
 * En esta clase creo la estructura para el formulario de búsqueda
 * */
class Search
{
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(max = 100, maxMessage = "El número máximo de caracteres que se pueden escribir en las búsquedas son {{ limit }}")
     * @Assert\Regex(
     *     pattern="/^[a-zA-Z0-9ñÑáÁéÉíÍóÓúÚ\-\s]+$/",
     *     match=true,
     *     message="Tu búsqueda solo puede contener letras, números y guiones (-)"
     * )
     */
    private $searchTerm;

    /**
     * @var integer
     *
     */
    private $forum = null;

    /**
     * @var integer
     *
     */
    private $category = null;

    /**
     * @var integer
     *
     */
    private $order;

    /**
     * @var integer
     *
     */
    private $searchInterval;

    public function __construct() {
        $this->forum = null;
        $this->category = null;
        $this->order = 1;
        $this->searchInterval = 1;
    }

    // *************** SETTERS Y GETTERS ***************

    /**
     * Set searchTerm
     *
     * @param string $searchTerm
     * @return Search
     */
    public function setSearchTerm($searchTerm)
    {
        $this->searchTerm = $searchTerm;

        return $this;
    }

    /**
     * Get searchTerm
     *
     * @return string 
     */
    public function getSearchTerm()
    {
        return $this->searchTerm;
    }

    /**
     * Set forum
     *
     * @param Deal\ForumBundle\Entity\Forum $forum
     * @return Search
     */
    public function setForum(\Deal\ForumBundle\Entity\Forum $forum)
    {
        $this->forum = $forum;

        return $this;
    }

    /**
     * Get forum
     *
     * @return integer
     */
    public function getForum()
    {
        return $this->forum;
    }

    /**
     * Set category
     *
     * @param Deal\TopicBundle\Entity\Category $category
     * @return Search
     */
    public function setCategory(\Deal\TopicBundle\Entity\Category $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Deal\TopicBundle\Entity\Category $category
     */
    public function getCategory()
    {
        return $this->category;
    }


    /**
     * Set order
     *
     * @param integer $order
     * @return Search
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return $order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set searchInterval
     *
     * @param integer $searchInterval
     * @return Search
     */
    public function setSearchInterval($searchInterval)
    {
        $this->searchInterval = $searchInterval;

        return $this;
    }

    /**
     * Get searchInterval
     *
     * @return $searchInterval
     */
    public function getSearchInterval()
    {
        return $this->searchInterval;
    }
}
