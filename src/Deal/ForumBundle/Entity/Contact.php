<?php

namespace Deal\ForumBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

/*
 * En esta clase creo la estructura para el formulario de contacto
 * */
class Contact
{
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(max = 255, maxMessage = "El número máximo de caracteres que se pueden escribir para el nombre son {{ limit }}")
     */
    private $name;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(max = 255, maxMessage = "El número máximo de caracteres que se pueden escribir para la dirección de correo son {{ limit }}")
     * @Assert\Email(
     *     message = "La dirección de correo no es valida.",
     *     checkMX = true
     * )
     */
    private $email;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(max = 255, maxMessage = "El número máximo de caracteres que se pueden escribir para el motivo del contacto {{ limit }}")
     */
    private $subject;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(max = 3000, maxMessage = "El número máximo de caracteres que se pueden escribir en el correo son {{ limit }}")
     */
    private $message;

    // *************** SETTERS Y GETTERS ***************

    /**
     * Set name
     *
     * @param string $name
     * @return Contact
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Contact
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return Contact
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Contact
     */
    public function setMessage($message)
    {
        $this->message = $message;
    
        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }
}
