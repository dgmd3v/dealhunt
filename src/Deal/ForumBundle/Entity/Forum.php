<?php

namespace Deal\ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Deal\ForumBundle\Util\Util;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Forum
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Deal\ForumBundle\Entity\ForumRepository")
 * @Serializer\ExclusionPolicy("all")
 * @UniqueEntity(fields= "slug", message="Ya existe un foro con el mismo slug.")
 * @UniqueEntity(fields="name", message="Ya existe un foro con el mismo nombre.")
 */
class Forum
{
    // *************** COLUMNAS DE LA TABLA ***************    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    // ID del foro
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Serializer\Expose
     * @Assert\Length(max = 255, maxMessage = "El número máximo de caracteres que se pueden escribir para el nombre del foro son {{ limit }}")
     */
    // Nombre del foro
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     * @Serializer\Expose
     * @Assert\Length(max = 255, maxMessage = "El número máximo de caracteres que se pueden escribir para el slug son {{ limit }}")
     */
    // Slug del foro
    private $slug;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Deal\ForumBundle\Entity\Role")
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id", nullable=false)
     */
    // Role necesario para acceder al contenido del foro
    private $accessRole;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Deal\ForumBundle\Entity\TopicsType")
     * @ORM\JoinColumn(name="topicstype_id", referencedColumnName="id", nullable=false)
     */
    // Tipo de temas que contiene el foro
    private $forumTopicsType;

    // *************** SETTERS Y GETTERS ***************    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Forum
     */
    public function setName($name)
    {
        $this->name = $name;
        
        // A la vez que le doy nombre al foro le pongo un slug
        $this->slug = Util::getSlug($name);
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Forum
     */
    public function setSlug($slug)
    {
        if($slug == null){
            $this->slug = Util::getSlug($this->name);
        }

        else{
            $this->slug = Util::getSlug($slug);
        }

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set role
     *
     * @param Deal\ForumBundle\Entity\Role $role
     * @return Forum
     */
    // Esta función espera que se le pase un objeto de tipo Role
    public function setAccessRole(\Deal\ForumBundle\Entity\Role $accessRole)
    {
        $this->accessRole = $accessRole;

        return $this;
    }

    /**
     * Get role
     *
     * @return integer
     */
    public function getAccessRole()
    {
        return $this->accessRole;
    }

    /**
     * Set forumTopicsType
     *
     * @param Deal\ForumBundle\Entity\Role $forumTopicsType
     * @return Forum
     */
    // Esta función espera que se le pase un objeto de tipo TopicsType
    public function setForumTopicsType(\Deal\ForumBundle\Entity\TopicsType $forumTopicsType)
    {
        $this->forumTopicsType = $forumTopicsType;

        return $this;
    }

    /**
     * Get forumTopicsType
     *
     * @return integer
     */
    public function getForumTopicsType()
    {
        return $this->forumTopicsType;
    }
    
    // Método mágico para que PHP sepa como convertir la entidad en una cadena de texto
    public function __toString()
    {
        return $this->getName();
    }
}
