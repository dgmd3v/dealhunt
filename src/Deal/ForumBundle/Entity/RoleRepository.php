<?php

namespace Deal\ForumBundle\Entity;

use Doctrine\ORM\EntityRepository;

/*
 * Clase que sirve como repositorio para las consultas relacionadas con los roles
 * */
class RoleRepository extends EntityRepository
{
    /*
     * Devuelve el role solicitado por su nombre
     *
     * @param string $roleName nombre del role del cual queremos información
     **/
    public function findRoleByName($roleName)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Genero la consulta
        $dql = (  ' SELECT r
                      FROM ForumBundle:Role r
                     WHERE r.name = :roleName');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'roleName' => $roleName
        ));

        // Limito los resultados
        $query->setMaxResults(1);

        // Uso la cache para guardar este resultado (durante 604800s = 1 semana )
        $query->useResultCache(true, 604800);

        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }
}
