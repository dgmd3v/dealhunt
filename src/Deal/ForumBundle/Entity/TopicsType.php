<?php

namespace Deal\ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * TopicsType
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Deal\ForumBundle\Entity\TopicsTypeRepository")
 * @UniqueEntity(fields={"name"}, message="Ya existe un tipo de temas con este nombre.")
 */
class TopicsType
{
    // *************** COLUMNAS DE LA TABLA ***************

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\Length(max = 255, maxMessage = "El número máximo de caracteres que se pueden escribir para el nombre son {{ limit }}")
     */
    private $name;

    // *************** SETTERS Y GETTERS ***************

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return TopicsType
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    // Método mágico para que PHP sepa como convertir la entidad en una cadena de texto
    public function __toString()
    {
        return $this->getName();
    }
}
