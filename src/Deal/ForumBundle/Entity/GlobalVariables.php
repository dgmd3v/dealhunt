<?php

namespace Deal\ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * GlobalVariables
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Deal\ForumBundle\Entity\GlobalVariablesRepository")
 */
class GlobalVariables
{
    // *************** COLUMNAS DE LA TABLA *************** 
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="max_topics_per_page", type="integer")
     * @Assert\NotBlank()
     * @Assert\GreaterThan(value = 0)
     */
    protected $maxTopicsPerPage;

    /**
     * @var integer
     *
     * @ORM\Column(name="max_search_result_topics_per_page", type="integer")
     * @Assert\NotBlank()
     * @Assert\GreaterThan(value = 0)
     */
    protected $maxSearchResultTopicsPerPage;

    /**
     * @var integer
     *
     * @ORM\Column(name="max_posts_per_topic_page", type="integer")
     * @Assert\NotBlank()
     * @Assert\GreaterThan(value = 0)
     */
    protected $maxPostsPerTopicPage;

    /**
     * @var integer
     *
     * @ORM\Column(name="max_signatures_per_page", type="integer")
     * @Assert\NotBlank()
     * @Assert\GreaterThan(value = 0)
     */
    protected $maxSignaturesPerPage;

    /**
     * @var integer
     *
     * @ORM\Column(name="max_profile_signatures", type="integer")
     * @Assert\NotBlank()
     * @Assert\GreaterThan(value = 0)
     */
    protected $maxProfileSignatures;

    /**
     * @var integer
     *
     * @ORM\Column(name="max_favorites_per_page", type="integer")
     * @Assert\NotBlank()
     * @Assert\GreaterThan(value = 0)
     */
    protected $maxFavoritesPerPage;

    /**
     * @var integer
     *
     * @ORM\Column(name="notifications_per_page", type="integer")
     * @Assert\NotBlank()
     * @Assert\GreaterThan(value = 0)
     */
    protected $notificationsPerPage;

    /**
     * @var integer
     *
     * @ORM\Column(name="reports_per_page", type="integer")
     * @Assert\NotBlank()
     * @Assert\GreaterThan(value = 0)
     */
    protected $reportsPerPage;

    /**
     * @var integer
     *
     * @ORM\Column(name="max_reports_per_day", type="integer")
     * @Assert\NotBlank()
     * @Assert\GreaterThan(value = 0)
     */
    protected $maxReportsPerDay;

    /**
     * @var integer
     *
     * @ORM\Column(name="min_votes_to_be_pop", type="integer")
     * @Assert\NotBlank()
     * @Assert\GreaterThan(value = 0)
     */
    protected $minVotesToBePop;

    /**
     * @var integer
     *
     * @ORM\Column(name="number_of_user_last_posts", type="integer")
     * @Assert\NotBlank()
     * @Assert\GreaterThan(value = 0)
     */
    protected $numberOfUserLastPosts;

    // *************** SETTERS Y GETTERS ***************  
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set maxTopicsPerPage
     *
     * @param integer $maxTopicsPerPage
     * @return GlobalVariables
     */
    public function setMaxTopicsPerPage($maxTopicsPerPage)
    {
        $this->maxTopicsPerPage = $maxTopicsPerPage;

        return $this;
    }

    /**
     * Get maxTopicsPerPage
     *
     * @return integer 
     */
    public function getMaxTopicsPerPage()
    {
        return $this->maxTopicsPerPage;
    }

    /**
     * Set maxSearchResultTopicsPerPage
     *
     * @param integer $maxSearchResultTopicsPerPage
     * @return GlobalVariables
     */
    public function setMaxSearchResultTopicsPerPage($maxSearchResultTopicsPerPage)
    {
        $this->maxSearchResultTopicsPerPage = $maxSearchResultTopicsPerPage;

        return $this;
    }

    /**
     * Get maxSearchResultTopicsPerPage
     *
     * @return integer 
     */
    public function getMaxSearchResultTopicsPerPage()
    {
        return $this->maxSearchResultTopicsPerPage;
    }

    /**
     * Set maxPostsPerTopicPage
     *
     * @param integer $maxPostsPerTopicPage
     * @return GlobalVariables
     */
    public function setMaxPostsPerTopicPage($maxPostsPerTopicPage)
    {
        $this->maxPostsPerTopicPage = $maxPostsPerTopicPage;

        return $this;
    }

    /**
     * Get maxPostsPerTopicPage
     *
     * @return integer 
     */
    public function getMaxPostsPerTopicPage()
    {
        return $this->maxPostsPerTopicPage;
    }

    /**
     * Set maxSignaturesPerPage
     *
     * @param integer $maxSignaturesPerPage
     * @return GlobalVariables
     */
    public function setMaxSignaturesPerPage($maxSignaturesPerPage)
    {
        $this->maxSignaturesPerPage = $maxSignaturesPerPage;

        return $this;
    }

    /**
     * Get maxSignaturesPerPage
     *
     * @return integer 
     */
    public function getMaxSignaturesPerPage()
    {
        return $this->maxSignaturesPerPage;
    }

    /**
     * Set maxProfileSignatures
     *
     * @param integer $maxProfileSignatures
     * @return GlobalVariables
     */
    public function setMaxProfileSignatures($maxProfileSignatures)
    {
        $this->maxProfileSignatures = $maxProfileSignatures;

        return $this;
    }

    /**
     * Get maxProfileSignatures
     *
     * @return integer 
     */
    public function getMaxProfileSignatures()
    {
        return $this->maxProfileSignatures;
    }

    /**
     * Set maxFavoritesPerPage
     *
     * @param integer $maxFavoritesPerPage
     * @return GlobalVariables
     */
    public function setMaxFavoritesPerPage($maxFavoritesPerPage)
    {
        $this->maxFavoritesPerPage = $maxFavoritesPerPage;

        return $this;
    }

    /**
     * Get maxFavoritesPerPage
     *
     * @return integer 
     */
    public function getMaxFavoritesPerPage()
    {
        return $this->maxFavoritesPerPage;
    }

    /**
     * Set notificationsPerPage
     *
     * @param integer $notificationsPerPage
     * @return GlobalVariables
     */
    public function setNotificationsPerPage($notificationsPerPage)
    {
        $this->notificationsPerPage = $notificationsPerPage;

        return $this;
    }

    /**
     * Get notificationsPerPage
     *
     * @return integer 
     */
    public function getNotificationsPerPage()
    {
        return $this->notificationsPerPage;
    }

    /**
     * Set reportsPerPage
     *
     * @param integer $reportsPerPage
     * @return GlobalVariables
     */
    public function setReportsPerPage($reportsPerPage)
    {
        $this->reportsPerPage = $reportsPerPage;

        return $this;
    }

    /**
     * Get reportsPerPage
     *
     * @return integer 
     */
    public function getReportsPerPage()
    {
        return $this->reportsPerPage;
    }

    /**
     * Set maxReportsPerDay
     *
     * @param integer $maxReportsPerDay
     * @return GlobalVariables
     */
    public function setMaxReportsPerDay($maxReportsPerDay)
    {
        $this->maxReportsPerDay = $maxReportsPerDay;

        return $this;
    }

    /**
     * Get maxReportsPerDay
     *
     * @return integer 
     */
    public function getMaxReportsPerDay()
    {
        return $this->maxReportsPerDay;
    }

    /**
     * Set minVotesToBePop
     *
     * @param integer $minVotesToBePop
     * @return GlobalVariables
     */
    public function setMinVotesToBePop($minVotesToBePop)
    {
        $this->minVotesToBePop = $minVotesToBePop;

        return $this;
    }

    /**
     * Get minVotesToBePop
     *
     * @return integer 
     */
    public function getMinVotesToBePop()
    {
        return $this->minVotesToBePop;
    }

    /**
     * Set numberOfUserLastPosts
     *
     * @param integer $numberOfUserLastPosts
     * @return GlobalVariables
     */
    public function setNumberOfUserLastPosts($numberOfUserLastPosts)
    {
        $this->numberOfUserLastPosts = $numberOfUserLastPosts;

        return $this;
    }

    /**
     * Get numberOfUserLastPosts
     *
     * @return integer 
     */
    public function getNumberOfUserLastPosts()
    {
        return $this->numberOfUserLastPosts;
    }

}
