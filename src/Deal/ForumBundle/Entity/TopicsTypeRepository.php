<?php

namespace Deal\ForumBundle\Entity;

use Doctrine\ORM\EntityRepository;

/*
 * Clase que sirve como repositorio para las consultas relacionadas con los tipos de tema
 * */
class TopicsTypeRepository extends EntityRepository
{
    /**
     * Devuelve el tipo de tema de un foro solicitado por su nombre
     *
     * @param string $topicsTypeName nombre del tipo de tema de un foro del cual queremos información
     **/
    public function findTopicsTypeByName($topicsTypeName)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getEntityManager();

        // Genero la consulta
        $dql = (  ' SELECT tt
                      FROM ForumBundle:TopicsType tt
                     WHERE tt.name = :topicsTypeName');

        $query = $em->createQuery($dql);

        // Parámetros de la consulta
        $query->setParameters(array(
            'topicsTypeName' => $topicsTypeName
        ));

        // Limito los resultados
        $query->setMaxResults(1);

        // Uso la cache para guardar este resultado (durante 604800s = 1 semana )
        $query->useResultCache(true, 604800);

        // Retorno el resultado de la consulta
        return $query->getOneOrNullResult();
    }
}
