<?php

namespace Deal\ForumBundle\Util;

/**
 * Clase donde guardo utilidades/funciones que utilizare en cualquier lugar de la aplicación
**/
class Util
{
    /**
     * Función para obtener el slug a partir de una palabra o texto dado, sobre todo la utilizaré para urls
     *
     * @param string $text texto a transformar en slug
     * @param string $delimiter delimitador/separador que usaremos para sustituir los caracteres / _|+ -
     * (espacios incluidos)
     **/
    static public function getSlug($text, $delimiter = '-')
    {
        // Código extraído de:
        // http://cubiq.org/the-perfect-php-clean-url-generator
        $slug = iconv('UTF-8', 'ASCII//TRANSLIT', $text);
        $slug = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $slug);
        $slug = strtolower(trim($slug, $delimiter));
        $slug = preg_replace("/[\/_|+ -]+/", $delimiter, $slug);

        return $slug;
    }

}
