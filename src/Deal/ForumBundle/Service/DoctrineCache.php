<?php

namespace Deal\ForumBundle\Service;

use Doctrine\ORM\EntityManager;

/*
 * Clase con utilidades para el manejo del la cache de doctrine
 **/
class DoctrineCache {

    // Inyecto el entityManager
    protected $em;

    public function __construct(EntityManager $entityManager) {
        $this->em = $entityManager;
    }

    /*
     * Función encargada de borrar la cache de doctrine (consultas, resultados, etc)
     **/
    public function remove(){

        $cacheDriver = $this->em->getConfiguration()->getMetadataCacheImpl();

        $cacheDriver->deleteAll();
    }
} 