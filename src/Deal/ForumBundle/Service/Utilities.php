<?php

namespace Deal\ForumBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

/*
 * Clase con utilidades que voy a utilizar en diferentes partes del proyecto
 **/
class Utilities {

    // Inyecto el contenedor de servicios
    protected $container;

    public function __construct(Container $container) {
        $this->container = $container;
    }

    /*
     * Devuelve el foro solicitado por slug o null si no se encuentra
     **/
    public function searchRequestedForum ($forumSlug){

        $forums = $this->container->get('doctrine.orm.entity_manager')->getRepository('ForumBundle:Forum')->findAllForums();

        $forum = null;
        foreach ($forums as $f) {
            if($f->getSlug()==$forumSlug) $forum = $f;
        }

        return $forum;
    }

    /*
     * Devuelve la categoría solicitada por slug o null si no se encuentra
     **/
    public function searchRequestedCategory ($categorySlug){

        $categories = $this->container->get('doctrine.orm.entity_manager')->getRepository('TopicBundle:Category')->findAllCategories();

        $category = null;
        foreach ($categories as $c) {
            if($c->getSlug()==$categorySlug) $category = $c;
        }

        return $category;
    }

    /*
     * Función encargada de calcular la página en la que se encuentra un post dado su número de post
     * */
    public function postPageNumber($postNumber)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->container->get('doctrine.orm.entity_manager');
        $globalVariables = $em->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
        $maxPostsPerTopicPage = $globalVariables->getMaxPostsPerTopicPage();

        if($postNumber == 1) $postNumber++;

        return ceil( ($postNumber-1) / $maxPostsPerTopicPage);
    }
} 