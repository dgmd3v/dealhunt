<?php

namespace Deal\ForumBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Deal\ForumBundle\Entity\Forum;

/*
 * Fixtures/Datos de prueba para la entidad forum (foro)
 * En este fixture creo los foros, les doy un nombre, slug, etc
 **/
class Forums extends AbstractFixture implements OrderedFixtureInterface
{
    // Orden en el que se carga este fixture
    public function getOrder()
    {
        return 80;
    }

    public function load(ObjectManager $manager)
    {
        // Array con los nombres de los foros
        $forums = array     (   'Ofertas',
                                'Cupones',
                                'Gratis',
                                'Miscelánea',
                                'Feedback',
                                'Moderación',
                                'Administración',
        );

        // Busco el role de moderador y el de usuario, todos los foros tendrán ROLE_USER excepto el de moderación que
        // tendrá ROLE_MODERATOR y el de administración que tendrá ROLE_ADMIN
        $administratorRole = $manager->getRepository('ForumBundle:Role')->findRoleByName('ROLE_ADMIN');
        $moderatorRole = $manager->getRepository('ForumBundle:Role')->findRoleByName('ROLE_MODERATOR');
        $userRole = $manager->getRepository('ForumBundle:Role')->findRoleByName('ROLE_USER');
        $topicDealType = $manager->getRepository('ForumBundle:TopicsType')->findTopicsTypeByName('TopicDeal');
        $topicFreeType = $manager->getRepository('ForumBundle:TopicsType')->findTopicsTypeByName('TopicFree');
        $topicVoucherType = $manager->getRepository('ForumBundle:TopicsType')->findTopicsTypeByName('TopicVoucher');
        $topicType = $manager->getRepository('ForumBundle:TopicsType')->findTopicsTypeByName('Topic');
        $topicWithTagsType = $manager->getRepository('ForumBundle:TopicsType')->findTopicsTypeByName('TopicWithTags');

        // Creo nuevas entidades de tipo foro y relleno con su nombre, su slug, y el número de temas por página
        foreach ($forums as $forum){
            $entity = new Forum();

            $entity->setName($forum);

            switch($forum){
                case 'Moderación':
                    $entity->setAccessRole($moderatorRole);
                    $entity->setForumTopicsType($topicType);
                    break;

                case 'Administración':
                    $entity->setAccessRole($administratorRole);
                    $entity->setForumTopicsType($topicType);
                    break;

                default:
                    $entity->setAccessRole($userRole);
                    switch($forum){
                        case 'Ofertas':
                            $entity->setForumTopicsType($topicDealType);
                            break;

                        case 'Cupones':
                            $entity->setForumTopicsType($topicVoucherType);
                            break;

                        case 'Gratis':
                            $entity->setForumTopicsType($topicFreeType);
                            break;

                        default:
                            $entity->setForumTopicsType($topicWithTagsType);
                            break;
                    }
            }

            // Hago la entidad persistente
            $manager->persist($entity);

        }// Fin foreach

        // Guardo los datos marcados como persistentes
        $manager->flush();
    }
}
