<?php

namespace Deal\ForumBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Deal\ForumBundle\Entity\GlobalVariables as GlobalVariables;

/*
 * Fixtures/Datos de prueba para la entidad GlobalVariables (variables globales)
 * En este fixture una tabla con las variables globales usadas en el foro
 **/
class GlobalVariablesFixture extends AbstractFixture implements OrderedFixtureInterface
{
    // Orden en el que se carga este fixture
    public function getOrder()
    {
        return 2;
    }

    public function load(ObjectManager $manager)
    {
        $entity = new GlobalVariables();

        // "Seteo" las variables globales
        $entity->setMaxTopicsPerPage(10);
        $entity->setMaxSearchResultTopicsPerPage(10);
        $entity->setMaxPostsPerTopicPage(15);
        $entity->setMaxSignaturesPerPage(15);
        $entity->setMaxProfileSignatures(5);
        $entity->setMaxFavoritesPerPage(10);
        $entity->setNotificationsPerPage(10);
        $entity->setReportsPerPage(15);
        $entity->setMaxReportsPerDay(10);
        $entity->setMinVotesToBePop(1);
        $entity->setNumberOfUserLastPosts(10);

        // Hago la entidad persistente
        $manager->persist($entity);

        // Guardo los datos marcados como persistentes
        $manager->flush();
    }
}
