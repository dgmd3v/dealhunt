<?php

namespace Deal\ForumBundle\DataFixtures\ORM;

use Deal\ForumBundle\Entity\TopicsType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/*
 * Fixtures/Datos de prueba para la entidad TopicsType (foro), tipos de temas (Ofertas, normales (topic), gratis, etc)
 * En este fixture creo la entidad y le doy un nombre
 **/
class TopicsTypes extends AbstractFixture implements OrderedFixtureInterface
{
    // Orden en el que se carga este fixture
    public function getOrder()
    {
        return 70;
    }

    public function load(ObjectManager $manager)
    {
        // Array con los nombres de los tipos de temas
        $topicTypes = array     (   'Topic',
                                    'TopicWithTags',
                                    'TopicDeal',
                                    'TopicFree',
                                    'TopicVoucher',
        );


        // Creo nuevas entidades de tipo de tema y relleno con su nombre
        foreach ($topicTypes as $topicType){
            $entity = new TopicsType();

            $entity->setName($topicType);

            // Hago la entidad persistente
            $manager->persist($entity);

        }// Fin foreach

        // Guardo los datos marcados como persistentes
        $manager->flush();
    }
}
