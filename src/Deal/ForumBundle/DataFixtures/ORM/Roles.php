<?php

namespace Deal\ForumBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Deal\ForumBundle\Entity\Role;

/*
 * Fixtures/Datos de prueba para la entidad role (rol)
 * En este fixture creo los roles y les doy un nombre
**/
class Roles extends AbstractFixture implements OrderedFixtureInterface
{
    // Orden en el que se carga este fixture
    public function getOrder()
    {
        return 75;
    }
   
    public function load(ObjectManager $manager)
    {
        // Array con los nombres de los roles
        $roles = array     (    'ROLE_ADMIN',
                                'ROLE_MODERATOR',
                                'ROLE_USER',
                            );
        
        // Creo nuevas entidades de tipo role y relleno con su nombre
        foreach ($roles as $role){
            $entity = new Role();

            $entity->setName($role);
            
            // Hago la entidad persistente
            $manager->persist($entity);
            
        }// Fin foreach
        
        // Guardo los datos marcados como persistentes
        $manager->flush();
    }
}