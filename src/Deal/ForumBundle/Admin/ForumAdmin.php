<?php

namespace Deal\ForumBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;

// Quito el máximo debido a que borrar un foro puede llevar mucho tiempo
ini_set("max_execution_time", 0);

/*
 * Clase de configuración para la administración de los foros en el panel de administración
 * */
class ForumAdmin extends Admin {

    //  Campos que se van a mostrar en los formularios crear/editar entidades
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Datos Obligatorios')
            ->add('name', 'text', array('label' => 'Nombre del foro'))
            ->add('accessRole', null, array('required' => true,
                                            'class' => 'Deal\ForumBundle\Entity\Role',
                                            'label' => 'Role necesario para acceder')
            )
            ->end()
            ->with('Datos No Obligatorios')
            ->add('slug', null, array('required' => false,'label' => 'Slug del foro'))
            ->end();


        // Muestro este campo solo si estoy creando la entidad
        if (!$this->getSubject()->getId() > 0) {
            $formMapper
                ->with('Datos Obligatorios')
                ->add('forumTopicsType', null, array(   'required' => true,
                                                        'label' => 'Tipo de temas que contiene el foro',
                                                        'class' => 'Deal\ForumBundle\Entity\TopicsType',
                ))
                ->end()
            ;
        }
    }

    // Campos a mostrar en los formularios para filtrar entidades
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, array('label' => 'Id'))
            ->add('name', null, array('label' => 'Nombre'))
            ->add('accessRole', null, array('label' => 'Role necesario para acceder'))
            ->add('forumTopicsType', null, array('label' => 'Tipo de temas que contiene el foro'))
        ;
    }

    // Campos para ser mostrados en los listados de entidades
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, array('label' => 'Id'))
            ->addIdentifier('name', null, array('label' => 'Nombre del foro'))
            ->add('slug', 'text', array('label' => 'Slug del foro'))
            ->add('accessRole', null, array(
                                    'sortable' => true,
                                    'sort_field_mapping' => array('fieldName'=>'id'),
                                    'sort_parent_association_mappings' => array(array('fieldName'=>'accessRole')),
                                    'class' => 'Deal\ForumBundle\Entity\Role',
                                    'label' => 'Role necesario para acceder'))
            ->add('forumTopicsType', null, array(
                                    'sortable' => true,
                                    'sort_field_mapping' => array('fieldName' => 'id'),
                                    'sort_parent_association_mappings' => array(array('fieldName'=>'forumTopicsType')),
                                    'class' => 'Deal\ForumBundle\Entity\TopicsType',
                                    'label' => 'Tipo de temas que contiene el foro'))
        ;
    }

    // Antes del remove borro los temas del foro para así actualizar los posts/temas de los usuarios
    public function preRemove($object) {
        $container = $this->getConfigurationPool()->getContainer();
        $container->get('deal.forumbundle.moderationtools.forumremover')->removeForumTopics($object);
    }

    // Después del remove borro la cache de doctrine
    public function postRemove($object) {
        $this->removeDoctrineCache();
    }

    // Después del persist borro la cache de doctrine
    public function postPersist($object) {
        $this->removeDoctrineCache();
    }

    // Después del update borro la cache de doctrine
    public function postUpdate($object) {
        $this->removeDoctrineCache();
    }

    // Funcion encargada de borrar la cache de doctrine
    protected function removeDoctrineCache(){
        $container = $this->getConfigurationPool()->getContainer();

        // Borro la cache de datos
        $container->get('deal.forumbundle.service.doctrinecache')->remove();
    }

    // Evito que se puedan borrar foros vía batch puede ser demasiado lento
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('batch');
    }

} 