<?php

namespace Deal\ForumBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;

/*
 * Clase de configuración para la administración de las variables globales en el panel de administración
 * */
class GlobalVariablesAdmin extends Admin {

    // Campos que se van a mostrar en los formularios crear/editar entidades
    protected function configureFormFields(FormMapper $formMapper)
    {
            $formMapper
                ->add('maxTopicsPerPage', null, array('label' => 'Número máximo de temas por página'))
                ->add('maxSearchResultTopicsPerPage', 'integer', array('label' => 'Número máximo de temas en búsquedas'))
                ->add('maxPostsPerTopicPage', 'integer', array('label' => 'Número máximo de mensajes por página de tema'))
                ->add('maxSignaturesPerPage', 'integer', array('label' => 'Número máximo de firmas por página'))
                ->add('maxProfileSignatures', 'integer', array('label' => 'Número máximo de firmas en perfil'))
                ->add('maxFavoritesPerPage', 'integer', array('label' => 'Número máximo de temas favoritos por página'))
                ->add('notificationsPerPage', 'integer', array('label' => 'Número máximo de notificaciones por página'))
                ->add('reportsPerPage', 'integer', array('label' => 'Número máximo de reportes por página'))
                ->add('maxReportsPerDay', 'integer', array('label' => 'Número máximo de reportes por día '))
                ->add('minVotesToBePop', 'integer', array('label' => 'Número mínimo de votos para que un tema sea popular'))
                ->add('numberOfUserLastPosts', 'integer', array('label' => 'Número máximo de últimos mensajes de usuario en perfil'))
            ;
            ;
    }

    // Campos para ser mostrados en los listados de entidades
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, array('label' => 'Id'))
            ->add('maxTopicsPerPage', 'integer', array('label' => 'Temas por página'))
            ->add('maxSearchResultTopicsPerPage', 'integer', array('label' => 'Temas por página en búsquedas'))
            ->add('maxPostsPerTopicPage', 'integer', array('label' => 'Mensajes por página de tema'))
            ->add('maxSignaturesPerPage', 'integer', array('label' => 'Firmas por página'))
            ->add('maxProfileSignatures', 'integer', array('label' => 'Firmas en perfil'))
            ->add('maxFavoritesPerPage', 'integer', array('label' => 'Temas favoritos por página'))
            ->add('notificationsPerPage', 'integer', array('label' => 'Notificaciones por página'))
            ->add('reportsPerPage', 'integer', array('label' => 'Reportes por página'))
            ->add('maxReportsPerDay', 'integer', array('label' => 'Reportes por día '))
            ->add('minVotesToBePop', 'integer', array('label' => 'Votos para que un tema sea popular'))
            ->add('numberOfUserLastPosts', 'integer', array('label' => 'Últimos mensajes de usuario en perfil'))
        ;
    }

    // Antes del update actualizo la fecha de popularidad de los temas si hubiese que hacerlo
    public function preUpdate($object) {

        // Actualizo la fecha de popularidad
        $container = $this->getConfigurationPool()->getContainer();
        $originalEntity = $container->get('doctrine.orm.entity_manager')->getUnitOfWork()->getOriginalEntityData($object);
        $oldMinVotesToBePop = $originalEntity["minVotesToBePop"];
        $newMinVotesToBePop = $object->getMinVotesToBePop();

        if($oldMinVotesToBePop != $newMinVotesToBePop){
            $container->get('deal.topicbundle.service.populardateupdater')->update($newMinVotesToBePop, $oldMinVotesToBePop);
        };
    }

    // Después del update borro la cache de doctrine
    public function postUpdate($object) {
        $container = $this->getConfigurationPool()->getContainer();

        // Borro la cache de datos
        $container->get('deal.forumbundle.service.doctrinecache')->remove();
    }

    // Evito que se puedan borrar las variables globales
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('create');
    }
} 