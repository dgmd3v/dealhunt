<?php

namespace Deal\ForumBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;

/*
 * Clase de configuración para la administración de los tipos de tema en el panel de administración
 * */
class TopicsTypeAdmin extends Admin {

    // Campos que se van a mostrar en los formularios crear/editar entidades
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', "text", array(
                'label' => 'Nombre del tipo de tema',
                'read_only' => true,
                'disabled'  => true,
            ))
            ;
    }

    // Campos a mostrar en los formularios para filtrar entidades
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, array('label' => 'Id'))
            ->add('name', null, array('label' => 'Nombre del tipo de tema'))
        ;
    }

    // Campos para ser mostrados en los listados de entidades
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, array('label' => 'Id'))
            ->addIdentifier('name', null, array('label' => 'Nombre del tipo de tema'))
        ;
    }

    // Evito que se puedan borrar los tipos de temas
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('create');
    }
} 