<?php

namespace Deal\ForumBundle\Form\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/*
 * Formulario para búsquedas
 **/
class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('searchTerm',   'text', array(
                'label_render' => false,
                'attr'  => array( 'placeholder' => "Introduce tu búsqueda"),
                'horizontal_input_wrapper_class' => 'col-xs-12 col-sm-12',
                'horizontal_label_offset_class' => false,
            ));
    }

    /**
     * Esta función indica el namespace de la entidad que vamos a modificar con este formulario. También indico que
     * no quiero que el formulario tenga fieldset
     **/
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(   'data_class'=> 'Deal\ForumBundle\Entity\Search',
                'validation_groups' => array('Default'),
                'render_fieldset' => false,
            )
        );
    }

    public function getName()
    {
        return 'deal_forumbundle_searchtype';
    }
} 