<?php

namespace Deal\ForumBundle\Form\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/*
 * Formulario para búsquedas avanzadas
 **/
class AdvancedSearchType extends SearchType
{
    private $forums;

    public function __construct($forums)
    {
        $this->forums = $forums;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('searchTerm',   'text', array(
                'label' => 'Búsqueda',
                'attr'  => array( 'placeholder' => "Introduce tu búsqueda"),
                'horizontal_label_class' => 'col-xs-12 col-sm-12 control-label-left',
                'horizontal_input_wrapper_class' => 'col-xs-12 col-sm-12',
                'widget_form_group_attr' => array('class'=>'form-group col-md-2'),
            ))
        ->add('forum',   'entity', array(
                'class' => 'ForumBundle:Forum',
                'empty_value' => 'Todos los foros',
                'choices' => $this->forums,
                'required' => false,
                'property' => 'name',
                'label' => 'Foro',
                'render_optional_text'  => false,
                'horizontal_label_class' => 'col-xs-12 col-sm-12 control-label-left',
                'horizontal_input_wrapper_class' => 'col-xs-12 col-sm-12',
                'widget_form_group_attr' => array('class'=>'form-group col-md-3'),
            ))
        ->add('category',   'entity', array(
                'class' => 'TopicBundle:Category',
                'empty_value' => 'Todas las categorías',
                'required' => false,
                'property' => 'name',
                'label' => 'Categoría',
                'render_optional_text'  => false,
                'horizontal_label_class' => 'col-xs-12 col-sm-12 control-label-left',
                'horizontal_input_wrapper_class' => 'col-xs-12 col-sm-12',
                'widget_form_group_attr' => array('class'=>'form-group col-md-3'),
            ))
            ->add('order', 'choice', array(
                'choices'   => array('ordenado-por-relevancia' => 'Relevancia', 'ordenado-por-fecha' => 'Fecha'),
                'empty_value' => false,
                'required'  => false,
                'label' => 'Orden',
                'render_optional_text'  => false,
                'horizontal_label_class' => 'col-xs-12 col-sm-12 control-label-left',
                'horizontal_input_wrapper_class' => 'col-xs-12 col-sm-12',
                'widget_form_group_attr' => array('class'=>'form-group col-md-2'),
            ))
            ->add('searchInterval', 'choice', array(
                'choices'   => array(   'cualquier-fecha' => 'Cualquier fecha',
                                        'hoy' => 'Hoy',
                                        'ultimos-7-dias' => 'Últimos 7 días',
                                        'ultimo-mes' => 'Último mes',
                                        'ultimo-anio' => 'Último año'),
                'empty_value' => false,
                'required'  => false,
                'label' => 'Intervalo',
                'render_optional_text'  => false,
                'horizontal_label_class' => 'col-xs-12 col-sm-12 control-label-left',
                'horizontal_input_wrapper_class' => 'col-xs-12 col-sm-12',
                'widget_form_group_attr' => array('class'=>'form-group col-md-3'),
            ))
        ;
    }

    /*
     * Esta función indica el namespace de la entidad que vamos a modificar con este formulario.
     **/
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(   'data_class'=> 'Deal\ForumBundle\Entity\Search',
                'validation_groups' => array('Default')
            )
        );
    }

    public function getName()
    {
        return 'deal_forumbundle_advancedsearchtype';
    }
} 