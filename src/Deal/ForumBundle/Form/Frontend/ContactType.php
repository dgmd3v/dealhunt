<?php

namespace Deal\ForumBundle\Form\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/*
 * Formulario para la creación de mensajes de contacto
 **/
class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',   'text', array(
                'label' => 'Nombre',
                'attr'  => array( 'placeholder' => "Introduce tu nombre"),
            ))
            ->add('email',   'email', array(
                'label' => 'Correo',
                'attr'  => array( 'placeholder' => "Introduce tu correo electrónico"),
            ))
            ->add('subject',   'text', array(
                'label' => 'Título',
                'attr'  => array( 'placeholder' => "Introduce un título para el mensaje"),
            ))
            ->add('message',   'textarea', array(
                'label' => 'Mensaje',
                'attr'  => array( 'placeholder' => "Introduce tu mensaje"),
            ))
            ->add('captcha', 'genemu_recaptcha', array(
                'mapped' => false,
                'configs' => array(
                'theme' => 'white',
                ),
                'invalid_message'   => 'Captcha erróneo',
            ));
    }

    /*
     * Esta función indica el namespace de la entidad que vamos a modificar con este formulario.
     **/
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(   'data_class'=> 'Deal\ForumBundle\Entity\Contact',
                'validation_groups' => array('Default')
            )
        );
    }

    public function getName()
    {
        return 'deal_forumbundle_contacttype';
    }
} 