<?php

namespace Deal\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/*
 * Clase encargada de las acciones en las que se usa AJAX y que pueden usar todos los usuarios registrados del foro
 * */
class AjaxController extends Controller
{

    /*
     * Utilizando AJAX muestro el top 10 de temas más votados, haciendo un top 10 diario, otro semanal y otro mensual.
     * Dependiendo de el foro y categoría en la que me encuentre mostrare un top 10 para ese foro y esa categoría.
     * Por ejemplo estando en el foro de ofertas con la categoría todas mostrará los tops para las ofertas
     * de todas las categorías, si en cambio estoy en el foro de cupones descuento en la categoría comida me mostrara
     * el top de cupones descuento de la categoría comida.
     *
     * @param string forumSlug slug del foro en el que estoy para saber que temas debo buscar (ofertas, cupones, etc)
     * @param string from cadena de fecha/hora en la cual especifico desde cuando empiezo a contar el día,
     * la semana o el mes
     * @param string to cadena de fecha/hora en la cual especifico donde acabo de contar el día, la semana o el mes
     * @param string category slug de la categoría en la que me encuentro para saber que temas debo buscar
     * (de moda, comida, etc)
     *
     * Nota: Al utilizar ajax todos estos parámetros no se pasan directamente a la función,
     * se le solicitan al script AJAX
     **/
    public function ajaxTopTopicsByTimeAction() {
        $request = $this->getRequest();

         // Si se trata de una petición AJAX serializo los resultados de la consulta a la base de datos y
         // devuelvo la respuesta
        if ($request->isXmlHttpRequest()) {
            // Obtengo el objeto del entity manager para realizar consultas
            $em = $this->getDoctrine()->getManager();

            // Solicito los datos pasados a través del script jquery, en este caso el slug del foro del cual
            // debo sacar el top de ofertas, cupones, etc
            $forumSlug = $request->request->get('forumSlug');
            $fromTime = $request->request->get('from');
            $categorySlug = $request->request->get('category');

            $forum = $this->get('deal.forumbundle.service.utilities')->searchRequestedForum($forumSlug);

            // Llamo al serializador para poder utilizarlo
            $serializer = $this->container->get('jms_serializer');

            if(!$forum){
                $responseArray = array("responseCode" => 400, "errorMsg" => "No existe el foro solicitado.");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            if(!$this->get('security.context')->isGranted('ROLE_ACCESS_FORUM', $forum)){
                $responseArray = array("responseCode" => 400, "errorMsg" => "No tienes permisos suficientes para acceder a esta zona.");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Dependiendo del foro buscare un tipo u otro de topics (Oferta, Gratis, Cupón descuento)
            switch($forum->getForumTopicsType()){
                case 'TopicDeal':
                    $repository = 'TopicBundle:TopicDeal';
                    break;

                case 'TopicFree':
                    $repository = 'TopicBundle:TopicFree';
                    break;

                case 'TopicVoucher':
                    $repository = 'TopicBundle:TopicVoucher';
                    break;

                default:
                    $responseArray = array("responseCode" => 400, "errorMsg" => "Tipo de foro no soportado para Top");
                    $jsonContent = $serializer->serialize($responseArray, 'json');

                    return new Response($jsonContent);
            }

            $globalVariables = $em->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
            $minVotesToBePop = $globalVariables->getMinVotesToBePop();

            // Existe un bug de php que hace que en domingo coja el lunes siguiente como el lunes de la semana actual
            // lo intento evitar aquí:
            if ( (date('l', strtotime('now')) == 'Sunday') && ($fromTime == 'monday this week')  ){
                $fromTime = 'last monday';
            }

            if ($categorySlug == 'todas') {
                $topics = $em->getRepository($repository)->findAllTopTopicsByTime($forum, $fromTime, $minVotesToBePop);
            }

            else{
                $category = $this->get('deal.forumbundle.service.utilities')->searchRequestedCategory($categorySlug);

                if(!$category){
                    $responseArray = array("responseCode" => 400, "errorMsg" => "No existe la categoría solicitada.");
                    $jsonContent = $serializer->serialize($responseArray, 'json');

                    return new Response($jsonContent);
                }

                // Realizo la consulta
                $topics = $em->getRepository($repository)->findTopTopicsByTime($forum, $category, $fromTime, $minVotesToBePop);
            }

            // Si no se han encontrado temas debo mostrar un mensaje de error
            if (!$topics) {

                $responseArray = array( "responseCode" => 400,
                                        "errorMsg" => "No se han encontrado temas para este top :(");
                $jsonContent = $serializer->serialize($responseArray, 'json');

                return new Response($jsonContent);
            }

            // Si encuentro temas los devuelvo serializados
            $responseArray = array("responseCode" => 200, "topic" => $topics);
            $jsonContent = $serializer->serialize($responseArray, 'json');

            return new Response($jsonContent);
        }

        // En caso de no ser una petición AJAX retorno un mensaje de error
        return new Response('Esto no es ajax!', 400);
    }
}