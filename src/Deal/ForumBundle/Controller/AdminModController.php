<?php

namespace Deal\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/*
 * Clase encargada de las acciones de administración relacionadas con el foro
 * */
class AdminModController extends Controller{

    // Función encargada de borrar toda la cache del bundle LiipImagineBundle
    public function removeAllLiipImagineBundleCacheAction(){

        $cacheManager = $this->get('liip_imagine.cache.manager');

        $cacheManager->remove();

        $this->get('session')->getFlashBag()->add(
                    'sonata_flash_success',
                    'Se ha borrado con éxito toda la cache de imágenes generada por LiipImagineBundle');

        return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
    }

    // Función encargada de borrar la cache de un filtro dado del bundle LiipImagineBundle
    public function removeFilterLiipImagineBundleCacheAction($filterName){

        $cacheManager = $this->get('liip_imagine.cache.manager');

        $cacheManager->remove(array(),array($filterName));

        $this->get('session')->getFlashBag()->add(
                    'sonata_flash_success',
                    'Se ha borrado con éxito la cache de imágenes generada por '
                    . 'LiipImagineBundle para el filtro ' . $filterName );

        return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
    }

    // Función encargada de borrar la cache de doctrine (consultas, resultados, etc)
    public function removeAllDoctrineCacheAction(){

        // Llamo al servicio encargado del borrado
        $this->get('deal.forumbundle.service.doctrinecache')->remove();

        $this->get('session')->getFlashBag()->add(
                    'sonata_flash_success',
                    'Se ha borrado con éxito la cache de Doctrine ');

        return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
    }

} 