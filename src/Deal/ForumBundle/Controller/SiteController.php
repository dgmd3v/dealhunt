<?php

namespace Deal\ForumBundle\Controller;

use Deal\ForumBundle\Entity\Contact;
use Deal\ForumBundle\Form\Frontend\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/*
 * Clase encargada de las páginas estáticas, como la de contacto, la de las normas, etc
 * */
class SiteController extends Controller
{
    /*
     * Muestra la página estática solicitada
     *
     * @param string $pageSlug el nombre-slug de la página estática (ayuda, sobre-nosotros, etc)
     **/
    public function staticAction($pageSlug)
    {
        if ( $this->get('templating')->exists('ForumBundle:Site:' . $pageSlug .'.html.twig') ) {
            return $this->render('ForumBundle:Site:' . $pageSlug .'.html.twig');
        }

        // Si no encuentro el template muestro una excepción de no encontrado
        throw $this->createNotFoundException(   'La página solicitada no existe');
    }

    /*
     * Función que muestra el formulario de contacto
     **/
    public function contactFormAction(){

        // Creo un nuevo contacto
        $contact = new Contact();
        $form = $this->createForm(new ContactType(), $contact);

        // Relaciono el formulario con los datos introducidos por el usuario en el mismo
        $request = $this->getRequest();
        $form->handleRequest($request);
        if($form->isValid()){

            $name = $form["name"]->getData();
            $subject = $form["subject"]->getData();
            $email = $form["email"]->getData();
            $contactMessage = $form["message"]->getData();

            $mailer = $this->get('mailer');
            $message = $mailer->createMessage()
                                ->setSubject($subject)
                                ->setFrom($email)
                                ->setSender($email)
                                ->setReplyTo($email)
                                ->setTo($this->container->getParameter('deal_webMail'))
                                ->setBody(
                                    $this->renderView(  '@Forum/Default/includes/contactMail.txt.twig',
                                                        array(  'subject'=>$subject,
                                                                'name'=> $name,
                                                                'email' => $email,
                                                                'contactMessage'=> $contactMessage)
                                    )
                                )
            ;

            $mailer->send($message);

            $this->get('session')->getFlashBag()->add('success',
                'Mensaje de contacto enviado con éxito.'
            );

            return $this->redirect($this->generateUrl('forum_front_page'));
        }

        return $this->render('@Forum/Site/contacto.html.twig', array(
            'contactForm' => $form->createView(),
        ));
    }
}
