<?php

namespace Deal\ForumBundle\Controller;

use Deal\ForumBundle\Entity\Search;
use Deal\ForumBundle\Form\Frontend\SearchType;
use Deal\ForumBundle\Form\Frontend\AdvancedSearchType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/*
 * Clase encargada de las acciones “habituales” del foro
 * */
class DefaultController extends Controller
{
    /*
     * Muestra en una plantilla los temas de un foro pertenecientes a alguna categoría, mostrándose ordenados
     * por un criterio.
     * Por ejemplo los ofertas pertenecientes a la categoría moda ordenados por su fecha de creación (mas-nuevos)
     *
     * @param string $forumSlug slug del foro al cual pertenecen los temas a buscar, por defecto 'ofertas'
     * @param string $criterionSlug slug del criterio por el cual se ordenan los resultados de la consulta,
     * por defecto 'mas-votados'
     * @param string $categorySlug slug de la categoría a la que pertenecen los temas a buscar, por defecto 'todas'
     * @param integer $pageNumber página de los temas que vamos a mostrar, por defecto 1
     **/
    public function forumOrderedByAction($forumSlug = 'ofertas', $criterionSlug = 'mas-votados', $categorySlug = 'todas'
        , $pageNumber = 1)
    {
        // Compruebo que el número de página sea mayor que 0
        if($pageNumber < 1){
            return new RedirectResponse(
                $this->generateUrl( 'forum_show_category_topics_ordered_by',
                    array ( 'forumSlug'     => $forumSlug,
                            'criterionSlug' => $criterionSlug,
                            'categorySlug'  => $categorySlug,
                            'pageNumber'    => 1))
            );
        }

        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getDoctrine()->getManager();

        $forum = $this->get('deal.forumbundle.service.utilities')->searchRequestedForum($forumSlug);

        // Obtengo todas las categorías
        $categories = $em->getRepository('TopicBundle:Category')->findAllCategories();

        // Por si el foro no existiese
        if(!$forum){
            throw $this->createNotFoundException(   'No se ha encontrado '
                . 'el foro solicitado');
        }

        if(!$this->get('security.context')->isGranted('ROLE_ACCESS_FORUM', $forum)){
            Throw new AccessDeniedHttpException('No tienes permisos suficientes para acceder a esta zona.');
        }

        $forumTopicsType = $forum->getForumTopicsType()->getName();

        $repository = 'TopicBundle:' . $forumTopicsType;

        // Si quiero mostrar los temas de todas las categorías ...
        if($categorySlug=='todas'){

            if($forumTopicsType == 'TopicDeal' || $forumTopicsType == 'TopicFree' || $forumTopicsType == 'TopicVoucher')
            {
                // Busco los temas, dependiendo del criterio de orden hago una consulta u otra
                switch ($criterionSlug) {
                    case 'mas-votados':
                        $globalVariables = $em->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
                        $minVotesToBePop = $globalVariables->getMinVotesToBePop();
                        $topics = $em->getRepository($repository)->findMostVotedTopics($forum, $minVotesToBePop);
                        $topicCount = $em->getRepository($repository)->findNumberOfMostVotedTopics($forum,
                                                                                                   $minVotesToBePop);
                        break;
                    case 'mas-nuevos':
                        $topics = $em->getRepository($repository)->findNewestTopics($forum);
                        $topicCount = $em->getRepository($repository)->findNumberOfTopicsByForumId($forum);
                        break;
                    case 'recientemente-comentados':
                        $topics = $em->getRepository($repository)->findRecentlyCommentedTopics($forum);
                        $topicCount = $em->getRepository($repository)->findNumberOfTopicsByForumId($forum);
                        break;
                    default:
                        $topicCount = 0;
                        break;
                }
            }

            else {
                switch ($criterionSlug) {
                    case 'mas-nuevos':
                        $topics = $em->getRepository($repository)->findNewestTopics($forum);
                        $topicCount = $em->getRepository($repository)->findNumberOfTopicsByForumId($forum);
                        break;
                    case 'recientemente-comentados':
                        $topics = $em->getRepository($repository)->findRecentlyCommentedTopics($forum);
                        $topicCount = $em->getRepository($repository)->findNumberOfTopicsByForumId($forum);
                        break;
                    default:
                        $topicCount = 0;
                        break;
                }
            }
        }

        // Si quiero mostrar los temas de una categoría en concreto ...
        elseif($categorySlug!='todas'){

            $category = $this->get('deal.forumbundle.service.utilities')->searchRequestedCategory($categorySlug);

            if(!$category){
                return $this->render(   'ForumBundle:Default:frontpage.html.twig',
                    array(
                        'forumSlug'         => $forumSlug,
                        'forumTopicsType'   => null,
                        'criterionSlug'     => $criterionSlug,
                        'categorySlug'      => 'Cat-Errónea',
                        'categories'        => $categories,
                        'results'           => false,
                    )
                );
            }

            // Dependiendo del foro buscare un tipo u otro de topics (Normal, Oferta/Gratis, Cupón descuento)
            if($forumTopicsType == 'TopicDeal' || $forumTopicsType == 'TopicFree' || $forumTopicsType == 'TopicVoucher')
            {
                // Busco los temas, dependiendo del criterio de orden hago una consulta u otra
                switch ($criterionSlug) {
                    case 'mas-votados':
                        $globalVariables = $em->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
                        $minVotesToBePop = $globalVariables->getMinVotesToBePop();
                        $topics = $em->getRepository($repository)->
                                                     findMostVotedTopicsByCategory($forum, $category, $minVotesToBePop);
                        $topicCount = $em->getRepository($repository)->
                                             findNumberOfMostVotedTopicsByCategory($forum, $category, $minVotesToBePop);
                        break;
                    case 'mas-nuevos':
                        $topics = $em->getRepository($repository)->findNewestTopicsByCategory($forum, $category);
                        $topicCount = $em->getRepository($repository)->findNumberOfTopicsByCategory($forum, $category);
                        break;
                    case 'recientemente-comentados':
                        $topics = $em->getRepository($repository)->
                                                               findRecentlyCommentedTopicsByCategory($forum, $category);
                        $topicCount = $em->getRepository($repository)->findNumberOfTopicsByCategory($forum, $category);
                        break;
                    default:
                        $topicCount = 0;
                        break;
                }
            }

            else {
                switch ($criterionSlug) {
                    case 'mas-nuevos':
                        $topics = $em->getRepository($repository)->findNewestTopicsByCategory($forum, $category);
                        $topicCount = $em->getRepository($repository)->findNumberOfTopicsByCategory($forum, $category);
                        break;
                    case 'recientemente-comentados':
                        $topics = $em->getRepository($repository)->
                                                               findRecentlyCommentedTopicsByCategory($forum, $category);
                        $topicCount = $em->getRepository($repository)->findNumberOfTopicsByCategory($forum, $category);
                        break;
                    default:
                        $topicCount = 0;
                        break;
                }
            }
        }

        if($topicCount == 0) {
            return $this->render(   'ForumBundle:Default:frontpage.html.twig',
                array(
                    'forumSlug'         => $forumSlug,
                    'forumTopicsType'   => $forumTopicsType,
                    'criterionSlug'     => $criterionSlug,
                    'categorySlug'      => $categorySlug,
                    'categories'        => $categories,
                    'results'           => false,
                )
            );
        }

        // Pagino los resultados con la ruta personalizada
        $globalVariables = $em->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
        $maxTopicsPerPage = $globalVariables->getMaxTopicsPerPage();

        // Primero busco el nombre de la ruta utilizada actualmente
        $request = $this->container->get('request');
        $routeName = $request->get('_route');

        // Si se solicita una página de un foro sin resultados, devuelvo la última página existente.
        // (Ej: tengo 30 temas con un limite de temas por página de 10 y solicito la página 4 cuando solo debería
        // existir hasta la 3, por lo que devuelvo la última existe es decir la 3)

        $maxResultsPages = ceil($topicCount/$maxTopicsPerPage);
        if( $pageNumber > $maxResultsPages){
            $pageNumber = $maxResultsPages;

            switch($routeName){
                case 'forum_front_page':
                    return new RedirectResponse(
                        $this->generateUrl($routeName, array ('pageNumber'=> $pageNumber))
                    );
                    break;

                case '_forum_show_category_topics_ordered_by':
                case 'forum_show_category_topics_ordered_by':
                    return new RedirectResponse(
                        $this->generateUrl($routeName, array (  'forumSlug'     => $forumSlug,
                                'criterionSlug' => $criterionSlug,
                                'categorySlug'  => $categorySlug,
                                'pageNumber'    => $pageNumber
                            )
                        )
                    );
                    break;

                case '_forum_show_topics_ordered_by':
                case 'forum_show_topics_ordered_by':
                    return new RedirectResponse(
                        $this->generateUrl($routeName, array (  'forumSlug'     => $forumSlug,
                                'criterionSlug' => $criterionSlug,
                                'pageNumber'    => $pageNumber
                            )
                        )
                    );
                    break;
                default:
                    return new RedirectResponse(
                        $this->generateUrl('forum_front_page')
                    );
                    break;
            }
        }

        $topics->setHint('knp_paginator.count', $topicCount);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate( $topics,
                                            $this->get('request')->query->get('page', $pageNumber),
                                            $maxTopicsPerPage, array('distinct' => false));

        // Ruta y plantilla de paginación personalizadas
        $pagination->setUsedRoute($routeName);

        // Número máximo de mensajes por página de tema (necesario para calcular el número de páginas de un tema)
        $maxPostsPerTopicPage = $globalVariables->getMaxPostsPerTopicPage();

        // Si es un usuario registrado compruebo los temas visitados/votados por el mismo para informar
        // de los temas leídos/votados o de nuevos posts/mensajes en los temas
        if ($this->get('security.context')->isGranted('ROLE_USER')) {
            $user = $this->get('security.context')->getToken()->getUser();

            $forumId = $forum->getId();

            // Creo un array con las ids de los hilos paginados
            for($i= 0; $i < count($pagination); $i++){
                $paginatedTopicsId[] = $pagination[$i]['id'];
            }

            // Información de los hilos visitados por el usuario
            $userVisitedTopics =  $em->getRepository('UserBundle:Visited')->
                                                  findVisitedTopicsLastMonthByUser($user, $forumId, $paginatedTopicsId);

            // Si estoy en un foro en el que se puede votar busco también los votos
            if($forumTopicsType == 'TopicDeal' || $forumTopicsType == 'TopicFree' || $forumTopicsType == 'TopicVoucher')
            {
                // Información de los hilos votados por el usuario
                $userVotedTopics =  $em->getRepository('UserBundle:User')->
                                                                       findVotedTopicsByUser($user, $paginatedTopicsId);

                // Si se ha visitado y votado alguno de los hilos
                if(($userVisitedTopics) &&  ($userVotedTopics) ){
                    // Arrays, uno con los ids de los temas que ha visitado el usuario y que están siendo paginados
                    // y otro con el número del último post visitado por el usuario
                    for($i= 0; $i < count($userVisitedTopics); $i++){
                        $tId = $userVisitedTopics[$i]['lastVisitedPost']['topic']['id'];
                        $visitedTopics[] = $tId;
                        $lastPostNumberVisitedTopics[$tId] = $userVisitedTopics[$i]['lastVisitedPost']['number'];
                    }

                    // Array con los ids de los temas votados por el usuario
                    for($i= 0; $i < count($userVotedTopics[0]['votes']); $i++){
                        $votedTopics[] = $userVotedTopics[0]['votes'][$i]['id'];
                    }

                    // Retorno una plantilla generada con twig a la que le paso los temas buscados, el foro al que
                    // pertenecen y el criterio por el cual están ordenados, su categoría y los arrays con los temas
                    // visitados y los últimos posts (sus números) leídos en el tema
                    return $this->render(   'ForumBundle:Default:frontpage.html.twig',
                        array(
                            'topics'                        => $topics,
                            'forumSlug'                     => $forumSlug,
                            'forumTopicsType'               => $forumTopicsType,
                            'criterionSlug'                 => $criterionSlug,
                            'categorySlug'                  => $categorySlug,
                            'pagination'                    => $pagination,
                            'userVisitedTopics'             => $visitedTopics,
                            'userVotedTopics'               => $votedTopics,
                            'lastPostNumberVisitedTopics'   => $lastPostNumberVisitedTopics,
                            'categories'                    => $categories,
                            'lastPageNumber'                => $maxResultsPages,
                            'maxPostsPerTopicPage'          => $maxPostsPerTopicPage,
                            'results'                       => true,
                        )
                    );
                }

                // Si no se ha visitado ninguno de los hilos
                elseif(!($userVisitedTopics) &&  !($userVotedTopics) ){
                    // Retorno una plantilla generada con twig a la que le paso los temas buscados, el foro al que
                    // pertenecen y el criterio por el cual están ordenados, etc
                    return $this->render(   'ForumBundle:Default:frontpage.html.twig',
                        array(
                            'topics'                => $topics,
                            'forumSlug'             => $forumSlug,
                            'forumTopicsType'       => $forumTopicsType,
                            'criterionSlug'         => $criterionSlug,
                            'categorySlug'          => $categorySlug,
                            'pagination'            => $pagination,
                            'categories'            => $categories,
                            'lastPageNumber'        => $maxResultsPages,
                            'maxPostsPerTopicPage'  => $maxPostsPerTopicPage,
                            'results'               => true,
                        )
                    );
                }

                else{
                    // Retorno una plantilla generada con twig a la que le paso los temas buscados, el foro al que
                    // pertenecen y el criterio por el cual están ordenados, etc
                    if($userVotedTopics){

                        // Array con los ids de los temas votados por el usuario
                        for($i= 0; $i < count($userVotedTopics[0]['votes']); $i++){
                            $votedTopics[] = $userVotedTopics[0]['votes'][$i]['id'];
                        }

                        return $this->render(   'ForumBundle:Default:frontpage.html.twig',
                            array(
                                'topics'                => $topics,
                                'forumSlug'             => $forumSlug,
                                'forumTopicsType'       => $forumTopicsType,
                                'criterionSlug'         => $criterionSlug,
                                'categorySlug'          => $categorySlug,
                                'pagination'            => $pagination,
                                'userVotedTopics'       => $votedTopics,
                                'categories'            => $categories,
                                'lastPageNumber'        => $maxResultsPages,
                                'maxPostsPerTopicPage'  => $maxPostsPerTopicPage,
                                'results'               => true,
                            )
                        );
                    }

                    else{

                        // Arrays, uno con los ids de los temas que ha visitado el usuario y que están siendo paginados
                        // y otro con el número del último post visitado por el usuario
                        for($i= 0; $i < count($userVisitedTopics); $i++){
                            $tId = $userVisitedTopics[$i]['lastVisitedPost']['topic']['id'];
                            $visitedTopics[] = $tId;
                            $lastPostNumberVisitedTopics[$tId] = $userVisitedTopics[$i]['lastVisitedPost']['number'];
                        }

                        return $this->render(   'ForumBundle:Default:frontpage.html.twig',
                            array(
                                'topics'                        => $topics,
                                'forumSlug'                     => $forumSlug,
                                'forumTopicsType'               => $forumTopicsType,
                                'criterionSlug'                 => $criterionSlug,
                                'categorySlug'                  => $categorySlug,
                                'pagination'                    => $pagination,
                                'userVisitedTopics'             => $visitedTopics,
                                'lastPostNumberVisitedTopics'   => $lastPostNumberVisitedTopics,
                                'categories'                    => $categories,
                                'lastPageNumber'                => $maxResultsPages,
                                'maxPostsPerTopicPage'          => $maxPostsPerTopicPage,
                                'results'                       => true,
                            )
                        );
                    }
                }
            }

            // Foros sin votos en los temas
            else{
                if($userVisitedTopics){
                    // Arrays, uno con los ids de los temas que ha visitado el usuario y que están siendo paginados
                    // y otro con el número del último post visitado por el usuario
                    for($i= 0; $i < count($userVisitedTopics); $i++){
                        $tId = $userVisitedTopics[$i]['lastVisitedPost']['topic']['id'];
                        $visitedTopics[] = $tId;
                        $lastPostNumberVisitedTopics[$tId] = $userVisitedTopics[$i]['lastVisitedPost']['number'];
                    }

                    return $this->render(   'ForumBundle:Default:frontpage.html.twig',
                        array(
                            'topics'                        => $topics,
                            'forumSlug'                     => $forumSlug,
                            'forumTopicsType'               => $forumTopicsType,
                            'criterionSlug'                 => $criterionSlug,
                            'categorySlug'                  => $categorySlug,
                            'pagination'                    => $pagination,
                            'userVisitedTopics'             => $visitedTopics,
                            'lastPostNumberVisitedTopics'   => $lastPostNumberVisitedTopics,
                            'categories'                    => $categories,
                            'lastPageNumber'                => $maxResultsPages,
                            'maxPostsPerTopicPage'          => $maxPostsPerTopicPage,
                            'results'                       => true,
                        )
                    );
                }
            }

        }

        // Retorno una plantilla generada con twig a la que le paso los temas buscados, el foro al que pertenecen
        // y el criterio por el cual están ordenados, etc
        return $this->render(   'ForumBundle:Default:frontpage.html.twig',
            array(
                'topics'                => $topics,
                'forumSlug'             => $forumSlug,
                'forumTopicsType'       => $forumTopicsType,
                'criterionSlug'         => $criterionSlug,
                'categorySlug'          => $categorySlug,
                'pagination'            => $pagination,
                'categories'            => $categories,
                'lastPageNumber'        => $maxResultsPages,
                'maxPostsPerTopicPage'  => $maxPostsPerTopicPage,
                'results'               => true,
            )
        );
    }

    /*
     * Muestra en una plantilla los temas de un foro las cuales tienen la etiqueta/tag solicitada.
     *
     * @param string $tagSlug slug de la etiqueta/tag de la cual queremos obtener resultados
     * @param string $forumSlug slug del foro del cual vamos a obtener los temas con una etiqueta
     * @param integer $pageNumber número de la página que estamos consultando
     **/
    public function forumShowForumByTagAction($tagSlug, $forumSlug = 'todos-los-foros', $pageNumber = 1)
    {
        // Compruebo que el número de página sea mayor que 0
        if($pageNumber < 1){
            return new RedirectResponse(
                $this->generateUrl( 'forum_show_forum_topics_by_tag_page',
                    array ( 'tagSlug'   => $tagSlug,
                            'forumSlug' => $forumSlug,
                            'pageNumber'=> 1))
            );
        }

        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getDoctrine()->getManager();

        // Si no existiese la etiqueta deseada devuelvo la plantilla informando
        $tag = $em->getRepository('TopicBundle:Tag')->findTagBySlug($tagSlug);

        // Foros visibles para el usuario
        $forums = $this->forumsThatUserCanAccessWithTags();

        if($tag == null){
            $forumName = $forumSlug;
            if($forumSlug == 'todos-los-foros'){
                $forumName = 'Todos los foros';
            }

            return $this->render(   '@Forum/Default/showForumByTag.html.twig',
                array(
                    'forumSlug'     => $forumSlug,
                    'forumName'     => $forumName,
                    'tagSlug'       => $tagSlug,
                    'tagName'       => $tagSlug,
                    'forums'        => $forums,
                    'results'       => false,
                )
            );
        }

        // Busco los temas que contienen el tag pedido
        // Dependiendo de los datos introducido devuelvo los resultados de un foro en concreto o de todos

        $globalVariables = $em->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
        $maxTopicsPerPage = $globalVariables->getMaxTopicsPerPage();


        if($forumSlug == 'todos-los-foros'){
            $ids = $em->getRepository('TopicBundle:Tag')->findTopicIdsByTag($tagSlug);
            $topicCount = $em->getRepository('TopicBundle:Topic')->findNumberOfPublicTopicsInArray($ids);
            $topics = $em->getRepository('TopicBundle:Topic')->findTopicsByIds($ids);
            $forumName = 'Todos los foros';
        }

        else {
            $forum = $this->get('deal.forumbundle.service.utilities')->searchRequestedForum($forumSlug);

            // Si no existiese el foro solicitado devuelvo la plantilla informando
            if($forum == null){
                return $this->render(   '@Forum/Default/showForumByTag.html.twig',
                    array(
                        'forumSlug'     => $forumSlug,
                        'forumName'     => $forumSlug,
                        'tagSlug'       => $tagSlug,
                        'tagName'       => $tag->getName(),
                        'forums'        => $forums,
                        'results'       => false,
                    )
                );
            }

            else{
                if(!$this->get('security.context')->isGranted('ROLE_ACCESS_FORUM', $forum)){
                    Throw new AccessDeniedHttpException('No tienes permisos suficientes para acceder a esta zona.');
                }

                $forumTopicsType = $forum->getForumTopicsType()->getName();
                $forumName = $forum->getName();

                // El tipo de foro tiene que ser uno que pueda tener etiquetas (Topic no tiene)
                if($forumTopicsType == 'Topic'){
                    return $this->render(   '@Forum/Default/showForumByTag.html.twig',
                        array(
                            'forumName'     => $forumName,
                            'forumSlug'     => $forumSlug,
                            'tagSlug'       => $tagSlug,
                            'tagName'       => $tag->getName(),
                            'forums'        => $forums,
                            'results'       => false,
                        )
                    );
                }

                $repository = 'TopicBundle:' . $forumTopicsType;

                // Extraigo un array de ids del tipo de temas deseado, después lo filtro por el foro y lo uso para buscar
                // los temas
                $ids = $em->getRepository('TopicBundle:Tag')->findTopicIdsByTypeAndTag($forumTopicsType.'Tag', $tag);
                $topicCount = $em->getRepository($repository)->findNumberOfTopicsInArray($ids, $forum);
                $topics = $em->getRepository($repository)->findTopicsByIdsAndForum($ids, $forum);
            }
        }

        // Si no hubiese encontrado temas con la etiqueta deseada retorno la plantilla informando
        if($topicCount == 0){
            return $this->render(   '@Forum/Default/showForumByTag.html.twig',
                array(
                    'forumName'     => $forumName,
                    'forumSlug'     => $forumSlug,
                    'tagSlug'       => $tagSlug,
                    'tagName'       => $tag->getName(),
                    'forums'        => $forums,
                    'results'       => false,
                )
            );
        }

        // Si se solicita una página de un foro sin resultados, devuelvo la última página existente.
        $maxResultsPages = ceil($topicCount/$maxTopicsPerPage);
        if( $pageNumber > $maxResultsPages){
            return new RedirectResponse(
                $this->generateUrl( 'forum_show_forum_topics_by_tag_page',
                                    array ( 'tagSlug' => $tagSlug,
                                            'forumSlug' => $forumSlug,
                                            'pageNumber'=> $maxResultsPages))
            );
        }

        if($forumSlug != 'todos-los-foros') {
            $forumTopicsType = $forum->getForumTopicsType();
        }

        else {
            $forumTopicsType = "AllTopicsType";
        }

        $topics->setHint('knp_paginator.count', $topicCount);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate( $topics,
                                            $this->get('request')->query->get('page', $pageNumber),
                                            $maxTopicsPerPage, array('distinct' => false));

        // Ruta y plantilla de paginación personalizadas
        $pagination->setUsedRoute('forum_show_forum_topics_by_tag_page');

        // Número máximo de mensajes por página de tema (necesario para calcular el número de páginas de un tema)
        $maxPostsPerTopicPage = $globalVariables->getMaxPostsPerTopicPage();

        // Retorno una plantilla generada con twig a la que le paso los temas buscados, el foro al que pertenecen
        // y el criterio por el cual están ordenados, etc
        return $this->render(   'ForumBundle:Default:showForumByTag.html.twig',
            array(
                'topics'                => $topics,
                'forumSlug'             => $forumSlug,
                'forumName'             => $forumName,
                'forumTopicsType'       => $forumTopicsType,
                'tagSlug'               => $tagSlug,
                'tagName'               => $tag->getName(),
                'pagination'            => $pagination,
                'forums'                => $forums,
                'results'               => true,
                'lastPageNumber'        => $maxResultsPages,
                'maxPostsPerTopicPage'  => $maxPostsPerTopicPage,
                'criterionSlug'         => 'mas-nuevos',
            )
        );
    }

    /*
     * Muestra en una plantilla los temas de un foro pertenecientes a la tienda solicitada
     *
     * @param string $shopSlug slug de la tienda
     * @param string $forumSlug slug del foro del cual vamos a obtener los temas
     * @param integer $pageNumber número de la página que estamos consultando
     **/
    public function forumShowForumByShopAction($shopSlug, $forumSlug = 'ofertas', $pageNumber = 1)
    {
        // Compruebo que el número de página sea mayor que 0
        if($pageNumber < 1){
            return new RedirectResponse(
                $this->generateUrl( 'forum_show_forum_topics_by_shop_page',
                    array ( 'shopSlug'  => $shopSlug,
                            'forumSlug' => $forumSlug,
                            'pageNumber'=> 1))
            );
        }

        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->getDoctrine()->getManager();

        $shop = $em->getRepository('TopicBundle:Shop')->findShopBySlug($shopSlug);

        // Foros visibles para el usuario
        $forums = $this->forumsThatUserCanAccessWithShops();

        // Si no existiese la tienda solicitada devuelvo la plantilla informando
        if(!$shop){
            $forumName = $forumSlug;
            if($forumSlug == 'todos-los-foros'){
                $forumName = 'Todos los foros';
            }

            return $this->render(   '@Forum/Default/showForumByShop.html.twig',
                array(
                    'forumSlug'     => $forumSlug,
                    'forumName'     => $forumName,
                    'shopSlug'      => $shopSlug,
                    'shopName'      => $shopSlug,
                    'forums'        => $forums,
                    'results'       => false,
                )
            );
        }

        $forum = $this->get('deal.forumbundle.service.utilities')->searchRequestedForum($forumSlug);

        // Busco los temas que pertenecen a la tienda pedida
        // Dependiendo de los datos introducido devuelvo los resultados de un foro en concreto o de todos

        $globalVariables = $em->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
        $maxTopicsPerPage = $globalVariables->getMaxTopicsPerPage();

        // Para todos los foros
        if($forumSlug == 'todos-los-foros'){
            $topicCount = $em->getRepository('TopicBundle:Topic')->findNumberOfTopicsByShopInPublicForums($shop);
            if($topicCount > 0){
                $topics = $em->getRepository('TopicBundle:Topic')->findTopicsByShopInPublicForums($shop);
            }
            $forumName = 'Todos los foros';
        }

        // Para un foro concreto
        else {
            // Si no existiese el foro solicitado devuelvo la plantilla informando
            if($forum == null){
                return $this->render(   '@Forum/Default/showForumByShop.html.twig',
                    array(
                        'forumSlug'     => $forumSlug,
                        'forumName'     => $forumSlug,
                        'shopSlug'      => $shopSlug,
                        'shopName'      => $shopSlug,
                        'forums'        => $forums,
                        'results'       => false,
                    )
                );
            }

            else{
                if(!$this->get('security.context')->isGranted('ROLE_ACCESS_FORUM', $forum)){
                    Throw new AccessDeniedHttpException('No tienes permisos suficientes para acceder a esta zona.');
                }

                $forumTopicsType = $forum->getForumTopicsType()->getName();
                $forumName = $forum->getName();

                // El tipo de foro tiene que ser uno que pueda tener tiendas (Topic y TopicWithTags no tienen)
                if($forumTopicsType == 'Topic' || $forumTopicsType == 'TopicWithTags'){
                    return $this->render(   '@Forum/Default/showForumByShop.html.twig',
                        array(
                            'forumSlug'     => $forumSlug,
                            'forumName'     => $forum->getName(),
                            'shopSlug'      => $shopSlug,
                            'shopName'      => $shopSlug,
                            'forums'        => $forums,
                            'results'       => false,
                        )
                    );
                }

                $repository = 'TopicBundle:' . $forumTopicsType;

                $topicCount = $em->getRepository($repository)->findNumberOfTopicsByShop($shop, $forum);
                if($topicCount > 0){
                    $topics = $em->getRepository($repository)->findTopicsByShop($shop, $forum);
                }
            }
        }

        // Si no hubiese encontrado temas con la etiqueta deseada retorno la plantilla informando
        if($topicCount==0) {
            return $this->render(   '@Forum/Default/showForumByShop.html.twig',
                array(
                    'forumSlug'     => $forumSlug,
                    'forumName'     => $forumName,
                    'shopSlug'      => $shopSlug,
                    'shopName'      => $shop->getName(),
                    'forums'        => $forums,
                    'results'       => false,
                )
            );
        }

        // Si se solicita una página de un foro sin resultados, devuelvo la última página existente.
        $maxResultsPages = ceil($topicCount/$maxTopicsPerPage);
        if( $pageNumber > $maxResultsPages){
            return new RedirectResponse(
                $this->generateUrl( 'forum_show_forum_topics_by_shop_page',
                    array ( 'shopSlug' => $shopSlug,
                        'forumSlug' => $forumSlug,
                        'pageNumber'=> $maxResultsPages))
            );
        }

        if($forumSlug != 'todos-los-foros') {
            $forumTopicsType = $forum->getForumTopicsType();
        }

        else {
            $forumTopicsType = "AllTopicsType";
        }

        $topics->setHint('knp_paginator.count', $topicCount);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate( $topics,
                                            $this->get('request')->query->get('page', $pageNumber),
                                            $maxTopicsPerPage, array('distinct' => false));

        // Ruta y plantilla de paginación personalizadas
        $pagination->setUsedRoute('forum_show_forum_topics_by_shop_page');

        // Número máximo de mensajes por página de tema (necesario para calcular el número de páginas de un tema)
        $maxPostsPerTopicPage = $globalVariables->getMaxPostsPerTopicPage();

        // Retorno una plantilla generada con twig a la que le paso los temas buscados, el foro al que pertenecen
        // y el criterio por el cual están ordenados, etc
        return $this->render(   'ForumBundle:Default:showForumByShop.html.twig',
            array(
                'forumSlug'             => $forumSlug,
                'forumName'             => $forumName,
                'forumTopicsType'       => $forumTopicsType,
                'shopSlug'              => $shopSlug,
                'shopName'              => $shop->getName(),
                'pagination'            => $pagination,
                'results'               => true,
                'forums'                => $forums,
                'lastPageNumber'        => $maxResultsPages,
                'maxPostsPerTopicPage'  => $maxPostsPerTopicPage,
                'criterionSlug'         => 'mas-nuevos',
            )
        );
    }

    /*
     * Muestra en una plantilla los temas relacionados con el termino buscado
     *
     * @param string $searchTerm termino a buscar
     * @param string $forumSlug slug del foro/s donde quiero buscar
     * @param string $categorySlug slug de la categoría en la que quiero buscar
     * @param string $orderSlug slug del tipo de ordenación a usar
     * @param string $searchIntervalSlug slug del periodo en el cual quiero hacer la búsqueda
     * @param integer $pageNumber número de página en los resultados
     **/
    public function searchAction($searchTerm, $forumSlug='todos-los-foros', $categorySlug='todas-las-categorias',
                                 $orderSlug='ordenado-por-relevancia', $searchIntervalSlug='cualquier-fecha',
                                 $pageNumber = 1)
    {
        // Compruebo que el número de página sea mayor que 0
        if($pageNumber < 1){
            return new RedirectResponse(
                $this->generateUrl( 'forum_search_page',
                    array ( 'searchTerm' => $searchTerm,
                            'forumSlug' => $forumSlug,
                            'categorySlug' => $categorySlug,
                            'orderSlug' => $orderSlug,
                            'searchIntervalSlug' => $searchIntervalSlug,
                            'pageNumber'=> 1))
            );
        }

        // Repositorio para la búsqueda de temas
        $repositoryManager = $this->container->get('fos_elastica.manager.orm');
        $repository = $repositoryManager->getRepository('TopicBundle:Topic');

        // Roles del usuario para filtrar resultados en función de sus roles
        $userRolesArray = array_map('strtolower', $this->userRolesArray());

        // Consulto ElasticSearch para encontrar los temas
        if($forumSlug == 'todos-los-foros'){

            if($categorySlug=='todas-las-categorias'){
                if($orderSlug=='ordenado-por-relevancia'){
                    if($searchIntervalSlug=='cualquier-fecha'){
                        // Todos los foros, todas las categorías, ordenados por relevancia y de cualquier fecha
                        $topics = $repository->findAllTopics($searchTerm, $userRolesArray);
                    }

                    else{
                        // Todos los foros, todas las categorías, ordenados por relevancia y de una fecha concreta
                        // hoy, última semana, etc
                        $dateStart = $this->getSearchDateStart($searchIntervalSlug);

                        $topics = $repository->findAllTopicsByInterval($searchTerm, $userRolesArray, $dateStart);
                    }
                }

                elseif($orderSlug=='ordenado-por-fecha') {
                    if($searchIntervalSlug=='cualquier-fecha'){
                        // Todos los foros, todas las categorías, ordenados por fecha y de cualquier fecha
                        $topics = $repository->findAllTopicsOrderedByDate($searchTerm, $userRolesArray);
                    }

                    else{
                        // Todos los foros, todas las categorías, ordenados por fecha y de una fecha concreta
                        // hoy, última semana, etc
                        $dateStart = $this->getSearchDateStart($searchIntervalSlug);

                        $topics = $repository->
                                        findAllTopicsByIntervalOrderedByDate($searchTerm, $userRolesArray, $dateStart);
                    }
                }

                else{
                    throw $this->createNotFoundException(   'La forma de ordenar las búsquedas no existe');
                }

            }

            else{
                $category = $this->get('deal.forumbundle.service.utilities')->searchRequestedCategory($categorySlug);

                // Por si la categoría no existiese
                if(!$category){
                   throw $this->createNotFoundException('La categoría en el que quiere realizar la búsqueda no existe');
                }

                if($orderSlug=='ordenado-por-relevancia'){
                    if($searchIntervalSlug=='cualquier-fecha'){
                        // Todos los foros, de una categoría en concreto, ordenados por relevancia y de cualquier fecha
                       $topics = $repository->findAllTopicsByCategory($searchTerm, $category->getId(), $userRolesArray);
                    }

                    else{
                        // Todos los foros, de una categoría en concreto, ordenados por relevancia y de una fecha
                        // concreta hoy, última semana, etc
                        $dateStart = $this->getSearchDateStart($searchIntervalSlug);

                        $topics = $repository->findAllTopicsByCategoryAndInterval(  $searchTerm,
                                                                                    $category->getId(),
                                                                                    $dateStart,
                                                                                    $userRolesArray);
                    }
                }

                elseif($orderSlug=='ordenado-por-fecha'){
                    if($searchIntervalSlug=='cualquier-fecha'){
                        // Todos los foros, de una categoría en concreto, ordenados por fecha y de cualquier fecha
                        $topics = $repository->
                                findAllTopicsByCategoryOrderedByDate($searchTerm, $category->getId(), $userRolesArray);
                    }

                    else{
                        // Todos los foros, de una categoría en concreto, ordenados por fecha y de una fecha concreta
                        // hoy, última semana, etc
                        $dateStart = $this->getSearchDateStart($searchIntervalSlug);
                        $topics = $repository->
                            findAllTopicsByCategoryAndIntervalOrderedByDate($searchTerm,
                                                                            $category->getId(),
                                                                            $dateStart,
                                                                            $userRolesArray);
                    }
                }

                else{
                    throw $this->createNotFoundException(   'La forma de ordenar las búsquedas no existe');
                }
            }
        }

        else{
            $forum = $this->get('deal.forumbundle.service.utilities')->searchRequestedForum($forumSlug);

            // Por si el foro no existiese
            if(!$forum){
                throw $this->createNotFoundException(   'El foro en el que quiere realizar la búsqueda no existe ');
            }

            if($categorySlug=='todas-las-categorias'){
                if($orderSlug=='ordenado-por-relevancia'){
                    if($searchIntervalSlug=='cualquier-fecha'){
                        // De un foro concreto, todas las categorías, ordenados por relevancia y de cualquier fecha
                        $topics = $repository->findTopicsByForum($searchTerm, $forum->getId(), $userRolesArray);
                    }

                    else{
                        // De un foro concreto, todas las categorías, ordenados por relevancia y de una fecha concreta
                        // hoy, última semana, etc
                        $dateStart = $this->getSearchDateStart($searchIntervalSlug);
                        $topics = $repository->findTopicsByForumByInterval( $searchTerm,
                                                                            $forum->getId(),
                                                                            $dateStart,
                                                                            $userRolesArray);
                    }
                }

                elseif($orderSlug=='ordenado-por-fecha'){
                    if($searchIntervalSlug=='cualquier-fecha'){
                        // De un foro concreto, todas las categorías, ordenados por fecha y de cualquier fecha
                        $topics = $repository->findTopicsByForumOrderedByDate(  $searchTerm,
                                                                                $forum->getId(),
                                                                                $userRolesArray);
                    }

                    else{
                        // De un foro concreto, todas las categorías, ordenados por fecha y de una fecha concreta
                        // hoy, última semana, etc
                        $dateStart = $this->getSearchDateStart($searchIntervalSlug);
                        $topics = $repository->findTopicsByForumIntervalOrderedByDate(  $searchTerm,
                                                                                        $forum->getId(),
                                                                                        $dateStart,
                                                                                        $userRolesArray);
                    }
                }

                else{
                    throw $this->createNotFoundException(   'La forma de ordenar las búsquedas no existe');
                }
            }

            else{
                $category = $this->get('deal.forumbundle.service.utilities')->searchRequestedCategory($categorySlug);

                // Por si la categoría no existiese
                if(!$category){
                   throw $this->createNotFoundException('La categoría en el que quiere realizar la búsqueda no existe');
                }

                if($orderSlug=='ordenado-por-relevancia'){
                    if($searchIntervalSlug=='cualquier-fecha'){
                        // De un foro concreto, de una categoría en concreto, ordenados por relevancia y de cualquier
                        // fecha
                        $topics = $repository->findTopicsByForumCategory(   $searchTerm,
                                                                            $forum->getId(),
                                                                            $category->getId(),
                                                                            $userRolesArray);
                    }

                    else{
                        // De un foro concreto, de una categoría en concreto, ordenados por relevancia y de una fecha
                        // concreta hoy, última semana, etc
                        $dateStart = $this->getSearchDateStart($searchIntervalSlug);
                        $topics = $repository->findTopicsByForumCategoryAndInterval($searchTerm,
                                                                                    $forum->getId(),
                                                                                    $category->getId(),
                                                                                    $dateStart,
                                                                                    $userRolesArray);
                    }
                }

                elseif($orderSlug=='ordenado-por-fecha'){
                    if($searchIntervalSlug=='cualquier-fecha'){
                        // De un foro concreto, de una categoría en concreto, ordenados por fecha y de cualquier fecha
                        $topics = $repository->findTopicsByForumCategoryOrderedByDate(  $searchTerm,
                                                                                        $forum->getId(),
                                                                                        $category->getId(),
                                                                                        $userRolesArray);
                    }

                    else{
                        // De un foro concreto, de una categoría en concreto, ordenados por fecha y de una fecha
                        // concreta hoy, última semana, etc
                        $dateStart = $this->getSearchDateStart($searchIntervalSlug);
                        $topics = $repository->findTopicsByForumCategoryAndIntervalOrderedByDate(   $searchTerm,
                                                                                                    $forum->getId(),
                                                                                                    $category->getId(),
                                                                                                    $dateStart,
                                                                                                    $userRolesArray);
                    }
                }
                else{
                    throw $this->createNotFoundException(   'La forma de ordenar las búsquedas no existe');
                }
            }
        }

        $finder = $this->container->get('fos_elastica.finder.topic_search.topic');

        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->container->get('doctrine.orm.entity_manager');
        $globalVariables = $em->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
        $maxSearchResultTopicsPerPage = $globalVariables->getMaxSearchResultTopicsPerPage();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate( $finder->createPaginatorAdapter($topics),
                                            $this->get('request')->query->get('page', $pageNumber),
                                            $maxSearchResultTopicsPerPage,
                                            array('distinct' => false) );

        // Ruta y plantilla de paginación personalizadas
        $pagination->setUsedRoute('forum_search_page');

        // Asocio al formulario devuelto el termino de búsqueda solicitado
        $search = new Search();
        $search->setSearchTerm($searchTerm);
        $search->setSearchInterval($searchIntervalSlug);
        $search->setOrder($orderSlug);

        if($forumSlug != 'todos-los-foros' ){
            $search->setForum($forum);
        }

        if($categorySlug != 'todas-las-categorias' ){
            $search->setCategory($category);
        }

        $forums = $this->forumsThatUserCanAccess();

        $form = $this->createForm(new AdvancedSearchType($forums), $search);

        // Si no se encuentran resultados
        if($pagination->getTotalItemCount() == 0){

            // Retorno una plantilla generada con twig a la que le paso los temas buscados por el termino introducido
            return $this->render(   'ForumBundle:Default:showForumSearch.html.twig',
                array(
                    'criterionSlug'     => 'mas-nuevos',
                    'searchForm'        => $form->createView(),
                    'searchTerm'        => $searchTerm,
                    'title'             => 'Resultado de búsqueda',
                    'type'              => 'avanzado'
                )
            );
        }

        else{
            // Si se solicita una página de un foro sin resultados, devuelvo la última página existente.
            $maxResultsPages = ceil($pagination->getTotalItemCount()/$maxSearchResultTopicsPerPage);

            // Número máximo de mensajes por página de tema (necesario para calcular el número de páginas de un tema)
            $maxPostsPerTopicPage = $globalVariables->getMaxPostsPerTopicPage();

            if( $pageNumber > $maxResultsPages){
                return new RedirectResponse(
                    $this->generateUrl( 'forum_search_page',
                        array ( 'searchTerm'            => $searchTerm,
                                'forumSlug'             => $forumSlug,
                                'categorySlug'          => $categorySlug,
                                'orderSlug'             => $orderSlug,
                                'searchIntervalSlug'    => $searchIntervalSlug,
                                'pageNumber'            => $maxResultsPages
                            ))
                );
            }

            // Retorno una plantilla generada con twig a la que le paso los temas buscados por el termino introducido
            return $this->render(   'ForumBundle:Default:showForumSearch.html.twig',
                array(
                    'pagination'            => $pagination,
                    'criterionSlug'         => 'mas-nuevos',
                    'searchForm'            => $form->createView(),
                    'searchTerm'            => $searchTerm,
                    'lastPageNumber'        => $maxResultsPages,
                    'maxPostsPerTopicPage'  => $maxPostsPerTopicPage,
                    'title'                 => 'Resultado de búsqueda',
                    'type'                  => 'avanzado'
                )
            );
        }
    }

    /*
     * Muestra si el termino introducido no es valido muestra el formulario de búsqueda con el error, en caso contrario
     * redirige a la acción de búsqueda donde se busca el termino
     *
     * @param string $type tipo de formulario desde el cual se hace la búsqueda, avanzado o básico
     **/
    public function searchFormAction($type)
    {
        $request = $this->getRequest();

        $search = new Search();

        if($type == 'basico' || $type == 'basico-cabecera'){
            $form = $this->createForm(new SearchType(), $search);
            $type= 'basico';
        }

        else{
            $forums = $this->forumsThatUserCanAccess();

            $form = $this->createForm(new AdvancedSearchType($forums), $search);
        }

        // Relaciono el formulario con los datos introducidos por el usuario en el mismo
        $form->handleRequest($request);

        // Compruebo que los datos enviados son correctos
        if($form->isValid()){

            $searchTerm = $form["searchTerm"]->getData();
            if($type == 'basico'){
                // Redirijo a la acción de búsqueda
                return new RedirectResponse(
                    $this->generateUrl('forum_search', array (  'searchTerm'            => $searchTerm,
                                                                'forumSlug'             => 'todos-los-foros',
                                                                'categorySlug'          => 'todas-las-categorias',
                                                                'orderSlug'             => 'ordenado-por-relevancia',
                                                                'searchIntervalSlug'    => 'cualquier-fecha'))
                );
            }

            else{
                $forum = $form["forum"]->getData();
                $category = $form["category"]->getData();
                $orderSlug = $form["order"]->getData();
                $searchIntervalSlug = $form["searchInterval"]->getData();

                if($forum == null){
                    $forumSlug = "todos-los-foros";
                }

                else{
                    $forumSlug = $forum->getSlug();
                }

                if($category == null){
                    $categorySlug = "todas-las-categorias";
                }

                else{
                    $categorySlug = $category->getSlug();
                }

                // Redirijo a la accion de búsqueda
                return new RedirectResponse(
                    $this->generateUrl('forum_search', array (  'searchTerm'            => $searchTerm,
                                                                'forumSlug'             => $forumSlug,
                                                                'categorySlug'          => $categorySlug,
                                                                'orderSlug'             => $orderSlug,
                                                                'searchIntervalSlug'    => $searchIntervalSlug ))
                );
            }
        }

        // En caso de no introducir los datos de forma valida devuelvo al usuario a una página con el
        // formulario de búsqueda
        return $this->render(   '@Forum/Default/showForumSearch.html.twig',
            array(  'searchForm'          => $form->createView(),
                    'title'               => 'Formulario de búsqueda',
                    'type'                => $type
            )
        );
    }

    /*
     * Muestra en una plantilla el recuadro de búsqueda
     *
     * @param string $searchTerm termino a buscar
     **/
    public function renderSearchFormAction()
    {
        $search = new Search();

        $form = $this->createForm(new SearchType(), $search);

        return $this->render(   ':includes:searchForm.html.twig',
            array(  'searchForm'          => $form->createView(),
                    'type'                => 'basico-cabecera'
            )
        );
    }

    /*
     * Muestra en una plantilla los foros disponibles
     **/
    public function showForumsNavListAction($forumSlug)
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->container->get('doctrine.orm.entity_manager');

        $forums = $em->getRepository('ForumBundle:Forum')->findAllForums();

        return $this->render(   ':includes:forumsList.html.twig',
            array(
                'forums'        => $forums,
                'forumSlug'     => $forumSlug,
            )
        );
    }

    /*
     * Retorna a la pagina de error si no se ha encontrado ninguna ruta que coincida con la URL deseada
     */
    public function routeNotFoundAction(){
            throw $this->createNotFoundException('Ninguna ruta coincide con la dirección introducida');
    }

    /*
     * Devuelve un array con los roles que tiene el usuario
     **/
    public function userRolesArray()
    {
        $roles = array('ROLE_USER');
        if ($this->get('security.context')->isGranted('ROLE_USER')) {
            $user = $this->get('security.context')->getToken()->getUser();
            $role = $user->getHighestRole();
            $roles[]= $role;
            if($role == 'ROLE_ADMIN'){
                $roles[]= 'ROLE_MODERATOR';
            }
        }
        return $roles;
    }

    /*
     * Devuelve el listado de todos los foros a los que puede acceder el usuario
     **/
    public function forumsThatUserCanAccess()
    {
        // Obtengo el objeto del entity manager para realizar consultas
        $em = $this->container->get('doctrine.orm.entity_manager');

        $roles = $this->userRolesArray();

        return $forums = $em->getRepository('ForumBundle:Forum')->findAllForumsByRoles($roles);
    }

    /*
     * Devuelve el listado de todos los foros a los que puede acceder el usuario y tiene temas asociados a tiendas
     **/
    public function forumsThatUserCanAccessWithShops()
    {
        // Obtengo los foros a los que puede acceder
        $forums = $this->forumsThatUserCanAccess();

        $forumsWithShops = array();
        foreach($forums as $forum){
            $forumsTopicType = $forum->getForumTopicsType();
            if(($forumsTopicType=='TopicDeal') or ($forumsTopicType=='TopicFree') or ($forumsTopicType=='TopicVoucher'))
            {
                $forumsWithShops[] = $forum;
            }
        }
        return $forumsWithShops;
    }

    /*
     * Devuelve el listado de todos los foros a los que puede acceder el usuario y tiene temas asociados a etiquetas
     **/
    public function forumsThatUserCanAccessWithTags()
    {
        // Obtengo los foros a los que puede acceder
        $forums = $this->forumsThatUserCanAccess();

        $forumsWithTags = array();
        foreach($forums as $forum){
            $forumsTopicType = $forum->getForumTopicsType();
            if($forumsTopicType!='Topic')
            {
                $forumsWithTags[] = $forum;
            }
        }
        return $forumsWithTags;
    }

    /*
     * Devuelve un objeto de tipo Datetime con la fecha a partir de la cual tengo que buscar resultados (se usa en las
     * búsquedas)
     **/
    public function getSearchDateStart($searchIntervalSlug)
    {
        switch($searchIntervalSlug){
            case 'hoy':
                return $dateStart = new \DateTime('today');
                break;

            case 'ultimos-7-dias':
                return $dateStart = new \DateTime('today - 1 week');
                break;

            case 'ultimo-mes':
                return $dateStart = new \DateTime('today - 1 month');
                break;

            case 'ultimo-anio':
                return $dateStart = new \DateTime('today - 1 year');
                break;

            default:
                throw $this->createNotFoundException(   'El intervalo en el que quiere hacer la búsqueda no existe');
        }
    }

}