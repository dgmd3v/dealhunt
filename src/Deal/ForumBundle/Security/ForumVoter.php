<?php

namespace Deal\ForumBundle\Security;

use Deal\ForumBundle\Security\AbstractVoter\AbstractVoter;
use Symfony\Component\Security\Core\User\UserInterface;

/*
 * Clase encargada del ForumVoter que se ocupa dar o denegar el acceso al recurso
 * */
class ForumVoter extends AbstractVoter {

    /**
     * Return an array of supported classes. This will be called by supportsClass
     *
     * @return array an array of supported classes, i.e. array('Acme\DemoBundle\Model\Product')
     */
    protected function getSupportedClasses()
    {
        // Para este voter la clase soportada será el foro/forum
        return array('Deal\ForumBundle\Entity\Forum');
    }

    /**
     * Return an array of supported attributes. This will be called by supportsAttribute
     *
     * @return array an array of supported attributes, i.e. array('CREATE', 'READ')
     */
    protected function getSupportedAttributes()
    {
        // Se llamara a este voter cuando se usen los siguiente atributos
        return array('ROLE_ACCESS_FORUM', 'ROLE_ACCESS_TOPIC', 'ROLE_CAN_MOD_FORUM');
    }

    /**
     * Perform a single access check operation on a given attribute, object and (optionally) user
     * It is safe to assume that $attribute and $object's class pass supportsAttribute/supportsClass
     * $user can be one of the following:
     *   a UserInterface object (fully authenticated user)
     *   a string               (anonymously authenticated user)
     *
     * @param string $attribute
     * @param object $object
     * @param UserInterface|string $user
     *
     * @return bool
     */
    protected function isGranted($attribute, $object, $user = null)
    {

       $forumAccessRole = $object->getAccessRole();

        // Si no está logueado dependiendo del role le doy permiso o no
        if(!is_object($user)){
            switch($forumAccessRole){
                case 'ROLE_USER':
                    if($attribute != 'ROLE_CAN_MOD_FORUM'){
                        return true;
                        break;
                    }

                    // Si se pide permiso para moderar
                    else{
                        return false;
                        break;
                    }

                default:
                    return false;
                    break;
            }
        }

        // Para los usuarios logueados debo comprobar si su role es igual o superior al exigido
        $userRole = $user->getHighestRole();

        switch($forumAccessRole){
            case 'ROLE_USER':
                if($attribute != 'ROLE_CAN_MOD_FORUM'){
                    if( ($userRole=='ROLE_USER') || ($userRole=='ROLE_MODERATOR') ||($userRole=='ROLE_ADMIN')  ){
                        return true;
                        break;
                    }

                    else{
                        return false;
                        break;
                    }
                }

                // Si se pide permiso para moderar
                else{
                    if( ($userRole=='ROLE_MODERATOR') ||($userRole=='ROLE_ADMIN')  ){
                        return true;
                        break;
                    }

                    else{
                        return false;
                        break;
                    }
                }
                break;

            case 'ROLE_MODERATOR':
                if( ($userRole=='ROLE_MODERATOR') ||($userRole=='ROLE_ADMIN')  ){
                    return true;
                }

                else{
                    return false;
                }
                break;

            case 'ROLE_ADMIN':
                if( $userRole=='ROLE_ADMIN' ){
                    return true;
                }

                else{
                    return false;
                }
                break;

            default:
                return false;
                break;
        }

        return false;
    }
}