$(document).ready(function(){
    
    // Utilizo ajax para hacer consultas y poder mostrar las mejores ofertas del día/semana/mes según se soliciten
    var path = $("#collapseDay").attr("data-path");
    var forum = $("#collapseDay").attr("data-forum");
    var cat = $("#collapseDay").attr("data-category");

    $.ajax({
        type: "POST",
        url: path,
        data: {forumSlug: forum, from: 'today', category: cat },
        dataType: "json",
        success: function(response) {
            if (response.responseCode == 200) {
                for (var i = 0; i < response.topic.length; i++) {

                    var topicUrl = 'foro-' + response.topic[i].forum.slug + "/" + response.topic[i].slug + "-" + response.topic[i].id;
                    var miniTopicTitle = response.topic[i].title.substring(0, 76) + "...";
                    var topicTitle = '<span class="panel-votes"> + ' + response.topic[i].votes + ' </span> ' + miniTopicTitle;

                    if (i % 2 == 0)
                        var paragraphClass = 'bg-first';
                    else {
                        paragraphClass = 'bg-second';
                    }

                    $('#collapseDay .panel-body').append("<a class=\"" + paragraphClass + "\" href=\"" + topicUrl + "\">" + topicTitle + "</a>");
                }
            }

            else if(response.responseCode == 400){
               $('#collapseDay .panel-body').append('<span class="fa fa-exclamation-triangle"></span><div class="top-not-found">' + response.errorMsg + '</div>');
            }
        }
    });

    $("#collapseWeek").one('shown.bs.collapse',
            function() {
                $.ajax({
                    type: "POST",
                    url: path,
                    data: {forumSlug: forum, from: 'monday this week', category: cat },
                    dataType: "json",
                    success: function(response) {
                        if (response.responseCode == 200) {
                            for (var i = 0; i < response.topic.length; i++) {

                                var topicUrl = 'foro-' + response.topic[i].forum.slug + "/" + response.topic[i].slug + "-" + response.topic[i].id;
                                var miniTopicTitle = response.topic[i].title.substring(0, 76) + "...";
                                var topicTitle = '<span class="panel-votes"> + ' + response.topic[i].votes + ' </span> ' + miniTopicTitle;

                                if (i % 2 == 0)
                                    var paragraphClass = 'bg-first';
                                else {
                                    paragraphClass = 'bg-second';
                                }

                                $('#collapseWeek .panel-body').append("<a class=\"" + paragraphClass + "\" href=\"" + topicUrl + "\">" + topicTitle + "</a>");
                            }
                        }

                        else if (response.responseCode == 400) {
                            $('#collapseWeek .panel-body').append('<span class="fa fa-exclamation-triangle"></span><div class="top-not-found">' + response.errorMsg + '</div>');
                        }
                    }
                });
            }
    );

    $('#collapseMonth').one('shown.bs.collapse',
            function() {
                $.ajax({
                    type: "POST",
                    url: path,
                    data: {forumSlug: forum, from: 'first day of this month 00:00:00', category: cat },
                    dataType: "json",
                    success: function(response) {
                        if (response.responseCode == 200) {
                            for (var i = 0; i < response.topic.length; i++) {

                                var topicUrl = 'foro-' + response.topic[i].forum.slug + "/" + response.topic[i].slug + "-" + response.topic[i].id;
                                var miniTopicTitle = response.topic[i].title.substring(0, 76) + "...";
                                var topicTitle = '<span class="panel-votes"> + ' + response.topic[i].votes + ' </span> ' + miniTopicTitle;


                                if (i % 2 == 0)
                                    var paragraphClass = 'bg-first';
                                else {
                                    paragraphClass = 'bg-second';
                                }

                                $('#collapseMonth .panel-body').append("<a class=\"" + paragraphClass + "\" href=\"" + topicUrl + "\">" + topicTitle + "</a>");
                            }
                        }

                        else if (response.responseCode == 400) {
                            $('#collapseMonth .panel-body').append('<span class="fa fa-exclamation-triangle"></span><div class="top-not-found">' + response.errorMsg + '</div>');
                        }
                    }
                });
            }
    );

});