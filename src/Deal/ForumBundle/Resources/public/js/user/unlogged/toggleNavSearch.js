/*
* Función destinada a mostrar/ocultar el formulario de búsqueda de la barra de navegación
* **/
$(document).on('click', '#nav-search-button, #search-param-button',
    function(e){
        e.preventDefault();

        var elementToAnimate = $(this).attr('id').slice(0,-7);

        $('#' + elementToAnimate ).animate({
            height: [ "toggle", "swing" ],
            opacity: "toggle"
        }, 700, "linear");
    }
);