/*
 * Script encargado de preparar el modal para el selector de página
 * */
$('#pageSelectorModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Botón que lanzó el modal
    var title = button.attr('data-title'); // Extraigo los datos de los atributos data-*
    var path = button.attr('data-path');
    var lastpage = button.attr('data-last-page');

        var modal = $(this);
        modal.find('.modal-title').text(title);
        modal.find("#pageSelectorAcceptButton").attr({
            href: path
        });

        if(lastpage != null){
            modal.find("#page-number").attr({
                max: lastpage
            });
        }

})

$('#pageSelectorModal').on('hidden.bs.modal', function (event) {

    var modal = $(this);
    modal.find("#pageSelectorAcceptButton").attr({
        href: ""
    });
})