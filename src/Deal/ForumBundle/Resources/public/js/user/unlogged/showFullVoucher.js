/*
 * Función destinada a mostrar/ocultar un cupón descuento que había sido recortado
 * **/
$(document).on('click', '.short-voucher',
    function () {
        $(this).nextAll().toggleClass('hidden-content');
    }
);