/*
 * Función destinada a ir a la página de un tema/foro que ha seleccionado el usuario a través de un formulario
 * */
$(document).on('click', '#pageSelectorAcceptButton',
    function(e){


        var $form = $('#pageSelectorModal form')
        if ($form[0].checkValidity()) {
            e.preventDefault();

            var stringNumber = $('#page-number').val();

            var link = $('#pageSelectorAcceptButton').attr("href");
            var lastChar = link[link.length -1];

            if(lastChar == '/'){
                var pageNumber = stringNumber;
            }

            else{
                var pageNumber = '/' + stringNumber;
            }

            var link = $('#pageSelectorAcceptButton').attr("href");
            var requestedPage = link + pageNumber;

            window.location.href = requestedPage;
        }

        else{
            $form.find('#pageSelectorAcceptButton').click();
        }

    }
);