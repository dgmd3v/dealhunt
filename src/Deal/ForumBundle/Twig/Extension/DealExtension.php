<?php

namespace Deal\ForumBundle\Twig\Extension;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

/*
 * Clase – Extensión Twig con funciones para filtros de twig
 * */
class DealExtension extends \Twig_Extension
{
    // Inyecto el EntityManager para poder hacer operaciones en la base de datos y el Router para trabajar con URLs
    protected $em;
    protected $router;

    public function __construct(EntityManager $entityManager, Router $router)
    {
        $this->em = $entityManager;
        $this->router = $router;
    }

    // Patrón de una url con http/ftp/https
    private $urlPattern = '((((ht|f)tps?:\/\/(www\.)?))([^\s[\]]+))';

    // Patrón de una url sin http/ftp/https delante (www.xxxx.com)
    private $urlPattern_2 = '(((?<!(http:\/\/)|(https:\/\/))(www\.))([^\s[\]]+))';


    public function getName()
    {
        return 'deal';
    }

    /*
     * Función que devuelve los filtros disponibles
     * */
    public function getFilters()
    {
        return array(
            'bbcode'            => new \Twig_Filter_Method($this, 'bbcode', array('is_safe' => array('html'))),
            'smilies'           => new \Twig_Filter_Method($this, 'smilies', array('is_safe' => array('html'))),
            'signatureBbcodes'  => new \Twig_Filter_Method($this, 'signatureBbcodes', array('is_safe' => array('html'))),
            'firstTopicPostBbcodes'  => new \Twig_Filter_Method($this, 'firstTopicPostBbcodes', array('is_safe' => array('html'))),
            'age'               => new \Twig_Filter_Method($this, 'age'),
            'postingDate'       => new \Twig_Filter_Method($this, 'postingDate'),
            'isUserOnline'      => new \Twig_Filter_Method($this, 'isUserOnline'),
            'numberShortener'   => new \Twig_Filter_Method($this, 'numberShortener'),
            'quotes'            => new \Twig_Filter_Method($this, 'quotes', array('is_safe' => array('html'))),
            'postPageNumber'    => new \Twig_Filter_Method($this, 'postPageNumber'),
            'removeDashes'      => new \Twig_Filter_Method($this, 'removeDashes'),
        );
    }


    /*
     * Función que devuelve las funciones disponibles
     * */
    public function getFunctions()
    {
        return array(
            'getGlobalVariables'            => new \Twig_Function_Method($this, 'getGlobalVariables'),
        );
    }

    /*
     * Función que devuelve las variables globales para poder ser usadas en plantillas
     * */
    public function getGlobalVariables(){
        return $this->em->getRepository('ForumBundle:GlobalVariables')->findAllGlobalVariables();
    }


    /*
     * Función para bbcode de centrado de texto
     * */
    public function centerBbcode($text){
        return preg_replace( array('/\[center\]/i', '/\[\/center\]/i'), array('<div class="text-center">','</div>'), $text);
    }

    /*
     * Función para bbcode de texto en negrita
     * */
    public function boldBbcode($text){
        return preg_replace( array('/\[b\]/i', '/\[\/b\]/i'), array('<span class="bold-text">','</span>'), $text);
    }

    /*
     * Función para bbcode de texto en cursiva
     * */
    public function italicBbcode($text){
        return preg_replace( array('/\[i\]/i', '/\[\/i\]/i'), array('<span class="italics-text">','</span>'), $text);
    }

    /*
     * Función para bbcode de texto subrayado
     * */
    public function underlineBbcode($text){
        return preg_replace( array('/\[u\]/i', '/\[\/u\]/i'), array('<span class="underlined-text">','</span>'), $text);
    }

    /*
     * Función para bbcode de texto de tipo título
     * */
    public function titleBbcode($text){
        return preg_replace( array('/\[title\]/i', '/\[\/title\]/i'), array('<h2>','</h2>'), $text);
    }

    /*
     * Función bbcode para hacer una imagen linkable
     * */
    public function linkableImageBbcode($text){

        return preg_replace( '/\[url=' . $this->urlPattern . '\]\[img\]' . $this->urlPattern . '\[\/img\]\[\/url\]/i', '<div><a href="$1" target="_blank"><img class="img-responsive" alt="imagen insertada" src="$7"></a></div>', $text);
    }

    /*
     * Función bbcode para incrustar una imagen
     * */
    public function imageBbcode($text){
        $pattern = "/\[img\]([^\s[\]]+)\[\/img\]/i";
        return preg_replace( $pattern, '<div><a href="$1" data-lightbox="$1"><img class="img-responsive" src="$1" alt="imagen insertada"></a></div>', $text);
    }

    /*
     * Función bbcode para introducir una url
     * */
    public function urlBbcode($text){

        $text = preg_replace( '/\[url=' . $this->urlPattern . '\](.+)\[\/url\]/i', '<a href="$1" target="_blank">$7</a>', $text);

        return $text;
    }

    /*
     * Función bbcode incrustar video
     * */
    public function videoBbcode($text){
        // Array con el patrón y el código para incrustar vídeos de diferentes sitios de vídeos
        $videoRegexs = array(
            array(
                "pattern"           => '(?:(?:https?:\/\/(?:m\.|www\.)?)|(?:www\.))(?:youtube\.com\/(?:\S*)v=|youtu\.be\/)([\w-_]*)',
                "embeddingCode"     => '<div class="embed-responsive embed-responsive-16by9">
                                            <iframe width="420" height="315" src="//www.youtube.com/embed/$1" frameborder="0" allowfullscreen></iframe>
                                        </div>'
            ),

            array(
                "pattern"           => '(?:https?:\/\/(?:www\.)?|:?www\.)?vimeo.com\/(?:\w*\/)*(\d+)',
                "embeddingCode"     => '<div class="embed-responsive embed-responsive-16by9">
                                            <iframe src="//player.vimeo.com/video/$1" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                        </div>'
            )
        );

        foreach($videoRegexs as $videoRegex){
            $text = preg_replace( '/\[video\]' . $videoRegex["pattern"] . '\[\/video\]/i', $videoRegex["embeddingCode"], $text);
        }

        return $text;
    }

    /*
     * Función para reducir el tamaño de las url
     * */
    public function urlShortenerBbcode($text){

        // La función anónima recorta un string, en mi caso se usara para recortar los nombres de las url que sean demasiado largas
        $text = preg_replace_callback(   '/(?<!(\[img\])|(\[url=\])|(\[url\])|(\[url=)|(\[video]))' . $this->urlPattern . '/i',
            function($matches){
                if(strlen($matches[6])>38){
                    $shortUrl = '<a href="' . $matches[6] . '" target="_blank">' . substr($matches[6], 0, 30) . '...' . substr($matches[6], -8) . '</a>';
                }

                else{
                    $shortUrl = '<a href="' . $matches[6] . '" target="_blank">' . $matches[6] . '</a>';
                }

                return $shortUrl;
            },
            $text);

        return $text;
    }

    /*
     * Función para reducir el tamaño de las url
     * */
    public function bbcodeUrlShortenerBbcode($text){

        // La función anónima recorta un string, en mi caso se usara para recortar los nombres de las url que sean demasiado largas
        $text = preg_replace_callback(  '/\[url=?\]' . $this->urlPattern . '\[\/url\]/i',
            function($matches){
                if(strlen($matches[1])>38){
                    $shortUrl = '<a href="' . $matches[1] . '" target="_blank">' . substr($matches[1], 0, 30) . '...' . substr($matches[1], -8) . '</a>';
                }

                else{
                    $shortUrl = '<a href="' . $matches[1] . '" target="_blank">' . $matches[1] . '</a>';
                }

                return $shortUrl;
            },
            $text);

        return $text;
    }

    /*
     * Función bbcode para el spoiler
     * */
    public function spoilerBbcode($text){
        $text = preg_replace_callback(   '/\[spoiler(?:\=(.*?))?\](.+?)\[\/spoiler\]/is',
            function($matches){
                static $matchNumber = 0;
                $spoilerTitle = 'Spoiler';

                if($matches[1] != null) $spoilerTitle = $matches[1];

                $spoiler =  '<a class="spoiler" title="Click para mostrar spoiler" href="#">+ ' . $spoilerTitle .'</a> ' .
                    '<div class="hidden-content" >' . $matches[2] . '</div>';
                $matchNumber++;
                return $spoiler;
            },
            $text);

        return $text;
    }

    /*
     * Función en la que se aplican ciertos bbcodes para las firmas
     * */
    public function signatureBbcodes($text){
        // Utilizo la función htmlspecialchars para evitar que se introduzca código html indeseado en los mensajes.
        $text = htmlspecialchars($text);
        $text = preg_replace( '/' . $this->urlPattern_2 . '/i', 'http://$1', $text);
        $text = $this->urlShortenerBbcode($text);

        return $text;
    }


    /*
     * Función en la que se aplican ciertos bbcodes para las los primeros mensajes del tema
     * */
    public function firstTopicPostBbcodes($text){
        return $this->signatureBbcodes($text);
    }


    /*
     * Función en la que se aplican ciertos bbcodes para un mensaje dado
     * */
    public function bbcode($text)
    {
        // Utilizo la función htmlspecialchars para evitar que se introduzca código html indeseado en los mensajes.
        // Para evitar problemas con ciertas combinaciones de bbcode cambio el orden en el que han sido escritos,
        // como por ejemplo [url=][b]LINK[/b][/url]
        // preg_replace($patrón, $sustitución, $cadena);

        $html = preg_replace( array('/\[url(=)?\]((?:\[(?:b|i|u|center|title)\])*)/i', '/((?:\[\/(?:b|i|u|center|title)\])*)\[\/url\]/i'), array('$2[url$1]','[/url]$1'), htmlspecialchars($text));
        $html = preg_replace( '/' . $this->urlPattern_2 . '/i', 'http://$1', $html);
        $html = preg_replace( array('/\[url=' . $this->urlPattern . '\]((?:\[(?:b|i|u|center|title)\])*)/i', '/((?:\[\/(?:b|i|u|center|title)\])*)\[\/url\]/i'), array('$7[url=$1]','[/url]$1'), $html);

        $html = $this->boldBbcode($html);
        $html = $this->italicBbcode($html);
        $html = $this->underlineBbcode($html);
        $html = $this->titleBbcode($html);
        $html = $this->spoilerBbcode($html);
        $html = $this->centerBbcode($html);
        $html = $this->urlShortenerBbcode($html);
        $html = $this->linkableImageBbcode($html);
        $html = $this->imageBbcode($html);
        $html = $this->bbcodeUrlShortenerBbcode($html);
        $html = $this->urlBbcode($html);
        $html = $this->videoBbcode($html);

        return $html;
    }

    /*
     * Función para incrustar smilies
     * */
    public function smilies($text)
    {
        // Utilizo la función htmlspecialchars para evitar que se introduzca código html indeseado en los mensajes.
        // Para evitar problemas con ciertas combinaciones de bbcode cambio el orden en el que han sido escritos,
        // como por ejemplo [url=][b]LINK[/b][/url]
        // preg_replace($patrón, $sustitución, $cadena);

        $firstImagePart =  '<img src="/bundles/topic/images/smilies/';
        $secondImagePart = '.png" class="smiley" alt="smiley" height="19" width="19">';

        $html = preg_replace(
                                array   (  '/:smile:/',
                                           '/:laugh:/',
                                           '/:silly:/',
                                           '/:wink:/',
                                           '/:blush:/',
                                           '/:sad:/',
                                           '/:cool:/',
                                           '/:angry:/',
                                           '/:surprised:/',
                                           '/:speechless:/',
                                           '/:geek:/',
                                           '/:tease:/',
                                           '/:crazy:/',
                                           '/:fools:/',
                                           '/:cry:/',
                                           '/:xd:/',
                                           '/:devil:/',
                                           '/:angel:/',
                                           '/:ill:/',
                                           '/:zipit:/',
                                           '/:annoyed:/',
                                           '/:please:/',
                                           '/:hay:/',
                                           '/:notguilty:/',
                                           '/:kissy:/',
                                           '/:zzz:/',
                                           '/:totalshock:/',
                                           '/:inlove:/',
                                           '/:notonecare:/',
                                           '/:boring:/',
                                           '/:minishock:/',
                                           '/:oh:/',
                                        ),
                                array   (
                                            $firstImagePart . 'smile' . $secondImagePart,
                                            $firstImagePart . 'laugh' . $secondImagePart,
                                            $firstImagePart . 'silly' . $secondImagePart,
                                            $firstImagePart . 'wink' . $secondImagePart,
                                            $firstImagePart . 'blush' . $secondImagePart,
                                            $firstImagePart . 'sad' . $secondImagePart,
                                            $firstImagePart . 'cool' . $secondImagePart,
                                            $firstImagePart . 'angry' . $secondImagePart,
                                            $firstImagePart . 'surprised' . $secondImagePart,
                                            $firstImagePart . 'speechless' . $secondImagePart,
                                            $firstImagePart . 'geek' . $secondImagePart,
                                            $firstImagePart . 'tease' . $secondImagePart,
                                            $firstImagePart . 'crazy' . $secondImagePart,
                                            $firstImagePart . 'fools' . $secondImagePart,
                                            $firstImagePart . 'cry' . $secondImagePart,
                                            $firstImagePart . 'xd' . $secondImagePart,
                                            $firstImagePart . 'devil' . $secondImagePart,
                                            $firstImagePart . 'angel' . $secondImagePart,
                                            $firstImagePart . 'ill' . $secondImagePart,
                                            $firstImagePart . 'zipit' . $secondImagePart,
                                            $firstImagePart . 'annoyed' . $secondImagePart,
                                            $firstImagePart . 'please' . $secondImagePart,
                                            $firstImagePart . 'hay' . $secondImagePart,
                                            $firstImagePart . 'notguilty' . $secondImagePart,
                                            $firstImagePart . 'kissy' . $secondImagePart,
                                            $firstImagePart . 'zzz' . $secondImagePart,
                                            $firstImagePart . 'totalshock' . $secondImagePart,
                                            $firstImagePart . 'inlove' . $secondImagePart,
                                            $firstImagePart . 'notonecare' . $secondImagePart,
                                            $firstImagePart . 'boring' . $secondImagePart,
                                            $firstImagePart . 'minishock' . $secondImagePart,
                                            $firstImagePart . 'oh' . $secondImagePart,
                                        ),
                                $text
                            );

        return $html;
    }
    /*
     * Función que calcula la edad en años dada una fecha de nacimiento
     * */
    public function age($birthday)
    {
        // Transformo $birthday a tipo DateTime
        if (!$birthday instanceof \DateTime) $birthday = new \DateTime($birthday);

        $currentDate = new \DateTime('now');
        $age = $birthday->diff($currentDate)->format('%y');

        return $age;
    }

    /*
     * Función para modificar una fecha dada y que sea algo más legible y elegante, en vez de poner solo la fecha y
     * la hora, si esa fecha y hora son de ayer nos mostrara Ayer a las ...
     * */
    public function postingDate($postingDate, $type = 'post')
    {
        // Función encargada de formatear las fechas

        if (!$postingDate instanceof \DateTime) $postingDate = new \DateTime($postingDate);

        $todayDate = new \DateTime('now');
        $yesterdayDate = new \DateTime('now -1 day');

        if($todayDate->format('mm "/" dd "/" y') == $postingDate->format('mm "/" dd "/" y')){
            $f = new \IntlDateFormatter(\Locale::getDefault(), \IntlDateFormatter::NONE, \IntlDateFormatter::NONE, null, null, 'HH:mm');
            return 'Hoy a las ' . $f->format($postingDate);
        }

        elseif($yesterdayDate->format('mm "/" dd "/" y') == $postingDate->format('mm "/" dd "/" y')){
            $f = new \IntlDateFormatter(\Locale::getDefault(), \IntlDateFormatter::NONE, \IntlDateFormatter::NONE, null, null, 'HH:mm');
            return 'Ayer a las ' . $f->format($postingDate);
        }

        elseif($todayDate->format('y') == $postingDate->format('y')){
            if($type == 'notification'){
                $f = new \IntlDateFormatter(\Locale::getDefault(), \IntlDateFormatter::NONE, \IntlDateFormatter::NONE, null, null, "'el' cccc d LLL 'a las' HH:mm");
            }
            elseif($type == 'postMini'){
                $f = new \IntlDateFormatter(\Locale::getDefault(), \IntlDateFormatter::NONE, \IntlDateFormatter::NONE, null, null, "d LLL HH:mm");
            }
            else {
                $f = new \IntlDateFormatter(\Locale::getDefault(), \IntlDateFormatter::NONE, \IntlDateFormatter::NONE, null, null, 'cccc d LLL HH:mm');
            }
            return $f->format($postingDate);
        }

        else {
            if($type == 'notification'){
                $f = new \IntlDateFormatter(\Locale::getDefault(), \IntlDateFormatter::NONE, \IntlDateFormatter::NONE, null, null, "'el' d LLL yy 'a las' HH:mm");
            }
            else {
                $f = new \IntlDateFormatter(\Locale::getDefault(), \IntlDateFormatter::NONE, \IntlDateFormatter::NONE, null, null, 'd LLL yy HH:mm');
            }
            return $f->format($postingDate);
        }
    }

    /*
     * Función encargada de decidir si un usuario está 'online' o no basándose en la fecha de la última actividad
     * del usuario
     **/
    public function isUserOnline($lastActivity)
    {
        if(is_null($lastActivity)){
            return false;
        }

        else{
            // Transformo $lastActivity a tipo DateTime
            if (!$lastActivity instanceof \DateTime) $lastActivity = new \DateTime($lastActivity);

            $currentTimeMinusFour = new \DateTime('now - 4 minutes');
            return($lastActivity > $currentTimeMinusFour);
        }
    }

    /*
     * Función encargada de acortar números excesivamente largos (igual o mayores a 1000), por ejemplo 1200 = 1.2k
     **/
    public function numberShortener($number)
    {
        if($number < 1000) return $number;

        elseif($number == 1000){
            return '1K';
        }

        elseif ($number > 1000 && $number < 10000){
            return  bcdiv($number, '1000', 1) . 'k+';
        }

        elseif ($number >= 10000){
            return  bcdiv($number, '1000', 0) . 'k+';
        }


    }

    /*
     * Función encargada de crear los quotes/citas en los posts
     **/
    public function quotes($text, $topicId)
    {
        // Variable con la ruta para las citas
        $showQuotePath = $this->router->generate('post_ajax_showquote_post');

        $pattern = '/\B#(\d+)(?!\B)/';
        $replacement = '<a href="#$1" class="quote" data-path="' . $showQuotePath . '" data-topicid="' . $topicId . '">#$1</a>';

        $html = preg_replace($pattern, $replacement, $text);

        return $html;
    }

    /*
     * Función encargada de calcular la página en la que se encuentra un post dado su número de post
     **/
    public function postPageNumber($postNumber, $maxPostsPerTopicPage)
    {
        if($postNumber == 1) $postNumber++;

        return ceil( ($postNumber-1) / $maxPostsPerTopicPage);
    }

    /*
     * Función encargada de quitar los guiones (-) a una cadena dada
     **/
    public function removeDashes($text)
    {
        $pattern = "/\-/i";
        return preg_replace( $pattern, ' ', $text);

    }

}

