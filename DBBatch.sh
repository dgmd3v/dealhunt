read -p "Te dispones a borrar la cache de symfony, borrar y volver a generar la base de datos de pruebas y borrar la cache de imagenes de liipimaginebundle, estas segur@? [s/n] (se te preguntara otra vez si quieres purgar la base de datos): " -n 1 -r
echo
if [[ $REPLY =~ ^[Ss]$ ]]
then
date
sudo rm -rf web/media/cache/*
php app/console cache:clear --env=prod
php app/console cache:clear --env=dev
php app/console doctrine:cache:clear-metadata
php app/console doctrine:cache:clear-query
php app/console doctrine:cache:clear-result 
php app/console assets:install
php app/console doctrine:database:drop --no-debug --force
php app/console doctrine:database:create --no-debug
php app/console doctrine:schema:create --no-debug
sudo php app/console doctrine:fixtures:load --no-debug -n
php app/console fos:elastica:populate
php app/console assetic:dump --no-debug
php app/console doctrine:query:sql "ALTER TABLE Tag MODIFY name VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL;"
date
fi

